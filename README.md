# Tau

## Description

This repository contains the Tau code and its associated scripts and programs used to simulate the relaxation dynamics of magnetic molecules.

## Documentation

The documentation for `tau` can be found [here](https://chilton-group.gitlab.io/tau).

## Repository Structure

The Tau source code and its ancillary programs is located in `src/tau`. Libraries and Fortran modules are located in `libs`. Stripped down copies of the C++ [boost](https://www.boost.org/) and [eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page) libraries are included in `libs` to avoid version compatibility issues.

The scripts and programs used to generate and workup vibrational and electronic structure data can be found in `src/prep_tau`.

Both `src/tau` and `src/prep_tau` contain `Makefiles` which can be used to compile and install all the programs and scripts. Additionally, there is a small `Makefile` in `src` which can be used to install all programs in the repository in a single command. 

## Requirements

* Fortran compiler (gfortran or ifort)
* Linear algebra fortran libraris (lapack+blas or MKL)
* Python3 with packages listed in `src/requirements.txt`
* Make

## Installation

For installation instructions, see the `tau` [documentation](https://chilton-group.gitlab.io/tau).

### Manchester - CSF Instructions

If you do not already have a `git` folder in your csf home, make one

```
mkdir git
```

The same goes for a `bin` folder

```
mkdir bin
```

Navigate to your `git` folder 

```
cd git
```

and clone this repository - this gives you your own version which you can use and modify if you need to.

```
git clone https://gitlab.com/chilton-group/tau
```

Then navigate to `src`

```
cd src
```

Load the following modules to install Tau. **You may need to purge other compilers you have loaded!**

```
module load compilers/intel/18.0.3 libs/intel-18.0/hdf5/1.10.5_mpi
```

and install the tools

```
make all install
```

You should now be able to use all of the Tau tools on the csf, try tau using

``` 
tau -h
```

Which should print a help section

## Updating

To update the code simply type

```
git pull
```
and reinstall

```
cd src; make all install
```

## Usage

For instructions on how to use Tau and its ancillary programs, consult the manual in `docs`, or on the Chilton Group Dropbox.

## Contributing

Please read the [guidelines](https://gitlab.com/nfchilton/tau/-/wikis/Contributing/) for contributing to the Tau repository.

## Bugs

Please report all bugs through Gitlab.
