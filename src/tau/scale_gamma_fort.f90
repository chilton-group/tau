PROGRAM SCALE_GAMMA
    USE prec
    IMPLICIT NONE
    REAL(KIND = WP), ALLOCATABLE :: gamma_1(:,:), gamma_2(:,:), gamma_1_mod(:,:), &
                                    gamma_2_mod(:,:), ratio(:,:), av_ratio(:,:), mult_array(:,:)
    INTEGER, ALLOCATABLE         :: mask(:,:)
    INTEGER                      :: num_states, row, col
    CHARACTER(LEN = 500)         :: gamma_1_file, gamma_2_file

    ! Read user input
    CALL READ_USER_INPUT(num_states, gamma_1_file, gamma_2_file)

    ! Allocate gamma arrays and set to zero
    ALLOCATE(gamma_1(num_states,num_states), gamma_1_mod(num_states,num_states), &
             gamma_2(num_states,num_states), gamma_2_mod(num_states,num_states), &
             ratio(num_states,num_states), av_ratio(num_states,num_states))
    
    gamma_1     = 0.0_WP
    gamma_1_mod = 0.0_WP
    gamma_2     = 0.0_WP
    gamma_2_mod = 0.0_WP
    ratio       = 0.0_WP
    av_ratio    = 0.0_WP


    ! Read in gamma matrices from file
    CALL READ_GAMMA(TRIM(gamma_1_file), gamma_1)
    CALL READ_GAMMA(TRIM(gamma_2_file), gamma_2)

    ! Calculate the ratio between the matrices
    ratio = gamma_1/gamma_2

    ! Calculate the average of the transposed elements to retain detailed balance
    av_ratio = 0.5 * (ratio + TRANSPOSE(ratio))

    ! Define a matrix of ones which will be used to scale the original matrices g1 and g2.    
    ALLOCATE(mult_array(num_states,num_states))
    mult_array = 1.0_WP
    
    ! Read in user provided mask which says yes or no to scaling of each element
    CALL READ_MASK(mask, num_states)

    ! Put average ratios in elements of mult array which are going to change
    DO row = 1, num_states
        DO col = 1, num_states
            IF (mask(row, col) == 0) CYCLE
            mult_array(row, col) = av_ratio(row, col)
            mult_array(col, row) = av_ratio(col, row)
        END DO
    END DO

    ! Scale the original matrices
    gamma_1_mod = gamma_1/mult_array
    gamma_2_mod = gamma_2*mult_array
    

    ! Save the scaled matrices
    CALL SAVE_GAMMA('1', gamma_1_mod)
    CALL SAVE_GAMMA('2', gamma_2_mod)

    ! Remove temporary mask file
    CALL SYSTEM('rm mask.tmp')

    CONTAINS

    SUBROUTINE READ_USER_INPUT(num_states, gamma_1_file, gamma_2_file)
        IMPLICIT NONE
        INTEGER, INTENT(OUT)            :: num_states
        CHARACTER(LEN = *), INTENT(OUT) :: gamma_1_file, gamma_2_file
        CHARACTER(LEN = 10)             :: CDUMMY
        LOGICAL                         :: file_exists

        CALL GET_COMMAND_ARGUMENT(1,CDUMMY)
        IF (TRIM(CDUMMY) == '-h' .OR. TRIM(CDUMMY) == '') THEN
            WRITE(6,'(A)') 'scale_gamma <gamma_1> <gamma_2>'
            WRITE(6,'(A)')
            WRITE(6,'(A)') 'Scales gamma_1 to match gamma_2 and vice versa'
            WRITE(6,'(A)') 'for specific matrix elements - returns new files'
            WRITE(6,'(A)') 'gamma_1_mod.dat and gamma_2_mod.dat'
            WRITE(6,'(A)')
            WRITE(6,'(A)') 'gamma_1_file      : CHARACTER            gamma_1 file name                e.g  gamma_1.dat'
            WRITE(6,'(A)') 'gamma_2_file      : CHARACTER            gamma_2 file name                e.g  gamma_2.dat'

            STOP
        END IF

        READ(CDUMMY, *) num_states

        CALL GET_COMMAND_ARGUMENT(2,gamma_1_file)

        !Check file 1 exists
        INQUIRE(FILE=gamma_1_file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(3A)') 'Specified file ', TRIM(ADJUSTL(gamma_1_file)),' does not exist'
            STOP
        END IF

        CALL GET_COMMAND_ARGUMENT(3,gamma_2_file)

        !Check file 2 exists
        INQUIRE(FILE=gamma_2_file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(3A)') 'Specified file ', TRIM(ADJUSTL(gamma_2_file)),' does not exist'
            STOP
        END IF

    END SUBROUTINE READ_USER_INPUT

    SUBROUTINE READ_GAMMA(gamma_file, gamma_matrix)
        IMPLICIT NONE
        REAL(KIND = WP), INTENT(OUT)   :: gamma_matrix(:,:)
        CHARACTER(LEN = *), INTENT(IN) :: gamma_file
        INTEGER                        :: row

        OPEN(33, FILE = gamma_file, STATUS='OLD')

            DO row = 1, SIZE(gamma_matrix,1)
                READ(33,*) gamma_matrix(row,:)
            END DO

        CLOSE(33)

    END SUBROUTINE READ_GAMMA

    SUBROUTINE SAVE_GAMMA(number, gamma_matrix)
        IMPLICIT NONE
        REAL(KIND = WP), INTENT(IN)    :: gamma_matrix(:,:)
        CHARACTER(LEN = *), INTENT(IN) :: number
        INTEGER                        :: row, col
        CHARACTER(LEN = 25)            :: FMT


        WRITE(FMT, '(A, I0, A)') '(', SIZE(gamma_matrix,1), 'E47.36E4)'

        OPEN(33, FILE = 'gamma_'//number//'_mod.dat', STATUS='UNKNOWN')

            DO row = 1, SIZE(gamma_matrix,1)
                write(33,FMT) (gamma_matrix(row,col), col = 1, SIZE(gamma_matrix,1))
            END DO

        CLOSE(33)

    END SUBROUTINE SAVE_GAMMA


    SUBROUTINE READ_MASK(mask, num_states)
        ! Reads in mask array from python wrapper
        IMPLICIT NONE
        INTEGER, INTENT(IN)               :: num_states
        INTEGER, INTENT(OUT), ALLOCATABLE :: mask(:,:)
        INTEGER                           :: row

        ALLOCATE(mask(num_states, num_states))
        mask = 0

        OPEN(33, FILE = 'mask.tmp', STATUS='OLD')

            DO row = 1, num_states

                READ(33, *) mask(row, :)

            END DO

        CLOSE(33)

    END SUBROUTINE READ_MASK

END PROGRAM SCALE_GAMMA