#!/usr/bin/env python3
#                       /$$              
#                      | $$              
#  /$$$$$$  /$$   /$$ /$$$$$$    /$$$$$$ 
# |____  $$| $$  | $$|_  $$_/   /$$__  $$
#  /$$$$$$$| $$  | $$  | $$    | $$  \ $$
# /$$__  $$| $$  | $$  | $$ /$$| $$  | $$
#|  $$$$$$$|  $$$$$$/  |  $$$$/|  $$$$$$/
# \_______/ \______/    \___/   \______/ 
# /$$$$$$$$$/
#|___ $$___/
#    |$$       $$$$$$\  $$\   $$\
#    |$$       \____$$\ $$ |  $$ |
#    |$$       $$$$$$$ |$$ |  $$ |
#    |$$  $$\/$$  __$$ |$$ |  $$ |
#    | $$$$ / \$$$$$$$ |\$$$$$$  |
#    \_____/   \_______| \______/

#Automation program for Tau
# Authors:
#  Jon Kragskow
#  Daniel Reta
#  Nicholas Chilton

import numpy as np
import argparse
import os
import time
import sys

def read_user_input():
    """
    Read in command line arguments
    Returns args object
    """
    parser = argparse.ArgumentParser(description='Automates multiple Tau calculations for different FWHM \n'+
                                     'Make sure all the files tau needs are in the current directory!',
                                     usage='autau fwhm_1, fwhm_2, ..., fwhm_N [options]')
    parser.add_argument('fwhm', type = float, nargs='*', 
                        help = 'list of FWHM values in cm-1')
    parser.add_argument('--num_cores', type = int, nargs=1,  metavar = 'number', default=4,
                        help = 'Number of cores for each tau calculations')
    parser.add_argument('--job_name', type = str, metavar = 'name', default='autau_job',
                        help = 'Custom job name')
    parser.add_argument('--remove_transition', type = str, nargs = '+', metavar = 'tau_output_file, <temperature>', 
                        help = 'Read in tau file with transition knockout, then separately remove each \n'+
                               'of the top 5 most important transitions for the top temperature or a given temperature, and recalculate rates')    
    parser.add_argument('--auto_remove', action='store_true',
                        help = 'Re-run autau with --remove_transition after initial jobs')
    parser.add_argument('--no_submit', action='store_true',
                        help = 'Do not submit .sh file')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    return args

def get_machine():
    """
    Currently hard coded for csf
    Sets machine name - this controls if jobs are run through SGE or not
    """
    machine = 'csf'

    return machine

class status_file():
    """
    Class for status file object
    Controls autau.status
    """
    def __init__(self, args):

        # Remove old autau.status file
        os.system('rm -f autau.status')
        os.system('touch autau.status')

        # Get the time
        the_time = time.asctime( time.localtime(time.time()) )

        # Check reuslts directory and input file exist
        in_exists = os.path.exists('input.dat')

        # Report missing input file and abort
        if not in_exists :
            print('Error: Cannot find input.dat in current directory')
            exit()

        # Write FWHM values to status file and add info on results directory
        if not args.remove_transition:
            with open('autau.status', 'w') as f:
                f.write('autau begins at '+ the_time + '\n \n')
                f.write('FWHM values (cm-1): \n')
                for val in args.fwhm:
                    f.write('{:f} \n'.format(val))

                f.write('\n')

                r_exists = os.path.exists('Results')

                if r_exists :
                    f.write('results directory found \n \n')
                else:
                    os.system('mkdir results')
                    f.write('results has been created \n \n')

            f.close()
        else:
            with open('autau.status', 'w') as f:
                f.write('autau begins at '+ the_time + '\n \n')
                f.write('FWHM value is read from provided output file: \n')

                r_exists = os.path.exists('removal_results')

                if r_exists :
                    f.write('removal_results directory found \n \n')
                else:
                    os.system('mkdir removal_results')
                    f.write('removal_results has been created \n \n')

                f.close()

    def running(self, val):

        """
        Currently unused on csf
        Updates autau.status with which fwhm calc is running
        """

        with open('autau.status', 'a') as f:
            f.write('Running Tau for FWHM = {:f} \n '.format(val))
        f.close()

    def finish_one(self, val, good_bad):
        """
        Currently unused on csf
        Updates autau.status when fwhm calc is done
        """

        with open('autau.status', 'a') as f:
            f.write(good_bad +' Tau for FWHM = {:f} \n '.format(val))
        f.close()

    def finish_all(self, good_bad):

        """
        Currently unused on csf
        Updates autau.status all fwhm calcs is done
        """

        the_time = time.asctime( time.localtime(time.time()) )

        with open('autau.status', 'a') as f:
            f.write('autau ' + good_bad + ' at '+ the_time + '\n')
        f.close()

    def made_temp_dirs(self, values, opmode):

        """
        Updates autau.status to give temporary fwhm directory locations
        """

        with open('autau.status', 'a') as f:
            f.write('Input files have been copied to temporary directories: \n')
            if opmode == 'fwhm':
                for it,val in enumerate(values):
                    f.write('FWHM = {:f} ==> fwhm_{:d}_temp \n'.format(val, it))
            
            if opmode == 'removal':
                for it,val in enumerate(values):
                    f.write('removal_{:d}-{:d}_temp \n'.format(val[0], val[1]))

        f.close()

    def report_batch(self):
        """
        Updates autau.status for csf jobscript creation
        """

        with open('autau.status', 'a') as f:
            f.write('Submission script written to tau_batch.sh \n\n')
        f.close()

    def report_submit(self):
        """
        Updates autau.status for csf jobscript submission
        """
        with open('autau.status', 'a') as f:
            f.write('Submission successful \n')
        f.close()


def change_fwhm(fwhm):

    """
    Changes fwhm value in input file
    """

    os.system("sed -i.bak 's/.*[Ff][Ww][Hh][Mm].*/fwhm '{:f}'/' input.dat && rm input.dat.bak".format(fwhm))

def change_removal(states):

    """
    Changes remove transitions line in input file
    """

    # Remove old "transition knockout" keyword
    os.system("sed -i.bak '/[Tt][Rr][Aa][Nn][Ss][Ii][Tt][Ii][Oo][Nn] [Kk][Nn][Oo][Cc][Kk][Oo][Uu][Tt]*/d' input.dat && rm input.dat.bak")

    # Remove old "remove transitions" keyword
    os.system("sed -i.bak '/[Rr][Ee][Mm][Oo][Vv][Ee] [Tt][Rr][Aa][Nn][Ss][Ii][Tt][Ii][Oo][Nn]*/d' input.dat && rm input.dat.bak")

    # Add new keyword line
    with open('input.dat', 'a') as f:
        f.write('remove transition {:d}-{:d} \n'.format(states[0], states[1]))
    f.close()

def read_ko_states(file, specific_temp):
    '''
    Get the most important transitions for highest temperature in tau output file.
    '''
    found_ko = False
    
    if specific_temp == None:
        transitions = []
        with open(file, 'r') as f:
            for line in f:
                if line.find('-----                               Knockout                               -----') != -1:
                    found_ko = True
                    for i in range(10): # skip the following 10 lines.
                        line=next(f)
                    for i in range(5): # Read state numbers
                        transitions.append((int(line.split()[1]),int(line.split()[4])))
                        line=next(f)
        if found_ko == False:
                print('Error: Cannot find Knockout in tau.out')
                exit()

    else:
        found_ko = False
        transitions = []
        with open(file, 'r') as f:
            for line in f:
                if line.find('-----                               Knockout                               -----') != -1:
                    while found_ko != True:
                        line = next(f)
                        if line.find('T = ') != -1 and line.find('{:d}'.format(specific_temp)) != -1:
                            line=next(f)
                            line=next(f)
                            found_ko = True
                    for i in range(5): # Read state numbers
                        transitions.append((int(line.split()[1]),int(line.split()[4])))
                        line=next(f)



        if found_ko == False:
            print('Error: Cannot find Knockout for {:d} K in tau.out'.format(specific_temp))
            exit()

    return transitions


def run_calcs_csf(args, sf):
    """
    Creates folders for batch submission of tau calculations
    If removal is requested, then states will be read from the provided output file
    """

    # Delete old directory list if it exists
    os.system('rm -f dir_list.txt')

    # Delete old names list if it exists
    os.system('rm -f names_list.txt')
    
    # Make new temporary directory for each tau calc

    if args.remove_transition:

        # Copy provided tau.out file to removal_results as tau_full.out
        os.system('cp '+args.remove_transition[0] +' removal_results/tau_full.out')
        if len(args.remove_transition) == 1:
            removals = read_ko_states(args.remove_transition[0], None)
        else:
            removals = read_ko_states(args.remove_transition[0], int(args.remove_transition[1]))
        num_calcs = len(removals)
        for it, val in enumerate(removals) :
            change_removal(val)
            copy_input_files(it + 1, 'removal', val)

        # Add directory locations to status file
        sf.made_temp_dirs(removals, 'removal')

    else :

        num_calcs = len(args.fwhm)

        for it, val in enumerate(args.fwhm) :
            change_fwhm(val)
            copy_input_files(it + 1, 'fwhm', val)

        # Add directory locations to status file
        sf.made_temp_dirs(args.fwhm, 'fwhm')

    # Make batch submission script
    make_batch(num_calcs, args)

    # Update status file
    sf.report_batch()

    if args.auto_remove: 
        print('\n When fwhm jobs are finished, you will need to submit the remove transition jobs \n'
              ' Move to each fwhm_N_temp folder and qsub the tau_batch.sh file \n')
        
    if args.no_submit:
        return

    # Submit batch job
    submit_batch()

    # Update status file
    sf.report_submit()

def copy_input_files(it, opmode, val=''):

    """
    Copies tau input files to new temporary directory
    """
    if opmode == 'fwhm':
        curr_folder = 'fwhm_{:d}_temp'.format(it)
        append_name_list('fwhm_{:.2f}'.format(val))

    
    if opmode == 'removal':
        curr_folder = 'removal_{:d}-{:d}_temp'.format(val[0], val[1])
        append_name_list('removal_{:d}-{:d}'.format(val[0], val[1]))
    os.system('mkdir ' + curr_folder)

    if os.path.exists('CFP_polynomials.dat'): 
        os.system('cp CFP_polynomials.dat ' + curr_folder)

    if os.path.exists('CFP_steps.dat'): 
        os.system('cp CFP_steps.dat ' + curr_folder)

    if os.path.exists('tau.hdf5'): 
        os.system('cp tau.hdf5 ' + curr_folder)

    if os.path.exists('EQ_CFPs.dat'): 
        os.system('cp EQ_CFPs.dat ' + curr_folder)

    os.system('cp input.dat ' + curr_folder)

    os.system('cp mode_energies.dat ' + curr_folder)

    os.system('rm -f '+ curr_folder +'/tau.out')

    append_dir_list(curr_folder)

def append_dir_list(folder):
    """
    Appends absolute path of folder to dir_list.txt
    """
    with open('dir_list.txt','a') as f:
        f.write(os.getcwd() + '/' + folder + '\n')
    f.close()

def append_name_list(name):
    """
    Appends calculation names to names_list.txt
    """
    with open('names_list.txt','a') as f:
        f.write(name+ '\n')
    f.close()

def make_batch(num_calcs, args):

    """
    Makes CSF batch submission script
    """

    if args.remove_transition:
        re_dir = 'removal_results'
    else:
        re_dir = 'results'

    with open('tau_batch.sh', 'w') as f:

        f.write('#!/bin/bash --login'+'\n')
        f.write('#$ -S /bin/bash'+'\n')
        f.write('#$ -N '+args.job_name+'\n')
        f.write('#$ -t 1-{:d}'.format(num_calcs)+'\n')
        f.write('#$ -cwd'+'\n')
        f.write('#$ -V'+'\n')
        f.write('#$ -pe smp.pe {:d} '.format(args.num_cores)+'\n')
        f.write('#$ -l s_rt=00:20:00 \n')
        f.write('export OMP_NUM_THREADS=$NSLOTS # DO NOT CHANGE THIS'+'\n')
        f.write('module load compilers/intel/18.0.3'+'\n')
        f.write('SUBDIR=`sed -n "${SGE_TASK_ID}p" dir_list.txt`'+'\n')
        f.write('CALCNAME=`sed -n "${SGE_TASK_ID}p" names_list.txt`'+'\n')
        f.write('tau $SUBDIR/input.dat -autau'+'\n')
        f.write('cp $SUBDIR/tau.out '+re_dir+'/tau_$CALCNAME.out' + '\n')
        f.write('rm $SGE_O_WORKDIR/'+args.job_name+'.e${JOB_ID}*'+'\n')
        f.write('rm $SGE_O_WORKDIR/'+args.job_name+'.o${JOB_ID}*'+'\n')
        if args.auto_remove : 
            f.write('cd $SUBDIR/; autau 0 --no_submit --remove_transition tau.out --job_name '+args.job_name+'_removals_$SGE_TASK_ID' + '\n')
    f.close()

def submit_batch():
    """
    Submits csf batch script
    """
    os.system('qsub tau_batch.sh')

if __name__ == '__main__':

    # Read user args
    args = read_user_input()

    # Make status file
    sf = status_file(args)

    # Make job folders and submission script, then submit
    run_calcs_csf(args, sf)
