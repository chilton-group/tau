#!/usr/bin/env python

# /$$$$$$$$$/
# |___ $$___/
#    |$$       $$$$$$\  $$\   $$\
#    |$$       \____$$\ $$ |  $$ |
#    |$$       $$$$$$$ |$$ |  $$ |
#    |$$  $$\/$$  __$$ |$$ |  $$ |
#    | $$$$ / \$$$$$$$ |\$$$$$$  |
#    \_____/   \_______| \______/

#############################################################
#
#
#   tau_strength_plot.py
#
#   This program is a part of tau and is called automatically
#
#   Plots crystal field strengths against mode energies 
#   
#      Author:
#       Jon Kragskow
#
#
#############################################################

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import argparse


def read_in_command_line():
    # Read in command line arguments

    parser = argparse.ArgumentParser(description='Automated crystal field strength plotting for Tau')
    parser.add_argument('--name', type=str, metavar='<FILE>',
                        help='Name for saved plot', default='CF_strength')
    parser.add_argument('--format', type=str, metavar='<FORMAT>',
                        help='File format for plot', default='svg')
    user_args = parser.parse_args()

    return user_args


def load_file(file):
    """

    Parameters
    ----------
    file

    Returns
    -------

    """
    return np.loadtxt(file, skiprows=0)


def plot_cf_strength(axis, cf_strength, mode_energies, **kwargs):
    """

    Parameters
    ----------
    axis
    cf_strength
    mode_energies
    args
    kwargs

    Returns
    -------

    """
    # Plots strength for positive and negative displacement along each mode

    # Plot data
    blob, stick, other = axis.stem(mode_energies, 0.5 * (np.abs(cf_strength[:, 0]) + np.abs(cf_strength[:, 1])),
                                   linefmt='C1-', markerfmt='C1o', basefmt=' ')
    plt.setp(stick, 'color', '#0072B2')
    plt.setp(blob, 'color', '#0072B2')

    # Remove right and top parts of plot box
    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)

    # Set x limits to user provided limits
    if 'x_lims' in kwargs:
        x_lims = kwargs.get('x_lims', None)
        axis.set_xlim(x_lims)

    # Make upper y limit larger to avoid legend overlap
    curr_y_lim = axis.get_ylim()
    axis.set_ylim(curr_y_lim[0], curr_y_lim[1] * 1.1)

    # Set y axis label
    axis.set_ylabel(r'$|S(\Delta B_k^q)| \ \ ($cm$^{-1})$')

    # Set x axis label
    axis.set_xlabel(r'Energy (cm$^{-1}$)')

    # Put a legend on the plot
    custom_lines = [Line2D([0], [0], color='#0072B2', lw=4),
                    Line2D([0], [0], color='red', lw=4)]

    axis.legend(custom_lines, [r'Average $|S(\Delta B_k^q)|$', r'CF transitions'], frameon=False)

    # Calculate maximum height of any stem line
    # Used to make electronic transition lines tall enough
    max_height = np.amax(np.abs(cf_strength))

    return max_height


def plot_elec_trans(axis, cf_energies, mode_energies, max_height):
    # Plots the electronic transition energies which occur within the min and max mode energies

    # Calculate all possible transition energies
    transitions = calc_trans_energies(cf_energies)

    baseline = ' '

    # Plot transition energies which are inside the min and max mode energies
    for transition in transitions:
        if transition and mode_energies[0] <= transition <= mode_energies[-1]:
            # Plot stem for electronic transition energy with height equal to the largest strength parameter
            blob, stick, other = axis.stem([transition], [max_height], basefmt=baseline, linefmt='-C2', markerfmt=' ')
            plt.setp(stick, 'linewidth', 2)
            plt.setp(stick, 'color', 'red')

    return


def calc_trans_energies(energies):
    transitions = np.zeros([np.size(energies) * np.size(energies)])

    p = -1

    for i in np.arange(np.size(energies)):
        for j in np.arange(np.size(energies)):
            p += 1
            if energies[i] > energies[j]:
                transitions[p] = np.abs(energies[i] - energies[j])
            else:
                transitions[p] = None

    return transitions


if __name__ == '__main__':

    # Read input from command line
    args = read_in_command_line()

    # Load in mode energies
    mode_energies = load_file('mode_energies.dat')

    # Read in crystal field strength parameters
    cf_strength = load_file('cf_strength.tmp')

    # Read in crystal field energies
    cf_energies = load_file('cf_energies.tmp')

    # Change plot font size - needs to be done before plot is created
    plt.rcParams.update({'font.size': 12})

    # Create plot figure
    fig, ax = plt.subplots(1, 1, figsize=(8, 5))

    # Plot strength parameter stemlines
    max_height = plot_cf_strength(ax, cf_strength, mode_energies)

    # Plot electronic transition energies
    plot_elec_trans(ax, cf_energies, mode_energies, max_height)

    # Fix clipping
    fig.tight_layout()

    # Make save name
    if args.name:
        save_name = 'CF_strength.svg'
    else:
        save_name = args.name

    # Save plot
    fig.savefig(save_name)
