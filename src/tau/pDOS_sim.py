#!/usr/bin/env python3
import numpy as np
import argparse
import matplotlib.pyplot as plt
import os


def parse_input():
    description = """
    Script to convolute mode energies and plot RMSD vs fwhm.
    """

    parser = argparse.ArgumentParser( description=description,
                                      formatter_class=argparse.RawDescriptionHelpFormatter )
    parser.add_argument(
        'input_file', type=str, help='File containing frequencies and one headerline. Obtained with gaussian_freq'
        )
    parser.add_argument(
        '--fwhm', type=int, nargs=3, 
        default=[1,20,1], 
        help='Min, max and step FWHM values used to convolute the data. Default: 1 20 1.',
        metavar="min max step"
        )
    parser.add_argument(
        '--xvals', type=int, nargs=3, 
        default=[1,1501,1], 
        help='Min, max and step x-values used to define the sampling. Default 1 1501 1.',
        metavar='min max step'
        )
    parser.add_argument(
        '--show_contributions', action='store_true', help='show the individual contributions that make up the convolution.'
        )

    return parser.parse_args()


def read_data(user_args):

    # Read-in the data
    energy = np.loadtxt(user_args.input_file, skiprows=1)

    # Define the x-values to use
    xval    = np.arange(user_args.xvals[0], user_args.xvals[1]+user_args.xvals[2], user_args.xvals[2])  
    energy  = energy[  energy < user_args.xvals[1] ]

    return energy, xval


def convolute_contrib( energy, xvals, fwhm):

    # Use a Gaussian to convolute the data
    contrib = []
    for i in range(len(energy)):
        contrib.append( gaussian_( xvals, energy[i], fwhm)  )

    return np.sum( contrib, axis=0  ), contrib


def gaussian_(xval, x_zero, fwhm):

    return ( 1./( ( fwhm/(2.*np.sqrt(2.*np.log(2.))) )*np.sqrt( 2.*np.pi ) ) )*np.exp( - ( xval - x_zero )**2 / ( 2.*( fwhm/(2.*np.sqrt(2.*np.log(2.))) )**2 ) )


def plot_convoluted(total, contributions, fwhm, energy, xvals):

    fig, ax = plt.subplots( 1, 1, sharex=True, sharey=True, figsize=(8,7))

    # Plot the convolute spectrum.
    ax.plot( xvals, total, lw=1.2, markerfacecolor='None', label='FWHM = '+str(fwhm) )

    if args.show_contributions()
        # Plot the individual contributions.
        for i in range(len(energy)):
            ax.vlines( mode_energies[i], 0, np.max(contributions[i]), linestyles='solid', colors='#ff7f0e', alpha=0.5 ) #colors='#ff7f0e', 
            ax.plot( xvals, contributions[i], c='#ff7f0e', lw=.5 ) #c='#ff7f0e', 
            ax.fill_between(xvals, 0, contributions[i], alpha=0.5, color='#ff7f0e')

    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    ax.legend(loc='upper left', frameon=False)

    ax.set_xlim([args.xvals[0], args.xvals[1]])
    ax.set_xlabel('Mode energy (cm$^{-1}$)')
    ax.set_ylabel('Intensity (arb. units)')
    #plt.show()
    plt.savefig('pDOS_fwhm'+str(fwhm)+'.png',dpi=300)
    plt.close('all')
    #exit()

    return

def calculate_RMSD( f1, f2 ):

    return np.sqrt( np.sum( [ ( f2[i]-f1[i] )**2 for i in range(len(f1)) ] )/len(f1) )

def plot_RMSD( xvals, yvals  ):

    fig, ax = plt.subplots( 1, 1, sharex=True, sharey=True, figsize=(8,7))

    ax.plot( xvals, yvals, lw=1, marker='o', markerfacecolor='None' )
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    ax.legend(loc=0, frameon=False)

    ax.set_xlabel(r'$\Delta$ FWHM (cm$^{-1}$)')
    ax.set_ylabel('RMSD')
    plt.show()
    plt.savefig('RMSD.png',dpi=300)
    plt.close('all')
    
    return

def write_results( _fwhm_diff, _rmsd ):
 
    filename = args.input_file.split('.')[0]+'_RMSD.out'
    path = os.path.join( os.getcwd(), filename )

    with open(path, 'w') as RMSD_res:
        RMSD_res.write('{:^6} {:^6}'.format('D FWHM','RMSD')+"\n")
        for i in range(len(_rmsd)):
            RMSD_res.write('{: 8.6f} {: 8.6f}'.format(_fwhm_diff[i], _rmsd[i])+"\n")

    return

if __name__ == '__main__':

    # Parse the input and store it in args.
    args = parse_input()

    # Read the data.
    mode_energies, xvals_sampling = read_data(args)    

    # Define the values of fwhm based on the input and their pairwise differences.
    fwhm = np.arange(args.fwhm[0], args.fwhm[1]+args.fwhm[2], args.fwhm[2])
    fwhm_diff = [ (fwhm[i]-fwhm[i-1])*i for i in range(len(fwhm)) if i > 0 ]

    # Loop over the fwhm values.
    convoluted = [] # here I will store each of the convoluted spectra.
    RMSD = []       # here I will store each of the pairwise RMSD values.
    for it,_fwhm in enumerate(fwhm):

        # Convolute the spectrum with a given fwhm value.
        _conv, _contrib = convolute_contrib( mode_energies, xvals_sampling, _fwhm )

        # Plot the convoluted spectrum and its contributions.
        plot_convoluted( _conv, _contrib, _fwhm, mode_energies, xvals_sampling )

        # Store the spectrum to later pass it to calculate_RMSD.
        convoluted.append( _conv )

        # Calculate the RMSD between pairs of convoluted spectra.
        if it > 0:
            RMSD.append( calculate_RMSD( convoluted[it-1], convoluted[it] ) )

    # Plot RMSD vs FWHM_diff
    plot_RMSD( fwhm_diff, RMSD )

    # Write the results.
    write_results( fwhm_diff, RMSD )
