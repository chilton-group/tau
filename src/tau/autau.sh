#!/bin/bash
#                       /$$              
#                      | $$              
#  /$$$$$$  /$$   /$$ /$$$$$$    /$$$$$$ 
# |____  $$| $$  | $$|_  $$_/   /$$__  $$
#  /$$$$$$$| $$  | $$  | $$    | $$  \ $$
# /$$__  $$| $$  | $$  | $$ /$$| $$  | $$
#|  $$$$$$$|  $$$$$$/  |  $$$$/|  $$$$$$/
# \_______/ \______/    \___/   \______/ 
# /$$$$$$$$$/
#|___ $$___/
#    |$$       $$$$$$\  $$\   $$\
#    |$$       \____$$\ $$ |  $$ |
#    |$$       $$$$$$$ |$$ |  $$ |
#    |$$  $$\/$$  __$$ |$$ |  $$ |
#    | $$$$ / \$$$$$$$ |\$$$$$$  |
#    \_____/   \_______| \______/

#Automation program for Tau
# Authors:
#  Jon Kragskow
#  Daniel Reta
#  Nicholas Chilton


#######################Input information
if [ $# -eq 0 ] || [ "$1" == "-h" ] || [ "$1" == "--h" ] || [ "$1" == "-H" ] || [ "$1" == "--H" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ] || [ "$1" == "-Help" ] || [ "$1" == "--Help" ] 
  then
    echo "Driver for tau to perform multiple calculations with different FWHM"
    echo "autau <MODE> <INPUT>"
    echo "MODE = 1 or 2"
    echo "  MODE = 1 uses the standard range of FWHM values in cm^-1 "
    echo "  [1, 2, 3, 4, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, IR]"
    echo "  IR is provided by the user as the INPUT argmuent"
    echo "  e.g. autau 1 15"
    echo " If IR is already included in the range then it will not be repeated"
    echo ""
    echo "  MODE = 2 uses a user defined range of FWHM values "
    echo "  In this case, a file containing the values is given as INPUT argmuent"
    echo "  e.g. autau 2 FWHM_vals.dat"
    echo "  FWHM_vals.dat should be structured as:"
    echo "  FIRSTLINE  :  FWHM_1   (in cm^-1)"
    echo "  SECONDLINE :  FWHM_2   (in cm^-1)"
    echo "  NTHLINE    :  FWHM_N   (in cm^-1)"
    echo ""
    echo " Progress of autau is printed to autau.out"
    echo " Additionally plots resulting data from tau (uses tau_rates_plot)"
    echo " Experimental data placed in 'Experimental_tau.dat' is also plotted if available"
    echo " 'Experimental_tau.dat' should be formatted as"
    echo " T (K) tau (s)"
    echo " With data separated by space, and no header lines"

    exit 2
fi

#######################Check which mode has been selected

#Mode 1 - standard range of FWHM values
if [ "$1" -eq "1" ] ; then

 #List of FWHM values
 FWHM=(1 2 3 4 5 10 20 30 40 50 60 70 80 90 100)

 #Check if IR_FWHM is present in standard range of values - if so do not repeat calculation!
 if [ "$2" != "" ] ; then

  IR_PRESENT=0

  for i in "${FWHM[@]}" ;  do
    if [ "$i" -eq "$2" ] ; then
      IR_PRESENT=1
    fi
  done

  if [ "$IR_PRESENT" == "0" ]; then
    FWHM+=($2)
  fi

 else
  echo "Please input IR FWHM as second argument"
  exit 1
 fi

#Mode 2 - user defined range of FWHM values

elif [ "$1" -eq "2" ] ; then

 #Read in file line by line until EOF
 while IFS= read -r line ; do
    FWHM+=($line)
  done < "$2" # < "$2" tells the while do read loop where to read from

else

 echo "Invalid mode selected"
 exit 1

fi

#Number of FWHM values
declare -i num_fwhm=${#FWHM[@]}

#Set number of failed tau calcs to zero
declare -i num_failed=0

#######################Automated calculation instructions

#Print output file header
current_time=`date`
echo "autau begins - " $current_time >> autau.out 

#Ascii in bash is hard!
echo "                     /\$\$\$\$\$\$\$\$\$/">> autau.out 
echo "                    |___ \$\$___/">> autau.out 
echo "  /\$\$\$\$\$\$  /\$\$   /\$\$    |\$\$       \$\$\$\$\$\$\  \$\$\   \$\$\ ">> autau.out 
echo " |____  \$\$| \$\$  | \$\$    |\$\$       \____\$\$\ \$\$ |  \$\$ |">> autau.out 
echo "  /\$\$\$\$\$\$\$| \$\$  | \$\$    |\$\$       \$\$\$\$\$\$\$ |\$\$ |  \$\$ |">> autau.out 
echo " /\$\$__  \$\$| \$\$  | \$\$    |\$\$  \$\$\/\$\$  __\$\$ |\$\$ |  \$\$ |">> autau.out 
echo "|  \$\$\$\$\$\$\$|  \$\$\$\$\$\$/    | \$\$\$\$ / \\$\$\$\$\$\$\$ |\\$\$\$\$\$\$  |">> autau.out 
echo " \_______/ \______/     \_____/   \_______| \______/          ">> autau.out 

#Check for results directory and create if not present
if [ -d "$PWD/Results" ]
then
 echo "Results Directory Found"
 echo "Results Directory Found" >> autau.out
 echo "Continuing with autau"
 echo "Continuing with autau" >> autau.out
else
 echo "Results Directory Not Found"
 echo "Results Directory Not Found" >> autau.out
 echo "Creating new directory at $PWD/Results"
 echo "Creating new directory at $PWD/Results" >> autau.out 
 mkdir Results
fi

if [ ! -f "$PWD/job.dat" ]
 then
  echo "Cannot find job.dat in current directory!"
  echo "Cannot find job.dat in current directory!" >> autau.out 
  echo "autau aborts"
  echo "autau aborts" >> autau.out 
  exit 1
 fi


#Loop over different sigma values
for N in  "${FWHM[@]}"; do

 #Check if fwhm value is same as IR
 if [[ "$1" -eq "1" && "$N" -eq "$2" ]] ; then
  fwhmname=IR_$N
 else
  fwhmname=$N
 fi

 ratefilenames=()
 echo "Running FWHM = ${fwhmname} cm^-1" >> autau.out 

 #Replace fwhm in calibration.dat
 sed -i.bak 's/.*[Ff][Ww][Hh][Mm].*/fwhm '$N'/' job.dat && rm job.dat.bak

 #Run tau on job.dat file
 #stderr is redirected to the temporary err.tmp file which is then read back into bash
 # |-> this is because bash cannot write stderr to a varable!
 #If tau encounters an error then it will write an error code = 1
 #Specifically, the error of detailed balance not being obeyed at any T returns an error code of 5
 tau 'job.dat' 2>"err.tmp"
 read -r status<"err.tmp"

 #Remove temporary error file
 rm -rf "err.tmp"

 #Remove STOP from error code if it is present
 #e.g. gfortran returns STOP 1, whereas ifort returns 1
 #Ridiculous
 #So if STOP is found in $status then split string and take second element of array 
 case "$status" in
    *STOP* )
   IFS=' ' read -ra status_array <<< "$status"
   status=${status_array[1]}
 esac


 # Termination due to error
 if [[ $status -eq 1 ]] ; then
   echo "!!!!       Failure in tau       !!!! "
   echo "!!!!       Failure in tau       !!!! " >> autau.out
   echo "!!!!        autau aborts!       !!!! " 
   echo "!!!!        autau aborts!       !!!! " >> autau.out 
   exit 1

 # Termination due to lack of detailed balance at all temperatures
 elif [[ $status -eq 5 ]] ; then 

  echo "!!!!       Detailed balance not obeyed at any temperatures when FWHM=$fwhmname cm^-1      !!!! " >> autau.out

  num_failed=$num_failed+1
 
  #Move files to results
  mv tau.out $PWD/Results/tau_${fwhmname}.out
 #Successful termination
 elif [[ $status -eq 0 ]] ; then

  #Move files to results
  mv tau.out $PWD/Results/tau_${fwhmname}.out

  #for fname in barrier_figure_*; do
  #  name_temp=$(basename ${fname} .svg)
  #  mv $fname $PWD/Results/${name_temp}_${fwhmname}.svg
  #done

 fi

 #Clean directory of unneeded files 
 tau -cleansilent

done

echo "All tau calculations complete"
echo "All tau calculations complete" >> autau.out 

current_time=`date`
echo "autau ends - " $current_time >> autau.out 

#Plot FWHM values onto the same plot and save
# only plot if at least one calculation finished successfully
if [[ $num_fwhm -ne $num_failed ]]; then
 if [ -f 'Results/Experimental_tau.dat' ]; then
  tau_rates_plot $PWD/Results/tau_*.out --E $PWD/Results/Experimental_tau.dat 
  echo "Plotting tau data against experiment"
 else
  tau_rates_plot $PWD/Results/tau_*.out
  echo "Plotting tau data"
 fi
 mv tau_rates.svg Results/
fi
