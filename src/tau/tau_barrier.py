#! /usr/bin/env python

# /$$$$$$$$$/
#|___ $$___/
#    |$$       $$$$$$\  $$\   $$\
#    |$$       \____$$\ $$ |  $$ |
#    |$$       $$$$$$$ |$$ |  $$ |
#    |$$  $$\/$$  __$$ |$$ |  $$ |
#    | $$$$ / \$$$$$$$ |\$$$$$$  |
#    \_____/   \_______| \______/

#############################################################
#
#
#   tau_barrier_plot.py
#
#   This program is a part of tau and is called automatically
#
#   Plots barrier figures
#   
#      Author:
#       Jon Kragskow
#
#
#############################################################

import numpy as np
import matplotlib.pyplot as plt
import argparse
import matplotlib.ticker as mtick
import matplotlib as mpl
from matplotlib.ticker import ScalarFormatter

def read_in_command_line():
	# !!! Read in command line arguments

	parser = argparse.ArgumentParser(description='Automated barrier plotting for Tau')
	parser.add_argument('--Name', type = str, metavar ='<FILE>',
						help = 'Name for saved plot', default = 'barrier_figure')
	parser.add_argument('--format', type = str, metavar ='<FORMAT>',
						help = 'File format for plot', default = 'svg')
	args = parser.parse_args()

	file_name = args.Name + '.' + args.format

	return file_name

def read_energies(energies_file):
	# !!! Read in energies
	return np.loadtxt(energies_file)


def read_magnetisation(mag_file):
	# !!! Read in magnetisation
	return np.loadtxt(mag_file)


def read_transitions(trans_file):
	# !!! Read in transitions and convert to a list of initial, final, strength
	# !!! Initial and final are numbers, whereas strength is a list of colors whose transparency differs
	trans_matrix = np.loadtxt(trans_file)
	trans = []
	num_trans=0
	num_states = np.shape(trans_matrix)[0]
	# Create array of initial and final indices and alpha channel values for transitions
	for row in np.arange(num_states):
		for col in np.arange(num_states):
			if (trans_matrix[row,col] > 1):
				alpha = float(trans_matrix[row, col]/100.0)
				trans.append([row, col, alpha])
				num_trans+=1

	return trans, num_trans


def plot_data(ax, energy, mag, trans, num_trans):
	# !!! Plots data - energy levels and transition arrows

	# Draw energy level lines
	ax.plot(mag, energy, marker='_', markersize='25', mew='2.5', linewidth=0, color = 'black')

	# Final magnetisation
	mags_final = []
	for row in np.arange(num_trans):
		mags_final.append(mag[trans[row][1]])
	
	# Difference between initial and final magnetisation
	mags_diff = []
	for row in np.arange(num_trans):
		mags_diff.append(mag[trans[row][0]] - mag[trans[row][1]])

	# Final energies
	energies_final = []
	for row in np.arange(num_trans):
		energies_final.append(energy[trans[row][1]])

	# Difference between initial and final energies
	energies_diff = []
	for row in np.arange(num_trans):
		energies_diff.append(energy[trans[row][0]] - energy[trans[row][1]])

	# Alpha channel values
	alphas = []
	for row in np.arange(num_trans):
		alphas.append(trans[row][2])

	# Make colours array
	# Columns are red, green, blue, alpha
	rgba_colors = np.zeros((num_trans,4))
	rgba_colors[:,0] = 1.0
	rgba_colors[:, 3] = np.asarray(alphas)

	# Draw lines between levels
	ax.quiver(mags_final, energies_final, mags_diff, energies_diff, scale_units='xy', angles='xy', scale=1, color = rgba_colors)


def adjust_plot(ax, K_ax, num_trans):
	# !!! Function to adjust plot options - axis, labels, style etc.

	# Set x axis options
	ax.set_xlabel(r'$\langle \ \hat{J}_{z} \ \rangle$')
	ax.tick_params(axis = 'both', which = 'both', length = 2.0)
	ax.xaxis.set_major_locator(plt.MaxNLocator(8))

	# Set y axis options for cm-1
	ax.set_ylabel(r'Energy $($cm$^{-1})$')
	ax.yaxis.set_major_locator(plt.MaxNLocator(7))

	# Set y axis options for K
	K_ax.set_ylabel(r'Energy $($K$)$')
	K_ax.set_ylim(ax.get_ylim()[0]*1.4, ax.get_ylim()[1]*1.4)
	K_ax.yaxis.set_major_locator(plt.MaxNLocator(7))

	return

# Read in command line
file_name = read_in_command_line()

# Read in energies
energies = read_energies('energy.dat')

# Read in magnetisation
mag = read_magnetisation('mag.dat')

# Read in transitions
trans, num_trans = read_transitions('trans.dat')

# Change plot font size - needs to be done before plot is created
plt.rcParams.update({'font.size': 17})

# Create plot and axes
fig, (ax) = plt.subplots( 1, 1, sharex=False, sharey='all', figsize=(8,6))
K_ax = ax.twinx()

# Plot data
plot_data(ax, energies, mag, trans, num_trans)

# Adjust plot axes
adjust_plot(ax, K_ax, num_trans)
fig.tight_layout()

# Show plot
# plt.show()
fig.savefig(file_name, dpi=fig.dpi)
