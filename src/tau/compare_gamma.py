#!/usr/bin/env python3

import numpy as np
import numpy.ma as ma
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import argparse
import copy

def read_gammas(file_1, file_2):

    # Read in the pair of files
    gamma_1 =  np.loadtxt(file_1) 
    gamma_2 =  np.loadtxt(file_2) 

    return gamma_1, gamma_2


def compare_gammas(gamma_1, gamma_2, threshold):

    lim = threshold

    # Set all elements smaller than lim to lim
    gamma_1[ abs(gamma_1) < lim ] = lim
    gamma_2[ abs(gamma_2) < lim ] = lim

    # Make the diagonal terms positive 
    gamma_1 = abs( gamma_1 ) 
    gamma_2 = abs( gamma_2 )
 
    # Disable warnings for hacky log stuff
    np.seterr(divide = 'ignore')
    np.seterr(invalid = 'ignore') 

    # Calculate the difference between the logs
    difference = np.log10(gamma_2) - np.log10(gamma_1)

    # Get the lower triangular part and zero the diagonals
    difference = np.tril( difference )
    np.fill_diagonal( difference, 0.0 )

    
    # Print the columns of the gammas and their difference
    if args.v:   
        for it in range(gamma_1.shape[0]):
            input(
            '\nPress Enter to see original rates and their difference for transition {}\n{}\n{}\n{}\n'.format(
            it+1, gamma_1[:,it], gamma_2[:,it], difference[:,it] ) )
            
    return difference


def calculate_average(difference):

    # Define the lengths of each column vector: 15, 14, ..., 2 without the diagonal terms
    lengths = np.linspace( difference.shape[0]-1, 1, difference.shape[0]-1 )

    _av = []

    # Define an array with the lower triangular matrix as elements (no diagonals)
    for it,val in enumerate(lengths):
        #input('Column {}'.format(it+1))
        #print(difference[:,it], '\n', difference[len(difference)-int(val)::,it], '\n', len(difference[len(difference)-int(val)::,it]), '\n')
        _av.append( difference[len(difference)-int(val)::,it] )

    _av = np.array( _av[0] )
    _av = _av.flatten()

    # Calculate the final average and std
    average = np.average( _av )
    std = np.std( _av )
    
    return average, std


def min_max_ticks_with_zero(axdata, nticks):
    # Calculates tick positions including zero given a specified number of ticks either size of zero

    # Add on extra tick for some reason
    nticks += 1

    lowticks = np.linspace(np.nanmin(axdata), 0, nticks)
    highticks = np.linspace(np.nanmax(axdata[axdata != np.inf]), 0, nticks)
    ticks = np.append(np.append(lowticks[:-1],[0.0]), np.flip(highticks[:-1]))

    return ticks

def even_ticks_with_zero(axdata, nticks):
    # Calculates tick positions including zero for a requested number of ticks

    upper_size = np.abs(np.max(axdata))
    lower_size = np.abs(np.min(axdata))

    if upper_size > lower_size:
        lowticks = np.linspace(np.min(axdata), 0, int(nticks/(1+int(upper_size/lower_size))) )
        highticks = np.linspace(np.max(axdata), 0, int(nticks* (1 - 1/(1+int(upper_size/lower_size)))) )
    elif lower_size > upper_size:
        highticks = np.linspace(np.max(axdata), 0, int(nticks/(1+int(lower_size/upper_size))) )
        lowticks = np.linspace(np.min(axdata), 0, int(nticks* (1 - 1/(1+int(lower_size/upper_size)))) )
    else:
        highticks = np.linspace(np.min(axdata), 0,int(0.5*nticks))
        lowticks = np.linspace(np.max(axdata), 0,int(0.5*nticks))

    ticks = np.append(np.append(lowticks[:-1],[0.0]), np.flip(highticks[:-1]))

    return ticks

class TwoSlopeNormGood(colors.Normalize):
    def __init__(self, vcenter, vmin=None, vmax=None):
        # This is a ripoff of the TwoSlopeNorm class but this one actually
        # marks the data outside of the vmin->vmax range as being outside of the range
        # rather than just setting them to vmin or vmax

        self.vcenter = vcenter
        self.vmin = vmin
        self.vmax = vmax
        if vcenter is not None and vmax is not None and vcenter >= vmax:
            raise ValueError('vmin, vcenter, and vmax must be in '
                             'ascending order')
        if vcenter is not None and vmin is not None and vcenter <= vmin:
            raise ValueError('vmin, vcenter, and vmax must be in '
                             'ascending order')

    def autoscale_None(self, A):
        """
        Get vmin and vmax, and then clip at vcenter
        """
        super().autoscale_None(A)
        if self.vmin > self.vcenter:
            self.vmin = self.vcenter
        if self.vmax < self.vcenter:
            self.vmax = self.vcenter


    def __call__(self, value, clip=True):

        # Check if the data is a scalar - matplotlib internal func
        result, is_scalar = self.process_value(value)

        # Switches around vmax, min and center if they are poorly ordered
        self.autoscale_None(result)
        # Copy to local variables
        vmin, vmax = self.vmin, self.vmax

        # Make mask which marks off values lower than vmin
        lower_mask = np.ma.masked_less(result, vmin, copy=True).mask
        # Make mask which marks off values greater than vmax
        upper_mask = np.ma.masked_greater(result, vmax, copy=True).mask

        # Use npinterp to assign each value to one in the range [0, 0.5, 1.]
        # Do this for all values - it doesnt use or know about the mask
        data = np.interp(result, [self.vmin, self.vcenter, self.vmax],[0, 0.5, 1.])

        # Mask off the data array made from the above interpolation
        # so that the values which were lower than vmin are marked
        output = np.ma.array(data, mask=lower_mask)
        # Replace masked values with -0.1 i.e. outside the 0 -> 1 range
        output = np.ma.filled(output,-0.1)

        # Mask off the data array made from the above interpolation
        # so that the values which were greater than vmax are marked
        output = np.ma.array(output, mask=upper_mask)
        # Replace masked values with 1.1 i.e. outside the 0 -> 1 range
        output = np.ma.filled(output,1.1)

        # Mask the array with False for some reason - something about np.filled I dont know
        output = np.ma.array(output, mask=False)

        return output


def plot_difference(difference, average, std, args, it1, it2):

    # Get the limits of the heatmap
    _low, _up = np.min(difference), np.max(difference)

    # Max and min cutoffs of colourbar
    max_cutoff = max( args.lims )
    min_cutoff = min( args.lims )

    # Define the color map
    cmap = copy.copy(plt.cm.PuOr)
    cmap.set_under('green') # color of values less than vmin
    cmap.set_over('yellow') # color of values greater than vmax
    
    # Plot the heatmap
    ax = plt.subplot()
    norm = TwoSlopeNormGood(vmin = min_cutoff, vcenter = 0., vmax = max_cutoff)
    im = ax.imshow(difference, cmap = cmap, interpolation='nearest', norm=norm)
    
    # Create the colorbar
    cbar = ax.figure.colorbar(im, ax=ax, extend = 'both', ticks = even_ticks_with_zero([min_cutoff,max_cutoff],13), format='%3.1f',)
    cbar.ax.set_ylabel(r'log$_{10}(\gamma_'+str(it2+1)+r')$ - log$_{10}(\gamma_'+str(it1+1)+')$', rotation=-90, va="bottom")
    

    # Set the axis
    ax.set_xticks(np.arange(len(states)))
    ax.set_yticks(np.arange(len(states)))
    ax.set_xticklabels( states )
    ax.set_yticklabels( states )
    ax.set_ylabel('Final state')
    ax.set_xlabel('Initial state' )
    ax.xaxis.set_label_position('top')
    ax.xaxis.set_ticks_position('top')

    # Delimit the lower triangular matrix
    x_grid = np.arange( 0.5, 15.5, 1)
    y_grid = np.arange( -0.5, 15.5, 1)
    for val in x_grid:
        ax.hlines( val, val-1, val, lw=0.75 )
    for val in y_grid:
        ax.vlines( val, val, val+1, lw=0.75 )           

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45)#, rotation_mode="anchor")#ha="right",

    # Annotate the plot with the average and std
    ax.text( x=8, y=1, s=r'$\mu \pm \sigma = {: 4.2f} \pm {: 4.2f}$'.format( average, std ) )

    # Save and show the figure
    plt.savefig('Compare_gamma_'+str(it1+1)+'_'+str(it2+1)+'.png', dpi=300)
    plt.show()
    plt.close('all')

    return

def parse():

    description = """
    Script to plot the differences between pairs of Gamma matrices from TAU as np.log10(gamma_2) - np.log10(gamma_1).
    """

    parser = argparse.ArgumentParser( description=description,
                                  formatter_class=argparse.RawDescriptionHelpFormatter )
    parser.add_argument( 'files', nargs='+', type=str, help='Gamma file names.' )

    parser.add_argument( '--lims', nargs='*', type=float, default=[-10.,10.], help='Upper and lower limits for colormap' )

    parser.add_argument( '--threshold', type=float, default=1E-1, help='Threshold value. All elements in the Gamma matrix smaller than this are set to threshold. Default: 1E-1' )

    parser.add_argument( '-v', action='store_true', help='Print the gamma matrices and their differences.' )

    args = parser.parse_args()

    # Make array of limits
    if args.lims != None :
        if len(args.lims) != 2 :
            parser.error('Incorrect x limits specified!')
    
    return args

# ---------------------------------------------

if __name__ == '__main__':

    # Store the names of the gamma files
    args = parse()

    # Define the mJ states
#    states = [r'-$\langle J_z^1\rangle$', r'$\langle J_z^1\rangle$',
#              r'-$\langle J_z^2\rangle$', r'$\langle J_z^2\rangle$',
#              r'-$\langle J_z^3\rangle$', r'$\langle J_z^3\rangle$',
#              r'-$\langle J_z^4\rangle$', r'$\langle J_z^4\rangle$',
#              r'-$\langle J_z^5\rangle$', r'$\langle J_z^5\rangle$',
#              r'-$\langle J_z^6\rangle$', r'$\langle J_z^6\rangle$',
#              r'-$\langle J_z^7\rangle$', r'$\langle J_z^7\rangle$',
#              r'-$\langle J_z^8\rangle$', r'$\langle J_z^8\rangle$']

    states = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16' ]

    # Loop over the gamma files, calculate and plot the pair-wise difference
    for it1, file_1 in enumerate(args.files):

        for it2, file_2 in enumerate(args.files):

            if it2 > it1:

                gamma_1, gamma_2 = read_gammas(file_1, file_2)

                difference = compare_gammas(gamma_1, gamma_2, args.threshold)

                average, std = calculate_average(difference)

                plot_difference(difference, average, std, args, it1, it2)




