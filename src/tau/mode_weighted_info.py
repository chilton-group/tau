#!/usr/bin/env python3

import numpy as np
import argparse
import os


def get_user_args():

    description = """
    Script to pull the information of mode weighted option for an arbitrary set of transitions.
    """

    parser = argparse.ArgumentParser( description=description,
                                      formatter_class=argparse.RawDescriptionHelpFormatter )

    parser.add_argument('transitions', nargs='*', type=str, help='String of transitions. Examples: 1-3 1-5 or 1-3 2-all or 1-[5,12].' )
    parser.add_argument('--temp', nargs='*', default=[100], type=str, help='List of temperatures considered. Default = 100' )
    parser.add_argument('--fwhm', nargs='*', default=[10],  type=str, help='List of FWHM considered. Default = 10' )
    # args.fwhm not currently neccessary as tau only prints T to the filenames. Here for the future

    args = parser.parse_args()
    
    return args


def init_final(transition,dimension):

    initial = []

    # Define the initial state
    initial.append( int( transition.split('-')[0] ) )

    # Define the final state(s) from this initial state
    if transition.split('-')[1] == 'all':
        first_final = int( transition.split('-')[0] ) + 1
        last_final  = dimension
        final = np.arange(first_final, last_final+1) 
    elif '[' and ']' in transition.split('-')[1]:
        first_final = int( transition.split('-')[1].replace('[','').replace(']','').split(',')[0] )
        last_final  = int( transition.split('-')[1].replace('[','').replace(']','').split(',')[1] )
        final = np.arange(first_final, last_final+1) 
    else:
        final = []
        final.append( int( transition.split('-')[1] ) )


    return initial, final


# ---------------------

if __name__ == '__main__':

    # Store the transitions requested
    args = get_user_args()
    if args.transitions == []:
        exit(10)

    # Loop over the temperatures
    for temp in args.temp:

        # Read the total gamma matrix for this temperature
        total_gamma = np.loadtxt('gamma_matrix_'+str(temp)+'.dat') 
        dimension = np.shape( total_gamma )[0]
        states = np.arange(1,dimension+1)
    
        # Loop over the indicated transitions and define initial and final states
        transitions = args.transitions
        for transition in transitions:

            initial, final = init_final(transition,dimension)

            # Loop over each initial and associated final states
            for i in initial:
                for f in final:

                    # Create the file that will contain the info
                    summary = open(os.getcwd()+'/'+'Summary_transition_'+str(i)+'_'+str(f)+'_'+str(temp)+'K.out', "w")
    
                    # Store the total gamma of that transition at that temperature
                    summary.write('{:^14} {: 8.4E}\n'.format( 'Total_gamma:', total_gamma[ f-1, i-1 ] ))
    
                    # Loop over all the relavant files
                    for prop in [ 'sp', 'pdos', 'occ', 'gamma' ]:
    
                        matrix = np.loadtxt('mode_weighted_'+prop+'_'+str(temp)+'_K.dat')
                        summary.write('{:^14} {: 8.4E}\n'.format( prop, matrix[ f-1, i-1 ] ))

                    matrix = np.loadtxt('effective_num_modes_'+str(temp)+'_K.dat')
                    summary.write('{:^14} {: 8.4E}\n'.format( 'num_modes', matrix[ f-1, i-1 ] ))
    
            summary.close()
 


