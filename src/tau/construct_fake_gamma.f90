PROGRAM CONSTRUCT_FAKE_GAMMA
    USE prec
    USE constants
    IMPLICIT NONE
    REAL(KIND = WP), ALLOCATABLE :: sp(:,:), occ(:,:), pdos(:,:), num_modes(:,:), gamma_out(:,:)
    INTEGER                      :: num_states, row, col, temperature
    CHARACTER(LEN = 10000)       :: sp_file,occ_file,pdos_file,num_modes_file

    ! Read user input
    CALL READ_USER_INPUT(num_states, sp_file,occ_file,pdos_file,num_modes_file, temperature)

    ! Allocate gamma arrays and set to zero
    ALLOCATE(sp(num_states,num_states), occ(num_states,num_states), &
             pdos(num_states,num_states), num_modes(num_states,num_states), &
             gamma_out(num_states,num_states))
    
    sp  = 0.0_WP
    occ = 0.0_WP
    pdos = 0.0_WP
    num_modes = 0.0_WP
    gamma_out = 0.0_WP

    ! Read in gamma matrices from file
    CALL READ_GAMMA(TRIM(sp_file), sp)
    CALL READ_GAMMA(TRIM(occ_file), occ)
    CALL READ_GAMMA(TRIM(pdos_file), pdos)
    CALL READ_GAMMA(TRIM(num_modes_file), num_modes)

    ! Put average ratios in elements of mult array which are going to change
    DO row = 1, num_states
        DO col = 1, num_states
            gamma_out(row, col) = sp(row,col)*occ(row,col)*pdos(row,col)*num_modes(row,col)
        END DO
    END DO
    DO col = 1, num_states
        gamma_out(col, col) = 0.0_WP
        DO row = 1, num_states
            IF(row == col) CYCLE
            gamma_out(col, col) = gamma_out(col, col) - gamma_out(row, col)
        END DO
    END DO

    gamma_out = (2.0_WP*pi/hbar)*gamma_out

    ! Save the fake gamma
    CALL SAVE_GAMMA(gamma_out, temperature)

    CONTAINS

    SUBROUTINE READ_USER_INPUT(num_states,sp_file,occ_file,pdos_file,num_modes_file, temperature)
        IMPLICIT NONE
        INTEGER, INTENT(OUT)            :: num_states, temperature
        CHARACTER(LEN = *), INTENT(OUT) :: sp_file,occ_file,pdos_file,num_modes_file
        CHARACTER(LEN = 10000)          :: CDUMMY
        LOGICAL                         :: file_exists

        CALL GET_COMMAND_ARGUMENT(1,CDUMMY)
        IF (TRIM(CDUMMY) == '-h' .OR. TRIM(CDUMMY) == '') THEN
            WRITE(6,'(A)') 'construct_fake_gamma <num_states> <mode_weighted_sp> <mode_weighted_occ> <mode_weighted_pdos> <effective_num_modes> <temperature>'
            WRITE(6,'(A)')
            WRITE(6,'(A)') 'Constructs a fake gamma matrix from the mode-weighted components'
            WRITE(6,'(A)')
            WRITE(6,'(A)') 'num_states            : INTEGER              number of states in gamma                 e.g  16 for Dy3+'
            WRITE(6,'(A)') 'mode_weighted_sp      : CHARACTER            mode_weighted_sp file name                e.g  mode_weighted_sp_xxK.dat'
            WRITE(6,'(A)') 'mode_weighted_occ     : CHARACTER            mode_weighted_occ file name               e.g  mode_weighted_occ_xxK.dat'
            WRITE(6,'(A)') 'mode_weighted_pdos    : CHARACTER            mode_weighted_pdos file name              e.g  mode_weighted_pdos_xxK.dat'
            WRITE(6,'(A)') 'effective_num_modes   : CHARACTER            effective_num_modes file name             e.g  effective_num_modes_xxK.dat'
            WRITE(6,'(A)') 'temperature           : INTEGER              Temperature for final gamma matrix name   e.g  100'
            STOP
        END IF

        read(CDUMMY,*) num_states
        
        CALL GET_COMMAND_ARGUMENT(2,CDUMMY)
        sp_file = TRIM(ADJUSTL(CDUMMY))
        !Check file 1 exists
        INQUIRE(FILE=sp_file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(3A)') 'Specified file ', TRIM(ADJUSTL(sp_file)),' does not exist'
            STOP
        END IF

        CALL GET_COMMAND_ARGUMENT(3,CDUMMY)
        occ_file = TRIM(ADJUSTL(CDUMMY))
        !Check file 2 exists
        INQUIRE(FILE=occ_file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(3A)') 'Specified file ', TRIM(ADJUSTL(occ_file)),' does not exist'
            STOP
        END IF

        CALL GET_COMMAND_ARGUMENT(4,CDUMMY)
        pdos_file = TRIM(ADJUSTL(CDUMMY))
        !Check file 3 exists
        INQUIRE(FILE=pdos_file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(3A)') 'Specified file ', TRIM(ADJUSTL(pdos_file)),' does not exist'
            STOP
        END IF

        CALL GET_COMMAND_ARGUMENT(5,CDUMMY)
        num_modes_file = TRIM(ADJUSTL(CDUMMY))
        !Check file 4 exists
        INQUIRE(FILE=num_modes_file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(3A)') 'Specified file ', TRIM(ADJUSTL(num_modes_file)),' does not exist'
            STOP
        END IF

        CALL GET_COMMAND_ARGUMENT(6,CDUMMY)
        READ(CDUMMY,*) temperature

    END SUBROUTINE READ_USER_INPUT

    SUBROUTINE READ_GAMMA(gamma_file, gamma_matrix)
        IMPLICIT NONE
        REAL(KIND = WP), INTENT(OUT)   :: gamma_matrix(:,:)
        CHARACTER(LEN = *), INTENT(IN) :: gamma_file
        INTEGER                        :: row

        OPEN(33, FILE = gamma_file, STATUS='OLD')

            DO row = 1, SIZE(gamma_matrix,1)
                READ(33,*) gamma_matrix(row,:)
            END DO

        CLOSE(33)

    END SUBROUTINE READ_GAMMA

    SUBROUTINE SAVE_GAMMA(gamma_matrix, temperature)
        IMPLICIT NONE
        REAL(KIND = WP), INTENT(IN)    :: gamma_matrix(:,:)
        INTEGER                        :: row, col
        CHARACTER(LEN = 25)            :: FMT
        CHARACTER(LEN = 500)           :: file_name
        INTEGER, INTENT(IN)            :: temperature

        WRITE(file_name,'(A,I0,A)') 'fake_gamma_',temperature,'_K.dat'

        WRITE(FMT, '(A, I0, A)') '(', SIZE(gamma_matrix,1), 'E47.36E4)'

        OPEN(33, FILE = file_name, STATUS='UNKNOWN')

            DO row = 1, SIZE(gamma_matrix,1)
                write(33,FMT) (gamma_matrix(row,col), col = 1, SIZE(gamma_matrix,1))
            END DO

        CLOSE(33)

    END SUBROUTINE SAVE_GAMMA

END PROGRAM CONSTRUCT_FAKE_GAMMA