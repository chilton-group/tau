!
!/$$$$$$$$$/
!___ $$___/
!   |$$      $$$$$$\  $$\   $$\
!   |$$      \____$$\ $$ |  $$ |
!   |$$      $$$$$$$ |$$ |  $$ |
!   |$$  $$\$$  __$$ |$$ |  $$ |
!   | $$$$ /\$$$$$$$ |\$$$$$$  |
!   \_____/  \_______| \______/
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                                  !
!   tau.f90                                                                                        !
!                                                                                                  !
!   This program is a part of tau                                                                  !
!                                                                                                  !
!                                                                                                  !
!                                                                                                  !
!                                                                                                  !
!      Authors:                                                                                    !
!       Jon Kragskow                                                                               !
!       Nicholas Chilton                                                                           !
!                                                                                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


PROGRAM tau
USE hdf5
USE ISO_C_BINDING
USE, INTRINSIC :: iso_fortran_env
USE prec, ONLY : WP
USE matrix_tools
USE stev_ops, ONLY : GET_STEVENS_OPERATORS
USE global, ONLY : files
USE populations
IMPLICIT NONE

REAL(KIND = WP), ALLOCATABLE      :: pho_en(:), pho_w(:), EQ_evals(:), sm_gamma(:,:,:,:), &
                                     total_gamma(:,:,:), sm_sp(:,:,:,:), &
                                     sm_occ(:,:,:,:), sm_pdos(:,:,:,:), &
                                     detailed_value(:), augmented_gamma(:,:), temp(:), &
                                     rates(:,:), state_labels(:), state_mu(:), barrier_temps(:), &
                                     knockout_rates(:,:), mode_weighted_sp(:,:,:), zpd_scale(:), &
                                     mode_weighted_occ(:,:,:), mode_weighted_pdos(:,:,:),&
                                     mode_weighted_gamma(:,:,:), effective_n_modes(:,:,:), &
                                     pho_data(:,:,:,:), cf_strength(:,:), fwhm(:,:), Zero_field_EQ_evals(:)
REAL(KIND = WP)                   :: EQ_CFPs(3,13), OEF(3), gJ
COMPLEX(KIND = WP), ALLOCATABLE   :: EQ_evecs(:,:), stevens_ops(:,:,:,:), JP(:,:), JM(:,:), &
                                     JZ(:,:), J2(:,:), CF_matrix(:,:), spin_matrix(:,:,:), &
                                     angmom_matrix(:,:,:)
INTEGER                           :: two_J, n_modes, i, j, t, f, first_two_states(2), &
                                     num_barriers, num_pairs, max_state, it
INTEGER, ALLOCATABLE              :: first_step_states(:,:), barrier_temps_indices(:), &
                                     knockout_pairs(:,:), top_trans_unique(:,:)

! User config variables
LOGICAL, ALLOCATABLE              :: config_logi(:), modes_logical(:), transitions_logical(:,:)
LOGICAL                           :: debug=.FALSE.,user_field_set=.FALSE.
INTEGER, ALLOCATABLE              :: config_int(:), user_barrier_temps(:,:), user_transitions(:,:),&
                                     single_modes(:)
CHARACTER(LEN = 500), ALLOCATABLE :: config_char(:)
REAL(KIND = WP), ALLOCATABLE      :: config_real(:)
REAL(KIND = WP), ALLOCATABLE      :: config_array(:,:)

! Set up hdf5 datatypes
INTEGER(HID_T) :: hdf5_complex_datatype, hdf5_data_id, hdf5_file_id, hdf5_group_id
INTEGER :: hdf5_error
TYPE(C_PTR) :: hdf5_ptr
CALL SETUP_HDF5

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Read input files and set program variables   !!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Check for user command line input
! and write either help or blank input file
CALL READ_USER_COMMANDS

! Write out logo header and start date and time
CALL SELF_PROMO

! Subroutine for preprocessor commands
CALL SYSTEM_CONFIGS

! Copy input file to output file
CALL COPY_INPUT_TO_OUTPUT

! Set up config arrays
CALL SET_UP_CONFIG_ARRAYS(config_logi, config_int, config_char, config_real,config_array)

! Read input file options into config arrays
! additionally, if remove_mode, user_barrier_temps, or user_transition are
! selected then read in the user specified values into separate arrays
CALL READ_INPUT_FILE(config_logi, config_int, config_char, config_real, config_array, &
                     modes_logical, user_transitions, temp, debug, user_barrier_temps, &
                     single_modes, transitions_logical, two_J, gJ, OEF)

! Find user data files and record path
! mode_energies.dat, EQ_CFPs.dat and either CFP_polynomials.dat, or CFP_steps.dat
CALL FIND_FILES(config_logi)

! Read in scaling factors for zero point displacements
! if scale zpd isnt selected then the returned array has elements of unity
CALL READ_ZPD_SCALE(config_int(1), config_logi(27), config_int(10), zpd_scale)

! Get dimension of total Hamiltonian for hdf5 input
IF(config_logi(34)) THEN
    CALL GET_HDF5_DIMENSION(config_int(15))
    IF(config_int(16) > config_int(15)) THEN
	! user selected dimension is larger than hdf5 dimension
	CALL WRITE_ERROR('Chosen HDF5 dimension larger than HDF5 file',&
                                     'Check input file')
    END IF
END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Set up calculation variables   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Set up Stevens Operators
IF (.NOT. config_logi(34)) CALL GET_STEVENS_OPERATORS(two_J, stevens_ops, JP, JM, J2, JZ)

! Read in Mode Energies
CALL READ_PHONON_ENERGIES(config_int(1), pho_en)
! Read in Mode Weights
IF (config_logi(30)) THEN
    CALL READ_PHONON_WEIGHTS(config_int(1), pho_w)
! Or set all equal to 1. if weighting not requested
ELSE
    ALLOCATE(pho_w(config_int(1)))
    pho_w = 1.0_WP
END IF

! Set FWHM Values per temperature per mode
ALLOCATE(fwhm(config_int(5), config_int(1)))
fwhm = 0.0_WP

! Lunghi FWHM, calculate per temperature per mode
IF (config_logi(7)) THEN
    DO t = 1, config_int(5)
        DO j = 1, config_int(1)
            fwhm(t, j) = 2.0_WP * LUNGHI_WIDTH(pho_en(j), temp(t))
        END DO
    END DO
! Read FWHM from file, per mode (equal for all temperatures)
ELSE IF (config_logi(32)) THEN
    CALL READ_PHONON_FWHM(config_int(1), fwhm)
! Fixed value for all modes and all temperatures
ELSE
    fwhm = config_real(4)
END IF


! Calibrate mode energies if requested
! and write to output file
IF (config_logi(14)) THEN
    CALL APPLY_CALIBRATION(pho_en, config_real(2), config_real(3), config_int(1))
END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Calculate static properties   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! READ hdf5 file or traditional CF method
IF (config_logi(34)) THEN
    CALL READ_EQ_HAMILTONIAN_HDF5(config_int(15),Zero_field_EQ_evals)
    CALL READ_ANGMOM_HDF5(config_int(15),spin_matrix,angmom_matrix)
    ! Calculate equilibrium CF Hamiltonian in quad precision
    CALL CALC_EQ_CF_HAMILTONIAN_HDF5(config_int(15), Zero_field_EQ_evals, spin_matrix, angmom_matrix, config_array(1,1:3), CF_matrix)
    ! Diagonalise equilibrium CF Hamiltonian
    CALL DIAGONALISE_EQ_CF_HAMILTONIAN_CPP_HDF5(config_int(15), CF_matrix, EQ_evecs, EQ_evals)
    ! Calculate which state number is the ground state by finding the state with maximum -ve mJ
    CALL CALC_MAGNETIC_MOMENT_HDF5(config_int(15), spin_matrix, angmom_matrix, EQ_evecs, first_two_states, state_mu, config_array(1,1:3))
    ! Write file containing information on the equilibrium electronic structure
    ! eigenvalues
    CALL WRITE_EQ_INFO_HDF5(config_int(16), EQ_evecs, EQ_evals, state_mu, config_array(1,1:3))
    ! Read in replacement CF energies if requested and write to output file
    IF (config_logi(19)) THEN
        CALL REPLACE_CF_ENERGIES(config_int(16), EQ_evals)
    END IF
ELSE
    ! Read in equilibrium crystal field parameters
    CALL READ_EQ_CFP(EQ_CFPs)
    ! Calculate equilibrium CF Hamiltonian in quad precision
    CALL CALC_EQ_CF_HAMILTONIAN(two_J, EQ_CFPs, JP, JM, JZ, gJ, OEF, stevens_ops, config_array(1,1:3),&
                            CF_matrix)
    ! Diagonalise equilibrium CF Hamiltonian
    IF (config_logi(25)) THEN
        CALL DIAGONALISE_EQ_CF_HAMILTONIAN_MM(two_J, CF_matrix, EQ_evecs, EQ_evals)
    ELSE
        CALL DIAGONALISE_EQ_CF_HAMILTONIAN_CPP(two_J, CF_matrix, EQ_evecs, EQ_evals)
    END IF
    ! Calculate which state number is the ground state by finding the state with maximum -ve mJ
    CALL CALC_MAGNETIC_MOMENT(two_J, JP, JM, JZ, gJ, EQ_evecs, first_two_states, state_mu, config_array(1,1:3))
    ! Write file containing information on the equilibrium electronic structure
    ! eigenvalues, eigenvectors, magnetic moment, mJ
    ! Also returns array of mJ values
    CALL WRITE_EQ_INFO(two_J, EQ_evecs, EQ_evals, state_mu, state_labels, config_array(1,1:3), &
                       config_logi(18))
    ! Read in replacement CF energies if requested and write to output file
    IF (config_logi(19)) THEN
        CALL REPLACE_CF_ENERGIES(two_J, EQ_evals)
    END IF
END IF


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   Calculate gamma matrices   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! Gamma matrix is written as:


!                initial states
!
!            ______________________
!            |                     |
!            |                     |
! final      |                     |
! states     |        GAMMA        |
!            |                     |
!            |                     |
!            |                     |
!            ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
!

! Calculate gamma matrices or read from file

! Read in gamma from file
IF (config_logi(22)) THEN
    CALL READ_USER_GAMMA(two_J, temp, config_int(5), total_gamma, config_char(3), max_state)
ELSE
    ! If not using HDF5 file input,
    ! read in Crystal field parameters which define spin-phonon coupling Hamiltonian,
    ! either from CFP_steps.dat or CFP_polynomials.dat
    IF (.NOT. config_logi(34)) THEN
        IF (config_logi(3))  THEN
            CALL READ_PHONON_CF_STEPS(pho_data, config_int(1), modes_logical)
        ELSE IF (config_logi(4) .OR. config_logi(23))  THEN
            CALL READ_PHONON_CF_POLYNOMIAL(pho_data, config_int(1), modes_logical)
        END IF
    END IF

! Calculate gamma for all modes
    CALL CALC_GAMMA(sm_gamma, sm_sp, sm_occ, sm_pdos, total_gamma, config_logi, config_int, &
                    EQ_evals, EQ_evecs, temp, fwhm, pho_en, pho_w, two_J, OEF, &
                    stevens_ops, zpd_scale, pho_data, max_state, config_char(4), config_int(16))
END IF

! Calculate crystal field strength parameters at high temperature and plot
! with external python script
IF (config_logi(20)) THEN
    ! Calculate strength
    CALL CALC_CF_STRENGTH(pho_data, pho_en, config_int(1), temp(config_int(5)), config_logi, &
                           cf_strength, zpd_scale)
    CALL PLOT_CF_STRENGTH(cf_strength, EQ_evals, temp(config_int(5)), config_logi(24))
END IF


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Gamma Analysis   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Calculate detailed balance condition
CALL CALC_DETAILED_BALANCE(max_state, total_gamma, EQ_evals, temp, detailed_value, &
                               config_int(5))

! Check if detailed balance is broken at some temperature
! t_det is minimum temperature at which detailed balance IS obeyed
IF (config_logi(1)) THEN
    CALL CHECK_DETAILED_BALANCE(detailed_value, temp, config_int(5))
END IF

! If single mode is requested write raw CF, DOS and OCC matrices to file
IF (config_logi(10) .AND. config_logi(1) .AND. .NOT. config_logi(2)) THEN
    CALL  WRITE_ORBACH_COMPONENTS(config_int(8), single_modes, temp, &
                                  config_int(5), sm_gamma, sm_sp, &
                                  sm_occ, sm_pdos)
END IF

! If weighted single mode is requested then weight CF, DOS and OCC matrices by the total gamma and
! write to file
IF (config_logi(26) .AND. config_logi(1) .AND. .NOT. config_logi(2)) THEN
    CALL  WEIGHT_ORBACH_COMPONENTS(max_state, config_int(5), config_int(1), sm_gamma, sm_sp, &
                                   sm_occ, sm_pdos, total_gamma, mode_weighted_sp, &
                                   mode_weighted_occ, mode_weighted_pdos, mode_weighted_gamma, &
                                   effective_n_modes)
    CALL  WRITE_WEIGHTED_ORBACH_COMPONENTS(temp, config_int(5), mode_weighted_sp, &
                                           mode_weighted_occ, mode_weighted_pdos, &
                                           mode_weighted_gamma, effective_n_modes)
END IF

! Remove transitions if requested
IF (config_logi(17) .AND. config_logi(1) .AND. .NOT. config_logi(2)) THEN
    CALL CUT_TRANSITIONS(sm_gamma, total_gamma, transitions_logical, config_int(5), two_J)
END IF

! Calculate the fastest step out of ground state for each temperature
CALL CALC_FIRST_STEP(first_two_states, total_gamma, first_step_states, config_int(5))

! Write contribution of each mode to the fastest initial step of relaxation
! This can only be done if gamma is calculated rather than read in
IF (config_logi(1) .AND. .NOT. config_logi(2) .AND. .NOT. config_logi(22)) THEN
    CALL WRITE_FIRST_STEP_CONTRIBUTIONS(first_step_states, sm_gamma, total_gamma, temp, &
                                        config_int(5), EQ_evals, config_int(1), pho_en, &
                                        state_labels)
END IF

! Write contribution of each mode to the user selected transition
! This can only be done if gamma is calculated rather than read in
IF (config_logi(11) .AND. .NOT. config_logi(22) .AND. config_logi(1) .AND. .NOT. config_logi(2)) THEN
    DO f = 1, SIZE(user_transitions,1)
        CALL WRITE_CONTRIBUTIONS(user_transitions(f,:), sm_gamma, &
                                 total_gamma, temp, config_int(5), &
                                 EQ_evals, config_int(1), pho_en, state_labels, 'user')
    END DO
END IF

! Write information on initial transiton probabilities
CALL WRITE_INITIAL_PROBABILITIES(total_gamma, first_step_states, max_state, temp, config_int(5))

! Write gamma matrices to file if mathematica is to be used, or if save gamma has been indicated
IF (config_logi(12) .OR. config_logi(25)) THEN
    DO t = 1, config_int(5)
        ! Write out Gamma matrices for temp which obey detailed balance
        CALL WRITE_GAMMA_MATRIX_FILE(total_gamma(t,:,:), temp(t), .FALSE.)
    END DO
END IF

IF (debug) THEN
    DO t = 1, config_int(5)
        CALL WRITE_GAMMA_MATRIX_FILE(total_gamma(t,:,:), temp(t), .FALSE.)
    END DO
END IF


! Knockout procedure to find important transitions

IF (config_logi(21) .AND. config_logi(1) .AND. .NOT. config_logi(22) .AND. .NOT. config_logi(2)) THEN

    ! If user has also selected transitions knockout then print a warning
    IF (config_logi(17)) THEN
        ! Open output file
            OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

        ! Status update
            CALL WARNING_HEADER('You have selected knockout and remove transitions ' // &
                         '- think about what this means!', 66)
            WRITE(66,*)

        ! Close output file
            CLOSE(66)
    END IF

    ! Loop over each pair of transitions, remove them, rediagonalise gamma, and store rates.
        CALL KNOCKOUT_TRANSITIONS(max_state, state_labels, sm_gamma, config_int(5), &
                                  temp, config_int(3), config_int(4), config_int(2), &
                                  knockout_rates, knockout_pairs, num_pairs, config_logi(25))
    ! Print most important transition for each temperature
        CALL PRINT_TOP_TRANSITIONS(temp, config_int(5), knockout_pairs, &
                                   knockout_rates, num_pairs, top_trans_unique)

    ! Print mode contributions for most important transitions
        DO f = 1, SIZE(top_trans_unique,1)
            CALL WRITE_CONTRIBUTIONS(top_trans_unique(f,:), sm_gamma, &
                                     total_gamma, temp, config_int(5), &
                                     EQ_evals, config_int(1), pho_en, state_labels, 'ko')
        END DO
END IF

! Calculate barrier figures

! Calculate list of temp for barrier to plotted at
    ! CALL CALC_BARRIER_TEMPS(user_barrier_temps, config_int(9), REAL(config_int(2),WP), &
    !                          1, t_min, num_temps, barrier_temps, barrier_temps_indices, &
    !                          num_barriers)

! Print barrier figure and info for each temperature requested
    ! DO t = 1, num_barriers
        ! CALL RELAXATION_PATHWAYS(two_J, EQ_evecs, EQ_evals, &
        !                           total_gamma(barrier_temps_indices(t), :, :), barrier_temps(t), &
        !                           state_labels, config_char(2))
    ! END DO


! Populations calculation
IF (config_logi(15)) THEN
    CALL POPSIM(total_gamma, EQ_evals, two_J+1, temp, state_mu, config_real(6), config_int(14))
END IF

! Exit program if user requests no rates
IF (config_logi(13)) THEN
! Delete gamma matrix unless requested
    IF (.NOT. config_logi(12)) CALL EXECUTE_COMMAND_LINE('rm -f sm_gamma.*')
! Print successful termination to screen
    CALL PRINT_DATE_AND_TIME('FNSH')
    STOP
END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   Calculate relaxation rates   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Diagonalise gamma and read in relaxation rates
ALLOCATE(rates(config_int(5), max_state))
rates = 0.0_WP

IF (config_logi(25)) THEN
    CALL DIAGONALISE_GAMMA_MM(max_state, config_int(5), config_int(3), config_int(4), &
                              config_int(2), config_logi(12), .FALSE., rates)
ELSE
    CALL DIAGONALISE_GAMMA_CPP(max_state, total_gamma, config_int(5), rates)
END IF

! do i = 1,config_int(5)
!     write(6,'(17E14.6E2)') temp(i), rates(i,:)
! end do

! Write out equilibrium rate at each temperature to output file
CALL WRITE_RATES_AND_DBALANCE(rates(:, max_state), rates(:, max_state-1), detailed_value, config_int(5), temp)

! Print successful termination to screen and end program
CALL PRINT_DATE_AND_TIME('FNSH')

CALL CLOSE_HDF5

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   END MAIN PROGRAM   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

CONTAINS


SUBROUTINE READ_USER_COMMANDS()
    USE global, only : SET_IO_FILE_NAMES
    ! Reads command line input from user and either return the help or a blank input file
    IMPLICIT NONE
    CHARACTER(LEN = 500)            :: INPUT, CDUMMY
    LOGICAL                         :: file_exists

    CALL GET_COMMAND_ARGUMENT(1,INPUT)

    ! Print help
    IF (TRIM(INPUT) == '' .OR. TRIM(INPUT) == '-h' .OR. TRIM(INPUT) == '--h' .OR. &
        TRIM(INPUT) == 'h' .OR. TRIM(INPUT) == '-H' .OR. TRIM(INPUT) == '--H') THEN
        WRITE(6,'(A)') 'Tau README'
        WRITE(6,'(A)') '    put Equilbrium CFPs in PHI notation into "EQ_CFPs.dat" in CWD'
        WRITE(6,'(A)') '    put list of mode energies into "mode_energies.dat" in CWD'
        WRITE(6,'(A)')
        WRITE(6,'(A)') '    EITHER:'
        WRITE(6,'(A)') '    put phonon polynomial fits into "CFP_polynomials.dat" in CWD'
        WRITE(6,'(A)') '        these are delta CFPs (referenced to equilibrium CFPs) and the OEFs'
        WRITE(6,'(A)') '        are not included (so these are the "real" CFPs)'
        WRITE(6,'(A)') '        the fits give both positive and negative displacements'
        WRITE(6,'(A)') '        the loop runs over: mode,k,q'
        WRITE(6,'(A)') '            Vibration 1 a*x3 + b*x2 + c*x'
        WRITE(6,'(A)') '            2 -2    0.218680932   -0.195879761   -0.123801319'
        WRITE(6,'(A)') '            2 -1   -0.286279584    0.397230707  -31.983155780'
        WRITE(6,'(A)') '            2  0   -0.088020235    0.566035013    2.192620636'
        WRITE(6,'(A)') '            2  1   -0.016686283   -0.791528742   31.352021469'
        WRITE(6,'(A)') '            2  2    6.422930260   -5.343011290  -11.560069625'
        WRITE(6,'(A)') '            etc.'
        WRITE(6,'(A)') '        positions 1 - 3 are x^3, x^2 and x^1 coefficients, respectively'
        WRITE(6,'(A)')
        WRITE(6,'(A)') '    set up input.dat in CWD'
        WRITE(6,'(A)') '    (Write blank input file using tau -input)'
        WRITE(6,'(A)') '    EITHER:'
        WRITE(6,'(A)') '        Run tau in CWD'
        WRITE(6,'(A)') '    OR:'
        WRITE(6,'(A)') '        Run autau in CWD '
        WRITE(6,'(A)') '        Run autau -h for explanation of options'
        STOP

    ! Print a blank input file
    ELSE IF (TRIM(INPUT) == '-input' .OR. TRIM(INPUT) == '--input') THEN

        OPEN(77, FILE = 'blank_input.dat', STATUS = 'UNKNOWN')

            WRITE(77,'(A)') '****core parameters'
            WRITE(77,'(A)') 'ion SYMBOL OXSTATE_NUMBER'
            WRITE(77,'(A)') 'modes NUMBER'
            WRITE(77,'(A)') 'temp LOW HIGH STEP'
            WRITE(77,'(A)') 'dynamics ORBACH/RAMAN'
            WRITE(77,'(A)') 'statistics BOLTZMANN/BOSE'
            WRITE(77,'(A)') 'fwhm NUMBER'
            WRITE(77,'(A)') ''
            WRITE(77,'(A)') ''

        CLOSE(77)

        WRITE(6,'(A)') '~~~~         Blank input file written to blank_input.dat in CWD         ~~~~'
        WRITE(6,'(A)') '!!!!                  Replace BOLD TEXT as appropriate                  !!!!'

        STOP

    ! Clean directories
    ELSE IF (TRIM(INPUT) == '-clean') THEN
        CALL CLEAN_DIRECTORY
        WRITE(6,'(A)') '!!!!                         DIRECTORY CLEANED                          !!!!'
        STOP

    ! Clean directories but do not print anything to screen
    ELSE IF (TRIM(INPUT) == '-cleansilent') THEN
        CALL CLEAN_DIRECTORY
        STOP

    ! Write CSF jobscript
    ELSE IF (TRIM(INPUT) == '-subscript' .OR. TRIM(INPUT) == '--subscript') THEN

        OPEN(77, FILE = 'tau.sh', STATUS = 'UNKNOWN')

            WRITE(77,'(A)') '#!/bin/bash --login'
            WRITE(77,'(A)') '#$ -S /bin/bash'
            WRITE(77,'(A)') '#$ -N tau_job # You can change this'
            WRITE(77,'(A)') '#$ -cwd'
            WRITE(77,'(A)') '#$ -pe smp.pe 4 # You can change this - default is 4 cores'
            WRITE(77,'(A)') '#$ -l short # requests short queue up to 12 cores for 1 hour'
            WRITE(77,'(A)') 'export OMP_NUM_THREADS=$NSLOTS # DO NOT CHANGE THIS'
            WRITE(77,'(A)') 'module load compilers/intel/18.0.3'
            WRITE(77,'(A)') 'tau input.dat # Change this if your tau input file is named differently'

        CLOSE(77)

        WRITE(6,'(A)') '~~~~             Submission script written to tau.sh  in CWD             ~~~~'

        STOP


    ! Else get the tau input file name
    ELSE

        ! Activate autau batch mode - uses path of input file to find working directory
        CALL GET_COMMAND_ARGUMENT(2,CDUMMY)
        IF (TRIM(CDUMMY) == '-autau') THEN
            CALL SET_IO_FILE_NAMES(files, input, .TRUE.)
        ELSE
            CALL SET_IO_FILE_NAMES(files, input, .FALSE.)
        END IF

        READ(INPUT,*) files%input

        INQUIRE(FILE = TRIM(files%input), EXIST = file_exists)

    ! Check the specified input file exists
        IF (.NOT. file_exists ) THEN
            CALL WRITE_ERROR('Cannot find '//TRIM(files%input))
        END IF
    END IF



END SUBROUTINE READ_USER_COMMANDS

SUBROUTINE SELF_PROMO
    USE global, only : files
    IMPLICIT NONE
    CHARACTER(LEN = 100)        :: DATE,TIME,ZONE
    INTEGER                     :: DATETIME(8)

    CALL DATE_AND_TIME(DATE,TIME,ZONE,DATETIME)

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

        IF (DATETIME(1) == 12) THEN

            WRITE (66,'(A)') '  TAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUT'//&
            '                    _/^\_'
            WRITE (66,'(A)') '  A                                            A'//&
            '                   <     >'
            WRITE (66,'(A)') '  U      /$$$$$$$$$/                           U'//&
            '  *                 /.-.\         *'
            WRITE (66,'(A)') '  T     |___ $$___/                            T'//&
            '           *        `/&\`             *'
            WRITE (66,'(A)') '  A         |$$       $$$$$$\  $$\   $$\       A'//&
            '                   ,@.*;@,'
            WRITE (66,'(A)') '  U         |$$       \____$$\ $$ |  $$ |      U'//&
            '                  /_o.I %_\    *'
            WRITE (66,'(A)') '  T         |$$       $$$$$$$ |$$ |  $$ |      T'//&
            '     *           (``--:o(_@;'
            WRITE (66,'(A)') '  A         |$$  $$\/$$  __$$ |$$ |  $$ |      A'//&
            '                /`;--.,__ ``)         *'
            WRITE (66,'(A)') '  U         | $$$$ / \$$$$$$$ |\$$$$$$  |      U'//&
            '               ;@`o % O,*```&\ '
            WRITE (66,'(A)') '  T         \_____/   \_______| \______/       T'//&
            '         *    (``--)_@ ;o %`()\      *'
            WRITE (66,'(A)') '  A                                            A'//&
            '              /`;--._```--._O`@;'
            WRITE (66,'(A)') '  U                    Authors:                U'//&
            '             /&*,()~o`;-.,_ `""`)'
            WRITE (66,'(A)') '  T                 Jon Kragskow               T'//&
            '  *          /`,@ ;+& () o*`;-`;\ '
            WRITE (66,'(A)') '  A                  Daniel Reta               A'//&
            '            (`""--.,_0 +% @` &()\ '
            WRITE (66,'(A)') '  U                Nicholas Chilton            U'//&
            '            /-.,_    ````--....-``)  *'
            WRITE (66,'(A)') '  T                                            T'//&
            '           `"="==""==,,,.,="=="==="`'
            WRITE (66,'(A)') '  A                                            A'//&
            '        __.----.(\-``#####---...___...'
            WRITE (66,'(A)') '  UTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAU'//&
            '                                          '

        ELSE

            WRITE(66,'(A)')'                 '// &
            'TAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUT                 '
            WRITE(66,'(A)')'                 '// &
            'A                                            A                 '
            WRITE(66,'(A)')'                 '// &
            'U      /$$$$$$$$$/                           U                 '
            WRITE(66,'(A)')'                 '// &
            'T     |___ $$___/                            T                 '
            WRITE(66,'(A)')'                 '// &
            'A         |$$       $$$$$$\  $$\   $$\       A                 '
            WRITE(66,'(A)')'                 '// &
            'U         |$$       \____$$\ $$ |  $$ |      U                 '
            WRITE(66,'(A)')'                 '// &
            'T         |$$       $$$$$$$ |$$ |  $$ |      T                 '
            WRITE(66,'(A)')'                 '// &
            'A         |$$  $$\/$$  __$$ |$$ |  $$ |      A                 '
            WRITE(66,'(A)')'                 '// &
            'U         | $$$$ / \$$$$$$$ |\$$$$$$  |      U                 '
            WRITE(66,'(A)')'                 '// &
            'T         \_____/   \_______| \______/       T                 '
            WRITE(66,'(A)')'                 '// &
            'A                                            A                 '
            WRITE(66,'(A)')'                 '// &
            'U                    Authors:                U                 '
            WRITE(66,'(A)')'                 '// &
            'T                 Jon Kragskow               T                 '
            WRITE(66,'(A)')'                 '// &
            'A                  Daniel Reta               A                 '
            WRITE(66,'(A)')'                 '// &
            'U                Nicholas Chilton            U                 '
            WRITE(66,'(A)')'                 '// &
            'T                                            T                 '
            WRITE(66,'(A)')'                 '// &
            'A                                            A                 '
            WRITE(66,'(A)')'                 '// &
            'UTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAUTAU                 '
            WRITE(66,'(A)')    ''

        END IF
! Close output file
    CLOSE(66)

! Print Time and Date
        CALL PRINT_DATE_AND_TIME('STRT')

END SUBROUTINE SELF_PROMO

SUBROUTINE COPY_INPUT_TO_OUTPUT
    USE global, only : files
    IMPLICIT NONE
    CHARACTER(LEN = 500)        :: line
    INTEGER                     :: reason

! Open input file
    OPEN(33, FILE = files%input, STATUS = 'OLD')

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write header in output file
    WRITE(66,'(A)') ''
    CALL SECTION_HEADER('Input File',66)
    WRITE(66,'(A)') ''

! Loop through file and copy input to output file
    DO
        READ(33,'(A)',IOSTAT = reason) line

    ! Check file is not corrupted
        IF (reason > 0)  THEN
            CALL WRITE_ERROR('Error reading input file')
    ! Check if EOF has been reached
        ELSE IF (reason < 0) THEN
            EXIT
        END IF

    ! Write input to output if line is not blank
        CALL lowercase(line)
        IF (LEN_TRIM(ADJUSTL(line)) /= 0) WRITE(66,'(A)') TRIM(ADJUSTL(line))

    END DO

! Close input file
    CLOSE(33)

END SUBROUTINE COPY_INPUT_TO_OUTPUT

PURE SUBROUTINE SET_UP_CONFIG_ARRAYS(config_logi, config_int, config_char, config_real, config_array)
    ! Set up the config arrays which contain the user specified configuration options
    ! Config arrays are separated by variable type:
    !   integer, real, logical, character
    IMPLICIT NONE
    INTEGER                                      :: num_config_logi, num_config_real, &
                                                    num_config_int ,num_config_char
    LOGICAL, INTENT(OUT), ALLOCATABLE            :: config_logi(:)
    INTEGER, INTENT(OUT), ALLOCATABLE            :: config_int(:)
    CHARACTER(LEN = *), INTENT(OUT), ALLOCATABLE :: config_char(:)
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE    :: config_real(:)
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE    :: config_array(:,:)

! Set the number of configurable parameters of each variable type
    num_config_logi    = 35
    num_config_int     = 16
    num_config_real    = 6
    num_config_char    = 4

! Allocate config arrays
    ALLOCATE(config_logi(num_config_logi), config_real(num_config_real),config_int(num_config_int),&
             config_char(num_config_char),config_array(1,3))

! Set initial values
    config_logi = .FALSE.
    config_logi(6) = .TRUE.     ! BOSE-EINSTEIN STATISTICS
    config_logi(23) = .TRUE.    ! TAYLOR CFPs

! Set inital values
    config_int = 0
    config_int(11) = 50 ! Default number of steps in raman trapz function
    config_int(13) =   2 ! Default width scaling for raman integration

! set initial/default values
    config_real  = 0.0_WP
    config_real(5) = 1.0_WP ! Default scaling factor for DOS
    config_array = 0.0_WP

! set initial/default values
    WRITE(config_char(1),*) 'EMPTY'
    WRITE(config_char(2),*) 'svg'
    WRITE(config_char(3),*) 'EMPTY'
    WRITE(config_char(4),*) 'antilorentzian'

    ! config_logi is laid out as follows

    ! 1  Direct
    ! 2  Raman
    ! 3  CFPs static
    ! 4  CFPs polynomial
    ! 5  Statistics Boltzmann
    ! 6  Statistics Bose-Einstein
    ! 7  Lunghi Linewidth expression
    ! 8  Harmonics
    ! 9  Remove modes
    ! 10 Single Mode
    ! 11 User selected transitions
    ! 12 Save Gamma
    ! 13 Deactivate diagonalisation of gamma
    ! 14 Calibration of IR energies
    ! 15 Calculate populations
    ! 16 User selected barrier temp
    ! 17 Remove transitions
    ! 18 Print eq eigenvectors
    ! 19 Replace CF Energies
    ! 20 Calculate strength parameters and plot
    ! 21 Transition Knockout
    ! 22 Read in precalculated/defined gamma matrices
    ! 23 Use proper taylor series of CFPs
    ! 24 Save CF strength information
    ! 25 Use mathematica for diagonalisation of CF Hamiltonian and Gamma
    ! 26 single mode weighted by gamma
    ! 27 Use zpd factors
    ! 28 User selected max state for raman
    ! 29 Read polynomials with old zpd and move them to new def
    ! 30 User selected weighting of phonon DOS
    ! 31 Skip Raman Sum Process terms (W--, W++)
    ! 32 Read fwhm from file
    ! 33 Output 2D-mode-dependent Raman info for plot 
    ! 34 Read Hamiltonians from hdf5 files
    ! 35 hdf5 input is a Kramers spin system

    ! config_int is laid out as follows

    ! 1  n_modes
    ! 2  t_max
    ! 3  t_min
    ! 4  t_step
    ! 5  num_temps
    ! 6  ox_state
    ! 7  number of removed modes
    ! 8  number of single modes
    ! 9  number of user selected barrier temp
    ! 10 Cut off for modes which new polynomial definitions
    ! 11 Number of steps in raman trapezoidal integration
    ! 12 Maximum initial and final states in raman calculation
    ! 13 Width scaling for raman integration
    ! 14 Number of time steps in population simulation
    ! 15 Dimension of total Hamiltonian for hdf5 input
    ! 16 Dimension of chosen subspace Hamiltonian for hdf5 dynamics

    ! config_real is laid out as follows

    ! 1 unused
    ! 2 IR calibration slope
    ! 3 IR calibration intercept in cm-1
    ! 4 FWHM in cm-1 (used only if Fixed value specified)
    ! 5 Scaling factor for density of states
    ! 6 Size of time step for population simulation

    ! config_array is laid out as follows

    ! 1,1-3 Vector of external magnetic field strength in T

    ! config_char is laid out as follows

    ! 1 ion
    ! 2 barrier figure save format
    ! 3 read gamma file header
    ! 4 choice of lineshape expression


END SUBROUTINE SET_UP_CONFIG_ARRAYS

SUBROUTINE READ_INPUT_FILE(config_logi, config_int, config_char, config_real, config_array, &
                           modes_logical, user_transitions, temp, debug, &
                           user_barrier_temps, single_modes, transitions_logical, two_J, gJ, OEF)
    ! Read input file options and assign to config arrays
    ! Config arrays are separated by variable type:
    !   integer, real, logical, character
    ! temp are given their own array 'temp'
    ! Additionally, if remove_mode, user_barrier_temps, or user_transition are
    ! selected then the user specified values are read into separate arrays
    USE global, only : files
    IMPLICIT NONE
    CHARACTER(LEN = 500)                      :: line, DUMMY
    CHARACTER(LEN = 500)                      :: error_message
    CHARACTER(LEN = 15), ALLOCATABLE          :: CDummy_array(:)
    CHARACTER(LEN = *), INTENT(OUT)           :: config_char(:)
    LOGICAL, INTENT(OUT)                      :: config_logi(:), debug
    LOGICAL, ALLOCATABLE, INTENT(OUT)         :: modes_logical(:), transitions_logical(:,:)
    LOGICAL, ALLOCATABLE                      :: removal(:), single_logical(:)
    LOGICAL                                   :: found
    INTEGER                                   :: reason, num_entries, i, IDUMMY1, IDUMMY2, j, k, &
                                                 num_transitions, num_temp_remove
    INTEGER, ALLOCATABLE, INTENT(OUT)         :: user_barrier_temps(:,:),  user_transitions(:,:), &
                                                 single_modes(:)
    INTEGER, ALLOCATABLE                      :: dummy_barrier_temps(:,:)
    INTEGER, INTENT(OUT)                      :: config_int(:), two_J
    REAL(KIND = WP), INTENT(OUT)              :: config_real(:), gJ, OEF(3), config_array(:,:)
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE :: temp(:)
    REAL(KIND = WP)                           :: RDUMMY1, RDUMMY2

! Open input file
    OPEN(33, FILE = files%input, STATUS = 'OLD')

! Loop through file and read input sections
    DO
        READ(33,'(A)',IOSTAT=reason) line

    ! Check file is not corrupted
        IF (reason > 0)  THEN
            CALL WRITE_ERROR('Error reading input file')
    ! Check if EOF has been reached
        ELSE IF (reason < 0) THEN
            EXIT
        ELSE

        ! Skip blank lines
            IF (LEN_TRIM(line) == 0) CYCLE

        ! Make all of line lowercase
            CALL LOWERCASE(line)

            !  Skip comment lines
            IF (GET_TRIMMED_VAL(line, 1, 1) == '! ') CYCLE
            IF (GET_TRIMMED_VAL(line, 1, 1) == '#') CYCLE
            IF (GET_TRIMMED_VAL(line, 1, 1) == '*') CYCLE


            IF (GET_TRIMMED_VAL(line, 1, 5) == 'debug') THEN
                debug = .TRUE.
            ! Print debug mode alert
                OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
                    WRITE(66,*)
                    CALL WARNING_HEADER('DEBUG MODE IS ACTIVE',66)
                    WRITE(66,*)
                CLOSE(66)
            END IF

        ! Ion type and oxidation state
            IF (GET_TRIMMED_VAL(line, 1, 3) == 'ion') THEN
            ! Check correct number of inputs are present
                IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                    READ(line,*, IOSTAT = reason) DUMMY, config_char(1), config_int(6)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading Ion',&
                                         'Check input file')
                    END IF
                ELSE
                ! Write error message
                    CALL WRITE_ERROR('Error in ion specification',&
                                     'Check input file')
                END IF

            ! Set two_J and OEF for the given element and oxidation state
                CALL SET_ATOM(config_char(1), OEF, config_int(6), two_J, gJ)

            ! Allocate array for transitions to be deleted - if none are selected then this is true for all entries
            ALLOCATE(transitions_logical(two_J + 1, two_J + 1))
            transitions_logical = .TRUE.

            END IF
        
        ! HDF5 input
            IF (GET_TRIMMED_VAL(line, 1, 4) == 'hdf5') THEN
                config_logi(34) = .TRUE.
            ! Check correct number of inputs are present
                IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                    READ(line,*, IOSTAT = reason) DUMMY, config_int(16), DUMMY
                    IF(TRIM(DUMMY(1:1)) == "k" .or. TRIM(DUMMY(1:1)) == "K") THEN
                        config_logi(35) = .TRUE.
                    ELSE IF(TRIM(DUMMY(1:1)) == "n" .or. TRIM(DUMMY(1:1)) == "N") THEN
                        config_logi(35) = .FALSE.
                    ELSE
                        CALL WRITE_ERROR('hdf5 command must specify "Kramers" or "Non-Kramers"',&
                                         'Check input file')
                    END IF

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading hdf5 command',&
                                         'Check input file')
                    END IF
                ELSE
                ! Write error message
                    CALL WRITE_ERROR('Error in hdf5 specification',&
                                     'Check input file')
                END IF

            ! Allocate array for transitions to be deleted - if none are selected then this is true for all entries
            ALLOCATE(transitions_logical(config_int(16), config_int(16)))
            transitions_logical = .TRUE.

            END IF

        ! DC Field strength
            IF (GET_TRIMMED_VAL(line, 1, 5) == 'field') THEN
            ! Check correct number of inputs are present
                IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 3) THEN
                    READ(line, *, IOSTAT = reason) DUMMY, config_array(1,1:3)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading field',&
                                         'Check input file')
                    END IF

                    user_field_set = .TRUE.

                ELSE
                ! Write error message
                    CALL WRITE_ERROR('Error in DC Field specification',&
                                     'Check input file')
                END IF
            END IF


        ! Number of modes
            IF (GET_TRIMMED_VAL(line, 1, 5) == 'modes') THEN
            ! Check correct number of inputs are present
                IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 1) THEN
                    READ(line,*, IOSTAT = reason) DUMMY, config_int(1)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading modes',&
                                         'Check input file')
                    END IF

                    ! Allocate boolean array for modes - T/F indicate if they
                    ! are used in dynamics calculations - this is changed
                    ! if remove_mode is present
                    ALLOCATE(modes_logical(config_int(1)))
                    modes_logical = .TRUE.
                ELSE
                ! Write error message
                    CALL WRITE_ERROR('Error in mode specification',&
                                     'Check input file')
                END IF
            END IF

        ! temp and steps
            IF (GET_TRIMMED_VAL(line, 1, 4) == 'temp') THEN

            ! Check correct number of inputs are present
                IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 3) THEN
                    READ(line,*, IOSTAT = reason) DUMMY, config_int(2), config_int(3), config_int(4)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading temp',&
                                         'Check input file')
                    END IF

                ELSE
                ! Write error message
                    CALL WRITE_ERROR('Error in temperature specification',&
                                     'Check input file')
                END IF

            ! Check step is non zero
                IF (config_int(4) == 0) THEN
                    CALL WRITE_ERROR('Invalid t_step value',&
                                     'Check input file')
                END IF

            ! Make step negative
                config_int(4) = -abs(config_int(4))

            ! If t_max is smaller than t_min swap them
                IF (config_int(2) < config_int(3)) THEN
                    IDUMMY1 = config_int(3)
                    config_int(3) = config_int(2)
                    config_int(2) = IDUMMY1
                END IF

            ! If t_max or t_min are very large or very small then error out
                IF (config_int(2) == 0 .or. config_int(2) > 300) THEN
                    CALL WRITE_ERROR('Invalid t_max value',&
                                     'Check input file')

                ELSE IF (config_int(3) == 0 .or. config_int(3) > 300) THEN
                    CALL WRITE_ERROR('Invalid t_min value',&
                                     'Check input file')
                END IF

            ! Check temperature step is able to connect t_max (config_int(2)) and t_min (config_int(3))
            ! If it is not then increase t_max (config_int(2)) until it does
                IF (config_int(2) /= config_int(3) .AND. MODULO(config_int(2) - config_int(3), abs(config_int(4))) /= 0) THEN
                    DO WHILE (MODULO(config_int(2) - config_int(3),abs(config_int(4))) /= 0)
                        config_int(2) = config_int(2) + 1
                    END DO

                    WRITE(6,'(A)')        '!!!!       Maximum temperature Adjusted to comply with step size        !!!!'
                    WRITE(6,'(A, F6.2, A)') '!!!!                          t_max now = ', REAL(config_int(2),WP),'                           !!!!'
                END IF

            ! Create array of temp
            ! From high to low T
                IF (config_int(2) /= config_int(3)) THEN

                    config_int(5) = 1 + (config_int(2) - config_int(3)) / abs(config_int(4))

                    ALLOCATE(temp(config_int(5)))
                    temp = 0.0_WP
                    temp(1) = REAL(config_int(2), WP)

                    DO i = 2, config_int(5)
                        temp(i) = REAL(temp(i - 1) - abs(config_int(4)), WP)
                    END DO
            ! Single temperature mode
                ELSE
                    config_int(5) = 1
                    ALLOCATE(temp(1))
                    temp(1) = REAL(config_int(2), WP)
                END IF

            END IF

        ! Direct Relaxation
            IF (INDEX(TRIM(line),'dynamics direct') /= 0 .OR. &
                INDEX(TRIM(line),'dynamics orbach') /= 0) THEN
                config_logi(1) = .TRUE.
            END IF

        ! Raman Relaxation
            IF (INDEX(TRIM(line),'dynamics raman') /= 0 ) THEN
                config_logi(2) = .TRUE.
            END IF

        ! Linear CFPs
            IF (INDEX(TRIM(line),'cfps linear') /= 0 .OR. &
                INDEX(TRIM(line),'cfps static') /= 0) THEN
                config_logi(3) = .TRUE.
            END IF

        ! Polynomial CFPs
            IF (INDEX(TRIM(line),'cfps polynomial') /= 0) THEN
                config_logi(4) = .TRUE.
            END IF

        ! Boltzmann Statistics
            IF (INDEX(TRIM(line),'statistics boltzmann') /= 0) THEN
                config_logi(5) = .TRUE.
            END IF

        ! Bose-Einstein Statistics
            IF (INDEX(TRIM(line),'statistics bose') /= 0) THEN
                config_logi(6) = .TRUE.
            END IF

        !Gaussian linewidth
            IF (INDEX(TRIM(line),'lineshape gaussian') /= 0) THEN
                config_char(4) = 'gaussian'
            END IF

        !Lorentzian linewidth
            IF (INDEX(TRIM(line),'lineshape lorentzian') /= 0) THEN
                config_char(4) = 'lorentzian'
            END IF

        !Antilorentzian linewidth
            IF (INDEX(TRIM(line),'lineshape antilorentzian') /= 0) THEN
                config_char(4) = 'antilorentzian'
            END IF

        ! Harmonics
            IF (INDEX(TRIM(line),'harmonics') /= 0) THEN
                config_logi(8) = .TRUE.
            END IF

        ! Remove modes
            IF (INDEX(TRIM(line),'remove mode') /= 0) THEN
                config_logi(9) = .TRUE.
                line = line(13:)

            ! Replace spaces and commas from line to give numbers and ranges
            ! e.g. 10  12-15,  30-35 --> 10 12-15 30-35
                line = replace_space_comma(line)

            ! Read numbers from line and set modes logical True or False to match
                num_entries = count_char(TRIM(ADJUSTL(line)), ' ') + 1
                ALLOCATE(CDummy_array(num_entries))
                READ(line, *, IOSTAT = reason) CDummy_array

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading remove mode',&
                                         'Check input file')
                    END IF

                DO i = 1, num_entries

                ! If a dash is present in the ith element, then remove it and read two numbers
                    IF (count_char(TRIM(ADJUSTL(CDummy_array(i))), '-') == 1) THEN

                        CDummy_array(i) = replace_text(TRIM(ADJUSTL(CDummy_array(i))), '-', ' ', count_char(TRIM(ADJUSTL(line)), '-'))
                        READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1, IDUMMY2

                    ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL WRITE_ERROR('Error reading remove mode',&
                                             'Check input file')
                        END IF

                    ! Check for errors
                        IF (IDUMMY1 > config_int(1)) THEN
                            WRITE(error_message,'(A, I4, A)') 'Invalid mode index ', IDUMMY1,' given in remove modes'
                            CALL WRITE_ERROR(error_message, 'Check input file')
                        ELSE IF (IDUMMY2 > config_int(1)) THEN
                            WRITE(error_message,'(A, I4, A)') 'Invalid mode index ', IDUMMY1,' given in remove modes'
                            CALL WRITE_ERROR(error_message, 'Check input file')
                        END IF

                    ! Set logicals
                        modes_logical(IDUMMY1:IDUMMY2) = .FALSE.

                ! Else read a single number
                    ELSE
                        READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1
                        ! Check for errors
                        IF (IDUMMY1 > config_int(1)) THEN
                            WRITE(error_message,'(A, I4, A)') 'Invalid mode index ', IDUMMY1,' given in remove modes'
                            CALL WRITE_ERROR(error_message, 'Check input file')
                        END IF

                    ! Set logical
                        modes_logical(IDUMMY1) = .FALSE.
                    END IF
                ! Count how many modes have been removed
                config_int(7) = config_int(1) - COUNT(modes_logical)
                END DO
                DEALLOCATE(CDummy_array)

            END IF

        ! Single mode
            IF (INDEX(TRIM(line),'single mode') /= 0) THEN

                config_logi(10) = .TRUE.

                ! Temporary logical array for single modes
                ALLOCATE(single_logical(config_int(1)))
                single_logical = .FALSE.

                line = line(13:)

            ! Replace spaces and commas from line to give numbers and ranges
            ! e.g. 10  12-15,  30-35 --> 10 12-15 30-35
                line = replace_space_comma(line)

            ! Read numbers from line and set modes logical True or False to match
                num_entries = count_char(TRIM(ADJUSTL(line)), ' ') + 1
                ALLOCATE(CDummy_array(num_entries))
                READ(line, *, IOSTAT = reason) CDummy_array

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading single mode',&
                                         'Check input file')
                    END IF

                DO i = 1, num_entries

                ! If a dash is present in the ith element, then remove it and read two numbers
                    IF (count_char(TRIM(ADJUSTL(CDummy_array(i))), '-') == 1) THEN

                        CDummy_array(i) = replace_text(TRIM(ADJUSTL(CDummy_array(i))), '-', ' ', count_char(TRIM(ADJUSTL(line)), '-'))
                        READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1, IDUMMY2

                    ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL WRITE_ERROR('Error reading single mode',&
                                             'Check input file')
                        END IF

                    ! Check for errors
                        IF (IDUMMY1 > config_int(1)) THEN
                            WRITE(error_message,'(A, I3, A)') 'Invalid mode index ', IDUMMY1,' given in single mode'
                            CALL WRITE_ERROR(error_message, 'Check input file')

                        ELSE IF (IDUMMY2 > config_int(1)) THEN
                            WRITE(error_message,'(A, I3, A)') 'Invalid mode index ', IDUMMY1,' given in single mode'
                            CALL WRITE_ERROR(error_message, 'Check input file')
                        END IF

                    ! Set logicals
                        single_logical(IDUMMY1:IDUMMY2) = .TRUE.

                ! Else read a single number
                    ELSE
                        READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1
                        ! Check for errors
                        IF (IDUMMY1 > config_int(1)) THEN
                            WRITE(error_message,'(A, I3, A)') 'Invalid mode index ', IDUMMY1,' given in single mode'
                            CALL WRITE_ERROR(error_message, 'Check input file')
                        END IF
                    ! Set logical
                        single_logical(IDUMMY1) = .TRUE.

                    END IF
                END DO
                DEALLOCATE(CDummy_array)

            ! Count how many modes will be calculated
                config_int(8) = COUNT(single_logical)

            ! Change logical array into array of mode numbers - these will have their dynamics calculated
                ALLOCATE(single_modes(config_int(8)))
                single_modes = 0
                j = 1
                DO i = 1, config_int(1)
                    IF (single_logical(i)) THEN
                        single_modes(j) = i
                        j = j + 1
                    END IF
                END DO

            END IF

        ! Read user specified transitions
        ! Vibrational contributions will be
        ! calculated for these transitions
            IF (INDEX(TRIM(line),'print transition') /= 0) THEN
                config_logi(11) = .TRUE.

                num_transitions = 0
                line = line(17:)

            ! Replace spaces and commas from line to give numbers and ranges
            ! e.g. 10  12-15,  30-35 --> 10 12-15 30-35
                line = replace_space_comma(line)

            ! Read numbers from line and add to array
                num_entries = count_char(TRIM(ADJUSTL(line)), ' ') + 1
                ALLOCATE(CDummy_array(num_entries))
                READ(line, *, IOSTAT = reason) CDummy_array

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading transitions',&
                                         'Check input file')
                    END IF

                DO i = 1, num_entries
                    IF (count_char(TRIM(ADJUSTL(CDummy_array(i))), '-') == 1) THEN
                        num_transitions = num_transitions + 1
                    END IF
                END DO
                    ALLOCATE(user_transitions(num_transitions,2))
                    user_transitions = 0

                k = 1
                DO i = 1, num_entries

                ! If a dash is present in the ith element, then remove it
                ! and read two numbers which define the requested transitions
                    IF (count_char(TRIM(ADJUSTL(CDummy_array(i))), '-') == 1) THEN
                        CDummy_array(i) = replace_text(TRIM(ADJUSTL(CDummy_array(i))), '-', ' ', count_char(TRIM(ADJUSTL(line)), '-'))
                        READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1, IDUMMY2

                    ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL WRITE_ERROR('Error reading transitions',&
                                             'Check input file')
                        END IF

                    ! Check for silly input - if OK then advance
                    ! THIS DOESNT CHECK THAT THE INPUTS ARE LESS THAN THE NUMBER OF STATES!
                        IF (IDUMMY1 < 1 .OR. IDUMMY2 < 1 ) THEN
                            OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
                                WRITE(66,'(A, I0, I0)') 'INPUT FILE ERROR'
                                WRITE(66,'(A, I0, I0)') 'Transitions - invalid state number(s) -- ', IDUMMY1, IDUMMY2
                                WRITE(66,'(A, I0, I0)') 'These transitions will not be calculated'
                            CLOSE(66)
                            CYCLE
                        ELSE
                            user_transitions(k,1) = IDUMMY1
                            user_transitions(k,2) = IDUMMY2
                            k = k + 1
                        END IF

                    END IF

                END DO
                DEALLOCATE(CDummy_array)
            END IF

        ! Save gamma matrices
            IF (INDEX(TRIM(line),'save gamma') /= 0) THEN
                config_logi(12) = .TRUE.
            END IF

        ! Calculate time evolution of population
            IF (INDEX(TRIM(line),'population') /= 0) THEN
                config_logi(15) = .TRUE.
            ! Read number of timesteps
                READ(line, *, IOSTAT = reason) DUMMY, config_int(14), config_real(6)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading population',&
                                         'Check input file')
                    END IF
            END IF

        ! Skip diagonalisation of gamma
            IF (INDEX(TRIM(line),'rates off') /= 0) THEN
                config_logi(13) = .TRUE.
            END IF

        ! Calibrate IR energies
            IF (INDEX(TRIM(line),'calibration') /= 0) THEN
                config_logi(14) = .TRUE.

            ! Read slope and intercept
                READ(line, *, IOSTAT = reason) DUMMY, config_real(2), config_real(3)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading calibration',&
                                         'Check input file')
                    END IF

            END IF

            ! Read fwhm lunghi
            IF (INDEX(TRIM(line),'fwhm lunghi') /= 0) THEN
                config_logi(7) = .TRUE.
                ! set fwhm value to 0 as placeholder
                config_real(4) = 0.0_WP
            END IF


            ! Read fwhm from file
            IF (INDEX(TRIM(line),'fwhm read') /= 0) THEN
                ! Check correct number of inputs are present
                IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 1) THEN
                    config_logi(32) = .TRUE.
                    ! set fwhm value to 0 as placeholder
                    config_real(4) = 0.0_WP
                ELSE
                    ! Write error message
                    CALL WRITE_ERROR('Error in FWHM specification',&
                                     'Check input file')
                END IF
            END IF

            ! Read fwhm value
            IF (.NOT. config_logi(7) .AND. .NOT. config_logi(32) .AND. INDEX(TRIM(line),'fwhm') /= 0 .AND. INDEX(TRIM(line), 'raman') == 0) THEN
                ! Check correct number of inputs are present
                IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 1) THEN
                    READ(line,*, IOSTAT = reason) DUMMY, config_real(4)
                    ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error in FWHM specification',&
                                         'Check input file')
                    END IF

                ELSE
                    ! Write error message
                    CALL WRITE_ERROR('Error in FWHM specification',&
                                     'Check input file')
                END IF
            END IF

            ! Plot format selection
            IF (INDEX(TRIM(line),'barrier format') /= 0) THEN
                line = line(15:)
            ! Read plot format selection
                IF (TRIM(ADJUSTL(line)) == 'svg' .OR. TRIM(ADJUSTL(line)) == 'png') THEN
                    config_char(2) = TRIM(ADJUSTL(line))
                ELSE
                    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
                        WRITE(66,'(A)')  ''
                        WRITE(66,'(A)') '--------------------------------------------------------------------------------'
                        WRITE(66,'(A)')  '                       Unsupported barrier format specified'
                        WRITE(66,'(A)')  '                                Defaulting to svg'
                        WRITE(66,'(A)') '--------------------------------------------------------------------------------'
                        WRITE(66,'(A)')  ''
                    CLOSE(66)
                END IF
            END IF

            ! Barrier figure temp
            IF (INDEX(TRIM(line),'print barrier') /= 0) THEN

                ! set logical option
                config_logi(16) = .TRUE.

                ! Skip over barrier string
                line = TRIM(ADJUSTL(line(15:)))

                ! Read temp for barrier figures

                ! Remove spaces and commas from line to give numbers and ranges
                line = replace_space_comma(TRIM(ADJUSTL(line)))

                ! Count the number of entries given by the user e.g. 45-50 is counted as single entry
                ! Then read these entries into separate elements of a character array
                num_entries = count_char(TRIM(ADJUSTL(line)), ' ') + 1
                ALLOCATE(CDummy_array(num_entries))
                READ(line, *, IOSTAT = reason) CDummy_array

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading barriers',&
                                         'Check input file')
                    END IF


                ! Run through arrays and sum count the number of temp being requested
                config_int(9) = 0 ! number of user barrier temp
                DO i = 1, num_entries
                    IF (count_char(TRIM(ADJUSTL(CDummy_array(i))), '-') == 1) THEN
                        CDummy_array(i) = replace_text(TRIM(ADJUSTL(CDummy_array(i))), '-', ' ', &
                                                       count_char(TRIM(ADJUSTL(line)), '-'))
                        READ(CDummy_array(i), *, IOSTAT = reason) RDUMMY1, RDUMMY2

                    ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL WRITE_ERROR('Error reading barriers',&
                                             'Check input file')
                        END IF

                        config_int(9) = config_int(9) + ABS(INT(RDUMMY1) - INT(RDUMMY2))+1
                    ELSE
                        config_int(9) = config_int(9) + 1
                    END IF
                END DO

                ! Record the number of barrier temp
                config_int(9) = config_int(9)

                ! Allocate the array containing the barrier temp
                ALLOCATE(user_barrier_temps(config_int(9),2))

                ! Create array containing the temp at which the barrier will be plotted
                k = 0
                DO i = 1, num_entries

                ! If a space is present in the ith element, then remove it, read two numbers and add
                ! the range of values to the list
                    IF (count_char(TRIM(ADJUSTL(CDummy_array(i))), ' ') == 1) THEN
                        READ(CDummy_array(i), *, IOSTAT = reason) RDUMMY1, RDUMMY2
                    ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL WRITE_ERROR('Error reading barriers',&
                                             'Check input file')
                        END IF

                        k = k + 1
                        user_barrier_temps(k,1) = INT(RDUMMY1)
                        DO j = 1, ABS(INT(RDUMMY1) - INT(RDUMMY2))
                            k = k + 1
                            user_barrier_temps(k,1) = INT(RDUMMY1) + j
                        END DO

                ! Else read a single number and set logical
                    ELSE
                        k = k + 1
                        READ(CDummy_array(i), *, IOSTAT = reason) RDUMMY1

                    ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL WRITE_ERROR('Error reading barriers',&
                                             'Check input file')
                        END IF

                        user_barrier_temps(k,1) = INT(RDUMMY1)
                    END IF
                END DO
                DEALLOCATE(CDummy_array)

            ! Check that barrier temp match temps requested by user
            ! loop through each barrier temp and see if it is in temp array

            ! If removal(i) == 1 then that temperature will be removed from the list
                ALLOCATE(removal(config_int(9)))
                removal = .FALSE.

                ! loop over the number of requested barrier temp
                DO i = 1, config_int(9)

                    found = .FALSE.

                    ! loop over all temperautres
                    DO j = 1, config_int(5)

                        ! If temperature is found, mark as found and then add index to 2nd column
                        IF (user_barrier_temps(i,1) == temp(j)) THEN

                            found = .TRUE.
                            user_barrier_temps(i,2) = j

                            END IF

                        END DO

                    ! If temperature is not found in temp array then mark for removal and inform user
                        IF (.NOT. found) THEN
                            OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
                                WRITE(66,'(A)') 'INPUT FILE ERROR'
                                WRITE(66,'(A, I0, A)') 'Barrier - invalid temperature -- ', user_barrier_temps(i,1), ' K'
                                WRITE(66,'(A)') 'This barrier will not be calculated'
                                WRITE(66,'(A)') ''
4                                removal(i) = .TRUE.
                            CLOSE(66)
                    END IF

                END DO

            ! Now loop over removal and remove modes
            ! Calculate how many temp are being removed
                num_temp_remove = COUNT(removal)
            ! Allocate dummy array for barrier temp and copy in temp
                ALLOCATE(dummy_barrier_temps(config_int(9),2))
                dummy_barrier_temps = user_barrier_temps
            ! Deallocate and reallocate smaller user_barrier_temps array
                DEALLOCATE(user_barrier_temps)
                ALLOCATE(user_barrier_temps(config_int(9) - num_temp_remove, 2))
                user_barrier_temps = 0

            ! loop over temp and copy in non-deleted temp to user_barrier_temps
                k = 1
                DO j = 1, config_int(9)
                    IF (.NOT. removal(j)) THEN
                        user_barrier_temps(k,1) = dummy_barrier_temps(j,1)
                        user_barrier_temps(k,2) = dummy_barrier_temps(j,2)
                        k = k + 1
                    END IF
                END DO

            ! Change number of barrier temp to include non-deleted temp only
                config_int(9) = config_int(9) - num_temp_remove
            END IF

            !Remove transitions
            IF ((line(1:17) == 'remove transition') .OR. line(1:18) == 'remove transitions') THEN
                config_logi(17) = .TRUE.

                line = line(19:)

            !Replace spaces and commas from line to give numbers and ranges
            !e.g. 10  12-15,  30-35 --> 10 12-15 30-35
                line = replace_space_comma(line)

            !Read numbers from line and set modes logical True or False to match
                num_entries = count_char(TRIM(ADJUSTL(line)), ' ') + 1
                ALLOCATE(CDummy_array(num_entries))
                READ(line, *, IOSTAT = reason) CDummy_array

                !Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL WRITE_ERROR('Error reading remove transition',&
                                         'Check input file')
                    END IF

                DO i = 1, num_entries

                !If a dash is present in the ith element, then remove it and read two numbers
                    IF (count_char(TRIM(ADJUSTL(CDummy_array(i))), '-') == 1) THEN

                        CDummy_array(i) = replace_text(TRIM(ADJUSTL(CDummy_array(i))), '-', ' ', count_char(TRIM(ADJUSTL(line)), '-'))
                        READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1, IDUMMY2

                    !Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL WRITE_ERROR('Error reading remove transition',&
                                             'Check input file')
                        END IF

                    !Check for errors
                        IF (IDUMMY1 > config_int(1)) THEN
                            WRITE(error_message,'(A, I0, A)') 'Invalid transition index ', IDUMMY1,' in remove transition'
                            CALL WRITE_ERROR(error_message, 'Check input file')
                        ELSE IF (IDUMMY2 > config_int(1)) THEN
                            WRITE(error_message,'(A, I0, A)') 'Invalid transition index ', IDUMMY2,' in remove transition'
                            CALL WRITE_ERROR(error_message, 'Check input file')
                        END IF

                    !Set logicals
                        transitions_logical(IDUMMY1,IDUMMY2) = .FALSE.
                        transitions_logical(IDUMMY2,IDUMMY1) = .FALSE.

                !Else read a single number
                    ELSE
                        READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1
                !Check file is not corrupted or line is too short
                        IF (reason > 0)  THEN
                            CALL WRITE_ERROR('Error reading remove transition',&
                                             'Check input file')
                        END IF

                    !Check for errors
                        IF (IDUMMY1 > config_int(1)) THEN
                            CALL WRITE_ERROR('Error reading remove transition',&
                                             'Check input file')
                        END IF

                    !Set logical
                        transitions_logical(IDUMMY1,:) = .FALSE.
                        transitions_logical(:,IDUMMY1) = .FALSE.
                    END IF
                END DO
                DEALLOCATE(CDummy_array)

            END IF
        ! Print eq eigenvectors
            IF (INDEX(TRIM(line),'print eq eigenvectors') /= 0 .OR. INDEX(TRIM(line),'print equilibrium eigenvectors') /= 0) THEN
                config_logi(18) = .TRUE.
            END IF

        ! Replacement CF Energies from caspt2
            IF (INDEX(TRIM(line),'replace cf energies') /= 0) THEN
                config_logi(19) = .TRUE.
            END IF

        ! Strength parameters
            IF (INDEX(TRIM(line),'calculate strength') /= 0) THEN
                config_logi(20) = .TRUE.
            END IF

            ! Remove transitions
            IF (INDEX(TRIM(line),'transition knockout') /= 0) THEN
                config_logi(21) = .TRUE.
            END IF

            ! Read in user defined gamma matrices
            IF (INDEX(TRIM(line),'read gamma') /= 0) THEN
                config_logi(22) = .TRUE.
                config_logi(12) = .TRUE. ! also enable gamma saving
                line = line((INDEX(TRIM(line),'read gamma') + 10):)
                ! Read header for name of new gamma file
                    IF (LEN_TRIM(line) /= 0) THEN
                        READ(line, '(A)') config_char(3)
                        config_char(3) = ADJUSTL(TRIM(config_char(3)))
                    END IF
            END IF

            ! Use proper taylor series for Bkq in perturbing Hamiltonian
            IF (INDEX(TRIM(line),'cfps taylor') /= 0) THEN
                config_logi(23) = .TRUE.
            END IF

            ! Save cf strength information
            IF (INDEX(TRIM(line),'save strength') /= 0) THEN
                config_logi(24) = .TRUE.
            END IF

            ! Use Mathematica for diagonalisation of CF Hamiltonian and Gamma matrix
            IF (INDEX(TRIM(line),'use mathematica') /= 0) THEN
                config_logi(25) = .TRUE.
            END IF

            ! Print mode weighted gamma, sp, dos, occ, and effective_n_modes
            IF (INDEX(TRIM(line), 'mode weighted') /= 0) THEN
                config_logi(26) = .TRUE.
                config_logi(12) = .TRUE. !also activate same gamma
            END IF

            ! Scale zpd of certain modes
            ! Optionally reads a cutoff for this treatment
            IF (INDEX(TRIM(line), 'scale zpd') /= 0) THEN
                config_logi(27) = .TRUE.
                line = line(INDEX(TRIM(line), 'scale zpd')+9:)
                IF (LEN_TRIM(line) > 0) THEN
                    READ(line, *) config_int(10)
                END IF

            END IF

            ! Number of steps in raman trapezoidal integration
            IF (INDEX(TRIM(line), 'trapz steps') /= 0) THEN
                line = line(INDEX(TRIM(line), 'trapz steps')+11:)
                IF (LEN_TRIM(line) > 0) THEN
                    READ(line, *) config_int(11)
                END IF
            END IF

            ! Specify highest state to use in raman calculation
            IF (INDEX(TRIM(line), 'raman max state') /= 0) THEN
                config_logi(28) = .TRUE.
                line = line(INDEX(TRIM(line), 'raman max state')+15:)
                IF (LEN_TRIM(line) > 0) THEN
                    READ(line, *) config_int(12)
                END IF

            END IF

            ! Scaling factor for raman fwhm
            IF (INDEX(TRIM(line), 'raman fwhm scale') /= 0) THEN
                line = line(INDEX(TRIM(line), 'raman fwhm scale')+16:)
                IF (LEN_TRIM(line) > 0) THEN
                    READ(line, *) config_int(13)
                END IF
            END IF

            ! Number of vibrational modes to use in raman part
            IF (INDEX(TRIM(line), 'dos scale') /= 0) THEN
                line = line(INDEX(TRIM(line), 'dos scale')+9:)
                IF (LEN_TRIM(line) > 0) THEN
                    READ(line, *) config_real(5)
                END IF

            END IF

            ! Read in old zpd and convert to proper value
            IF (INDEX(TRIM(line), 'zpd fix') /= 0) THEN
                config_logi(29) = .TRUE.
            END IF

            ! Read per-mode weights from file to normalise DOS.
            IF (INDEX(TRIM(line),'use mode weights') /= 0 &
                .OR. INDEX(TRIM(line),'use phonon weights')/= 0) THEN
                config_logi(30) = .TRUE.
            END IF
            
            ! Allow to skip sum process
            IF (INDEX(TRIM(line),'raman skip sum process') /= 0) THEN
                config_logi(31) = .TRUE.
            END IF
            
            ! Print out 2D-mode-dependent Raman data for plotting
            IF (INDEX(TRIM(line),'raman decomposition') /= 0) THEN
                config_logi(33) = .TRUE.
            END IF

        END IF
    END DO

! Check input options for errors
    CALL OPTIONS_CHECK(config_logi, config_int, config_char, config_real, two_J)

! Close input file
CLOSE(33)

! Close output file
CLOSE(66)

END SUBROUTINE READ_INPUT_FILE

SUBROUTINE OPTIONS_CHECK(config_logi, config_int, config_char, config_real, two_J)
    ! Checks if any user selected options conflict with each other
    USE global, only : files
    IMPLICIT NONE
    LOGICAL            , INTENT(IN) :: config_logi(:)
    INTEGER            , INTENT(IN) :: config_int(:), two_J
    CHARACTER(LEN = *), INTENT(IN)  :: config_char(:)
    REAL(KIND = WP)    , INTENT(IN) :: config_real(:)

    ! Neither Direct nor Raman
    IF (.NOT. config_logi(1) .AND. .NOT. config_logi(2)) THEN
        CALL WRITE_ERROR('No relaxation process selected',&
                         'Check input file')
    END IF

    ! Raman with non-kramers ion
    ! Check if j even
    ! IF((two_J/2)*2 == two_J .AND. config_logi(2)) THEN
    !     write(6,*) MODULO(REAL(two_J,WP)/2.0_WP, 2.0_WP)
    !     CALL WRITE_ERROR('Raman transitions do not yet support non-Kramers ions',&
    !                              'Check input file')
    ! END IF

    ! Raman max states with no raman
    IF(config_logi(28) .AND. .NOT. config_logi(2)) THEN
        OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
            WRITE(66,*)
            CALL WARNING_HEADER('raman max state given but not dynamics raman',66)
            WRITE(66,*)
        CLOSE(66)
    END IF

    ! Raman fwhm with no raman
    IF(config_int(13) /= 3 .AND. .NOT. config_logi(2)) THEN
        OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
            WRITE(66,*)
            CALL WARNING_HEADER('raman fwhm scale given but not dynamics raman',66)
            WRITE(66,*)
        CLOSE(66)
    END IF
    
    ! Raman fwhm greater than 3
    IF(config_int(13) > 3) THEN
        CALL WRITE_ERROR('raman fwhm scale only valid for 1 to 3',&
                         'Check input file')
    END IF
    
    ! Raman trapz steps minimum limits
    IF (INDEX(TRIM(config_char(4)),'gaussian') /= 0) THEN
        IF(config_int(11) < 10) CALL WRITE_ERROR(&
                  'Integration steps are too large. Trapz steps >= 10 with the gaussian lineshape.','Check input file')
    ELSE IF (INDEX(TRIM(config_char(4)),'lorentzian') /= 0 .OR. INDEX(TRIM(config_char(4)),'antilorentzian') /= 0) THEN
        IF(config_int(13) == 1 .AND. config_int(11) < 10) CALL WRITE_ERROR(&
                  'Integration steps are too large. Trapz steps >= 10 for Raman FWHM scale = 1 with the (anti)lorentzian lineshape.','Check input file')
        IF(config_int(13) == 2 .AND. config_int(11) < 20) CALL WRITE_ERROR(&
                  'Integration steps are too large. Trapz steps >= 20 for Raman FWHM scale = 2 with the (anti)lorentzian lineshape.','Check input file')
        IF(config_int(13) == 3 .AND. config_int(11) < 10) CALL WRITE_ERROR(&
                  'Integration steps are too large. Trapz steps >= 80 for Raman FWHM scale = 3 with the (anti)lorentzian lineshape.','Check input file')
        IF(config_int(13) == 4 .AND. config_int(11) < 10) CALL WRITE_ERROR(&
                  'Integration steps are too large. Trapz steps >= 3500 for Raman FWHM scale = 4 with the (anti)lorentzian lineshape.','Check input file')
    END IF

    ! Single mode and remove mode
    IF (config_logi(10) .AND. config_logi(9)) THEN
        CALL WRITE_ERROR('Both single mode and remove mode selected',&
                         'Check input file')
    END IF

    ! Single mode and Raman
    IF (config_logi(10) .AND. config_logi(2)) THEN
        CALL WRITE_ERROR('Both single mode and Raman selected',&
                         'Check input file')
    END IF

    ! Weighted single mode and Raman
    IF (config_logi(26) .AND. config_logi(2)) THEN
        CALL WRITE_ERROR('Both weighted mode and Raman selected',&
                         'Check input file')
    END IF

    ! Weighted single mode and read gamma
    IF (config_logi(26) .AND. config_logi(22)) THEN
        CALL WRITE_ERROR('Both weighted mode and read gamma selected',&
                         'Check input file')
    END IF

    ! Weighted single mode and read gamma
    IF (config_char(4)=="") THEN
        CALL WRITE_ERROR('Incorrect/missing lineshape definition',&
                         'Check input file')
    END IF

    ! Both Bose-Einstein and Boltzmann
    IF (config_logi(5) .AND. config_logi(6)) THEN
        CALL WRITE_ERROR('Both Bose-Einstein and Boltzmann statistics selected',&
                         'Check input file')
    END IF


    ! Neither Bose-Einstein nor Boltzmann
    IF (.NOT. config_logi(5) .AND. .NOT. config_logi(6)) THEN
        CALL WRITE_ERROR('Neither Bose-Einstein nor Boltzmann statistics selected',&
                         'Check input file')
    END IF


    ! Both Static and Polynomial CFPs
    IF (config_logi(3) .AND. config_logi(4)) THEN
        CALL WRITE_ERROR('Both static and polynomial CFPs selected',&
                         'Check input file')
    END IF

    ! Both Static and Taylor CFPs
    IF (config_logi(3) .AND. config_logi(23)) THEN
        CALL WRITE_ERROR('Both static and polynomial CFPs selected',&
                         'Check input file')
    END IF

    ! Both Taylor and Polynomial CFPs
    IF (config_logi(23) .AND. config_logi(4)) THEN
        CALL WRITE_ERROR('Both Taylor and polynomial CFPs selected',&
                         'Check input file')
    END IF

    ! Neither Static nor Polynomial nor Taylor CFPs
    IF (.NOT. config_logi(3) .AND. .NOT. config_logi(4) .AND. .NOT. config_logi(23) ) THEN
        CALL WRITE_ERROR('Neither static nor polynomial nor Taylor CFPs selected',&
                         'Check input file')
    END IF

    ! FWHM and lunghi
    IF (config_real(4) /= 0.0_WP .AND. (INDEX(TRIM(config_char(4)), 'lunghi') /= 0)) THEN
        CALL WRITE_WARNING('Lunghi linewidth requested but FWHM given',&
                         'FWHM will be ignored')
    END IF

    ! Remove mode but no mode given
    IF (config_logi(9) .AND. config_int(7) == 0) THEN
        CALL WRITE_ERROR('Remove mode indicated but no modes given',&
                         'Check input file')
    END IF

    ! Single mode but no mode given
    IF (config_logi(10) .AND. config_int(8) == 0) THEN
        CALL WRITE_ERROR('Single mode indicated but no mode given',&
                         'Check input file')
    END IF

    ! No ion given and not using HDF5 input
    IF (ADJUSTL(TRIM(config_char(1))) == 'EMPTY' .AND. .NOT. config_logi(34)) THEN
        CALL WRITE_ERROR('No ion given',&
                         'Check input file')
    END IF

    ! User transition and read gamma
    IF (config_logi(11) .AND. config_logi(22)) THEN
        CALL WRITE_ERROR('Cannot have print transition and read gamma',&
                         'Check input file')
    END IF

    ! Number of population timesteps
    IF (config_logi(15) .AND. config_int(14) == 0) THEN
        CALL WRITE_ERROR('Number of population timesteps has not been specified',&
                         'Check input file')
    END IF

! Mathematica and CSF
#ifdef csf
    IF (config_logi(25)) THEN
        CALL WRITE_ERROR('Mathematica is not supported on the csf',&
                         'Delete use mathematica')
    END IF
#endif

END SUBROUTINE OPTIONS_CHECK

SUBROUTINE PRINT_DATE_AND_TIME(FLAG)
    ! Print date and time using intrinsic PURE SUBROUTINE
    ! Three different flags for different uses
    ! Start, Finish
    USE global, only : files
    IMPLICIT NONE
    CHARACTER(LEN = 10)           :: DATE, TIME
    CHARACTER(LEN = *), INTENT(IN) :: FLAG

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Get date and time
    CALL DATE_AND_TIME(DATE = DATE,TIME = TIME)

! Write start or finish message to file
! if finish then stop program
    IF (TRIM(FLAG) == 'STRT') THEN
        WRITE(66,'(13A)')'-----------           Tau begins at ',TIME(1:2),':',&
                          TIME(3:4),':',TIME(5:6),' on ',DATE(7:8),'-',&
                          DATE(5:6),'-',DATE(1:4),'           -----------'
        WRITE(66,'(A)') ''
    ELSE IF (TRIM(FLAG) == 'FNSH') THEN
        WRITE(66,'(A)') ''
        WRITE(66,'(13A)')'-----------       Tau ends normally at ',TIME(1:2),':',&
                          TIME(3:4),':',TIME(5:6),' on ',DATE(7:8),'-',&
                          DATE(5:6),'-',DATE(1:4),'       -----------'
        CLOSE(66)
        STOP
    END IF

! Close output file
    CLOSE(66)

END SUBROUTINE PRINT_DATE_AND_TIME

SUBROUTINE WRITE_ERROR(message, message_2, RC)
    USE global, only : files
    ! Print date and time followed by error message
    ! then abort
    IMPLICIT NONE
    CHARACTER(LEN = 10)                      :: DATE, TIME
    CHARACTER(LEN = *), INTENT(IN)           :: message
    CHARACTER(LEN = *), INTENT(IN), OPTIONAL :: message_2
    INTEGER, INTENT(IN), OPTIONAL            :: RC

    ! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Get date and time
    CALL DATE_AND_TIME(DATE = DATE,TIME = TIME)

    ! Write error message(s) to output file

    CALL ERROR_HEADER(message, 66)
    IF (PRESENT(message_2)) CALL ERROR_HEADER(message_2, 66)

    ! Write abort time and date to output file
    WRITE(66,'(13A)')'-----------           Tau aborts at ',TIME(1:2),':',&
                      TIME(3:4),':',TIME(5:6),' on ',DATE(5:6),'-',&
                      DATE(7:8),'-',DATE(1:4),'           -----------'

    ! Close output file and stop program
    CLOSE(66)
    IF (PRESENT(RC)) THEN
        STOP 5
    ELSE
        STOP 1
    END IF

END SUBROUTINE WRITE_ERROR

SUBROUTINE WRITE_WARNING(message, message_2, RC)
    USE global, only : files
    IMPLICIT NONE
    CHARACTER(LEN = 10)                      :: DATE, TIME
    CHARACTER(LEN = *), INTENT(IN)           :: message
    CHARACTER(LEN = *), INTENT(IN), OPTIONAL :: message_2
    INTEGER, INTENT(IN), OPTIONAL            :: RC

    ! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Get date and time
    CALL DATE_AND_TIME(DATE = DATE,TIME = TIME)

    ! Write error message(s) to output file

    CALL WARNING_HEADER(message, 66)
    IF (PRESENT(message_2)) CALL WARNING_HEADER(message_2, 66)

    ! Close output file
    CLOSE(66)

END SUBROUTINE WRITE_WARNING

SUBROUTINE SYSTEM_CONFIGS
    USE global, only : files
    USE OMP_LIB
    USE version, only : git_version
    ! Sets up OS dependent configurations
    IMPLICIT NONE
    CHARACTER(LEN = 200)  :: CDUMMY, s
    INTEGER               :: num_threads,reason

    ! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Set and print number of threads
    CALL get_environment_variable("OMP_NUM_THREADS", CDUMMY)
    READ(CDUMMY,*,IOSTAT=reason) num_threads
    IF(reason /= 0 .OR. num_threads == 0) num_threads = 1
    CALL omp_set_num_threads(num_threads)
    IF (num_threads > 1) THEN
        s = 's'
    ELSE
        s = ''
    END IF

    WRITE(CDUMMY,'(A, I0, 2A)') 'Tau is running on ', num_threads, ' thread',TRIM(ADJUSTL(s))
    CALL WARNING_HEADER(CDUMMY, 66)

    ! Print git version number
    WRITE(CDUMMY,'(2A)') 'Version :   ', TRIM(git_version)
    CALL WARNING_HEADER(CDUMMY, 66)

! Set platform
! Preprocessor commands for operating system
#ifdef macos
    IF(debug) CALL WARNING_HEADER('Mathematica command is WolframScript',66)
#else
    IF(debug) CALL WARNING_HEADER('Mathematica command is math',66)
#endif

    ! Print debug message if COMPILED in debug mode
    ! Preprocessor commands for debug mode

#ifdef compilerdebug
    CALL WARNING_HEADER('COMPILED IN DEBUG MODE',66)
    WRITE(66,*)
#else
    WRITE(66,*)
#endif

    ! Print compiler if RUNNING in debug mode
    IF (debug) THEN

! Preprocessor commands for compiler
#ifdef gcc
    CALL WARNING_HEADER('Compiled using gfortran',66)
#endif

#ifdef intel
    CALL WARNING_HEADER('Compiled using ifort',66)
#endif
    ! Close output file
    CLOSE(66)
    END IF

#ifdef csf
    ! Check tau isnt being run on the login node
    CALL EXECUTE_COMMAND_LINE('hostname > '//TRIM(files%path)//'tmp.tmp')
    OPEN(33, FILE=TRIM(files%path)//'tmp.tmp', STATUS='OLD')
        READ(33,*) CDUMMY
    CLOSE(33)
    CALL EXECUTE_COMMAND_LINE('rm -f '//TRIM(files%path)//'tmp.tmp')
    IF (INDEX(TRIM(CDUMMY), 'login') /= 0) THEN
        CALL WRITE_ERROR('Tau will not run on the login node',&
                         'Use tau -subscript or autau instead')
    END IF
#endif


END SUBROUTINE SYSTEM_CONFIGS

SUBROUTINE READ_ZPD_SCALE(n_modes, select_scale, scale_limit, zpd_scale)
    USE global, only : files
    IMPLICIT NONE
    LOGICAL, INTENT(IN)                       :: select_scale
    INTEGER, INTENT(INOUT)                    :: scale_limit
    INTEGER, INTENT(IN)                       :: n_modes
    INTEGER                                   :: row
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE :: zpd_scale(:)

    ALLOCATE(zpd_scale(n_modes))
    zpd_scale = 1.0_WP

    ! Read in user provided scaling values
    IF (select_scale) THEN

        IF (scale_limit == 0) THEN
            scale_limit = n_modes
        END IF

        OPEN(33, FILE = TRIM(files%path)//'zpd_scale.dat', STATUS = 'OLD')

        DO row = 1, scale_limit
            READ(33,*) zpd_scale(row)
        END DO

        CLOSE(33)

    END IF

END SUBROUTINE READ_ZPD_SCALE

SUBROUTINE FIND_FILES(config_logi)
    USE global, only : files, SET_FILE_NAMES
    IMPLICIT NONE
    LOGICAL, INTENT(IN) :: config_logi(:)

    CALL SET_FILE_NAMES(files, config_logi(3))


    ! Open output file and write path
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

        CALL SECTION_HEADER('Files',66)

        WRITE(66,'(A)') 'Tau path:'
        WRITE(66,'(A)') ADJUSTL(TRIM(files%path))
        WRITE(66,'(A)') ''

        ! Check files exist
        CALL CHECK_EXISTS('mode_energies.dat')
        IF(config_logi(34)) THEN
            CALL CHECK_EXISTS('tau.hdf5')
        ELSE
            CALL CHECK_EXISTS('EQ_CFPs.dat')
            IF (.NOT. config_logi(22)) THEN
                IF (.NOT. config_logi(3)) THEN
                    CALL CHECK_EXISTS('CFP_polynomials.dat')
                ELSE
                    CALL CHECK_EXISTS('CFP_steps.dat')
                END IF
            END IF
            IF (config_logi(19)) THEN
                CALL CHECK_EXISTS('new_CF_energies.dat')
            END IF
            IF (config_logi(27)) THEN
                CALL CHECK_EXISTS('zpd_scale.dat')
            END IF
            IF (config_logi(29)) THEN
                CALL CHECK_EXISTS('reduced_masses.dat')
                CALL CHECK_EXISTS('force_constants.dat')
            END IF
            IF (config_logi(30)) THEN
                CALL CHECK_EXISTS('mode_weights.dat')
            END IF
        END IF
        IF (config_logi(32)) THEN
            CALL CHECK_EXISTS('fwhm.dat')
        END IF
        
        ! Write file locations

        WRITE(66,'(A)') 'Tau will use the following files:'
        WRITE(66,'(A)') ADJUSTL(TRIM(files%mode_energies))
        IF(config_logi(34)) THEN
            WRITE(66,'(A)') ADJUSTL(TRIM(files%path)//'tau.hdf5')
        ELSE
            WRITE(66,'(A)') ADJUSTL(TRIM(files%EQ_CFPs))
            IF (.NOT. config_logi(22)) THEN
               WRITE(66,'(A)') ADJUSTL(TRIM(files%SP_CFPs))
            END IF
            IF (config_logi(27)) THEN
                WRITE(66,'(A)') ADJUSTL(TRIM(files%path))//'zpd_scale.dat'
            END IF
            IF (config_logi(19)) THEN
                WRITE(66,'(A)') ADJUSTL(TRIM(files%path))//'new_CF_energies.dat'
            END IF
            IF (config_logi(29)) THEN
                WRITE(66,'(A)') ADJUSTL(TRIM(files%path))//'reduced_masses.dat'
                WRITE(66,'(A)') ADJUSTL(TRIM(files%path))//'force_constants.dat'
            END IF
            IF (config_logi(30)) THEN
                WRITE(66,'(A)') ADJUSTL(TRIM(files%path))//'mode_weights.dat'
            END IF
        END IF
        IF (config_logi(32)) THEN
            WRITE(66,'(A)') ADJUSTL(TRIM(files%path))//'fwhm.dat'
        END IF
    ! Close output file
    CLOSE(66)

END SUBROUTINE FIND_FILES

SUBROUTINE CHECK_EXISTS(file)
    USE global, ONLY : files
    IMPLICIT NONE
    CHARACTER(LEN = *) , INTENT(IN) :: file
    LOGICAL                         :: file_exists

    INQUIRE(FILE = TRIM(files%path)//TRIM(file), EXIST = file_exists)
    ! If file doesnt exist then print to screen and abort
    IF (.NOT. file_exists) THEN
        CALL WRITE_ERROR('Cannot find '//TRIM(files%path)//TRIM(file)//' in path')
    END IF

END SUBROUTINE CHECK_EXISTS

SUBROUTINE CLEAN_DIRECTORY
    ! Removes files created by tau
    IMPLICIT NONE

! Equilibrium data files
    CALL EXECUTE_COMMAND_LINE('rm -f get_ref_eig.m ')
    CALL EXECUTE_COMMAND_LINE('rm -f equilibrium_CF_matrix.real equilibrium_CF_matrix.imag ')
    CALL EXECUTE_COMMAND_LINE('rm -f equilibrium_eigvals.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f equilibrium_eigvecs.real.dat')
    CALL EXECUTE_COMMAND_LINE('rm -f equilibrium_eigvecs.imag.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f equilibrium_crystal_field_energies.dat')
    CALL EXECUTE_COMMAND_LINE('rm -f equilibrium_crystal_field_eigenvectors.dat')

! Misc.
    CALL EXECUTE_COMMAND_LINE('rm -f nohup.out')
    CALL EXECUTE_COMMAND_LINE('rm -f err.tmp')

! Barriers, pathways, contributions and probabilties files
    CALL EXECUTE_COMMAND_LINE('rm -f barrier-high.plt')! LEGACY
    CALL EXECUTE_COMMAND_LINE('rm -f barrier-low.plt')! LEGACY
    CALL EXECUTE_COMMAND_LINE('rm -f barrier-high.pdf')! LEGACY
    CALL EXECUTE_COMMAND_LINE('rm -f barrier-low.pdf')! LEGACY
    CALL EXECUTE_COMMAND_LINE('rm -f barrier_figure_*_K.*')
    CALL EXECUTE_COMMAND_LINE('rm -f energy.dat')
    CALL EXECUTE_COMMAND_LINE('rm -f mag.dat')
    CALL EXECUTE_COMMAND_LINE('rm -f trans.dat')
    CALL EXECUTE_COMMAND_LINE('rm -f gamma_mode_*.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f lw_mode_*.dat ') ! LEGACY
    CALL EXECUTE_COMMAND_LINE('rm -f dos_mode_*.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f cf_mode_*.dat ') ! LEGACY
    CALL EXECUTE_COMMAND_LINE('rm -f sp_mode_*.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f occ_mode_*.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f mode_weighted_sp_*.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f mode_weighted_occ_*.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f mode_weighted_pdos_*.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f mode_weighted_gamma_*.dat ')
    CALL EXECUTE_COMMAND_LINE('rm -f effective_n_modes_*.dat ')


! Gamma files
    CALL EXECUTE_COMMAND_LINE('rm -f qp_mathematica.m')
    CALL EXECUTE_COMMAND_LINE('rm -f gamma_matrix_*')
    CALL EXECUTE_COMMAND_LINE('rm -f all_rates_Mathematica.dat slow_rates_Mathematica.dat')


END SUBROUTINE CLEAN_DIRECTORY


SUBROUTINE SET_ATOM(ion, OEF, ox_state, two_J, gJ)
    ! Sets OEF and two_J using ion name and oxidation state
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(OUT)                :: OEF(3)
    CHARACTER(LEN = *), INTENT(IN)              :: ion
    INTEGER, INTENT(IN)                         :: ox_state
    INTEGER, INTENT(OUT)                        :: two_J
    REAL(KIND = WP)                             :: S, J, L
    REAL(KIND = WP), INTENT(OUT)                :: gJ
    INTEGER                                     :: num_f_elec

! Calculate number of f electrons
    IF (TRIM(ion) == 'ce') num_f_elec =  4
    IF (TRIM(ion) == 'pr') num_f_elec =  5
    IF (TRIM(ion) == 'nd') num_f_elec =  6
    IF (TRIM(ion) == 'pm') num_f_elec =  7
    IF (TRIM(ion) == 'sm') num_f_elec =  8
    IF (TRIM(ion) == 'eu') num_f_elec =  9
    IF (TRIM(ion) == 'gd') num_f_elec = 10
    IF (TRIM(ion) == 'tb') num_f_elec = 11
    IF (TRIM(ion) == 'dy') num_f_elec = 12
    IF (TRIM(ion) == 'ho') num_f_elec = 13
    IF (TRIM(ion) == 'er') num_f_elec = 14
    IF (TRIM(ion) == 'tm') num_f_elec = 15
    IF (TRIM(ion) == 'yb') num_f_elec = 16

    num_f_elec = num_f_elec - ox_state

! Calculate S
    IF (num_f_elec == 7 .OR. num_f_elec == 14) THEN
        CALL WRITE_ERROR('No orbital moment! ')
    ELSE IF (num_f_elec < 7) THEN
        S = real(num_f_elec, WP) / 2.0_WP
    ELSE IF (num_f_elec > 7) THEN
        S = (14.0_WP - num_f_elec)  / 2.0_WP
    END IF

! Calculate L
    IF (num_f_elec == 1 .OR. num_f_elec == 6 .OR. num_f_elec == 8 .OR. num_f_elec == 13) THEN
        L = 3.0_WP
    ELSE IF (num_f_elec == 2 .OR. num_f_elec == 5 .OR. num_f_elec == 9 .OR. num_f_elec == 12) THEN
        L = 5.0_WP
    ELSE IF (num_f_elec == 3 .OR. num_f_elec == 4 .OR. num_f_elec == 10 .OR. num_f_elec == 11) THEN
        L = 6.0_WP
    END IF

! Calculate J
    IF (num_f_elec < 7) THEN
        J = ABS(L - S)
    ELSE IF (num_f_elec > 7) THEN
        J = L + S
    END IF

! Calculate two_J
    two_J = NINT(2.0_WP * J)

! Calculate gJ (Lande g factor) for a given L, S, J
    gJ = 1.5_WP + (S*(S + 1.0_WP) - L*(L + 1.0_WP))/(2.0_WP*J*(J + 1.0_WP))

! Set operator equivalent factors
    IF (ion == 'ce' .AND. ox_state == 3) THEN
        OEF(1) =    -2.0_WP/35.0_WP
        OEF(2) =    2.0_WP/315.0_WP
        OEF(3) =    0.0_WP
    ELSE IF (ion == 'pr' .AND. ox_state == 3) THEN
        OEF(1) =    -52.0_WP/2475.0_WP
        OEF(2) =    -4.0_WP/5445.0_WP
        OEF(3) =    272.0_WP/4459455.0_WP
    ELSE IF (ion == 'nd' .AND. ox_state == 3) THEN
        OEF(1) =    -7.0_WP/1089.0_WP
        OEF(2) =    -136.0_WP/467181.0_WP
        OEF(3) =    -1615.0_WP/42513471.0_WP
    ELSE IF (ion == 'pm' .AND. ox_state == 3) THEN
        OEF(1) =    14.0_WP/1815.0_WP
        OEF(2) =    952.0_WP/2335905.0_WP
        OEF(3) =    2584.0_WP/42513471.0_WP
    ELSE IF (ion == 'sm' .AND. ox_state == 3) THEN
        OEF(1) =    13.0_WP/315.0_WP
        OEF(2) =    26.0_WP/10395.0_WP
        OEF(3) =    0.0_WP
    ELSE IF (ion == 'eu' .AND. ox_state == 3) THEN
        CALL WRITE_ERROR('Eu3+ has J=0 and hence no mJ states')
    ELSE IF (ion == 'gd' .AND. ox_state == 3) THEN
        CALL WRITE_ERROR('Gd3+ has no orbital angular momentum')
    ELSE IF (ion == 'tb' .AND. ox_state == 3) THEN
        OEF(1) =    -1.0_WP/99.0_WP
        OEF(2) =    2.0_WP/16335.0_WP
        OEF(3) =    -1.0_WP/891891.0_WP
    ELSE IF (ion == 'dy' .AND. ox_state == 3) THEN
        OEF(1) =    -2.0_WP/315.0_WP
        OEF(2) =    -8.0_WP/135135.0_WP
        OEF(3) =    4.0_WP/3864861.0_WP
    ELSE IF (ion == 'ho' .AND. ox_state == 3) THEN
        OEF(1) =    -1.0_WP/450.0_WP
        OEF(2) =    -1.0_WP/30030.0_WP
        OEF(3) =    -5.0_WP/3864861.0_WP
    ELSE IF (ion == 'er' .AND. ox_state == 3) THEN
        OEF(1) =    4.0_WP/1575.0_WP
        OEF(2) =    2.0_WP/45045.0_WP
        OEF(3) =    8.0_WP/3864861.0_WP
    ELSE IF (ion == 'tm' .AND. ox_state == 3) THEN
        OEF(1) =    1.0_WP/99.0_WP
        OEF(2) =    8.0_WP/49005.0_WP
        OEF(3) =    -5.0_WP/891891.0_WP
    ELSE IF (ion == 'yb' .AND. ox_state == 3) THEN
        OEF(1) =    2.0_WP/63.0_WP
        OEF(2) =    -2.0_WP/1155.0_WP
        OEF(3) =    4.0_WP/27027.0_WP
    ELSE
        CALL WRITE_ERROR('Atom and oxidation state not supported')
    END IF

    ! Write to output file
    CALL WRITE_ION_DATA(ion, ox_state, num_f_elec, L, J, gJ, S)

END SUBROUTINE SET_ATOM

SUBROUTINE WRITE_ION_DATA(ion, ox_state, num_f_elec, L, J, gJ, S)
    USE global, only : files
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: ion
    INTEGER, INTENT(IN)            :: ox_state, num_f_elec
    REAL(KIND = WP), INTENT(IN)    ::  L, J, gJ, S

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    WRITE(66,'(A)') ''
    CALL SECTION_HEADER('Metal Ion Data',66)
    WRITE(66,'(A)') ''

    WRITE(66,'(3A,I0,A)') 'Ion = ',TRIM(ion),'(',ox_state,')'

    WRITE(66,'(I0, A)') num_f_elec, ' f electrons'

    WRITE(66,'(A, I0)') 'L = ', INT(L)

    IF (S - 0.5_WP == INT(S)) THEN ! Non Integer S
        WRITE(66,'(A, I0, A)') 'S = ', INT(S*2.0_WP),'/2'
    ELSE
        WRITE(66,'(A, I0)') 'S = ', INT(S) ! Integer S
    END IF

    IF (INT(J) == INT(J*2.0_WP)/2) THEN ! Non Integer J
        WRITE(66,'(A, I0, A)') 'J = ', INT(J*2.0_WP),'/2'
    ELSE
        WRITE(66,'(A, I0)') 'J = ', INT(J) ! Integer J
    END IF
    WRITE(66,'(A, F7.5)') 'gJ = ', gJ
    WRITE(66,*)

! Close output file
    CLOSE(66)

END SUBROUTINE WRITE_ION_DATA

SUBROUTINE READ_PHONON_ENERGIES(n_modes, pho_en)
    USE global, only : files
    ! Reads mode_energies.dat
    IMPLICIT NONE
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT) :: pho_en(:)
    INTEGER, INTENT(IN)                       :: n_modes
    INTEGER                                   :: reason
    CHARACTER(LEN = 1000)                     :: line
    ! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Write mode_info header in output file
    WRITE(66,'(A)') ''
    CALL SECTION_HEADER('Mode Information',66)

    ! Allocate pho_en array
    ALLOCATE(pho_en(n_modes))
    pho_en = 0.0_WP

    ! Open mode energies file
    OPEN(33, FILE = files%mode_energies, STATUS = 'OLD')

    READ(33, *, IOSTAT=reason) line
    ! Check file is not corrupted or too short
    IF (reason > 0)  THEN
        CALL WRITE_ERROR('Error reading mode_energies.dat')
    ELSE IF (reason < 0) THEN
        CALL WRITE_ERROR('Error: Reached end of mode_energies.dat too soon')
    END IF

    ! Read first line and check for header, if not found, reset
    IF (INDEX(TRIM(line), 'Frequency') == 0) THEN
        REWIND(33)
    END IF

    ! Loop through file and read mode energies
        DO i = 1,n_modes

            READ(33, *, IOSTAT=reason) pho_en(i)

            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading mode_energies.dat')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of mode_energies.dat too soon')
            END IF

        END DO
    ! Close mode energies file
    CLOSE(33)

    ! Close output file
    CLOSE(66)

END SUBROUTINE READ_PHONON_ENERGIES

SUBROUTINE READ_PHONON_WEIGHTS(n_modes, pho_w)
    USE global, only : files
    ! Reads mode_weights.dat
    IMPLICIT NONE
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT) :: pho_w(:)
    INTEGER, INTENT(IN)                       :: n_modes
    INTEGER                                   :: reason
    CHARACTER(LEN = 1000)                     :: line
    ! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Allocate pho_w array
    ALLOCATE(pho_w(n_modes))
    pho_w = 1.0_WP

    ! Open mode weights file
    OPEN(33, FILE = files%mode_weights, STATUS = 'OLD')

    READ(33, *, IOSTAT=reason) line
    ! Check file is not corrupted or too short
    IF (reason > 0)  THEN
        CALL WRITE_ERROR('Error reading mode_weights.dat')
    ELSE IF (reason < 0) THEN
        CALL WRITE_ERROR('Error: Reached end of mode_weights.dat too soon')
    END IF

    ! Read first line and check for header, if not found, reset
    IF (INDEX(TRIM(line), 'Weight') == 0) THEN
        REWIND(33)
    END IF

    ! Write header in output file
    WRITE(66,'(A)') ''
    WRITE(66,'(A)') '              The following weighting factors were supplied by user             '
    WRITE(66,'(A)') ''

    ! Loop through file and read mode weights
        DO i = 1, n_modes

            READ(33, *, IOSTAT=reason) pho_w(i)
            ! Echo fwhm to output file
            IF (n_modes < 500) THEN
                WRITE(66,'(A, I5, A, F15.7)') 'Mode ', i, ': ', pho_w(i)
            END IF
            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading mode_weights.dat')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of mode_weights.dat too soon')
            END IF

        END DO
    ! Close mode weights file
    CLOSE(33)

    ! Close output file
    CLOSE(66)

END SUBROUTINE READ_PHONON_WEIGHTS

SUBROUTINE READ_PHONON_FWHM(n_modes, fwhm)
    USE global, only : files
    ! Reads fwhm.dat
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(OUT) :: fwhm(:,:)
    REAL(KIND = WP)              :: val
    INTEGER, INTENT(IN)          :: n_modes
    INTEGER                      :: reason
    CHARACTER(LEN = 1000)        :: line
    ! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Open fwhm file
    OPEN(33, FILE = files%fwhm, STATUS = 'OLD')

    READ(33, *, IOSTAT=reason) line
    ! Check file is not corrupted or too short
    IF (reason > 0)  THEN
        CALL WRITE_ERROR('Error reading fwhm.dat')
    ELSE IF (reason < 0) THEN
        CALL WRITE_ERROR('Error: Reached end of fwhm.dat too soon')
    END IF

    ! Read first line and check for header, if not found, reset
    IF (INDEX(TRIM(line), 'fwhm') == 0) THEN
        REWIND(33)
    END IF

    ! Write header in output file
    WRITE(66,'(A)') ''
    WRITE(66,'(A)') '                        FWHM values were supplied by user                       '
    WRITE(66,'(A)') ''

    ! Loop through file and read fwhm
        DO i = 1, n_modes

            READ(33, *, IOSTAT=reason) val
            fwhm(:,i) = val
            ! Echo fwhm to output file
            !IF (n_modes < 500) THEN
            !    WRITE(66,'(A, I5, A, F15.7)') 'Mode ', i, ': ', val
            !END IF

            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading fwhm.dat')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of fwhm.dat too soon')
            END IF

        END DO
    ! Close fwhm file
    CLOSE(33)

    ! Close output file
    CLOSE(66)

END SUBROUTINE READ_PHONON_FWHM

SUBROUTINE APPLY_CALIBRATION(pho_en, slope, intercept, n_modes)
    USE global, only : files
    ! Applies calibration to pho_en and writes energies to output file
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)    :: slope, intercept
    REAL(KIND = WP), INTENT(INOUT) :: pho_en(:)
    REAL(KIND = WP)                :: pho_en_keep(SIZE(pho_en))
    INTEGER                        :: J
    INTEGER, INTENT(IN)            :: n_modes

! Keep copy of uncalibrated energies
    pho_en_keep = pho_en

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Apply slope and intercept correction
    pho_en = (slope * pho_en) + intercept

! Write mode energies header
CALL SUB_SECTION_HEADER('Mode Energies',66)
CALL SUB_SECTION_HEADER('Number  |  Original cm^-1   |   Calibrated cm^-1',66)

! Loop over modes and write original and corrected energies
    DO J = 1, n_modes
        WRITE(66, '(I3, 2F15.4)') J, pho_en_keep(J), pho_en(J)
    END DO

! Close output file
    CLOSE(66)

END SUBROUTINE APPLY_CALIBRATION

SUBROUTINE REPORT_GAUSS_WIDTH(FWHM, g_width, section)
    USE global, only : files
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)   :: FWHM, g_width
    CHARACTER(LEN = *)            :: section

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write FWHM info to output file
    WRITE(66,'(A)') ''
    CALL SUB_SECTION_HEADER(TRIM(section),66)

    WRITE(66,'(A, F8.3, A)') 'FWHM            = ', FWHM, ' cm^-1'
    WRITE(66,'(A, F8.3, A)') 'Gaussian Width  = ', g_width, ' cm^-1'

! Close output file
    CLOSE(66)
END SUBROUTINE REPORT_GAUSS_WIDTH

ELEMENTAL FUNCTION GAUSS_WIDTH(FWHM)
    USE global, only : files
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN) :: FWHM
    REAL(KIND = WP) :: GAUSS_WIDTH

! Calculate Gaussian width using FWHM in cm-1
! $\sigma = \frac{\text{FWHM}}{2\sqrt{2\log(2)}}$
    GAUSS_WIDTH = FWHM/(2.0_WP*sqrt(2.0_WP*log(2.0_WP)))

END FUNCTION GAUSS_WIDTH


SUBROUTINE READ_PHONON_CF_STEPS(pho_data, n_modes, modes_logical)
    ! Reads in CFP_steps.dat
    USE global, only : files
    IMPLICIT NONE
    INTEGER, INTENT(IN)                       :: n_modes
    INTEGER                                   :: q, IDUM, i, col, reason
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT) :: pho_data(:,:,:,:)
    LOGICAL, INTENT(IN)                       :: modes_logical(:)

! Read phonon steps
! these are delta CFPs (referenced to equilibrium CFPs) and the OEFs are not included
! (so these are the "real" CFPs)
! these are calculated at each step +'ve and -'ve zero-point vibrational motion
    ALLOCATE(pho_data(n_modes, 3, 13, 2))
    pho_data = 0.0_WP

! Open file
    OPEN(33,FILE = TRIM(files%SP_CFPs), STATUS = 'OLD')

        DO i = 1,n_modes
            READ(33, *, IOSTAT=reason)

            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading CFP_steps.dat')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of CFP_steps.dat too soon')
            END IF
        ! k = 2 q = -2 -> 2
            DO q = 1, 5
                READ(33, *, IOSTAT=reason) IDUM, IDUM, (pho_data(i,1,q,col), col = 1, 2)
                ! Check file is not corrupted or too short
                IF (reason > 0)  THEN
                    CALL WRITE_ERROR('Error reading CFP_steps.dat')
                ELSE IF (reason < 0) THEN
                    CALL WRITE_ERROR('Error: Reached end of CFP_steps.dat too soon')
                END IF
            END DO

        ! k = 4 q = -4 -> 4
            DO q = 1, 9
                READ(33, *, IOSTAT=reason) IDUM, IDUM, (pho_data(i,2,q,col), col = 1, 2)

                ! Check file is not corrupted or too short
                IF (reason > 0)  THEN
                    CALL WRITE_ERROR('Error reading CFP_steps.dat')
                ELSE IF (reason < 0) THEN
                    CALL WRITE_ERROR('Error: Reached end of CFP_steps.dat too soon')
                END IF
            END DO

        ! k = 6 q = -6 -> 6
            DO q = 1, 13
                READ(33, *, IOSTAT=reason) IDUM, IDUM, (pho_data(i,3,q,col), col = 1, 2)

                ! Check file is not corrupted or too short
                IF (reason > 0)  THEN
                    CALL WRITE_ERROR('Error reading CFP_steps.dat')
                ELSE IF (reason < 0) THEN
                    CALL WRITE_ERROR('Error: Reached end of CFP_steps.dat too soon')
                END IF
            END DO

        ! If mode has been specified for removal, then set the values all to zero
            IF(.NOT. modes_logical(i)) pho_data(i,:,:,:) = 0.0_WP

        END DO
! Close file
    CLOSE(33)

END SUBROUTINE READ_PHONON_CF_STEPS

SUBROUTINE READ_PHONON_CF_POLYNOMIAL(pho_data, n_modes, modes_logical)
    ! Reads in CFP_polynomials.dat
    USE global, only : files
    IMPLICIT NONE
    INTEGER, INTENT(IN)                       :: n_modes
    INTEGER                                   :: q, IDUM, j, col, reason
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT) :: pho_data(:,:,:,:)
    REAL(KIND = WP), ALLOCATABLE              :: pho_mu(:), pho_k(:)
    REAL(KIND = WP)                           :: corr
    LOGICAL, INTENT(IN)                       :: modes_logical(:)
    CHARACTER(LEN = 500)                      :: line

    ! Read polynomial fits of delta CFPs i.e. the equilibrium CFPs have been removed
    !  OEFs are not included (so these are the "real" CFPs)
    !  The cf_parameter_array is laid out as
    !  (n_modes, num_ranks, num_orders, num_coefficients)
    !  Where n_modes is the number of vibrational modes
    !  num_ranks and num_orders are the k and q values of the crystal field
    !  e.g. (1,1) = B_2^-2; (2,3) = B_4^-2 etc.
    !  n.b. that elements which contain non-existant parameters
    !  such as cf_parameter_array(1, 1, 12) are zero
    !  num_coefficients is the number of coefficients in the 3rd order
    !  polynomial fit where (1) = a of ax^3 etc

    ! The array is laid out as follows
    ALLOCATE(pho_data(n_modes,3, 13, 3))
    pho_data = 0.0_WP

    ! Open file
    OPEN(33,FILE = TRIM(files%SP_CFPs), STATUS = 'OLD')
        ! Loop over modes and read in coefficients for each parameter
        DO j = 1,n_modes
            READ(33, *, IOSTAT=reason)

            ! Check file is not corrupted or too short
                IF (reason > 0)  THEN
                    CALL WRITE_ERROR('Error reading CFP_polynomials.dat')
                ELSE IF (reason < 0) THEN
                    CALL WRITE_ERROR('Error: Reached end of CFP_polynomials.dat too soon')
                END IF
            ! k = 2 q = -2 -> 2
            DO q = 1, 5
                READ(33,*, IOSTAT = reason) IDUM, IDUM, (pho_data(j,1,q,col), col=1, 3)

                ! Check file is not corrupted or too short
                IF (reason > 0)  THEN
                    CALL WRITE_ERROR('Error reading CFP_polynomials.dat')
                ELSE IF (reason < 0) THEN
                    CALL WRITE_ERROR('Error: Reached end of CFP_polynomials.dat too soon')
                END IF
            END DO

            ! k = 4 q = -4 -> 4
            DO q = 1, 9
                READ(33,*, IOSTAT = reason) IDUM, IDUM, (pho_data(j,2,q,col), col=1, 3)

                ! Check file is not corrupted or too short
                IF (reason > 0)  THEN
                    CALL WRITE_ERROR('Error reading CFP_polynomials.dat')
                ELSE IF (reason < 0) THEN
                    CALL WRITE_ERROR('Error: Reached end of CFP_polynomials.dat too soon')
                END IF
            END DO

            ! k = 6 q = -6 -> 6
            DO q = 1, 13
                READ(33,*, IOSTAT = reason) IDUM, IDUM, (pho_data(j,3,q,col), col=1, 3)

                ! Check file is not corrupted or too short
                IF (reason > 0)  THEN
                    CALL WRITE_ERROR('Error reading CFP_polynomials.dat')
                ELSE IF (reason < 0) THEN
                    CALL WRITE_ERROR('Error: Reached end of CFP_polynomials.dat too soon')
                END IF
            END DO

            ! If mode has been specified for removal, then set the values all to zero
            IF(.NOT. modes_logical(j)) pho_data(j,:,:,:) = 0.0_WP

        END DO
    ! Close CFP_polynomials.dat
    CLOSE(33)

    ! Replace old versions of phonon polynomials for updated definition of x0
    IF(config_logi(29)) THEN
        AlLOCATE(pho_mu(n_modes), pho_k(n_modes))
        pho_mu = 0.0_WP
        pho_k  = 0.0_WP

        OPEN(36, FILE = TRIM(files%path)//"reduced_masses.dat", STATUS="UNKNOWN")
        OPEN(37, FILE = TRIM(files%path)//"force_constants.dat", STATUS="UNKNOWN")
            ! Read first line and check for header, if not found, reset
            READ(36,*) line
            IF (INDEX(TRIM(line), 'educed') == 0) THEN
                REWIND(36)
            END IF
            READ(37,*) line
            IF (INDEX(TRIM(line), 'orce') == 0) THEN
                REWIND(37)
            END IF
            ! Read in arrays
            DO j = 1,n_modes
                READ(36,*) pho_mu(j)
                READ(37,*) pho_k(j)
            END DO
        CLOSE(36)
        CLOSE(37)
        DO j = 1,n_modes
            corr = 8.42242656804021_WP*(10.0_WP**(-3))*SQRT(pho_en(j)*pho_mu(j)/pho_k(j))
            pho_data(j,:,:,1) = pho_data(j,:,:,1)*(corr**3)
            pho_data(j,:,:,2) = pho_data(j,:,:,2)*(corr**2)
            pho_data(j,:,:,3) = pho_data(j,:,:,3)*corr
            if(debug) then
                write(6,*) "----------------------"
                write(6,*) "mode ",j
                write(6,*) "en ",pho_en(j)
                write(6,*) "mu ",pho_mu(j)
                write(6,*) "k  ",pho_k(j)
                write(6,*) "co  ",corr
            end if
        END DO
    END IF

END SUBROUTINE READ_PHONON_CF_POLYNOMIAL

SUBROUTINE REPORT_ZEROTH_ORDER_CFP_HARMONICS(pho_data, zpd_scale)
    IMPLICIT NONE
    INTEGER                     :: q
    REAL(KIND = WP)             :: alt_CFP(3,13)
    REAL(KIND = WP), INTENT(IN) :: pho_data(:,:,:,:), zpd_scale(:)

    WRITE(6,'(A)') 'Zeroth-order change to CFPs due to vibrations'
    WRITE(6,'(A)') 'NOTE: this is not utilised - FYI only'
    WRITE(6,'(A)') '300 K'

    CALL vib_zeroth_order(300.0_WP, alt_CFP, pho_en, n_modes, pho_data, zpd_scale)

    DO q = 1, 5
        WRITE(6,*) 2,q - 3,alt_CFP(1,q)
    END DO
    DO q = 1, 9
        WRITE(6,*) 4,q - 5,alt_CFP(2,q)
    END DO
    DO q = 1, 13
        WRITE(6,*) 6,q - 7,alt_CFP(3,q)
    END DO

    WRITE(6,'(A)') '2 K'
    CALL vib_zeroth_order(2.0_WP, alt_CFP, pho_en, n_modes, pho_data, zpd_scale)

    DO q = 1, 5
        WRITE(6,*) 2,q - 3,alt_CFP(1,q)
    END DO
    DO q = 1, 9
        WRITE(6,*) 4,q - 5,alt_CFP(2,q)
    END DO
    DO q = 1, 13
        WRITE(6,*) 6,q - 7,alt_CFP(3,q)
    END DO

    WRITE(6,*)

END SUBROUTINE REPORT_ZEROTH_ORDER_CFP_HARMONICS

SUBROUTINE READ_EQ_CFP(EQ_CFPs)
    USE global, only : files
    ! Reads equilibrium crystal field parameters from EQ_CFPs.dat
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(OUT) :: EQ_CFPs(:,:)
    INTEGER                      :: q, IDUM, reason

    EQ_CFPs = 0.0_WP

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Open EQ_CFPs.dat file
    OPEN(33, FILE = TRIM(files%EQ_CFPs), STATUS = 'OLD')

! k = 2 q = -2 -> 2 parameters
    DO q = 1, 5
        READ(33,*, IOSTAT=reason) IDUM, IDUM, IDUM, EQ_CFPs(1,q)

    ! Check file is not corrupted or too short
        IF (reason > 0)  THEN
            CALL WRITE_ERROR('Error reading EQ_CFPs.dat')
        ELSE IF (reason < 0) THEN
            CALL WRITE_ERROR('Error: Reached end of EQ_CFPs.dat too soon')
        END IF

    END DO

! k = 4 q = -4 -> 4 parameters
    DO q = 1, 9
        READ(33,*, IOSTAT=reason) IDUM, IDUM, IDUM, EQ_CFPs(2,q)

    ! Check file is not corrupted or too short
        IF (reason > 0)  THEN
            CALL WRITE_ERROR('Error reading EQ_CFPs.dat')
        ELSE IF (reason < 0) THEN
            CALL WRITE_ERROR('Error: Reached end of EQ_CFPs.dat too soon')
        END IF
    END DO

! k = 6 q = -6 -> 6 parameters
    DO q = 1, 13
        READ(33,*, IOSTAT=reason) IDUM, IDUM, IDUM, EQ_CFPs(3,q)

    ! Check file is not corrupted or too short
        IF (reason > 0)  THEN
            CALL WRITE_ERROR('Error reading EQ_CFPs.dat')
        ELSE IF (reason < 0) THEN
            CALL WRITE_ERROR('Error: Reached end of EQ_CFPs.dat too soon')
        END IF
    END DO

! Close EQ_CFPs.dat file
    CLOSE(33)

! Close output file
    CLOSE(66)
END SUBROUTINE READ_EQ_CFP

SUBROUTINE CALC_EQ_CF_HAMILTONIAN(two_J, EQ_CFPs, JP, JM, JZ, gJ, OEF, stevens_ops, AC_field, &
                                       CF_matrix)
    ! Calculates equilibrium crysal field hamiltonian with a small magnetic field applied
    ! in z direction
    USE constants, ONLY : muB, ii
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)                  :: EQ_CFPs(:,:), OEF(:), gJ
    REAL(KIND = WP), INTENT(INOUT)                  :: AC_field(3)
    INTEGER, INTENT(IN)                          :: two_J
    COMPLEX(KIND = WP), ALLOCATABLE              :: AC_matrix(:,:), Zee(:,:,:)
    COMPLEX(KIND = WP), ALLOCATABLE, INTENT(OUT) :: CF_matrix(:,:)
    COMPLEX(KIND = WP), INTENT(IN)               :: JP(:,:), JM(:,:), JZ(:,:), stevens_ops(:,:,:,:)
    INTEGER                                      :: k, l
    REAL(KIND = WP)                              :: Gmat(3,3),Gvec(3,3),Gval(3)


! AC_field is strength of magnetic field used to split states
! This is to split +- projections of mJ states
! (otherwise they are exactly degenerate and you get arbitrary linear combinations
!  from eigensolver)
! Field is vector of (x,y,z) values
! e.g. AC_field = 1.0_WP/10000.0_WP        Is (1,1,1) Oe -> T

! Allocate and zero Magnetic Field and Crystal Field matrices
    ALLOCATE(AC_matrix(two_J+1, two_J + 1), CF_matrix(two_J+1, two_J + 1))
    CF_matrix = (0.0_WP,0.0_WP)
    AC_matrix = (0.0_WP,0.0_WP)

! Calculate equilibrium CF matrix
    CALL GET_CF_MATRIX(EQ_CFPs,CF_matrix, OEF, stevens_ops)

! If field_user_set = .FALSE., choose largest moment of ground doublet
! Set default field strength as 2 Oe and convert to Tesla
    IF(.NOT. user_field_set) THEN
        ALLOCATE(Zee(3,two_J + 1,two_J + 1))
        Zee(1,:,:) = gJ*0.5_WP*(JP+JM)
        Zee(2,:,:) = gJ*(1.0_WP/(2.0_WP*ii))*(JP-JM)
        Zee(3,:,:) = gJ*JZ
        DO k = 1,3
            DO l = 1,3
                Gmat(k,l) = 2.0_WP*real(Zee(k,1,2)*Zee(l,2,1) &
                                      + Zee(k,2,1)*Zee(l,1,2) &
                                      + Zee(k,1,1)*Zee(l,1,1) &
                                      + Zee(k,2,2)*Zee(l,2,2),WP)
            END DO
        END DO
        call real_c_diag(3,Gmat,Gvec,Gval)
        AC_field(:) = (2.0_WP/10000.0_WP)*Gvec(:,maxloc(Gval,1))
        DEALLOCATE(Zee)
    END IF

! Calculate Zeeman hamiltonian
    AC_matrix = muB*gJ*( &
                        0.5_WP*(JP+JM)*AC_field(1) &
                        + (1.0_WP/(2.0_WP*ii))*(JP-JM)*AC_field(2) &
                        + JZ*AC_field(3) &
                        )

! Add Zeeman to CF Hamiltonian
    CF_matrix = CF_matrix + AC_matrix

    DEALLOCATE(AC_matrix)

END SUBROUTINE CALC_EQ_CF_HAMILTONIAN

SUBROUTINE CALC_EQ_CF_HAMILTONIAN_HDF5(dimension, vals, spin, angmom, AC_field, CF_matrix)
    ! Calculates equilibrium crysal field hamiltonian with a small magnetic field applied
    ! in z direction
    USE constants, ONLY : muB,ii,ge
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)                  :: vals(:)
    REAL(KIND = WP), INTENT(INOUT)               :: AC_field(3)
    INTEGER, INTENT(IN)                          :: dimension
    COMPLEX(KIND = WP), ALLOCATABLE              :: AC_matrix(:,:)
    COMPLEX(KIND = WP), ALLOCATABLE, INTENT(OUT) :: CF_matrix(:,:)
    COMPLEX(KIND = WP), INTENT(IN)               :: spin(:,:,:), angmom(:,:,:)
    INTEGER                                      :: i, k, l
    REAL(KIND = WP)                              :: Gmat(3,3),Gvec(3,3),Gval(3)

! AC_field is strength of magnetic field used to split states
! This is to split +- projections of mJ states
! (otherwise they are exactly degenerate and you get arbitrary linear combinations
!  from eigensolver)
! Field is vector of (x,y,z) values
! e.g. AC_field = 1.0_WP/10000.0_WP        Is (1,1,1) Oe -> T

! Allocate and zero Magnetic Field and Crystal Field matrices
    ALLOCATE(AC_matrix(dimension, dimension), CF_matrix(dimension, dimension))
    CF_matrix = (0.0_WP,0.0_WP)
    AC_matrix = (0.0_WP,0.0_WP)

! Calculate equilibrium CF matrix
    DO i = 1,dimension
        CF_matrix(i,i) = vals(i)
    END DO

! If field_user_set = .FALSE., choose largest moment of ground doublet
! Set default field strength as 2 Oe and convert to Tesla
    IF(.NOT. user_field_set) THEN
        DO k = 1,3
            DO l = 1,3
                Gmat(k,l) = 2.0_WP*real((angmom(k,1,2) + ge*spin(k,1,2))*(angmom(l,2,1) + ge*spin(l,2,1)) &
                                      + (angmom(k,2,1) + ge*spin(k,2,1))*(angmom(l,1,2) + ge*spin(l,1,2)) &
                                      + (angmom(k,1,1) + ge*spin(k,1,1))*(angmom(l,1,1) + ge*spin(l,1,1)) &
                                      + (angmom(k,2,2) + ge*spin(k,2,2))*(angmom(l,2,2) + ge*spin(l,2,2)),WP)
            END DO
        END DO
        call real_c_diag(3,Gmat,Gvec,Gval)
        AC_field(:) = (2.0_WP/10000.0_WP)*Gvec(:,maxloc(Gval,1))
    END IF

! Calculate Zeeman hamiltonian
    AC_matrix = muB*(AC_field(1)*(angmom(1,:,:) + ge*spin(1,:,:)) &
                        + AC_field(2)*(angmom(2,:,:) + ge*spin(2,:,:)) &
                        + AC_field(3)*(angmom(3,:,:) + ge*spin(3,:,:)) &
                        )

! Add Zeeman to CF Hamiltonian
    CF_matrix = CF_matrix + AC_matrix

    DEALLOCATE(AC_matrix)

END SUBROUTINE CALC_EQ_CF_HAMILTONIAN_HDF5

SUBROUTINE DIAGONALISE_EQ_CF_HAMILTONIAN_MM(two_J, CF_matrix, vecs, vals)
    ! Diagonalises equilibrium crystal field hamiltonian using mathematica
    IMPLICIT NONE
    CHARACTER(LEN = 200) :: MM_CALL_COMMAND
    COMPLEX(KIND = WP), ALLOCATABLE, INTENT(OUT) :: vecs(:,:)
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT)    :: vals(:)
    COMPLEX(KIND = WP), INTENT(IN)               :: CF_matrix(:,:)
    INTEGER, INTENT(IN)                          :: two_J

#ifdef macos
    MM_CALL_COMMAND = 'WolframScript'
#else
    MM_CALL_COMMAND = 'math'
#endif

    ! Write real and imaginary parts of CF Hamiltonian to temp files used by mathematica

    CALL WRITE_ARRAY(array=real(CF_matrix,WP), fmt = 'E47.36E4)', file='equilibrium_CF_matrix.real')
    CALL WRITE_ARRAY(array=aimag(CF_matrix), fmt = 'E47.36E4)', file='equilibrium_CF_matrix.imag')

    ! Write mathematica script file
    OPEN(77, FILE = 'get_ref_eig.m', STATUS = 'UNKNOWN')
        WRITE(77,'(A)') '(* ::Package:: *)'
        WRITE(77,'(A)')
        WRITE(77,'(A)') 'SortEigensystem[eigsys_]:=Transpose[Sort[Transpose[eigsys]]];'
        WRITE(77,'(A)') 'CFmat=SetPrecision[Import[StringJoin[Directory[],"/equilibrium_CF_matrix.real"]]+I Import[StringJoin[Directory[],"/equilibrium_CF_matrix.imag"]],39];'
        WRITE(77,'(A)') '{Evals,Evecs}=SortEigensystem[Chop[Eigensystem[CFmat]]];'
        WRITE(77,'(A)') 'Evals=Chop[Evals-Min[Evals]];'
        WRITE(77,'(A)') 'Export[StringJoin[Directory[],"/equilibrium_eigvals.dat"],Evals];'
        WRITE(77,'(A)') 'Export[StringJoin[Directory[],"/equilibrium_eigvecs.real.dat"],Re[Transpose[Evecs]]];'
        WRITE(77,'(A)') 'Export[StringJoin[Directory[],"/equilibrium_eigvecs.imag.dat"],Im[Transpose[Evecs]]];'
        WRITE(77,'(A)') 'Total[Total[Chop[CFmat-ConjugateTranspose[CFmat]]]]==0'
    CLOSE(77)

    ! Run get_ref_eig.m
    CALL EXECUTE_COMMAND_LINE(TRIM(MM_CALL_COMMAND)//' -script get_ref_eig.m')

    ! Delete get_ref_eig.m
    IF (.NOT. debug) CALL EXECUTE_COMMAND_LINE('rm -f get_ref_eig.m')
    ! Delete reference CF matrix files
    IF (.NOT. debug) CALL EXECUTE_COMMAND_LINE('rm -f equilibrium_CF_matrix.real equilibrium_CF_matrix.imag')

    ! Read in eigenvectors and eigenvalues
    CALL READ_EQ_CF_EVEC_EVAL(two_J, vecs, vals)

END SUBROUTINE DIAGONALISE_EQ_CF_HAMILTONIAN_MM

SUBROUTINE DIAGONALISE_EQ_CF_HAMILTONIAN_CPP(two_J, CF_matrix, vecs, vals)
    ! Diagonalises equilibrium crystal field hamiltonian using c++
    USE fort_diag
    IMPLICIT NONE
    COMPLEX(KIND = WP), ALLOCATABLE, INTENT(OUT) :: vecs(:,:)
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT)    :: vals(:)
    COMPLEX(KIND = WP), INTENT(IN)               :: CF_matrix(:,:)
    INTEGER, INTENT(IN)                          :: two_J
    INTEGER(KIND = C_INT)                        :: arr_len

    ALLOCATE(vecs(two_J + 1,two_J + 1), vals(two_J + 1))
    vecs = (0.0_WP, 0.0_WP)
    vals = 0.0_WP

    arr_len = two_J + 1

    ! Call c function
    CALL complex_c_diag(arr_len, CF_matrix, vecs, vals)

    ! Set ground state to E = 0
    vals(:) = vals(:) - vals(1)

END SUBROUTINE DIAGONALISE_EQ_CF_HAMILTONIAN_CPP

SUBROUTINE DIAGONALISE_EQ_CF_HAMILTONIAN_CPP_HDF5(dimension, CF_matrix, vecs, vals)
    ! Diagonalises equilibrium crystal field hamiltonian using c++
    USE fort_diag
    IMPLICIT NONE
    COMPLEX(KIND = WP), ALLOCATABLE, INTENT(OUT) :: vecs(:,:)
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT)    :: vals(:)
    COMPLEX(KIND = WP), INTENT(IN)               :: CF_matrix(:,:)
    INTEGER, INTENT(IN)                          :: dimension
    INTEGER(KIND = C_INT)                        :: arr_len

    ALLOCATE(vecs(dimension,dimension), vals(dimension))
    vecs = (0.0_WP, 0.0_WP)
    vals = 0.0_WP

    arr_len = dimension

    ! Call c function
    CALL complex_c_diag(arr_len, CF_matrix, vecs, vals)

    ! Set ground state to E = 0
    vals(:) = vals(:) - vals(1)

END SUBROUTINE DIAGONALISE_EQ_CF_HAMILTONIAN_CPP_HDF5

SUBROUTINE READ_EQ_CF_EVEC_EVAL(two_J, EQ_evecs, EQ_evals)
    ! Read in equilbrium crystal field hamiltonian eigenvalues and eigenvectors from mathematica
    ! output
    ! If debug is false then also deletes these files after reading them
    USE constants, ONLY : ii
    IMPLICIT NONE
    INTEGER                                      :: row, col, reason
    INTEGER, INTENT(IN)                          :: two_J
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT)    :: EQ_evals(:)
    REAL(KIND = WP), ALLOCATABLE                 :: mat(:,:)
    COMPLEX(KIND = WP), ALLOCATABLE, INTENT(OUT) :: EQ_evecs(:,:)

! Allocate eigenvector, eigenvalue arrays, and dummy array mat
    ALLOCATE(EQ_evals(two_J+1), EQ_evecs(two_J+1, two_J + 1), mat(two_J+1, two_J + 1))

! Zero eigenvector, eigenvalue arrays, and dummy array mat
    EQ_evecs = (0.0_WP,0.0_WP)
    EQ_evals = 0.0_WP
    mat      = 0.0_WP

! Read equilbrium eigenvalues
    OPEN(33, FILE = 'equilibrium_eigvals.dat', STATUS = 'OLD')
        DO row = 1, two_J + 1
            READ(33,*, IOSTAT = reason) EQ_evals(row)

            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading equilibrium_eigvals.dat')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of equilibrium_eigvals.dat to soon')
            END IF
        END DO
    CLOSE(33)

! Read real equilbrium eigenvectors
    OPEN(33, FILE = 'equilibrium_eigvecs.real.dat', STATUS = 'OLD')

        DO row = 1, two_J + 1
            READ(33,*, IOSTAT = reason) (mat(row,col),col=1, two_J + 1)
            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading equilibrium_eigvecs.real')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of equilibrium_eigvecs.real to soon')
            END IF
        END DO
    CLOSE(33)

    EQ_evecs = EQ_evecs + mat

    mat = 0.0_WP

! Read imaginary equilbrium eigenvectors
    OPEN(33, FILE = 'equilibrium_eigvecs.imag.dat', STATUS = 'OLD')

        DO row = 1, two_J + 1
            READ(33,*, IOSTAT = reason) (mat(row,col),col=1, two_J + 1)
            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading equilibrium_eigvecs.imag')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of equilibrium_eigvecs.imag to soon')
            END IF
        END DO
    CLOSE(33)

    EQ_evecs = EQ_evecs + ii*mat

! Remove mathematica files
    IF (.NOT. debug) CALL EXECUTE_COMMAND_LINE('rm -f equilibrium_eigvals.dat equilibrium_eigvecs.real.dat equilibrium_eigvecs.imag.dat')

END SUBROUTINE READ_EQ_CF_EVEC_EVAL

SUBROUTINE WRITE_EQ_INFO(two_J, EQ_evecs, EQ_evals, state_mu, state_labels, field_strength, &
                         evec_print)
    USE global, only : files
    ! Writes Equilibrium eigenvectors and eigenvalues to output file
    IMPLICIT NONE
    INTEGER, INTENT(IN)                       :: two_J
    INTEGER                                   :: i, j, row, col
    REAL(KIND = WP), INTENT(IN)               :: EQ_evals(:), state_mu(:), field_strength(3)
    REAL(KIND = WP), ALLOCATABLE              :: mJ(:), cf_percents(:,:)
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT) :: state_labels(:)
    COMPLEX(KIND = WP), INTENT(IN)            :: EQ_evecs(:,:)
    CHARACTER(LEN = 200)                      :: FMT, FMTT, state_labels_char, output_char
    LOGICAL, INTENT(IN)                       :: evec_print

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write equilbrium electronic structure section header
    WRITE(66,'(A)') ''
    CALL SECTION_HEADER('Equilibrium electronic structure',66)
    WRITE(66,'(A)') ''

! Make vector of mJ values
! -mJ to +mJ
    ALLOCATE(mJ(two_J + 1))
    mJ = 0.0_WP
    DO i = 1,two_J + 1
        mJ(i) = -real(two_J, WP)/2.0_WP + (i-1)*1.0_WP
    END DO

! Calculate actual mJ values of each state
    ALLOCATE(state_labels(two_J + 1))
    state_labels = 0.0_WP
    DO i = 1,two_J + 1
        DO j = 1,two_J + 1
            state_labels(i) = state_labels(i) + mJ(j)*(ABS(REAL(EQ_evecs(j,i),WP))**2 + ABS(aimag(EQ_evecs(j, i)))**2)
        END DO
    END DO

! Write <J_z> values to output file
    CALL SUB_SECTION_HEADER('<J_z> Values',66)
    DO row = 1, two_J + 1
        WRITE(66,'(A, I2, A, F7.3)') 'State ', row,'   ',state_labels(row)
    END DO
    WRITE(66,*)

! Write magnetic moment values to output file
    CALL SUB_SECTION_HEADER('Magnetic Moment (mu_B)',66)
    DO row = 1, two_J + 1
        WRITE(66,'(A, I2, A, F7.3)') 'State ', row,'   ',state_mu(row)
    END DO
    WRITE(66,*)

! Write DC Field information
    CALL SUB_SECTION_HEADER('DC Field',66)
    WRITE(66,'(A)') 'A DC Field has been applied:'
    WRITE(66,'(A, 3F10.3, A)') 'Field strength = (',field_strength * 10000.0_WP,') Oe'
    WRITE(66,*)

! Write eigenvalues
    CALL SUB_SECTION_HEADER('Equilibrium Crystal Field Eigenvalues (cm^-1)',66)
    DO row = 1, two_J + 1
        WRITE(66,'(A, I2, A, F9.3)') 'State ', row,'   ', EQ_evals(row)
    END DO
    WRITE(66,*)

! Write eigenvectors

! % composition
    CALL SUB_SECTION_HEADER('Equilibrium Crystal Field Vectors (percentages)',66)

! Calculate % composition
    ALLOCATE(cf_percents(two_J + 1, two_J + 1))
    DO row = 1, two_J + 1
        cf_percents(row,:) = ABS(REAL(EQ_evecs(:,row),WP))**2 + ABS(aimag(EQ_evecs(:,row)))**2
    END DO

! Formatting for mJ labels used in % composition table
    WRITE(FMTT,*) '(F5.1,A',(',F5.1,A',i = 2, two_J + 1),')'
    WRITE(state_labels_char,TRIM(FMTT)) (mJ(i),'  ',i = 1, two_J + 1)

! Write <J_z> labels for % composition
    WRITE(output_char,'(2A)') ' mJ =     ', TRIM(state_labels_char)
    WRITE(66,'(A)') TRIM(output_char)

! Formatting for the number of underscores
    WRITE(66,'(A)') REPEAT('‾',(LEN_TRIM(ADJUSTL(output_char))) + 1)

! Formatting for % compositions
    WRITE(FMT, '(A, I0, A)') '(A, I2, ', two_J + 1,'F7.2)'

! Write % compositions
    DO row = 1, two_J + 1
        WRITE(66,FMT) 'State ', row, (100.0_WP*cf_percents(row,col),col = 1, two_J + 1)
    END DO
    WRITE(66,*)

! Write raw eigenvector values
IF (evec_print) THEN

    ! Formatting for each row of matrix of variable size
        WRITE(FMT,'(A, I0, A)') '(', two_J + 1,'E47.36E4)'

    ! Real parts
        CALL SUB_SECTION_HEADER('Equilibrium Crystal Field Vectors - Real',66)

        ! Loop over states
        DO row = 1, two_J + 1
            WRITE(66,FMT) (real(EQ_evecs(row,col),WP),col = 1, two_J + 1)
        END DO

    ! Imaginary Parts
        CALL SUB_SECTION_HEADER('Equilibrium Crystal Field Vectors - Imaginary',66)

        ! Loop over states
        DO row = 1, two_J + 1
            WRITE(66,FMT) (aimag(EQ_evecs(row, col)),col = 1, two_J + 1)
        END DO

END IF

    ! Close output file
        CLOSE(66)

END SUBROUTINE WRITE_EQ_INFO

SUBROUTINE WRITE_EQ_INFO_HDF5(dimension, EQ_evecs, EQ_evals, state_mu, field_strength)
    USE global, only : files
    ! Writes Equilibrium eigenvectors and eigenvalues to output file
    IMPLICIT NONE
    INTEGER, INTENT(IN)                       :: dimension
    INTEGER                                   :: row
    REAL(KIND = WP), INTENT(IN)               :: EQ_evals(:), state_mu(:), field_strength(3)
    COMPLEX(KIND = WP), INTENT(IN)            :: EQ_evecs(:,:)

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write equilbrium electronic structure section header
    WRITE(66,'(A)') ''
    CALL SECTION_HEADER('Equilibrium electronic structure',66)
    WRITE(66,'(A)') ''

! Write magnetic moment values to output file
    CALL SUB_SECTION_HEADER('Magnetic Moment (mu_B)',66)
    DO row = 1, dimension
        WRITE(66,'(A, I2, A, F7.3)') 'State ', row,'   ',state_mu(row)
    END DO
    WRITE(66,*)

! Write DC Field information
    CALL SUB_SECTION_HEADER('DC Field',66)
    WRITE(66,'(A)') 'A DC Field has been applied:'
    WRITE(66,'(A, 3F10.3, A)') 'Field strength = (',field_strength * 10000.0_WP,') Oe'
    WRITE(66,*)

! Write eigenvalues
    CALL SUB_SECTION_HEADER('Equilibrium Crystal Field Eigenvalues (cm^-1)',66)
    DO row = 1, dimension
        WRITE(66,'(A, I2, A, F9.3)') 'State ', row,'   ', EQ_evals(row)
    END DO
    WRITE(66,*)

    ! Close output file
        CLOSE(66)

END SUBROUTINE WRITE_EQ_INFO_HDF5


SUBROUTINE REPLACE_CF_ENERGIES(two_J, EQ_evals)
    USE global, only : files
    ! Reads in replacement crystal field energies from file
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(INOUT) :: EQ_evals(:)
    INTEGER                        :: row, reason
    INTEGER, INTENT(IN)            :: two_J

! Read new CF energies
    OPEN(33, FILE = TRIM(files%path)//'new_CF_energies.dat', STATUS = 'OLD')
        DO row = 1, two_J + 1
            READ(33,*, IOSTAT = reason) EQ_evals(row)
            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading new_CF_energies.dat')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of new_CF_energies.dat to soon')
            END IF
        END DO
    CLOSE(33)

! Write information to output file

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write eigenvalues
    CALL WARNING_HEADER('Equilibrium crystal field energies were shifted by request', 66)
    CALL SUB_SECTION_HEADER('New Equilibrium Crystal Field Eigenvalues (cm^-1)',66)

    DO row = 1, two_J + 1
        WRITE(66,'(A, I2, A, F9.3)') 'State ', row,' E = ', EQ_evals(row)
    END DO

! Close output file
    CLOSE(66)

END SUBROUTINE REPLACE_CF_ENERGIES

SUBROUTINE REPLACE_CF_ENERGIES_HDF5(dimension, EQ_evals)
    USE global, only : files
    ! Reads in replacement crystal field energies from file
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(INOUT) :: EQ_evals(:)
    INTEGER                        :: row, reason
    INTEGER, INTENT(IN)            :: dimension

! Read new CF energies
    OPEN(33, FILE = TRIM(files%path)//'new_CF_energies.dat', STATUS = 'OLD')
        DO row = 1, dimension
            READ(33,*, IOSTAT = reason) EQ_evals(row)
            ! Check file is not corrupted or too short
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading new_CF_energies.dat')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of new_CF_energies.dat to soon')
            END IF
        END DO
    CLOSE(33)

! Write information to output file

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write eigenvalues
    CALL WARNING_HEADER('Equilibrium crystal field energies were shifted by request', 66)
    CALL SUB_SECTION_HEADER('New Equilibrium Crystal Field Eigenvalues (cm^-1)',66)

    DO row = 1, dimension
        WRITE(66,'(A, I2, A, F9.3)') 'State ', row,' E = ', EQ_evals(row)
    END DO

! Close output file
    CLOSE(66)

END SUBROUTINE REPLACE_CF_ENERGIES_HDF5

PURE SUBROUTINE ZCW(m, VecDir, NumDir)
    USE constants, ONLY : pi
    IMPLICIT NONE
    INTEGER                                   :: i, c(3)
    INTEGER, INTENT(IN)                       :: m
    INTEGER, INTENT(OUT)                      :: NumDir
    INTEGER, ALLOCATABLE                      :: g(:)
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT) :: VecDir(:,:)
    REAL(KIND = WP)                           :: a, b

    ALLOCATE(g(m+3))
    c(1) = -1
    c(2) = 1
    c(3) = 1
    g(1) = 8
    g(2) = 13

    DO i=3,m+3
        g(i) = g(i-1) + g(i-2)
    END DO

    NumDir = g(m+3)

    ALLOCATE(VecDir(NumDir,3))

    DO i=1,NumDir
        a = (2.0_WP*pi/real(c(3),WP))*mod(real(i-1,WP)*real(g(m+1),WP)/NumDir,1.0_WP)
        b = acos(real(c(1),WP)*((real(c(2),WP)*mod(real(i-1,WP)/NumDir,1.0_WP))-1.0_WP))
        VecDir(i,1) = sin(b)*cos(a)
        VecDir(i,2) = sin(b)*sin(a)
        VecDir(i,3) = cos(b)
    END DO

    DEALLOCATE(g)

END SUBROUTINE ZCW

SUBROUTINE CALC_MAGNETIC_MOMENT(two_J, JP, JM, JZ, gJ, EQ_evecs, first_two_states, state_mu, AC_field)
    USE constants, ONLY : ii
    IMPLICIT NONE
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT) :: state_mu(:)
    REAL(KIND = WP), INTENT(IN)               :: gJ, AC_field(3)
    REAL(KIND = WP)                           :: AC_norm
    COMPLEX(KIND = WP), INTENT(IN)            :: JP(:,:), JM(:,:), JZ(:,:), EQ_evecs(:,:)
    COMPLEX(KIND = WP), ALLOCATABLE           :: pos(:,:)
    INTEGER, INTENT(IN)                       :: two_J
    INTEGER, INTENT(OUT)                      :: first_two_states(:)
    INTEGER                                   :: k

! Allocate array for magnetic moment values
    ALLOCATE(state_mu(two_J+1), pos(two_J + 1, two_J + 1))

! Calculate magnetic moment of each state
    state_mu = 0.0_WP
    AC_norm = sqrt(AC_field(1)**2 + AC_field(2)**2 + AC_field(3)**2)

! Use Zeeman hamiltonian in Z direction and change to eigenbasis of crystal field hamiltonian
    pos = -gJ*(0.5_WP*(JP+JM)*AC_field(1) &
               + (1.0_WP/(2.0_WP*ii))*(JP-JM)*AC_field(2) &
               + JZ*AC_field(3))/AC_norm
    CALL EXPECTATION('F',EQ_evecs,pos)
    DO k = 1, two_J + 1
        state_mu(k) = -real(pos(k,k),WP)
    END DO

! Find ground state with -ve mJ
! This is the 'first state' - i.e. where all population starts in the barrier evolution
    IF(state_mu(1) < 0.0_WP) THEN
        first_two_states(1) = 1
    ELSE
        first_two_states(1) = 2
    END IF
    IF(state_mu(3) < 0.0_WP) THEN
        first_two_states(2) = 3
    ELSE
        first_two_states(2) = 4
    END IF

END SUBROUTINE CALC_MAGNETIC_MOMENT

SUBROUTINE CALC_MAGNETIC_MOMENT_HDF5(dimension, spin, angmom, EQ_evecs, first_two_states, state_mu, AC_field)
    USE constants, only : ge
    IMPLICIT NONE
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT) :: state_mu(:)
    REAL(KIND = WP), INTENT(IN)               :: AC_field(3)
    REAL(KIND = WP)                           :: AC_norm
    COMPLEX(KIND = WP), INTENT(IN)            :: spin(:,:,:), angmom(:,:,:), EQ_evecs(:,:)
    COMPLEX(KIND = WP), ALLOCATABLE           :: pos(:,:)
    INTEGER, INTENT(IN)                       :: dimension
    INTEGER, INTENT(OUT)                      :: first_two_states(:)
    INTEGER                                   :: k

! Allocate array for magnetic moment values
    ALLOCATE(state_mu(dimension), pos(dimension, dimension))

! Calculate magnetic moment of each state
    state_mu = 0.0_WP
    AC_norm = sqrt(AC_field(1)**2 + AC_field(2)**2 + AC_field(3)**2)

! Use Zeeman hamiltonian in Z direction and change to eigenbasis of crystal field hamiltonian
    pos = -(AC_field(1)*(angmom(1,:,:) + ge*spin(1,:,:)) &
           + AC_field(2)*(angmom(2,:,:) + ge*spin(2,:,:)) &
           + AC_field(3)*(angmom(3,:,:) + ge*spin(3,:,:)))/AC_norm
    CALL EXPECTATION('F',EQ_evecs,pos)
    DO k = 1, dimension
        state_mu(k) = -real(pos(k,k),WP)
    END DO

! Find ground state with -ve mJ
! This is the 'first state' - i.e. where all population starts in the barrier evolution
    IF(state_mu(1) < 0.0_WP) THEN
        first_two_states(1) = 1
    ELSE
        first_two_states(1) = 2
    END IF
    IF(state_mu(3) < 0.0_WP) THEN
        first_two_states(2) = 3
    ELSE
        first_two_states(2) = 4
    END IF

END SUBROUTINE CALC_MAGNETIC_MOMENT_HDF5

PURE SUBROUTINE GET_CF_MATRIX(CFPs, matrix, OEF, stevens_ops)
    ! Calculates crystal field matrix by adding each operator-parameter product to matrix
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)   :: CFPs(:,:), OEF(:)
    COMPLEX(KIND=WP), INTENT(OUT) :: matrix(:, :)
    COMPLEX(KIND=WP), INTENT(IN)  :: stevens_ops(:, :, :, :)
    INTEGER                       :: q

    matrix = (0.0_WP,0.0_WP)
    ! k = 2 q = -2 -> 2 terms
    DO q = 1, 5
    ! q = 3
        matrix = matrix + OEF(1)*CFPs(1,q)*stevens_ops(1,q,:,:)
    END DO

    ! k = 4 q = -4 -> 4 terms
    DO q = 1, 9
    ! q = 5
        matrix = matrix + OEF(2)*CFPs(2,q)*stevens_ops(2,q,:,:)
    END DO

    ! k = 6 q = -6 -> 6 terms
    DO q = 1, 13
    ! q = 7
        matrix = matrix + OEF(3)*CFPs(3,q)*stevens_ops(3,q,:,:)
    END DO

END SUBROUTINE GET_CF_MATRIX

SUBROUTINE CALC_GAMMA(sm_gamma, sm_sp, sm_occ, sm_pdos, total_gamma, config_logi, config_int, &
                      EQ_evals, EQ_evecs, temp, fwhm, pho_en, pho_w, two_J, OEF, &
                      stevens_ops, zpd_scale, pho_data, max_state, lineshape, hdf5_subspace_dimension)
    USE OMP_LIB
    USE constants, ONLY : pi, hbar, kB
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)               :: EQ_evals(:), temp(:), pho_en(:), pho_w(:), &
                                                 OEF(:), zpd_scale(:), fwhm(:,:)
    REAL(KIND = WP), INTENT(IN), ALLOCATABLE  :: pho_data(:,:,:,:)
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE :: total_gamma(:,:,:), &
                                                 sm_gamma(:,:,:,:), &
                                                 sm_pdos(:,:,:,:), &
                                                 sm_sp(:,:,:,:), &
                                                 sm_occ(:,:,:,:)
    REAL(KIND = WP), ALLOCATABLE              :: linewidth(:,:), g_width(:,:)
    REAL(KIND = WP)                           :: min_phonon_energy_diff, max_phonon_energy_diff, &
                                                 mode_j_max, mode_j_min, mode_k_max, &
                                                 mode_k_min, temporary, E_k_max_allowed, &
                                                 E_k_min_allowed, width, decomp_save(4)
    COMPLEX(KIND = WP), INTENT(IN)            :: EQ_evecs(:,:), stevens_ops(:,:,:,:)
    COMPLEX(KIND = WP), ALLOCATABLE           :: phonon_matrix(:,:,:,:), raman_numerator_L(:), &
                                                 raman_numerator_R(:),temp_phonon_matrix(:,:,:)
    INTEGER                                   :: t, i, f, j, c, k, max_r_state, p, n_indep_calcs, dim
    INTEGER, INTENT(IN)                       :: config_int(:), two_J, hdf5_subspace_dimension
    INTEGER, INTENT(OUT)                      :: max_state
    INTEGER, ALLOCATABLE                      :: parallel_index(:,:)
    LOGICAL, INTENT(IN)                       :: config_logi(:) 
    CHARACTER(LEN=*), INTENT(IN)              :: lineshape

    IF(config_logi(34)) THEN !HDF5 defined
        dim = hdf5_subspace_dimension
    ELSE
        dim = two_J + 1
    END IF

    ! Set maximum number of states depending upon specified process
    max_state = dim
    max_r_state = dim
    ! If max raman state is altered by user input
    IF (config_logi(28)) max_r_state = config_int(12)
    ! Only doing raman
    IF ((.NOT. config_logi(1)) .AND. (config_logi(2))) max_state = max_r_state

    ! Report zeroth-order alteration to CFPs from vibrations
    ! this is not used in the subsequent calculation
    !IF (config_logi(8)) THEN
        !CALL REPORT_ZEROTH_ORDER_CFP_HARMONICS(pho_data, zpd_scale)
    !END IF

    ! Allocate array for gamma summed over each mode
    ! Array is (num_temps, dim, dim)
    ALLOCATE(total_gamma(config_int(5),dim, dim))
    total_gamma = 0.0_WP

    ! Allocate Phonon matrix
    ! Larger if using using tdep CFPs
    ! Larger still for harmonics
    ! Array is (n_temps, n_modes, dim, dim)
    IF(config_logi(8)) THEN
        ALLOCATE(phonon_matrix(config_int(5), config_int(1)*3, dim, dim))
    ELSE
        ALLOCATE(phonon_matrix(config_int(5), config_int(1), dim, dim))
    END IF
    phonon_matrix = (0.0_WP, 0.0_WP)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Spin-Phonon Coupling  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! If defined from HDF5:
    IF(config_logi(34)) THEN
        ALLOCATE(temp_phonon_matrix(config_int(1), config_int(15), config_int(15)))
        CALL READ_PHONON_MATRIX_HDF5(config_int(15),config_int(1),temp_phonon_matrix(:,:,:))
        ! Change basis into equilibrium eigenbasis
        DO j = 1, config_int(1)
            CALL EXPECTATION('F',EQ_evecs,temp_phonon_matrix(j,:,:))
            phonon_matrix(1,j,1:dim,1:dim) = temp_phonon_matrix(j,1:dim,1:dim)
        END DO
        DEALLOCATE(temp_phonon_matrix)
        IF(config_int(5) > 1) THEN
            DO t = 2, config_int(5)
                phonon_matrix(t,:,:,:) = phonon_matrix(1,:,:,:)
            END DO
        END IF
    ELSE
        ! Temperature independent
        ! Uses phonon matrix calculated at extremes of displacement
        IF(config_logi(3)) THEN
            CALL CALC_SP_MAT_STATIC(pho_data, config_int(1), phonon_matrix(1,:,:,:), EQ_evecs, OEF, &
                                    stevens_ops, two_J)
            IF(config_int(5) > 1) THEN
                DO t = 2, config_int(5)
                    phonon_matrix(t,:,:,:) = phonon_matrix(1,:,:,:)
                END DO
            END IF
        END IF

        ! Temperature independent
        ! Uses phonon matrix calcalated from linear term of polynomials
        IF(config_logi(23)) THEN
            CALL CALC_SP_MAT_TAYLOR(pho_data, config_int(1), phonon_matrix(1,:,:,:), EQ_evecs, OEF, &
                                    stevens_ops, two_J)
            IF(config_int(5) > 1) THEN
                DO t = 2, config_int(5)
                    phonon_matrix(t,:,:,:) = phonon_matrix(1,:,:,:)
                END DO
            END IF
        END IF

        ! Temperature dependent
        ! Uses phonon matrix calculated at thermally averaged displacement
        IF(config_logi(4)) THEN
            !$OMP PARALLEL SHARED(temp, config_int, EQ_evecs, OEF, stevens_ops, pho_data, pho_en, phonon_matrix)
            !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, i, f, j)
                DO t = 1, config_int(5)
                    CALL CALC_SP_MAT_POLY(temp(t), pho_en, pho_data, config_int(1), &
                                          phonon_matrix(t,:,:,:), EQ_evecs, OEF, stevens_ops, zpd_scale,two_J)
                END DO
            !$OMP END DO NOWAIT
            !$OMP END PARALLEL
        END IF
    END IF
    
    ! OPEN(33,FILE="phonon_check.txt",STATUS="unknown")
    ! DO j = 1,config_int(1)
    !     DO t = 1,16
    !         WRITE(33,'(16E25.16E2)') abs(phonon_matrix(1,j,t,:))**2
    !     END DO
    !     WRITE(33,*)
    ! END DO
    ! CLOSE(33)
    
    ! OPEN(33,FILE="diag_check.txt",STATUS="unknown")
    ! DO j = 1,config_int(1)
    !     WRITE(6,*) phonon_matrix(1,j,:,:)
    !     CALL TEMP_DIAG(config_int(15), phonon_matrix(1,j,:,:), EQ_evecs, EQ_evals)
    !     WRITE(33,'(16E25.16E2)') EQ_evals
    !     WRITE(33,*)
    ! END DO
    ! CLOSE(33)
    
    

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Orbach/Direct  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    IF (config_logi(1)) THEN

        ! Allocate arrays for gamma, phonon density of states, spin-phonon coupling, and occupancy
        ! sm means single mode - as these arrays are not yet summed over phonon modes
        ! Arrays are (num_temps, n_modes, dim, dim)
        ALLOCATE(sm_gamma(config_int(5), config_int(1), dim, dim), &
                    sm_sp(config_int(5), config_int(1), dim, dim), &
                   sm_occ(config_int(5), config_int(1), dim, dim), &
                  sm_pdos(config_int(5), config_int(1), dim, dim))
        sm_gamma   = 0.0_WP
        sm_sp      = 0.0_WP
        sm_occ     = 0.0_WP
        sm_pdos    = 0.0_WP

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   Q   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! These are the |matrix elements|^2 of the normal mode operator Q = sqrt(hbar/(2*omega*mu))(a^dagger + a)
    ! The factor sqrt(hbar/(omega*mu)) is included in the definition of the zero-point displacement
    ! so here Q = sqrt(1/2)(a^dagger + a)
    ! We then take the thermodynamic average of the occupancy (a^dagger + a)
    ! In our treatment assume that for each transition there is a mode at that energy
    ! Of course this is not true, and is corrected for in our phonon density of states
    ! Therefore, for a given transition i->f, our assumption makes the occupancy the same for every mode
    ! See tau manaual for full explanation

        ! Boltzmann
        IF (config_logi(5)) THEN
        !$OMP PARALLEL SHARED(sm_occ, EQ_evals, temp, config_int, two_J, dim)
        !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, i, f)
            DO t = 1, config_int(5)
                DO i = 2, dim
                    DO f = 1, i - 1
                        ! CF Up, phonon bath loses quantum
                        sm_occ(t, :, i, f) = BOLTZMANN_OCC(EQ_evals(f), EQ_evals(i), temp(t))
                        ! CF Down, phonon bath gains quantum
                        sm_occ(t, :, f, i) = 1.0_WP
                    END DO
                END DO
            END DO
        !$OMP END DO NOWAIT
        !$OMP END PARALLEL
        END IF

        ! Bose-Einstein
        IF (config_logi(6)) THEN
        !$OMP PARALLEL SHARED(sm_occ, EQ_evals, temp, config_int, two_J, dim)
        !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, i, f)
            DO t = 1, config_int(5)
                DO i = 2, dim
                    DO f = 1, i - 1
                        ! CF Up, phonon bath loses quantum
                        sm_occ(t, :, i, f) = BOSE_OCC_EMIT(EQ_evals(f), EQ_evals(i), temp(t))
                        ! CF Down, phonon bath gains quantum
                        sm_occ(t, :, f, i) = BOSE_OCC_ABSORB(EQ_evals(i), EQ_evals(f), temp(t))
                    END DO
                END DO
            END DO
        !$OMP END DO NOWAIT
        !$OMP END PARALLEL
        END IF


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Density of states   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        ! These are the matrix elements of the phonon density of states
        ! We assume the pDOS is a sum of gaussians each centered at a vibrational mode energy
        ! Then for each CF transition we calculate the value of each gaussian at the energy of
        ! the transition

        ! NOTE: the commented out terms below could be included to account for changes in the ZPD of the mode as the energy is integrated over the lineshape

        ! Gaussian Lineshape
        ! Temperature independent
        IF (INDEX(TRIM(lineshape),'gaussian') /= 0) THEN
        IF (.NOT. config_logi(7) .AND. .NOT. config_logi(32)) THEN
            width = GAUSS_WIDTH(fwhm(1, 1))
            CALL REPORT_GAUSS_WIDTH(fwhm(1, 1), width, 'Phonon linewidth')
        END IF
        !$OMP PARALLEL SHARED(sm_pdos, EQ_evals, pho_en, config_int, two_J, fwhm, dim)
        !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, i, f, j, width)
            DO t = 1, config_int(5)    
                DO j = 1, config_int(1)
                    width = GAUSS_WIDTH(fwhm(t, j))
                    DO i = 2, dim
                        DO f = 1, i - 1
                            ! CF Up, phonon bath loses quantum
                            sm_pdos(t, j, i, f) = GAUSSIAN(ABS(EQ_evals(i)-EQ_evals(f)),pho_en(j),width)*config_real(5)*pho_w(j)
  !* (pho_en(j)/ABS(EQ_evals(i)-EQ_evals(f)))
                            ! CF Down, phonon bath gains quantum
                            sm_pdos(t, j, f, i) = GAUSSIAN(ABS(EQ_evals(f)-EQ_evals(i)),pho_en(j),width)*config_real(5)*pho_w(j)
  !* (pho_en(j)/ABS(EQ_evals(f)-EQ_evals(i)))
                        END DO
                    END DO
                END DO
            END DO
        !$OMP END DO NOWAIT 
        !$OMP END PARALLEL 
        !ELSE IF (INDEX(TRIM(lineshape),'lunghi') /= 0) THEN
        !!$OMP PARALLEL SHARED(sm_pdos, EQ_evals, pho_en, config_int, two_J, fwhm, dim)
        !!$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, i, f, j, width)
        !    DO t = 1, config_int(5)
        !        DO j = 1, config_int(1)
        !            width = fwhm(t, j)/2.0_WP
        !            DO i = 2, dim
        !                DO f = 1, i - 1
        !                    ! CF Up, phonon bath loses quantum
        !                    sm_pdos(t, j, i, f) = width / (width**2 + (ABS(EQ_evals(i)-EQ_evals(f)) - pho_en(j))**2)
        !                    ! CF Down, phonon bath gains quantum
        !                    sm_pdos(t, j, i, f) = width / (width**2 + (ABS(EQ_evals(f)-EQ_evals(i)) + pho_en(j))**2)
        !                END DO
        !            END DO
        !        END DO
        !    END DO
        !!$OMP END DO NOWAIT 
        !!$OMP END PARALLEL 
        ELSE IF (INDEX(TRIM(lineshape),'antilorentzian') /= 0) THEN
        !$OMP PARALLEL SHARED(sm_pdos, EQ_evals, pho_en, config_int, two_J, fwhm, dim)
        !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, i, f, j, width)
            DO t = 1, config_int(5)
                DO j = 1, config_int(1)
                    width = fwhm(t, j)
                    DO i = 2, dim
                        DO f = 1, i - 1
                            ! CF Up, phonon bath loses quantum
                            sm_pdos(t, j, i, f) = ANTILORENTZIAN(ABS(EQ_evals(i)-EQ_evals(f)),pho_en(j),width)*config_real(5)*pho_w(j)
  !* (pho_en(j)/ABS(EQ_evals(i)-EQ_evals(f)))
                            ! CF Down, phonon bath gains quantum
                            sm_pdos(t, j, f, i) = ANTILORENTZIAN(ABS(EQ_evals(f)-EQ_evals(i)),pho_en(j),width)*config_real(5)*pho_w(j)
  !* (pho_en(j)/ABS(EQ_evals(f)-EQ_evals(i)))
                        END DO
                    END DO
                END DO
            END DO
        !$OMP END DO NOWAIT
        !$OMP END PARALLEL
        ELSE IF (INDEX(TRIM(lineshape),'lorentzian') /= 0) THEN
        !$OMP PARALLEL SHARED(sm_pdos, EQ_evals, pho_en, config_int, two_J, fwhm, dim)
        !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, i, f, j, width)
            DO t = 1, config_int(5)
                DO j = 1, config_int(1)
                    width = fwhm(t, j)
                    DO i = 2, dim
                        DO f = 1, i - 1
                            ! CF Up, phonon bath loses quantum
                            sm_pdos(t, j, i, f) = LORENTZIAN(ABS(EQ_evals(i)-EQ_evals(f)),pho_en(j),width)*config_real(5)*pho_w(j)
  !* (pho_en(j)/ABS(EQ_evals(i)-EQ_evals(f)))
                            ! CF Down, phonon bath gains quantum
                            sm_pdos(t, j, f, i) = LORENTZIAN(ABS(EQ_evals(f)-EQ_evals(i)),pho_en(j),width)*config_real(5)*pho_w(j)
  !* (pho_en(j)/ABS(EQ_evals(f)-EQ_evals(i)))
                        END DO
                    END DO
                END DO
            END DO
        !$OMP END DO NOWAIT 
        !$OMP END PARALLEL 
        END IF

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Electronic term HSP  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! Matrix elements of the spin-phonon coupling Hamiltonian H_SP
    sm_sp = abs(phonon_matrix)**2

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Gamma Matrix  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        ! Sum components of the gamma matrix
        sm_gamma = (2.0_WP*pi/hbar) * sm_occ * sm_pdos * sm_sp

        ! Calculate Diagonal Entries
        DO j = 1, config_int(1)
            DO t = 1, config_int(5)
                DO i = 1, dim
                ! Diagonal Entries summing down from diagonal
                    DO f = i+1, dim
                        sm_gamma(t, j, i, i) = sm_gamma(t, j, i, i) - sm_gamma(t, j, f, i)
                    END DO
                ! Diagonal Entries summing up from diagonal
                    DO f = i-1, 1, -1
                        sm_gamma(t, j, i, i) = sm_gamma(t, j, i, i) - sm_gamma(t, j, f, i)
                    END DO
                END DO
            END DO
        END DO

        ! Sum gammas for each mode into the true total gamma
        total_gamma = SUM(sm_gamma, 2)

        DO t = 1, config_int(5)
            DO i = 1, dim
                total_gamma(t,i,i) = 0.0_WP
            END DO
        END DO

    END IF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Raman  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    IF (config_logi(2)) THEN

    ! Open output file
        OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

            CALL SECTION_HEADER('Raman Information', 66)

            ! Abort if Boltzmann
            IF (config_logi(5)) THEN
                WRITE(6,'(A)') '        Raman dynamics incompatible with Boltzmann statistics '
                STOP
            END IF

            ! Write number of steps used in trapz
            WRITE(66,*)
            WRITE(66,'(A, I0, A)') '    ',config_int(11), ' steps will be used in the trapezoidal integration of the Raman rate '
            WRITE(66,'(A)')     ' expression over of the Raman rate expression over the bandwidth of each phonon '

            ! Write maximum raman state
            WRITE(66,*)
            WRITE(66,'(A, I0, A)') '           Only initial and final states up to state ', max_r_state, ' will be used'
            WRITE(66,'(A)')        '                      all intermediate states will be used                      '
            WRITE(66,*)

            ! Write Raman Gaussian width scale
            WRITE(66,*)
            WRITE(66,'(A, I0, A)') '        Linewidth for Raman integration is equivalent to ', config_int(13), ' standard deviations '
            WRITE(66,*)


        ! Close output file
        CLOSE(66)


        ! Written based on Lunghi's work: https://arxiv.org/abs/2112.09236 Equations 11 - 16

        ! Allocate variables for parallel working
        n_indep_calcs = config_int(5)*(max_r_state*max_r_state-max_r_state)
        ALLOCATE(parallel_index(n_indep_calcs,3),raman_numerator_L(dim),raman_numerator_R(dim))
        raman_numerator_L = (0.0_WP,0.0_WP)
        raman_numerator_R = (0.0_WP,0.0_WP)
        p = 0
        DO t = 1,config_int(5) ! temperature indexing
            DO f = 1, max_r_state! final step, state a
                DO i = 1, max_r_state! initial step, state b
                    IF(i == f) CYCLE
                    p = p + 1
                    parallel_index(p,1) = t
                    parallel_index(p,2) = f
                    parallel_index(p,3) = i
                END DO
            END DO
        END DO
        
        ! Pre-calculate lineshape width
        ALLOCATE(linewidth(config_int(5), config_int(1)),g_width(config_int(5), config_int(1)))
        IF (INDEX(TRIM(lineshape),'gaussian') /= 0) THEN
            DO t = 1, config_int(5)
                DO j = 1, config_int(1)
                    g_width(t,j) = GAUSS_WIDTH(fwhm(t, j))
                    linewidth(t, j) = config_int(13)*g_width(t,j)
                END DO
            END DO
        ELSE IF (INDEX(TRIM(lineshape),'lorentzian') /= 0 .OR. INDEX(TRIM(lineshape),'antilorentzian') /= 0) THEN
            ! these numbers are exact for lorentzian lines and independent of fwhm,
            ! but are super variable for the antilorentzian.
            ! Hence, lorentzian numbers are used as conservative overestimates.
            DO t = 1, config_int(5)
                DO j = 1, config_int(1)
                    IF(config_int(13) == 1) linewidth(t, j) = 0.9186686007358166_WP*fwhm(t, j)
                    IF(config_int(13) == 2) linewidth(t, j) = 6.983865097677518_WP*fwhm(t, j)
                    IF(config_int(13) == 3) linewidth(t, j) = 117.9007489804327_WP*fwhm(t, j)
                    IF(config_int(13) == 4) linewidth(t, j) = 5025.219516464893_WP*fwhm(t, j)
                END DO
            END DO
        END IF

        !$OMP PARALLEL &
        !$OMP& SHARED(temp, config_int, two_J, EQ_evals, OEF, stevens_ops, pho_data, n_indep_calcs, dim), &
        !$OMP& SHARED(pho_en, phonon_matrix, max_r_state, g_width, pho_w, parallel_index, fwhm, linewidth, total_gamma)
        !$OMP DO SCHEDULE(DYNAMIC,1) , &
        !$OMP& PRIVATE(t, i, f, c, j, k, mode_j_max, mode_j_min, mode_k_max), &
        !$OMP& PRIVATE(mode_k_min, min_phonon_energy_diff, max_phonon_energy_diff, temporary), &
        !$OMP& PRIVATE(E_k_max_allowed,E_k_min_allowed,raman_numerator_L,raman_numerator_R, decomp_save)
        DO p = 1,n_indep_calcs ! Loop over t, i and f for parallelisation
            t = parallel_index(p,1) ! temperature indexing
            f = parallel_index(p,2) ! final step, state a
            i = parallel_index(p,3) ! initial step, state b
            
            IF(t == 1 .and. i == 1 .and. f == 2 .and. config_logi(33)) THEN
                OPEN(66, FILE = TRIM(TRIM(files%path)//"raman_decomposition.dat"), STATUS = 'UNKNOWN')
            END IF

            DO k = 1, config_int(1) ! mode beta
                DO j = k, config_int(1) ! mode alpha
                    decomp_save = 0.0_WP

                    ! Calculate max and min mode energies of modes j and k using
                    ! gaussian placed on modes
                    mode_k_max = pho_en(k) + linewidth(t, k)
                    mode_k_min = MAX(pho_en(k) - linewidth(t, k),0.0_WP)
                    mode_j_max = pho_en(j) + linewidth(t, j)
                    mode_j_min = MAX(pho_en(j) - linewidth(t, j),0.0_WP)

                    ! Pre-calculate numerator matrix elements
                    DO c = 1,dim ! intermediate step
                        IF(i == c .or. f == c) cycle
                        raman_numerator_L(c) = phonon_matrix(t, j, c, f) * phonon_matrix(t, k, i, c)
                        raman_numerator_R(c) = phonon_matrix(t, k, c, f) * phonon_matrix(t, j, i, c)
                    END DO

                    temporary = 0.0_WP

                    ! If not skipping Raman Sum Process
                    IF(.NOT. config_logi(31)) THEN
                        ! W-- term
                        ! determine range of energies for mode 2 (k or beta) based on delta function
                        E_k_min_allowed = MIN(EQ_evals(f) - EQ_evals(i) - mode_j_max, EQ_evals(f) - EQ_evals(i) - mode_j_min)
                        E_k_max_allowed = MAX(EQ_evals(f) - EQ_evals(i) - mode_j_max, EQ_evals(f) - EQ_evals(i) - mode_j_min)
                        IF((E_k_max_allowed >= mode_k_min .and. E_k_max_allowed <= mode_k_max) .or. (E_k_min_allowed >= mode_k_min .and. E_k_min_allowed <= mode_k_max) .or. (E_k_min_allowed <= mode_k_min .and. E_k_max_allowed >= mode_k_max)) THEN
                            ! INTEGRAL
                            temporary = TRAPZ("--", mode_j_min, mode_j_max, config_int(11), temp(t), t, j, k, f, i, EQ_evals, g_width, fwhm, raman_numerator_L, raman_numerator_R, pho_en, pho_w, lineshape, dim)
                            if(j == k) temporary = 0.5_WP * temporary
                            total_gamma(t,f,i) = total_gamma(t,f,i) + (2.0_WP*pi/hbar)*temporary
                            decomp_save(1) = (2.0_WP*pi/hbar)*temporary
                        END IF

                        ! W++ term
                        ! determine range of energies for mode 2 (j or beta) based on delta function
                        E_k_min_allowed = MIN(-(EQ_evals(f) - EQ_evals(i)) - mode_j_max, -(EQ_evals(f) - EQ_evals(i)) - mode_j_min)
                        E_k_max_allowed = MAX(-(EQ_evals(f) - EQ_evals(i)) - mode_j_max, -(EQ_evals(f) - EQ_evals(i)) - mode_j_min)
                        IF((E_k_max_allowed >= mode_k_min .and. E_k_max_allowed <= mode_k_max) .or. (E_k_min_allowed >= mode_k_min .and. E_k_min_allowed <= mode_k_max) .or. (E_k_min_allowed <= mode_k_min .and. E_k_max_allowed >= mode_k_max)) THEN
                            ! INTEGRAL
                            temporary = TRAPZ("++", mode_j_min, mode_j_max, config_int(11), temp(t), t, j, k, f, i, EQ_evals, g_width, fwhm, raman_numerator_L, raman_numerator_R, pho_en, pho_w, lineshape, dim)
                            if(j == k) temporary = 0.5_WP * temporary
                            total_gamma(t,f,i) = total_gamma(t,f,i) + (2.0_WP*pi/hbar)*temporary
                            decomp_save(2) = (2.0_WP*pi/hbar)*temporary
                        END IF
                    END IF

                    ! W+- term
                    ! determine range of energies for mode 2 (j or beta) based on delta function
                    E_k_min_allowed = MIN(-(EQ_evals(f) - EQ_evals(i)) + mode_j_max, -(EQ_evals(f) - EQ_evals(i)) + mode_j_min)
                    E_k_max_allowed = MAX(-(EQ_evals(f) - EQ_evals(i)) + mode_j_max, -(EQ_evals(f) - EQ_evals(i)) + mode_j_min)
                    IF((E_k_max_allowed >= mode_k_min .and. E_k_max_allowed <= mode_k_max) .or. (E_k_min_allowed >= mode_k_min .and. E_k_min_allowed <= mode_k_max) .or. (E_k_min_allowed <= mode_k_min .and. E_k_max_allowed >= mode_k_max)) THEN
                        ! INTEGRAL
                        temporary = TRAPZ("+-", mode_j_min, mode_j_max, config_int(11), temp(t), t, j, k, f, i, EQ_evals, g_width, fwhm, raman_numerator_L, raman_numerator_R, pho_en, pho_w, lineshape, dim)
                        if(j == k) temporary = 0.5_WP * temporary
                        total_gamma(t,f,i) = total_gamma(t,f,i) + (2.0_WP*pi/hbar)*temporary
                        decomp_save(3) = (2.0_WP*pi/hbar)*temporary
                    END IF

                    ! W-+ term
                    ! determine range of energies for mode 2 (j or beta) based on delta function
                    E_k_min_allowed = MIN(EQ_evals(f) - EQ_evals(i) + mode_j_max, EQ_evals(f) - EQ_evals(i) + mode_j_min)
                    E_k_max_allowed = MAX(EQ_evals(f) - EQ_evals(i) + mode_j_max, EQ_evals(f) - EQ_evals(i) + mode_j_min)
                    IF((E_k_max_allowed >= mode_k_min .and. E_k_max_allowed <= mode_k_max) .or. (E_k_min_allowed >= mode_k_min .and. E_k_min_allowed <= mode_k_max) .or. (E_k_min_allowed <= mode_k_min .and. E_k_max_allowed >= mode_k_max)) THEN
                        ! INTEGRAL
                        temporary = TRAPZ("-+", mode_j_min, mode_j_max, config_int(11), temp(t), t, j, k, f, i, EQ_evals, g_width, fwhm, raman_numerator_L, raman_numerator_R, pho_en, pho_w, lineshape, dim)
                        if(j == k) temporary = 0.5_WP * temporary
                        total_gamma(t,f,i) = total_gamma(t,f,i) + (2.0_WP*pi/hbar)*temporary
                        decomp_save(4) = (2.0_WP*pi/hbar)*temporary
                    END IF
                    
                    IF(t == 1 .and. i == 1 .and. f == 2 .and. config_logi(33)) THEN
                        WRITE(66,'(5E25.16E3)') pho_en(k), pho_en(j), fwhm(t,k), fwhm(t,j), decomp_save(4)
                    END IF

                END DO
            END DO
            IF(t == 1 .and. i == 1 .and. f == 2 .and. config_logi(33)) THEN
                CLOSE(66)
            END IF
        END DO
        !$OMP END DO NOWAIT
        !$OMP END PARALLEL

        DO t = 1,config_int(5)
            DO f = 1, dim
                DO i = f+1, dim
                    IF(f == i .or. (f <= max_r_state .and. i <= max_r_state)) CYCLE
                    total_gamma(t,f,i) = total_gamma(t,f,i) + 10.0_WP**(-200)
                    total_gamma(t,i,f) = total_gamma(t,f,i) + EXP((EQ_evals(f)-EQ_evals(i))/(kB*temp(t)))*total_gamma(t,f,i)
                END DO
            END DO
        END DO

        DEALLOCATE(parallel_index,raman_numerator_L,raman_numerator_R,linewidth,g_width)

    END IF


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Total Gamma !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! Calculate diagonal elements of total gamma
    DO t = 1, config_int(5)
        DO i = 1, max_state
            ! Diagonal Entries summing down from diagonal
            DO f = i+1, max_state
                total_gamma(t,i,i) = total_gamma(t,i,i) - total_gamma(t,f,i)
            END DO
            ! Diagonal Entries summing up from diagonal
            DO f = i-1, 1, -1
                total_gamma(t,i,i) = total_gamma(t,i,i) - total_gamma(t,f,i)
            END DO
        END DO
    END DO

END SUBROUTINE CALC_GAMMA

SUBROUTINE READ_USER_GAMMA(two_J, temp, num_temps, total_gamma, gamma_head, max_state)
    USE global, only : files
    ! Reads in user defined gamma matrices for a set of temperautres
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)               :: temp(:)
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE :: total_gamma(:,:,:)
    INTEGER, INTENT(IN)                       :: num_temps, two_J
    INTEGER, INTENT(OUT)                      :: max_state
    INTEGER                                   :: t, row, col, reason, i, f
    CHARACTER(LEN = 300)                      :: file_name
    CHARACTER(LEN = *), INTENT(IN)            :: gamma_head

    max_state = two_J + 1

    ! Allocate Gamma Matrix (temp, states, states),
    ALLOCATE(total_gamma(num_temps, two_J+1, two_J+1))
    total_gamma = 0.0_WP

    DO t = 1, num_temps
        IF (ADJUSTL(TRIM(gamma_head)) == 'EMPTY') THEN
            WRITE(file_name, '(A,I0,A)') 'gamma_matrix_',NINT(temp(t)),'_K.dat'
        ELSE
            WRITE(file_name, '(2A,I0,A)') ADJUSTL(TRIM(gamma_head)),'_',NINT(temp(t)),'_K.dat'
        END IF

        ! Check the specified input file exists
        CALL CHECK_EXISTS(TRIM(file_name))

        ! Open gamma matrix
        OPEN(33, FILE = TRIM(TRIM(files%path)//file_name), STATUS = 'OLD')
            DO row = 1, two_J + 1
                READ(33,*, IOSTAT = reason) (total_gamma(t,row,col), col = 1, two_J + 1)

                ! Check file is not corrupted or too short
                IF (reason > 0)  THEN
                    CALL WRITE_ERROR('Error reading '//&
                                     TRIM(file_name))
                ELSE IF (reason < 0) THEN
                    CALL WRITE_ERROR('Error: Reached end of '//&
                                     TRIM(file_name))
                END IF

            END DO
        CLOSE(33)

    END DO

    ! Set all diagonal elements of gamma to zero
    DO row = 1, two_J + 1
        total_gamma(:,row, row) = 0.0_WP
    END DO

    ! Recalculate diagonal elements of gamma
    DO t = 1, num_temps
        DO i = 1, two_J + 1
            ! Diagonal Entries summing down from diagonal
            DO f = i+1, two_J+1
                total_gamma(t,i,i) = total_gamma(t,i,i) - total_gamma(t,f,i)
            END DO
            ! Diagonal Entries summing up from diagonal
            DO f = i-1, 1, -1
                total_gamma(t,i,i) = total_gamma(t,i,i) - total_gamma(t,f,i)
            END DO
        END DO
    END DO

    ! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Write note about user supplied gamma matrix
    WRITE(66,*)
    CALL WARNING_HEADER('read gamma specified - gamma matrices are read from file', 66)
    WRITE(66,*)

    ! Close output file
    CLOSE(66)

END SUBROUTINE READ_USER_GAMMA

ELEMENTAL FUNCTION MODE_DISPLACEMENT(mode_energy, temperature, scale_factor) RESULT(distance)
    ! Calculates thermally averaged displacement along a given mode at a given temperature
    USE constants, only : harmonic_limit, kB
    IMPLICIT NONE
    REAL(KIND = WP), ALLOCATABLE  :: state_energies(:), state_displacements(:), state_populations(:)
    REAL(KIND = WP)               :: distance, Z
    REAL(KIND = WP), INTENT(IN)   :: mode_energy, temperature, scale_factor
    INTEGER                       :: n

    ! Allocate arrays
    ALLOCATE(state_energies(0:harmonic_limit), &
             state_displacements(0:harmonic_limit), &
             state_populations(0:harmonic_limit))

    ! Set variables to zero
    ! Partition function, thermally averaged displacement,
    !   harmonic oscillator state energies, displacements, populations
    Z                   = 0.0_WP
    distance            = 0.0_WP
    state_energies      = 0.0_WP
    state_displacements = 0.0_WP
    state_populations   = 0.0_WP

    ! Calculate harmonic energy levels of a given mode
    DO n = 0,harmonic_limit
        state_energies(n) = mode_energy*(0.5_WP+real(n,WP)) - mode_energy*0.5_WP
    END DO

    ! Calculate populations of the harmonic energy levels of a given mode
    DO n = 0,harmonic_limit
    ! Partition function for harmonic energy levels
        Z = Z + exp(-state_energies(n)/(kB * temperature))

    ! Displacement along each harmonic energy level in units of zero point displacement
        IF(n == 0) THEN
            state_displacements(n) = 1.0_WP * scale_factor! Zero point (n=0)
        ELSE
            state_displacements(n) = state_displacements(0)*(real(1+2*n,WP)**0.5_WP)
        END IF
    END DO

    ! Calculate thermally averaged displacement along mode in units of zero point displacement
    DO n = 0,harmonic_limit
    ! Calculate population of each harmonic energy level at given temperature
        state_populations(n) = exp(-state_energies(n)/(kB * temperature))/Z
    ! Calculate thermally averaged displacement
        distance = distance + state_populations(n)*state_displacements(n)
    END DO

END FUNCTION MODE_DISPLACEMENT

SUBROUTINE CALC_CF_STRENGTH(pho_data, pho_en, n_modes, temperature, config_logi, &
                                 cf_strength, scale_factor)
    ! Calculates the strength of the Delta CFPs - these are the CFPs with the equilibrium set
    !  subtracted off
    USE stev_ops, ONLY : STE_2_WYB
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)                  :: pho_data(:,:,:,:), pho_en(:), temperature, &
                                                    scale_factor(:)
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT)    :: cf_strength(:,:)
    REAL(KIND = WP), ALLOCATABLE                 :: dist_cfps(:,:,:,:)
    REAL(KIND = WP)                              :: distance
    COMPLEX(KIND = WP), ALLOCATABLE              :: wyb_dist_cfps(:,:,:,:)
    INTEGER, INTENT(IN)                          :: n_modes
    INTEGER                                      :: j, p, q
    LOGICAL, INTENT(IN)                          :: config_logi(:)

    ! Allocate array of CFPs for displacement along each mode in negative and positive directions
    ! Array is ordered as (mode (j), rank (k), order (q), pos/neg (1/2))
    ALLOCATE(dist_cfps(n_modes, 3, 13, 2))
    dist_cfps = 0.0_WP

    ! Polynomial
    ! Calculate crystal field parameters for each mode when displaced along a given mode up to the
    !  thermal displacement
    ! This is temperature dependent and uses the CFP_polynomials to calculate the value of the CFPs
    IF(config_logi(4)) THEN
        DO j = 1, n_modes

            ! Calculate thermally averaged displacement along mode at this temperature
            distance = MODE_DISPLACEMENT(pho_en(j), temperature, scale_factor(j))

            ! Calculate CFPs at positive distance defined by population of vibrational modes
            dist_cfps(j,:,:,1) = (distance**3) * pho_data(j,:,:,1) &
                               + (distance**2) * pho_data(j,:,:,2) &
                               + (distance)    * pho_data(j,:,:,3)

            ! Switch direction of distance to negative displacement
            distance = -distance

            ! Calculate CFPs at negative distance defined by population of vibrational modes
            dist_cfps(j,:,:,2) = (distance**3) * pho_data(j,:,:,1) &
                               + (distance**2) * pho_data(j,:,:,2) &
                               + (distance)    * pho_data(j,:,:,3)

        END DO

    ! Taylor
    ! Plot strength of (dB_k^q(Q_j)/dQ_j)Q_eq - use the linear term
    ELSE IF (config_logi(23)) THEN
        DO j = 1, n_modes
            dist_cfps(j,:,:,2) = pho_data(j,:,:,3)
        END DO

    ! Static
    ! Calculate crystal field parameters for each mode when displaced along a given mode up to the
    ! max thermal displacement
    ! This is temperature independent and uses the CFP_steps to calculate the value of the CFPs
    ELSE
        DO j = 1, n_modes
        ! Get CFPs from max positive displacement
            dist_cfps(j,:,:,1) = pho_data(j,:,:,1)
        END DO
        DO j = 1, n_modes
        ! Get CFPs from max negative displacement
            dist_cfps(j,:,:,2) = pho_data(j,:,:,2)
        END DO
    END IF

    ! Convert Stevens CFPs $B_k^q$ to Wybourne $B_{k,q}$ - this is needed because the strength
    ! parameters are defined
    !  using the Wybourne scheme

    ! Allocate array of Wybourne CFPs for displacement along each mode in negative
    ! and positive directions
    ! Array is ordered as (mode (j), rank (k), order (q), pos/neg (1/2))
    ALLOCATE(wyb_dist_cfps(n_modes, 3, 13, 2))
    wyb_dist_cfps = (0.0_WP, 0.0_WP)

    DO j = 1, n_modes
        DO p = 1, 2
            CALL STE_2_WYB(dist_cfps(j,:,:,p),wyb_dist_cfps(j,:,:,p))
        END DO
    END DO

    ! Calculate strength of each mode for positive and negative displacement
    !  This is the Chang Strength parameter - DOI: 10.1063/1.443530
    !  $S = \left[\frac{1}{3}\sum_{k}\frac{1}{2k+1}\sum_{q = -k}^{k}|B_{k,q}|^{2}\right]^{\frac{1}{2}}$
    !  $|B_{k,q}| = \sqrt{\text{Re}(B_{k,q})^2+\text{Im}(B_{k,q})^2}\\$

    ! Allocate array for strength parameters
    ! Array is ordered as (mode (j), pos/neg (1/2))
    ALLOCATE(cf_strength(n_modes, 2))
        cf_strength = 0.0_WP

    DO j = 1, n_modes
        DO p = 1, 2
        ! k = 2 terms
            DO q = 1, 5
                cf_strength(j,p) = cf_strength(j,p) &
                                 + (1.0_WP/5.0_WP) &
                                 * (REAL(wyb_dist_cfps(j,1,q,p), WP)**2.0_WP &
                                 +   AIMAG(wyb_dist_cfps(j,1,q,p))**2.0_WP)
            END DO
        ! k = 4 terms
            DO q = 1, 9
                cf_strength(j,p) = cf_strength(j,p) &
                                 + (1.0_WP/9.0_WP) &
                                 * (REAL(wyb_dist_cfps(j,2,q,p), WP)**2.0_WP &
                                 +   AIMAG(wyb_dist_cfps(j,2,q,p))**2.0_WP)
            END DO
        ! k = 6 terms
            DO q = 1, 13
                cf_strength(j,p) = cf_strength(j,p) &
                                 + (1.0_WP/13.0_WP) &
                                 * (REAL(wyb_dist_cfps(j,3,q,p), WP)**2.0_WP &
                                 +   AIMAG(wyb_dist_cfps(j,3,q,p))**2.0_WP)
            END DO
        END DO
    END DO

    cf_strength = cf_strength * 1.0_WP/3.0_WP

    cf_strength = cf_strength**0.5_WP

    ! Deallocate arrays
    DEALLOCATE(wyb_dist_cfps, dist_cfps)

END SUBROUTINE CALC_CF_STRENGTH


SUBROUTINE PLOT_CF_STRENGTH(cf_strength, cf_energies, temperature, save_strength)
! Plots crystal field strength parameters using external python script
    USE global, only : files
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)      :: cf_strength(:,:), temperature, cf_energies(:)
    CHARACTER(LEN = 200)             :: file_name
    INTEGER                          :: j
    LOGICAL, INTENT(IN)              :: save_strength

! Write temporary file containing strength parameters for python script to use
! Data is ordered by modes in each row and each column is direction (1 = pos), (2 = neg)

! Open file
    OPEN(77, FILE = TRIM(files%path)//'cf_strength.tmp', STATUS = 'UNKNOWN')

! Write strength for each mode
    DO j = 1, SIZE(cf_strength, 1)
        WRITE(77,'(2E18.10E2)') cf_strength(j,1), cf_strength(j,2)
    END DO

! Close file
    CLOSE(77)

! Write temporary file containing crystal field energies for python script to use

! Open file
    OPEN(77, FILE = TRIM(files%path)//'cf_energies.tmp', STATUS = 'UNKNOWN')

! Write strength for each mode
    DO j = 1, SIZE(cf_energies)
        WRITE(77,'(E18.10E2)') cf_energies(j)
    END DO

! Close file
    CLOSE(77)

! Create file name for final plot
    WRITE(file_name, '(A,I0,A)') TRIM(files%path)//'CF_strength_', NINT(temperature), '_K'

! Call plotting program
    CALL EXECUTE_COMMAND_LINE('tau_strength_plot --name '//TRIM(file_name)//' --format svg')

! Remove temporary file
    IF (.NOT. save_strength) CALL EXECUTE_COMMAND_LINE('rm '//TRIM(files%path)//'cf_strength.tmp')

END SUBROUTINE PLOT_CF_STRENGTH

RECURSIVE SUBROUTINE CALC_SP_MAT_POLY(T, pho_en, pho_data, n_modes, phonon_matrix, EQ_evecs,&
                                           OEF, stevens_ops, scale_factor, two_J)
    ! Calculates spin-phonon coupling matrix elements in the eigenbasis of the equilibrium CFPs
    ! using polynomial fits of CFPs vs distortion
    ! Matrix elements are defined as the phase corrected average of those from positive
    ! and negative displacements
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)                  :: T, pho_en(:), &
                                                    pho_data(:,:,:,:), &
                                                    OEF(:), scale_factor(:)
    COMPLEX(KIND = WP), INTENT(IN)               :: EQ_evecs(:,:), stevens_ops(:,:,:,:)
    COMPLEX(KIND = WP), INTENT(OUT)              :: phonon_matrix(:,:,:)
    COMPLEX(KIND = WP), ALLOCATABLE              :: pos(:,:), neg(:,:)
    INTEGER, INTENT(IN)                          :: n_modes, two_J

    INTEGER                                      :: k, jj, q
    REAL(KIND = WP)                              :: distance, alt_CFP(3,13), magnitude, phase


    ALLOCATE(pos(two_J + 1, two_J + 1), neg(two_J + 1, two_J + 1))
    pos           = (0.0_WP, 0.0_WP)
    neg           = (0.0_WP, 0.0_WP)
    phonon_matrix = (0.0_WP, 0.0_WP)

    DO jj = 1, n_modes
    ! Get thermally averaged displacement for current mode
        distance = MODE_DISPLACEMENT(pho_en(jj), T, scale_factor(jj))

    ! ! ! Calculate matrix elements of spin-phonon interaction for positive and negative
    !     displacement at a given temp

    ! POSITIVE

    ! Calculate CFPs at positive distance defined by population of vibrational modes
        alt_CFP(:,:) = (distance**3) * pho_data(jj,:,:,1) &
                     + (distance**2) * pho_data(jj,:,:,2) &
                     + (distance)    * pho_data(jj,:,:,3)
    ! Calculate CF matrix elements for positive displacement
        CALL GET_CF_MATRIX(alt_CFP, pos, OEF, stevens_ops)
    ! Change basis into equilibrium eigenbasis
        CALL EXPECTATION('F',EQ_evecs,pos)

    ! NEGATIVE

    ! Switch direction of distance to negative displacement
        distance = -distance
    ! Calculate CFPs at negative distance defined by population of vibrational modes
        alt_CFP(:,:) = (distance**3) * pho_data(jj,:,:,1) &
                     + (distance**2) * pho_data(jj,:,:,2) &
                     + (distance)    * pho_data(jj,:,:,3)
    ! Calculate CF matrix elements for negative displacement
        CALL GET_CF_MATRIX(alt_CFP, neg, OEF, stevens_ops)
    ! Change basis into equilibrium eigenbasis
        CALL EXPECTATION('F',EQ_evecs,neg)

    ! Average the positive and negative spin-phonon coupling
    ! matrix elements to give the final spin-phonon couplings.
    ! The phase of the positive element is used, see Nature SI
    ! $|\langle f| \hat{H}_{CF_{j}}| i /rangle | ^2 = \frac{\langle f| \hat{H}_{CF_{j}}(+Q_j)| i \rangle \langle f| \hat{H}_{CF_{j}}(-Q_j)| i \rangle}{2} \exp(i \tan^{-1} (\frac{\langle f| \hat{H}_{CF_{j}}(+Q_j)| i \rangle}{ \langle f| \hat{H}_{CF_{j}}(+Q_j)| i \rangle}))$
        DO k = 1, two_J + 1
            DO q = 1, two_J + 1
                magnitude = 0.5_WP*(SQRT(REAL(pos(k,q)*CONJG(pos(k,q)), WP)) &
                          + SQRT(REAL(neg(k,q)*CONJG(neg(k,q)), WP)))
                phase = ATAN2(AIMAG(pos(k,q)), REAL(pos(k,q),WP))
                phonon_matrix(jj,k,q) = magnitude*EXP((0.0_WP, 1.0_WP)* phase)
            END DO
        END DO
    END DO
END SUBROUTINE CALC_SP_MAT_POLY

RECURSIVE SUBROUTINE CALC_SP_MAT_STATIC(pho_data, n_modes, phonon_matrix, EQ_evecs, OEF, &
                                             stevens_ops, two_J)
    ! Calculates spin-phonon coupling matrix elements in the eigenbasis of the equilibrium CFPs
    ! using CFPs at defined limits of distortion
    ! Matrix elements are defined as the phase corrected average of those from positive and
    ! negative displacements
    IMPLICIT NONE
    COMPLEX(KIND = WP), INTENT(IN)               :: EQ_evecs(:,:), stevens_ops(:,:,:,:)
    COMPLEX(KIND = WP), INTENT(OUT)              :: phonon_matrix(:,:,:)
    COMPLEX(KIND = WP), ALLOCATABLE              :: pos(:,:), neg(:,:)
    INTEGER, INTENT(IN)                          :: n_modes, two_J
    INTEGER                                      :: k, j, q
    REAL(KIND = WP)                              :: alt_CFP(3,13), magnitude, phase
    REAL(KIND = WP), INTENT(IN)                  :: pho_data(:,:,:,:), OEF(:)

    ALLOCATE(pos(two_J + 1, two_J + 1), neg(two_J + 1, two_J + 1))
    phonon_matrix = (0.0_WP,0.0_WP)

    ! Temperature independent matrix elements without harmonics
    DO j = 1, n_modes
    ! Get CFPs from max positive displacement
        alt_CFP(:,:) = pho_data(j,:,:,1)
    ! Calculate crystal field matrix elements for positive displacement
        CALL GET_CF_MATRIX(alt_CFP,pos, OEF, stevens_ops)
    ! Similarity transform matrix into equilibrium eigenbasis
        CALL EXPECTATION('F',EQ_evecs,pos)

    ! Get CFPs from max negative displacement
        alt_CFP(:,:) = pho_data(j,:,:,2)
    ! Calculate crystal field matrix elements for negative displacement
        CALL GET_CF_MATRIX(alt_CFP, neg, OEF, stevens_ops)
    ! Similarity transform matrix into equilibrium eigenbasis
        CALL EXPECTATION('F',EQ_evecs,neg)

    ! Average the positive and negative spin-phonon coupling
    ! matrix elements to give the final spin-phonon couplings.
    ! The phase of the positive element is used, see Nature SI
    ! $|\langle f| \hat{H}_{CF_{j}}| i /rangle | ^2 = \frac{\langle f| \hat{H}_{CF_{j}}(+Q_j)| i \rangle \langle f| \hat{H}_{CF_{j}}(-Q_j)| i \rangle}{2} \exp(i \tan^{-1} (\frac{\langle f| \hat{H}_{CF_{j}}(+Q_j)| i \rangle}{ \langle f| \hat{H}_{CF_{j}}(+Q_j)| i \rangle}))$
        DO k = 1, two_J + 1
            DO q = 1, two_J + 1
                magnitude = 0.5_WP*(SQRT(REAL(pos(k,q) &
                          * CONJG(pos(k,q)),WP)) &
                          + SQRT(REAL(neg(k,q) &
                          * CONJG(neg(k,q)),WP)))
                phase = ATAN2(AIMAG(pos(k,q)), REAL(pos(k,q),WP))
                phonon_matrix(j,k,q) = magnitude * EXP((0.0_WP,1.0_WP) * phase)
            END DO
        END DO
    END DO
END SUBROUTINE CALC_SP_MAT_STATIC

RECURSIVE SUBROUTINE CALC_SP_MAT_TAYLOR(pho_data, n_modes, phonon_matrix, EQ_evecs, OEF, &
                                             stevens_ops, two_J)
    ! Calculates spin-phonon coupling matrix elements in the eigenbasis of the equilibrium CFPs
    ! using linear term of polynomial cfp fit
    ! Matrix elements are defined as the phase corrected average of those from positive and
    ! negative displacements
    IMPLICIT NONE
    COMPLEX(KIND = WP), INTENT(IN)               :: EQ_evecs(:,:), stevens_ops(:,:,:,:)
    COMPLEX(KIND = WP), INTENT(OUT)              :: phonon_matrix(:,:,:)
    COMPLEX(KIND = WP), ALLOCATABLE              :: pos(:,:)
    INTEGER, INTENT(IN)                          :: n_modes, two_J
    INTEGER                                      :: j
    REAL(KIND = WP)                              :: alt_CFP(3,13)
    REAL(KIND = WP), INTENT(IN)                  :: pho_data(:,:,:,:), OEF(:)

    ALLOCATE(pos(two_J + 1, two_J + 1))
    phonon_matrix = (0.0_WP,0.0_WP)

    ! Use Taylor series of Bkq for CF hamiltonian
    DO j = 1, n_modes
        ! Set CFPs as $(dB_k^q(Q_j)/dQ_j)_{Q_{eq}}$ where Bkq(Q_j) is the fit of Bkq for that mode
            alt_CFP(:,:) = pho_data(j,:,:,3)
        ! Calculate CF matrix elements for positive displacement
            CALL GET_CF_MATRIX(alt_CFP, pos, OEF, stevens_ops)
        ! Change basis into equilibrium eigenbasis
            CALL EXPECTATION('F',EQ_evecs,pos)
            ! if(j == 1) then
            !     write(6,*) "post diag"
            !     write(6,*) abs(pos(1,2))**2,abs(pos(2,1))**2
            ! end if
        ! Put temp array into final array
            phonon_matrix(j,:,:) = pos
    END DO

END SUBROUTINE CALC_SP_MAT_TAYLOR

RECURSIVE SUBROUTINE CALC_SPIN_PHONON_MATRIX_HARMONICS(T, pho_en, pho_data, n_modes, &
                                                            phonon_matrix, EQ_evecs, OEF, &
                                                            stevens_ops, scale_factor, two_J)
    ! Calculates spin-phonon coupling matrix elements in the eigenbasis of the equilibrium CFPs
    ! using polynomial fits of CFPs and corrections for anharmonicity
    ! Matrix elements are defined as the phase corrected average of those from positive and
    ! negative displacements
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)                  :: T, pho_en(:), &
                                                    pho_data(:,:,:,:), OEF(:), &
                                                    scale_factor(:)
    COMPLEX(KIND = WP), INTENT(IN)               :: EQ_evecs(:,:), stevens_ops(:,:,:,:)
    COMPLEX(KIND = WP), INTENT(OUT)              :: phonon_matrix(:,:,:)
    INTEGER, INTENT(IN)                          :: n_modes, two_J

    INTEGER                                      :: j
    REAL(KIND = WP)                              :: distance, alt_CFP(3,13)
    COMPLEX(KIND = WP), ALLOCATABLE              :: pos(:,:), neg(:,:)


    ALLOCATE(pos(two_J + 1, two_J + 1), neg(two_J + 1, two_J + 1))
    pos           = (0.0_WP, 0.0_WP)
    neg           = (0.0_WP, 0.0_WP)
    phonon_matrix = (0.0_WP, 0.0_WP)


    DO j = 1,n_modes
        ! Get thermally averaged displacement for current mode
        distance = MODE_DISPLACEMENT(pho_en(j), T, scale_factor(j))
        ! get first order matrix elements
        alt_CFP(:,:) = distance*pho_data(j,:,:,1) + 0.75_WP*(distance**3)*pho_data(j,:,:,3)
        CALL GET_CF_MATRIX(alt_CFP, pos, OEF, stevens_ops)
        CALL EXPECTATION('F',EQ_evecs,pos)
        phonon_matrix(j,:,:) = pos
        ! get second order matrix elements
        alt_CFP(:,:) = -0.5_WP*(distance**2)*pho_data(j,:,:,2)
        CALL GET_CF_MATRIX(alt_CFP, pos, OEF, stevens_ops)
        CALL EXPECTATION('F',EQ_evecs,pos)
        phonon_matrix(n_modes+j,:,:) = pos
        ! get third order matrix elements
        alt_CFP(:,:) = -0.25_WP*(distance**3)*pho_data(j,:,:,3)
        CALL GET_CF_MATRIX(alt_CFP, pos, OEF, stevens_ops)
        CALL EXPECTATION('F',EQ_evecs,pos)
        phonon_matrix(2*n_modes+j,:,:) = pos
    END DO

END SUBROUTINE CALC_SPIN_PHONON_MATRIX_HARMONICS

SUBROUTINE VIB_ZEROTH_ORDER(T, alt_CFP,pho_en, n_modes, pho_data, scale_factor)
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)                  :: T, pho_en(:), pho_data(:,:,:,:), scale_factor(:)
    INTEGER, INTENT(IN)                          :: n_modes

    REAL(KIND = WP)                              :: distance, alt_CFP(3,13)
    INTEGER                                      :: j

    alt_CFP = 0.0_WP

    DO j = 1,n_modes
        ! Get thermally averaged displacement for current mode
        distance = MODE_DISPLACEMENT(pho_en(j), T, scale_factor(j))
        ! get zeroth order change to CFPs
        alt_CFP(:,:) = alt_CFP(:,:) + 0.5_WP*(distance**2)*pho_data(j,:,:,2)
    END DO

END SUBROUTINE VIB_ZEROTH_ORDER

ELEMENTAL FUNCTION LUNGHI_WIDTH(mode_energy, t)
    ! Lunghi linewidth expression from nature comms, width is 0.5*fwhm of lorentzian
    USE constants, ONLY : kB
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)  :: mode_energy, t
    REAL(KIND = WP)              :: LUNGHI_WIDTH

! $\sqrt{\frac{E_j^2 \exp\left(E_j/k_B T\right)}{\left(\exp\left(E_j/k_B T\right) - 1\right)^2}}$
    LUNGHI_WIDTH = SQRT(mode_energy &
                                 * mode_energy &
                                 * EXP(mode_energy/(kB * t)) &
                                 / ((EXP(mode_energy / (kB * t)) - 1.0_WP)**2))

END FUNCTION LUNGHI_WIDTH

ELEMENTAL FUNCTION GAUSSIAN(E, mu, sigma)
    ! Calculates the value of the gaussian centered at 'mu' with width 'sigma' at the point 'E'
    USE constants, ONLY : pi
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN) :: E,mu,sigma
    REAL(KIND = WP)             :: gaussian

! $\frac{1}{\sigma\sqrt{2\pi}} e^{ -\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2 }$
    gaussian = (E-mu)/sigma
    gaussian = gaussian*gaussian
    gaussian = (1.0_WP/(sigma*sqrt(2.0_WP*pi)))*exp(-0.5_WP*gaussian)

END FUNCTION GAUSSIAN

ELEMENTAL FUNCTION LORENTZIAN(E, mu, gamma)
    ! Calculates the value of the lorentzian centered at 'mu' with fwhm 'Gamma' at the point 'E'
    USE constants, ONLY : pi
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN) :: E,mu,gamma
    REAL(KIND = WP)             :: lorentzian

    ! $\frac{\pi\Gamma}{2} \frac{1}{(E-\mu)^2+\frac{1}{4}\Gamma^2}$
    lorentzian = (0.5_WP/pi)*gamma/((E-mu)**2 + 0.25_WP*(gamma**2))

END FUNCTION LORENTZIAN

ELEMENTAL FUNCTION ANTILORENTZIAN(E, mu, gamma)
    ! Calculates the value of the antilorentzian centered at 'mu' with fwhm 'gamma' at the point 'E'
    USE constants, ONLY : pi
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN) :: E,mu,gamma
    REAL(KIND = WP)             :: antilorentzian

    antilorentzian = (pi/(2.0_WP*atan(2.0_WP*mu/gamma)))*(LORENTZIAN(E, mu, gamma) - LORENTZIAN(E, -mu, gamma))

END FUNCTION ANTILORENTZIAN

ELEMENTAL FUNCTION BOLTZMANN_OCC(energy_1, energy_2, t)
    USE constants, ONLY : kB
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)  :: energy_1, energy_2, t
    REAL(KIND = WP)              :: BOLTZMANN_OCC

! $\exp\bigg(-\frac{|\Delta E|}{k_B T}\bigg)$
    BOLTZMANN_OCC = exp(-abs(energy_2 - energy_1)/(kB*t))

END FUNCTION BOLTZMANN_OCC

ELEMENTAL FUNCTION BOSE_OCC_EMIT(energy_1, energy_2, t)
! Occupancy for phonon emission, and CF excitation
! n --> n-1 
    USE constants, ONLY : kB
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)  :: energy_1, energy_2, t
    REAL(KIND = WP)              :: BOSE_OCC_EMIT

! $\frac{1}{\exp\big(\|\Delta E|/k_B T\big) - 1}$
    BOSE_OCC_EMIT = 1.0_WP/(EXP(ABS(energy_2 - energy_1)/(kB*t)) - 1.0_WP)

    IF (energy_1 == energy_2) BOSE_OCC_EMIT = 0

END FUNCTION BOSE_OCC_EMIT

ELEMENTAL FUNCTION BOSE_OCC_ABSORB(energy_1, energy_2, t)
! Occupancy for phonon absorption, and CF decay
! n --> n+1 
    USE constants, ONLY : kB
    IMPLICIT NONE
    REAL(KIND = WP)              :: BOSE_OCC_ABSORB
    REAL(KIND = WP), INTENT(IN)  :: energy_1, energy_2, t

! $\frac{1}{1 - \exp\left(\|\Delta E|/k_B T\right)}$
    BOSE_OCC_ABSORB = 1.0_WP/(1.0_WP - EXP(-ABS(energy_2 - energy_1)/(kB*t)))

    IF (energy_1 == energy_2) BOSE_OCC_ABSORB = 0

END FUNCTION BOSE_OCC_ABSORB

RECURSIVE FUNCTION TRAPZ(term, lim1, lim2, num_steps, temp, temp_index, mode_1_index, mode_2_index, state_a, &
               state_b, cf_energies, g_width, fwhm, &
               raman_numerator_L, raman_numerator_R, pho_en, pho_w, lineshape, dim) RESULT(output)

    USE constants
    IMPLICIT NONE
    CHARACTER(LEN=2), INTENT(IN)    :: term
    REAL(KIND = WP), INTENT(IN)     :: lim1, lim2, temp, cf_energies(:), &
                                       g_width(:,:), fwhm(:,:), pho_en(:), pho_w(:)
    COMPLEX(KIND = WP), INTENT(IN)  :: raman_numerator_L(:), raman_numerator_R(:)
    REAL(KIND = WP)                 :: width, output, int_lower_limit
    INTEGER, INTENT(IN)             :: num_steps, state_a, state_b, mode_1_index, &
                                       mode_2_index, temp_index, dim
    INTEGER                         :: i, x, non_zero_index(2)
    REAL(KIND = WP), ALLOCATABLE    :: x_vals(:), y_vals(:), trapz_func_vals(:)
    CHARACTER(LEN=*), INTENT(IN)    :: lineshape

    ! Set lower limit of phonon integration to avoid singularities
    int_lower_limit = 0.0_WP

    ! Calculate x and y values at which the function shall be calculated, where x and y
    ! are the two continuous integration variables
    ALLOCATE(x_vals(num_steps + 1), &
             y_vals(num_steps + 1), &
             trapz_func_vals(num_steps + 1))
    trapz_func_vals = 0.0_WP
    x_vals = 0.0_WP
    y_vals = 0.0_WP

    x_vals(1) = lim1
    width = (lim2 - lim1)/real(config_int(11),WP)

    ! Set up grid of initial phonon energies across phonon_k bandwidth
    DO i = 2, num_steps + 1
        x_vals(i) = x_vals(i - 1) + width
    END DO

    ! Choose grid of secondary phonon energies across phonon_j bandwidth, to satisfy delta function
    IF(term == "--") THEN
        y_vals = cf_energies(state_a) - cf_energies(state_b) - x_vals
    ELSE IF(term == "++") THEN
        y_vals = -(cf_energies(state_a) - cf_energies(state_b)) - x_vals
    ELSE IF(term == "+-") THEN
        y_vals = -(cf_energies(state_a) - cf_energies(state_b)) + x_vals
    ELSE IF(term == "-+") THEN
        y_vals = cf_energies(state_a) - cf_energies(state_b) + x_vals
    END IF

    ! Make sure only integrating over non-zero phonon energies
    non_zero_index = 0
    DO i = 1, num_steps + 1
        IF(y_vals(i) >= int_lower_limit) THEN
            IF(non_zero_index(1) == 0) non_zero_index(1) = i
            non_zero_index(2) = i
        END IF
    END DO
    
    ! Calculate function for all x and all y - matrix of values
    DO x = non_zero_index(1), non_zero_index(2)
        trapz_func_vals(x) = Raman_trapz_func(term, temp, temp_index, mode_1_index, mode_2_index, x_vals(x), &
                                                y_vals(x), state_a, state_b, cf_energies, &
                                                raman_numerator_L, raman_numerator_R, g_width, fwhm, pho_en, pho_w, lineshape, dim)
    END DO

    output = 0.0_WP
    DO x = non_zero_index(1)+1, non_zero_index(2)
        output = output + 0.5_WP*(trapz_func_vals(x-1) + trapz_func_vals(x))*width
    END DO

END FUNCTION TRAPZ

RECURSIVE FUNCTION Raman_trapz_func(term, temp, temp_index, mode_1_index, mode_2_index, mode_energy_1, mode_energy_2, &
                                    state_a, state_b, cf_energies, raman_numerator_L, raman_numerator_R, &
                                    g_width, fwhm, pho_en, pho_w, lineshape, dim) RESULT(output)
    IMPLICIT NONE
    CHARACTER(LEN = 2), INTENT(IN)  :: term
    REAL(KIND = WP), INTENT(IN)     :: temp, mode_energy_1, mode_energy_2, cf_energies(:), &
                                       g_width(:,:), fwhm(:,:),  pho_en(:), pho_w(:)
    REAL(KIND = WP)                 :: output, DOS1, DOS2, PHO_k, PHO_j
    INTEGER, INTENT(IN)             :: mode_1_index, mode_2_index, state_a, state_b, temp_index,dim
    INTEGER                         :: c
    COMPLEX(KIND = WP), INTENT(IN)  :: raman_numerator_L(:), raman_numerator_R(:)
    COMPLEX(KIND = WP), ALLOCATABLE :: c_sum(:)
    COMPLEX(KIND = WP)              :: c_total
    CHARACTER(LEN=*), INTENT(IN)    :: lineshape

    output = 0.0_WP
    ! Calculate array of c-dependent terms
    ALLOCATE(c_sum(dim))
    c_sum = (0.0_WP, 0.0_WP)
    IF(term == "--") THEN
        c_sum(1:dim) = raman_numerator_L(1:dim)/(cf_energies(1:dim) - cf_energies(state_b) - mode_energy_2) + raman_numerator_R(1:dim)/(cf_energies(1:dim) - cf_energies(state_b) - mode_energy_1)
        !if(ANY(cf_energies(1:dim) - cf_energies(state_b) - mode_energy_2 == 0.0_WP) .OR. ANY(cf_energies(1:dim) - cf_energies(state_b) - mode_energy_1 == 0.0_WP)) then
	!	write(6,*) "Divide by zero in Raman denominator --:"
	!	write(6,*) "Mode 1:",mode_1_index,"Energy:",mode_energy_1
	!	write(6,*) "Mode 2:",mode_2_index,"Energy:",mode_energy_2
	!	write(6,*) "Energy gap:",abs(cf_energies(1)-cf_energies(2))
	!	return
	!end if
    ELSE IF(term == "++") THEN
        c_sum(1:dim) = raman_numerator_L(1:dim)/(cf_energies(1:dim) - cf_energies(state_b) + mode_energy_2) + raman_numerator_R(1:dim)/(cf_energies(1:dim) - cf_energies(state_b) + mode_energy_1)
        !if(ANY(cf_energies(1:dim) - cf_energies(state_b) + mode_energy_2 == 0.0_WP) .OR. ANY(cf_energies(1:dim) - cf_energies(state_b) + mode_energy_1 == 0.0_WP)) then
	!	write(6,*) "Divide by zero in Raman denominator ++:"
	!	write(6,*) "Mode 1:",mode_1_index,"Energy:",mode_energy_1
	!	write(6,*) "Mode 2:",mode_2_index,"Energy:",mode_energy_2
	!	write(6,*) "Energy gap:",abs(cf_energies(1)-cf_energies(2))
	!	return
	!end if
    ELSE IF(term == "+-") THEN
        c_sum(1:dim) = raman_numerator_L(1:dim)/(cf_energies(1:dim) - cf_energies(state_b) + mode_energy_2) + raman_numerator_R(1:dim)/(cf_energies(1:dim) - cf_energies(state_b) - mode_energy_1)
        !if(ANY(cf_energies(1:dim) - cf_energies(state_b) + mode_energy_2 == 0.0_WP) .OR. ANY(cf_energies(1:dim) - cf_energies(state_b) - mode_energy_1 == 0.0_WP)) then
	!	write(6,*) "Divide by zero in Raman denominator +-:"
	!	write(6,*) "Mode 1:",mode_1_index,"Energy:",mode_energy_1
	!	write(6,*) "Mode 2:",mode_2_index,"Energy:",mode_energy_2
	!	write(6,*) "Energy gap:",abs(cf_energies(1)-cf_energies(2))
	!	return
	!end if
    ELSE IF(term == "-+") THEN
        c_sum(1:dim) = raman_numerator_L(1:dim)/(cf_energies(1:dim) - cf_energies(state_b) - mode_energy_2) + raman_numerator_R(1:dim)/(cf_energies(1:dim) - cf_energies(state_b) + mode_energy_1)
        !if(ANY(cf_energies(1:dim) - cf_energies(state_b) - mode_energy_2 == 0.0_WP) .OR. ANY(cf_energies(1:dim) - cf_energies(state_b) + mode_energy_1 == 0.0_WP)) then
	!	write(6,*) "Divide by zero in Raman denominator -+:"
	!	write(6,*) "Mode 1:",mode_1_index,"Energy:",mode_energy_1
	!	write(6,*) "Mode 2:",mode_2_index,"Energy:",mode_energy_2
	!	write(6,*) "Energy gap:",abs(cf_energies(1)-cf_energies(2))
	!	return
	!end if
    END IF

    ! Take sum of c-dependent terms
    c_total = (0.0_WP, 0.0_WP)
    DO c = 1,dim ! intermediate step
        IF(state_a == c .or. state_b == c) CYCLE
        c_total = c_total + c_sum(c)
    END DO

    ! Calculate DOS terms
    IF (INDEX(TRIM(lineshape),'gaussian') /= 0) THEN
        DOS1 = GAUSSIAN(mode_energy_1, pho_en(mode_1_index), g_width(temp_index,mode_1_index))*config_real(5)*pho_w(mode_1_index)
        DOS2 = GAUSSIAN(mode_energy_2, pho_en(mode_2_index), g_width(temp_index,mode_2_index))*config_real(5)*pho_w(mode_2_index)
    ELSE IF (INDEX(TRIM(lineshape),'antilorentzian') /= 0) THEN
        DOS1 = ANTILORENTZIAN(mode_energy_1,pho_en(mode_1_index),fwhm(temp_index,mode_1_index))*config_real(5)*pho_w(mode_1_index)
        DOS2 = ANTILORENTZIAN(mode_energy_2,pho_en(mode_2_index),fwhm(temp_index,mode_2_index))*config_real(5)*pho_w(mode_2_index)
    ELSE IF (INDEX(TRIM(lineshape),'lorentzian') /= 0) THEN
        DOS1 = LORENTZIAN(mode_energy_1,pho_en(mode_1_index),fwhm(temp_index,mode_1_index))*config_real(5)*pho_w(mode_1_index)
        DOS2 = LORENTZIAN(mode_energy_2,pho_en(mode_2_index),fwhm(temp_index,mode_2_index))*config_real(5)*pho_w(mode_2_index)
    END IF

    ! Calculate phonon occupation terms
    IF(term == "--") THEN
        ! here UP means promotion of spin, so loss of phonon, hence -- term
        PHO_j = BOSE_OCC_EMIT(0.0_WP, mode_energy_1, temp)
        PHO_k = BOSE_OCC_EMIT(0.0_WP, mode_energy_2, temp)
    ELSE IF(term == "++") THEN
        PHO_j = BOSE_OCC_ABSORB(0.0_WP, mode_energy_1, temp)
        PHO_k = BOSE_OCC_ABSORB(0.0_WP, mode_energy_2, temp)
    ELSE IF(term == "+-") THEN
        PHO_j = BOSE_OCC_EMIT(0.0_WP, mode_energy_1, temp)
        PHO_k = BOSE_OCC_ABSORB(0.0_WP, mode_energy_2, temp)
    ELSE IF(term == "-+") THEN
        PHO_j = BOSE_OCC_ABSORB(0.0_WP, mode_energy_1, temp)
        PHO_k = BOSE_OCC_EMIT(0.0_WP, mode_energy_2, temp)
    END IF

    ! Calculate Raman transition rate
    output = ABS(c_total)**2 * PHO_j * PHO_k * DOS1 * DOS2
    ! The below term could be included to account for changes in the ZPD of the mode as the energy is integrated over the lineshape
    !* (pho_en(mode_1_index)/mode_energy_1) * (pho_en(mode_2_index)/mode_energy_2)

    DEALLOCATE(c_sum)

END FUNCTION Raman_trapz_func

PURE SUBROUTINE CALC_FIRST_STEP(first_two_states, total_gamma, first_step_states, num_temps)
! Find fastest first step of relaxation
    IMPLICIT NONE
    INTEGER, INTENT(IN)                :: first_two_states(:), num_temps
    INTEGER                            :: row
    INTEGER, INTENT(OUT), ALLOCATABLE  :: first_step_states(:,:)
    REAL(KIND = WP), INTENT(IN)        :: total_gamma(:,:,:)

! Allocate first_step_states
! Arranged as : first state, second state
    ALLOCATE(first_step_states(num_temps,2))
    first_step_states = 0

! Find initial state in first step
    first_step_states(:,1) = first_two_states(1)

! Find final state in first step - maximum in column of gamma corresponding to initial state
    DO row = 1, num_temps
        first_step_states(row,2) = maxloc(total_gamma(row, :,first_two_states(1)), 1)
    END DO

END SUBROUTINE CALC_FIRST_STEP

SUBROUTINE WRITE_FIRST_STEP_CONTRIBUTIONS(states, sm_gamma, total_gamma, &
                                          temp, num_temps, CF_Energies, n_modes, &
                                          pho_en, mJ)
    USE global, only : files
    ! Writes contributions from each mode to the first step of relaxtion
    ! at each temperature
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)                 :: sm_gamma(:,:,:,:), &
                                                   total_gamma(:,:,:), CF_Energies(:), &
                                                   pho_en(:), temp(:), mJ(:)
    REAL(KIND = WP)                             :: EGAP, contribution
    INTEGER, INTENT(IN)                         :: n_modes, states(:,:), num_temps
    INTEGER                                     :: j, t
    CHARACTER(LEN = 200)                        :: FMT

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write transitions section header
    WRITE(66,'(A)') ''
    CALL SECTION_HEADER('First Step Contributions',66)

! Loop over temp
    DO t = 1, num_temps

    ! Calculate energy gap between final and initial states
        EGAP = ABS(CF_Energies(states(t,1)) - CF_Energies(states(t,2)))

    ! Write header to indicate state numbers if not using HDF5 input
        IF(.NOT. config_logi(34)) THEN
            WRITE(66,'(A)') ''
            FMT = '(I2, A, F8.3, A, I2, A, F8.3, A, F7.2, A)'
            WRITE(66,FMT) states(t,1), ' (<J_z> = ',mJ(states(t,1)),') --> ',states(t,2), &
                          ' (<J_z> = ',mJ(states(t,2)),') (E_gap = ',EGAP,' cm-1) step'
            WRITE(66,'(A)') REPEAT('‾',75)
        END IF

    ! Reset contribution number
    contribution = 0.0_WP

    ! Write temperature header
        WRITE(66,'(A, I0, A)') 'T = ',INT(temp(t)),' K:'
        WRITE(66,'(A)') REPEAT('‾',9)
    ! Calculate % contribution from each mode and write to file
        DO j = 1, n_modes
            contribution = sm_gamma(t, j, states(t,2), states(t,1)) &
                         / total_gamma(t, states(t,2), states(t,1))
            FMT = '(A, I4, A, F7.2, A, F7.2, A, F5.1, A)'
            IF (contribution > 0.01_WP) THEN
                WRITE(66,FMT) 'Mode ',j, ' (', pho_en(j),' cm-1, D(E) = ', &
                              ABS(EGAP - pho_en(j)),' cm-1) contributes ', &
                              contribution * 100.0_WP, ' %'
            END IF
        END DO

    END DO

! Close output file
    CLOSE(66)

END SUBROUTINE WRITE_FIRST_STEP_CONTRIBUTIONS

SUBROUTINE WRITE_CONTRIBUTIONS(states, sm_gamma, total_gamma, temp, &
                               num_temps, CF_Energies, n_modes, pho_en, mJ, select)
    USE global, only : files
    ! Writes contributions from each mode to the transition initial -> final
    ! at each temperature
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)    :: sm_gamma(:,:,:,:), &
                                      total_gamma(:,:,:), CF_Energies(:), &
                                      pho_en(:), temp(:), mJ(:)
    REAL(KIND = WP)                :: EGAP, contribution
    INTEGER, INTENT(IN)            :: n_modes, states(:), num_temps
    INTEGER                        :: j, t
    CHARACTER(LEN = 200)           :: FMT
    CHARACTER(LEN = *), INTENT(IN) :: select

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write transitions section header

    WRITE(66,'(A)') ''
    WRITE(66,'(A)') ''

    IF (TRIM(select) == 'ko') CALL SECTION_HEADER('Knockout Contributions',66)
    IF (TRIM(select) == 'user') CALL SECTION_HEADER('Requested Contributions',66)

! Calculate energy gap between final and initial states
    EGAP = ABS(CF_Energies(states(1)) - CF_Energies(states(2)))

! Write header to indicate state numbers
    WRITE(66,'(A)') ''
    FMT = '(I2, A, F8.3, A, I2, A, F8.3, A, F7.2, A)'
    WRITE(66,FMT) states(1), ' (<J_z> = ',mJ(states(1)),') --> ',states(2),&
                  ' (<J_z> = ',mJ(states(2)),') (E_gap = ',EGAP,' cm-1) step'
    WRITE(66,'(A)') REPEAT('‾',75)
! Loop over temp
    DO t = 1, num_temps

    ! Reset contribution number
    contribution = 0.0_WP

    ! Write temperature header
        WRITE(66,'(A)') ''
        WRITE(66,'(A, I0, A)') 'T = ',INT(temp(t)),' K:'
        WRITE(66,'(A)') REPEAT('‾',9)

    ! Calculate % contribution from each mode and write to file
        DO j = 1, n_modes
            contribution = sm_gamma(t, j, states(2), states(1)) &
                         / total_gamma(t, states(2), states(1))
            IF (contribution > 0.01_WP) THEN
                FMT = '(A, I4, A, F7.2, A, F7.2, A, F5.1, A)'
                WRITE(66, FMT) 'Mode ',j, ' (', pho_en(j),' cm-1, D(E) = ', &
                               ABS(EGAP - pho_en(j)),' cm-1) contributes ', &
                               contribution * 100.0_WP, ' %'
            END IF
        END DO
    END DO

! Close output file
    CLOSE(66)

END SUBROUTINE WRITE_CONTRIBUTIONS


SUBROUTINE WRITE_INITIAL_PROBABILITIES(total_gamma, first_step_states, max_state, temp, num_temps)
    USE global, only : files
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)    :: total_gamma(:,:,:), temp(:)
    REAL(KIND = WP), ALLOCATABLE   :: gamma_initial(:)
    INTEGER, INTENT(IN)            :: first_step_states(2), num_temps
    INTEGER, ALLOCATABLE           :: companion(:, :)
    INTEGER                        :: t
    INTEGER, INTENT(IN)            :: max_state
    CHARACTER(LEN = 200)           :: FMT, line

    ! Exit if only two states
    IF (max_state == 2) RETURN

    ! Allocate array for inital transitions
    ALLOCATE(gamma_initial(max_state))
    gamma_initial = 0.0_WP

    ! Allocate array for sorted initial transitions
    ALLOCATE(companion(max_state, 2))
    companion = 0.0_WP

    ! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Write header for inital transitions
        WRITE(66,'(A)') ''
        WRITE(line, '(A, I0, A)') 'Top ',MIN(5, max_state),' initial transitions'
        CALL SECTION_HEADER(TRIM(line),66)
        WRITE(66,'(A)') ''

    ! Loop over temp and write initial probabilities of top 5 most likely transitions
        DO t = 1, num_temps
        ! Choose column of initial transitions
            gamma_initial = total_gamma(t, :max_state, first_step_states(1))
        ! Sort initial transitions high to low
            CALL BUBBLE(gamma_initial, companion, 'max')

        ! Write to file
            WRITE(66,'(A, I3, A)') 'T = ', INT(temp(t)), ' K'
            WRITE(66,'(A)') REPEAT('‾',9)
            FMT = '(A, I2, A, E45.36E4, A)'
            DO j = 1, MIN(5, max_state)
                WRITE(66,FMT) 'State 1 to state ', companion(j,2), ' gamma = ', &
                               gamma_initial(j), ' s^-1'
            END DO
            WRITE(66,*)

        END DO
! Close output file
    CLOSE(66)

END SUBROUTINE WRITE_INITIAL_PROBABILITIES

SUBROUTINE CALC_BARRIER_TEMPS(user_barrier_temps, num_user_temps, t_high, t_high_index, t_det,&
                              t_det_index, barrier_temps, barrier_temps_indices, num_barriers)
    IMPLICIT NONE
    INTEGER, INTENT(IN)                       :: user_barrier_temps(:,:), num_user_temps, &
                                                 t_high_index, t_det_index
    INTEGER, INTENT(OUT)                      :: num_barriers
    INTEGER, INTENT(OUT), ALLOCATABLE         :: barrier_temps_indices(:)
    INTEGER, ALLOCATABLE                      :: companion(:,:), Itemp(:)
    INTEGER                                   :: j, nut, k
    REAL(KIND = WP), INTENT(IN)               :: t_high, t_det
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE :: barrier_temps(:)


    ! Calculate how many barriers will exist in total - num_barriers

    ! Check if t_high and t_det are the same
    IF (t_high == t_det) THEN
        num_barriers = 1
    ELSE
        num_barriers = 2
    END IF


    ! Check how many barriers have been requested and if they are the same as t_high or t_det
    IF (num_user_temps > 0) THEN
        nut = num_user_temps

        DO j = 1, num_user_temps
            IF (REAL(user_barrier_temps(j,1),WP) == t_high &
                .OR. REAL(user_barrier_temps(j,1),WP) == t_det) THEN
                nut = nut - 1
            END IF
        END DO

        num_barriers = num_barriers + nut

    END IF

    ! Allocate final list of barriers and temperature indices using the total number of barriers
    ALLOCATE(barrier_temps(num_barriers), barrier_temps_indices(num_barriers))
    barrier_temps = 0.0_WP
    barrier_temps_indices = 0


    ! Fill barrier temperature and barrier temperature index lists with values
    IF (t_high == t_det) THEN
        barrier_temps(1) = t_high
        barrier_temps_indices(1) = t_high_index
    ELSE
        barrier_temps(1) = t_high
        barrier_temps_indices(1) = t_high_index
        barrier_temps(2) = t_det
        barrier_temps_indices(2) = t_det_index
    END IF

    IF (num_user_temps > 0) THEN

        k = num_barriers - nut + 1

        DO j = 1, num_user_temps
            IF (REAL(user_barrier_temps(j,1),WP) /= t_high &
                .OR. REAL(user_barrier_temps(j,1),WP) /= t_det) THEN
                barrier_temps(k) = REAL(user_barrier_temps(j,1),WP)
                barrier_temps_indices(k) = user_barrier_temps(j,2)
            END IF
        END DO
    END IF


    ! Reorder barrier temperature and barrier temperature index lists so they start with high
    ! temperature and go to low temperature
    AlLOCATE(companion(num_barriers,2), Itemp(num_barriers))
    companion = 0
    Itemp = 0

    ! Reorder temp high to low
    CALL BUBBLE(barrier_temps, companion, 'max')

    ! Change barrier_temps_indices to match order above
    DO j = 1, num_barriers
        Itemp(j) = barrier_temps_indices(companion(j,2))
    END DO

    barrier_temps_indices = Itemp

    DEALLOCATE(companion, ITemp)

END SUBROUTINE CALC_BARRIER_TEMPS

SUBROUTINE RELAXATION_PATHWAYS(two_J, input_eigenvectors, energy, input_gamma, t, state_labels, &
                               save_format)
    USE global, only : files
    ! Calculates relaxation pathways
    ! Evolves population over time given unit population in lowest energy state
    ! Does not allow backwards or downwards transitions, so gives pathway of relaxation
    IMPLICIT NONE
    INTEGER                         :: i, j, k, state, minimal_trans, pathway_number, num_trans, &
                                       next_state, current_state, num_steps, step, col, row
    INTEGER, INTENT(IN)             :: two_J
    CHARACTER(LEN = 500)            :: FMT,FMTT, NUMBERS
    CHARACTER(LEN = *), INTENT(IN)  :: save_format
    INTEGER, ALLOCATABLE            :: companion(:,:), trans_from_state(:), route(:,:)
    REAL(KIND = WP)                 :: total, speed
    REAL(KIND = WP), INTENT(IN)     :: energy(:), input_gamma(:,:), t, state_labels(:)
    REAL(KIND = WP), ALLOCATABLE    :: mJ(:), eigenvectors(:,:), sm_gamma(:,:), &
                                       sm_gamma_save(:,:), input_state_labels(:)
    COMPLEX(KIND = WP), INTENT(IN)  :: input_eigenvectors(:,:)

! Open output file
        OPEN(66, FILE = TRIM(files%output), POSITION = 'APPEND')

! Allocate arrays
    ALLOCATE(mJ(two_J + 1), &
             eigenvectors(two_J + 1, two_J + 1), &
             sm_gamma(two_J + 1, two_J + 1), &
             sm_gamma_save(two_J + 1, two_J + 1))
! Zero arrays
    mJ                     = 0.0_WP
    eigenvectors           = 0.0_WP
    sm_gamma      = 0.0_WP
    sm_gamma_save = 0.0_WP

! Make copy of gamma matrix
! This is modified during evolution
    sm_gamma = input_gamma

! Calculate eigenvectors as percentage compositions
    DO i = 1, two_J + 1
        DO k = 1, two_J + 1
            eigenvectors(i,k) = real(input_eigenvectors(i,k),WP)**2 + aimag(input_eigenvectors(i,k))**2
        END DO
    END DO

! Make vector of mJ values
! -mJ to +mJ
    DO k = 1,two_J + 1
        mJ(k) = -real(two_J, WP)/2.0_WP + (k-1)*1.0_WP
    END DO

! Remove all diagonal elements and transitions backwards over the barrier or downwards between states
    DO i = 1,two_J + 1 ! from
        DO j = 1,two_J + 1 ! to
            IF (i == j) sm_gamma(j,i) = 0.0_WP                            ! No diagonal elements!
            IF (state_labels(i) > state_labels(j)) sm_gamma(j,i) = 0.0_WP ! No backwards or downwards steps
        END DO
    END DO


! Normalise each column so transition probability is a fraction of 1
    DO i = 1,two_J + 1
        total = 0.0_WP
        DO j = 1,two_J + 1
            total = total + sm_gamma(j,i)
        END DO
        IF (total > 0.0_WP) sm_gamma(:,i) = sm_gamma(:,i)/total
    END DO

! Find indexing which relates the current arrangement of the array state_labels to the arrangement it would
! have if it was written in descending order (largest first)
! This is done because our pathway consists only of transitions which increase (-ve to eventually +ve)
! the magnetic state_labels
    ALLOCATE(companion(two_J + 1,2), input_state_labels(two_J + 1))
    input_state_labels = state_labels
    CALL BUBBLE(input_state_labels, companion,'min')

! Calculate how much goes into a state starting with most negative state_labels
! Then scale all transitions out of that state by how much goes in
! sm_gamma(:,list(1)) is already 1
    DO i = 2, two_J + 1
        total = 0.0_WP

        DO j = i-1, 1, -1
            total = total + sm_gamma(companion(i,2),companion(j,2))
        END DO

        sm_gamma(:,companion(i,2)) = sm_gamma(:,companion(i,2))*total

    END DO

! Scale matrix to be a percentage
    sm_gamma = 100.0_WP*sm_gamma
    sm_gamma_save = sm_gamma

! Write barrier section header
    CALL SECTION_HEADER('Relaxation Barrier',66)
    WRITE(66,'(A)') ''

! Write relaxation pathway header
    WRITE(66,'(A, I3, A)') 'Relaxation pathway information at T = ', NINT(t),' K'
    WRITE(66,'(A)')REPEAT('‾',43)

! Write state to state rate matrix to file
    WRITE(66,'(A)') 'Relaxation rate matrix'
    WRITE(66,'(A)') 'Written as % of population from an initial unit population (100%) in most negative mJ state'
    WRITE(66,*)

! Convoluted method to write state numbers above calculated mJ values
    WRITE(FMTT,*) '(I2,A',(',I2,A',i = 2, two_J + 1),')'
    WRITE(NUMBERS,TRIM(FMTT)) (i,'    ',i = 1, two_J + 1)
    WRITE(66,'(2A)') 'State No.             ', TRIM(NUMBERS)

! Write header rows for matrix - contains calc. mJ val. and state number
    WRITE(FMT,'(A,I0,A)') '(A, ',two_J + 1,'F6.1)'
    WRITE(66,TRIM(FMT)) '       Calc. <J_z> ', state_labels(:)

! Formatting for each row of matrix of variable size
    WRITE(FMT,'(A, I0, A)') '(I2, A, F5.1, A, ', two_J + 1,'F6.1)'

    WRITE(66,*)
    DO row = 1, two_J + 1
        WRITE(66,FMT) row,'      ', state_labels(row), '     ',(sm_gamma(row,col), col = 1, two_J + 1)
    END DO


! Calculate pathways over the barrier
    ALLOCATE(trans_from_state(two_J + 1))
    ALLOCATE(route(two_J + 1,two_J + 1))

! Calculate number of transitions out of each state
    DO col = 1, two_J + 1
        trans_from_state(col) = NUM_GREATER_THAN_EQ_TO(sm_gamma(:,col), 10.0_WP)
    END DO

! Calculate the minimum number of transitions present
! as the number of states which have transitions moving outwards
    minimal_trans = NUM_GREATER_THAN_EQ_TO(trans_from_state,1)

    WRITE(66,*)
    WRITE(66,'(A)') 'Writing pathways with population change >10%'

! Index for how many paths have been calculated
    pathway_number = 0

! Write relaxation pathways over barrier to file
    DO WHILE (.TRUE.)

    ! Keep count of number of pathways
    ! Exit if more 5 pathways written
        pathway_number = pathway_number + 1
        IF(pathway_number > 15) THEN
            WRITE(66,'(A)') 'Too many pathways, check matrix, ABORTING...'
            EXIT
        END IF

    ! Calculate number of transitions out of a given state
        DO col = 1, two_J + 1
            trans_from_state(col) = NUM_GREATER_THAN_EQ_TO(sm_gamma(:,col), 10.0_WP)
        END DO

    ! reset and recalculate number of paths
        num_trans = SUM(trans_from_state)

        WRITE(66,* )
        WRITE(66,'(A, I0)') 'Pathway number ', pathway_number
        WRITE(66,'(A)') REPEAT('‾',16)


    ! start in state 1
        current_state = 1

        step = 0
        route = 0
    ! Write transitions over barrier for this path
    ! Quit if the other side (state 2) is reached
        speed = 0.0_WP
        DO WHILE (current_state /= 2)

        ! Catch infinite loop if other side of barrier (state 2) cannot be reached
            step = step + 1
            IF (step > 10) THEN
                WRITE(66,*) 'Cannot find other side of barrier, ABORTING...'
                EXIT
            END IF

        ! Find next state as maximum in column of gamma corresponding to current state
            next_state = maxloc(sm_gamma(:,current_state),1)
        ! Write step to file
            FMT = '(I2, A, I2, A, F5.1, A, E47.36E4)'
            WRITE(66,FMT) current_state, ' --> ', next_state, ' with ', &
                          sm_gamma(next_state, current_state), '% probability - rate = ', &
                          input_gamma(next_state,current_state)

            speed = speed + 1.0_WP/input_gamma(next_state,current_state)
        ! Mark which transition has been used
            route(step,1) = current_state
            route(step,2) = next_state
        ! Advance state label
            current_state = next_state

        END DO
        WRITE(66,'(A)') ''
        WRITE(66,'(A, E47.36E4, A)') 'Overall rate = ', 1.0_WP/speed, ' s^-1'

        num_steps = step

    ! Use route array to remove used up transitions by setting gamma for that transition to zero
    ! Remove only if this transition does not lead to unused transitions
        DO state = num_steps - 1, 1, -1
            col = route(state,1)
            row = route(state,2)
            IF (trans_from_state(col) > 1) THEN
                sm_gamma(row, col) = 0.0_WP
                EXIT
            END IF
        END DO


    ! Exit when all paths found
        IF(num_trans == minimal_trans) THEN
            EXIT
        END IF

    END DO

! Reload clean gamma
    sm_gamma = sm_gamma_save

! Plot barrier figures

! Write gamma matrix, energies, and mJ values, then plot
    CALL PLOT_BARRIER_FIGURE(two_J, state_labels, energy, sm_gamma, t, save_format)

! Close output file
    CLOSE(66)

! CALL ALL_PATHWAYS(input_gamma, two_J, t, state_labels)

END SUBROUTINE RELAXATION_PATHWAYS

SUBROUTINE ALL_PATHWAYS(sm_gamma, two_J, t, state_labels)
! Calculates all possible pathways
    USE global, only : files
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)  :: sm_gamma(:,:), t, state_labels(:)
    REAL(KIND = WP), ALLOCATABLE :: input_state_labels(:), total_rates(:), total_times(:)
    INTEGER, INTENT(IN)          :: two_J
    INTEGER, ALLOCATABLE         :: set(:), subsets(:,:), companion(:,:), temp_arr(:), temp_mat(:,:)
    INTEGER                      :: i, start, finish, k, num_subsets, current, previous

! Make array containing all the numbers of intermediate states
!  excluding the initial and final states
    start = 1
    finish = 2


    DO i = 1, two_J + 1
        CALL ARRAY_APPEND(set,i)
    END DO

! Reorder <J_z> to be -ve to +ve
    ALLOCATE(companion(two_J + 1,2), input_state_labels(two_J + 1))
    input_state_labels = state_labels
    CALL BUBBLE(input_state_labels, companion,'min')

! Use companion to reorder the state labels in set in order of ascending <J_z>
    ALLOCATE(temp_arr(SIZE(set,1)))
    temp_arr = 0
    temp_arr = set
    set  = 0
    k = 0
    DEALLOCATE(set)
    AlLOCATE(set(two_J - 1))
    DO i = 1, SIZE(companion,1)
        IF (companion(i,2) == start .OR. companion(i,2) == finish) CYCLE
        k = k + 1
        set(k) = temp_arr(companion(i,2))
    END DO

    DEALLOCATE(companion)
    DEALLOCATE(temp_arr)

! Calculate total number of subsets
    num_subsets = 2**(two_J - 1)

! Find all possible subsets of intermediate states
    CALL GET_SUBSETS(set, subsets)

! These will all, by definition, be the pathways with increasing <J_z>
! Loop over each of the subsets and calculate the overall rate
    ALLOCATE(total_times(num_subsets))
    total_times = 0.0_WP

    DO i = 1, num_subsets

        previous = start
        DO current = 1, SIZE(subsets,2)

        ! If a state label of zero is found then skip!
            IF (subsets(i,current) == 0) CYCLE

        ! Add on the time for this step
            total_times(i) = total_times(i) + 1.0_WP/sm_gamma(subsets(i,current),previous)
            previous = subsets(i,current)

        END DO

    ! Add final step
        total_times(i) = total_times(i) + 1.0_WP/sm_gamma(finish,previous)

    END DO


! Sort the list of total times
    ALLOCATE(companion(num_subsets,2))
    CALL BUBBLE(total_times, companion,'min')

! Use companion to sort the list of subsets into the same order as above
    ALLOCATE(temp_mat(SIZE(subsets, 1), SIZE(subsets, 2) ))
    temp_mat = subsets
    subsets  = 0
    DO i = 1, SIZE(companion,1)
        subsets(i,:) = temp_mat(companion(i,2), :)
    END DO
    DEALLOCATE(temp_mat)

! Convert times back to rates
    ALLOCATE(total_rates(num_subsets))
    total_rates = 0.0_WP

    DO i = 1, num_subsets
        total_rates(i) = 1.0_WP/total_times(i)
    END DO


! Print the top 5 relaxation pathways
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

        WRITE(66,'(A)')          '*******************************************************'
        WRITE(66,'(A, F5.1, A)') 'Top 5 pathways calculated by brute force at T = ', t, ' K'
        WRITE(66,'(A)')          '*******************************************************'

        DO i = 1, 5

            WRITE(66,'(A, I0)') 'Pathway Number ', i
            WRITE(66,'(A)') REPEAT('‾',16)

            previous = start
            DO current = 1, SIZE(subsets,2)

            ! If a state label of zero is found then skip!
                IF (subsets(i,current) == 0) CYCLE
                WRITE(66,'(I2, A, I2, A, E47.36E4, A)') previous, ' --> ' ,subsets(i,current), ' with rate ', sm_gamma(subsets(i,current),previous), ' s^-1'
                previous = subsets(i,current)

            END DO

            WRITE(66,'(I2, A, I2, A, E47.36E4, A)') previous, ' --> ' ,finish, ' with rate ', sm_gamma(finish,previous), ' s^-1'
            WRITE(66,'(A)') ''
            WRITE(66,'(A, E47.36E4)') 'Overall rate = ', total_rates(i), ' s^-1'
            WRITE(66,'(A)') ''

        END DO
    CLOSE(66)


END SUBROUTINE ALL_PATHWAYS

SUBROUTINE PLOT_BARRIER_FIGURE(two_J, state_labels, energy, sm_gamma, t, save_format)
! Plots barrier figure with transition probabilties from the evolved gamma matrix
! Calls external python script to do plotting
    IMPLICIT NONE
    CHARACTER(len = 200)            :: FMT, plotname
    INTEGER                         :: row, col
    INTEGER, INTENT(IN)             :: two_J
    REAL(KIND = WP), INTENT(IN)     :: state_labels(:), energy(:), sm_gamma(:,:), t
    CHARACTER(LEN = *), INTENT(IN)  :: save_format

! Write state_labels to temporary file
    OPEN(77, FILE = 'mag.dat')
        WRITE(77,*) state_labels
    CLOSE(77)

! Write energies to temporary file
    OPEN(77, FILE = 'energy.dat')
        WRITE(77,*) energy
    CLOSE(77)

! Write sm_gamma of probability weighted transitions to temporary file
    OPEN(77, FILE = 'trans.dat')
        WRITE(FMT, '(A,I0,A)') '(',two_J + 1,'E47.36E4)'
        DO row = 1, two_J + 1
            WRITE(77,TRIM(FMT)) (sm_gamma(row, col), col = 1, two_J + 1)
        END DO
    CLOSE(77)

! Create final plot name
    WRITE(plotname, '(A,I0,A)') 'barrier_figure_',NINT(t),'_K'

! Call python script to plot barrier figure
    CALL EXECUTE_COMMAND_LINE('tau_barrier --Name '//TRIM(plotname)//' --format '//TRIM(save_format))

! Remove temporary files
    CALL EXECUTE_COMMAND_LINE('rm -f mag.dat trans.dat energy.dat')

! Write status to output file
    WRITE(66,'(A)') ''
    WRITE(66,'(A, I3, A, I0, 2A)') 'Barrier figure plotted for T = ',NINT(t),' K and written to barrier_figure_',NINT(t),'_K.',ADjUSTL(TRIM(save_format))
    WRITE(66,'(A)') ''

END SUBROUTINE PLOT_BARRIER_FIGURE

SUBROUTINE GET_SUBSETS(set,subsets)
    USE matrix_tools
    IMPLICIT NONE
    INTEGER, INTENT(IN)            :: set(:)
    INTEGER,ALLOCATABLE            :: binlist(:,:), subsets(:,:), arr(:)
    INTEGER                        :: i, n
    INTEGER(KIND = 8)              :: s

! Number of elements of the set
    n = SIZE(set)

! Build all possible binary arrays
!  Essentially an array of yes and no which is
!  then used to select the subsets
    DO i = 1, 2**n
    ! Deallocate temporary array arr
        IF (ALLOCATED(arr)) DEALLOCATE(arr)

    ! Convert i-1 into a binary number --> s
    ! e.g. 3 = 10
    ! e.g. 4 = 11
    ! e.g. 5 = 100
    ! Note that large numbers require KIND = 8 Integers for s
        CALL DEC2BIN(i - 1, s)

    ! Convert binary number into an array of individual digits
    ! e.g. 10 --> 1,0
    ! e.g. 11 --> 1,1
    ! e.g. 100 --> 1,0,0
        CALL MAKE_LIST(s, arr)

    ! Append the binary number array to a big set of arrays
    ! Note, this list will have trailing zeros, as fortran arrays must be regular
    ! e.eg
        CALL ARRAY_APPEND(binlist, arr)
    END DO

!  Use binary array to calculate all subsets
    CALL SELECT_VALUES(binlist, set, subsets)

END SUBROUTINE GET_SUBSETS

PURE SUBROUTINE DEC2BIN(n, s)
    USE matrix_tools
! Converts number n to binary number s
! e.g. 3 = 10
! e.g. 4 = 11
! e.g. 5 = 100
    IMPLICIT NONE
    INTEGER(KIND = 8), INTENT(OUT)    :: s
    INTEGER,           INTENT(IN)     :: n
    INTEGER                           :: ncopy, r
    INTEGER(KIND = 8)                 :: i

! Initialise variables
    s = 0
    i = 1

! Create a copy of n which can be modified
    ncopy = n

! Keep dividing by 10 and adding the modulo*index to s
! This will eventually change the number to binary
    DO WHILE (ncopy /= 0)
        r = MODULO(ncopy, 2)
        s = s + (r * i)
        ncopy = ncopy / 2
        i = i * 10
    END DO

END SUBROUTINE DEC2BIN


SUBROUTINE MAKE_LIST(s, a)
    USE matrix_tools
! Turns the binary number s into an array a containing its constituent digits
    IMPLICIT NONE
    INTEGER, INTENT(INOUT), ALLOCATABLE :: a(:)
    INTEGER(KIND = 8), INTENT(INOUT) :: s

! If the number is zero then just append a zero
    IF (s == 0) THEN
        CALL ARRAY_APPEND(a, 0)
    END IF

! If the number is not zero then loop over and strip out each digit
    DO WHILE (s>0)
        CALL ARRAY_APPEND(a, INT(MODULO(s,10_8),4))
        s = s / 10
    END DO

    ! a = FLIPUD(a)
END SUBROUTINE MAKE_LIST

SUBROUTINE SELECT_VALUES(bin, l, out)
    USE matrix_tools
! Use the binary array as a mask to make an
!  array of values chosen from the list l
    IMPLICIT NONE
    INTEGER, INTENT(IN)                   :: bin(:,:), l(:)
    INTEGER, INTENT(INOUT), ALLOCATABLE   :: out(:,:)
    INTEGER                               :: i, j
    INTEGER, ALLOCATABLE                  :: ITemp(:)


! Loop over each row of bin and create the
!  corresponding row of selected digits from l
!  Then append this row to the output array
    DO i = 1, SIZE(bin,1)
        DO j = 1, SIZE(bin,2)
            IF (bin(i,j)== 1) THEN
                CALL ARRAY_APPEND(ITemp,l(j))
            ELSE
                CALL ARRAY_APPEND(ITemp, 0)
            END IF
        END DO
        CALL ARRAY_APPEND(out, ITemp)
        DEALLOCATE(ITemp)
    END DO

END SUBROUTINE SELECT_VALUES

PURE SUBROUTINE CUT_TRANSITIONS(sm_gamma, total_gamma, transitions_logical, num_temps, two_J)
! Sets transition rates to zero for requested transitions
! Then recalculates the diagonal elements of gamma so that the rates from
! diagonalisation make sense
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(INOUT)   :: sm_gamma(:,:,:,:)
    REAL(KIND = WP), INTENT(INOUT)   :: total_gamma(:,:,:)
    LOGICAL, INTENT(IN)              :: transitions_logical(:,:)
    INTEGER                          :: i, f, t
    INTEGER, INTENT(IN)              :: num_temps, two_J

! Remove requested transitions from each gamma matrix for each mode
! This will remove both up and down transitions due how transitions_logical is defined
    DO i = 2, two_J + 1
        DO f = 1, i - 1
            IF (.NOT. transitions_logical(i,f)) THEN
                sm_gamma(:,:,i,f) = 0.0_WP
                sm_gamma(:,:,f,i) = 0.0_WP
            END IF
        END DO
    END DO

! Set total gamma to zero
    total_gamma = 0.0_WP

! Sum gammas for each mode into the true total gamma
    total_gamma = SUM(sm_gamma, 2)

! Re-zero the diagonal
    DO t = 1, config_int(5)
        DO i = 1, two_J+1
            total_gamma(t,i,i) = 0.0_WP
        END DO
    END DO

! Calculate diagonal elements of true gamma
    DO t = 1, num_temps
        DO i = 1, two_J + 1
        ! Diagonal Entries summing down from diagonal
            DO f = i+1, two_J+1
                total_gamma(t,i,i) = total_gamma(t,i,i) - total_gamma(t,f,i)
            END DO
        ! Diagonal Entries summing up from diagonal
            DO f = i-1, 1, -1
                total_gamma(t,i,i) = total_gamma(t,i,i) - total_gamma(t,f,i)
            END DO
        END DO
    END DO


END SUBROUTINE CUT_TRANSITIONS

SUBROUTINE KNOCKOUT_TRANSITIONS(max_state, state_labels, sm_gamma, num_temps, temp, &
                                t_min, t_step, t_max, knockout_rates, knockout_pairs, &
                                num_pairs, select_MM_diag)
! Loops over all pairs of transitions, sets the probability of moving between them to zero
! then recalculates the rates and stores them
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(INOUT)             :: sm_gamma(:,:,:,:)
    REAL(KIND = WP), INTENT(IN)                :: temp(:), state_labels(:)
    REAL(KIND = WP), ALLOCATABLE               :: sm_gamma_mod(:,:,:,:), vals(:,:)
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE  :: knockout_rates(:,:)
    INTEGER, INTENT(OUT), ALLOCATABLE          :: knockout_pairs(:,:)
    INTEGER, INTENT(OUT)                       :: num_pairs
    INTEGER                                    :: i, f, t, lower, upper, pair_number, row
    INTEGER, INTENT(IN)                        :: max_state, num_temps, t_max, t_step, t_min
    LOGICAL, INTENT(IN)                        :: select_MM_diag

! Count how many pairs will be calculated
    pair_number = 0

    DO upper = 2, max_state
        DO lower = 1, upper - 1

        ! Skip backwards steps as this means we double count
        ! both backwards and forwards are set to zero for lower->upper
            IF (state_labels(lower) > state_labels(upper)) CYCLE

        ! Increase current pair number by one
            pair_number = pair_number + 1

        END DO
    END DO


! Allocate array for knockout rates and knockout state indices
    num_pairs = pair_number !INT(NCR(2, max_state),4)
    ALLOCATE(knockout_rates(num_temps, num_pairs), knockout_pairs(num_pairs, 2),&
             vals(num_temps, max_state))
    knockout_rates = 0.0_WP
    knockout_pairs = 0
    pair_number = 0
    vals        = 0


! Make copy of gamma which is edited
    ALLOCATE(sm_gamma_mod(SIZE(sm_gamma,1), &
                                   SIZE(sm_gamma,2), &
                                   SIZE(sm_gamma,3), &
                                   SIZE(sm_gamma,4)))

    DO upper = 2, max_state
        DO lower = 1, upper - 1

        ! Skip backwards steps as this means we double count
        ! both backwards and forwards are set to zero for lower->upper
            IF (state_labels(lower) > state_labels(upper)) CYCLE

        ! Increase current pair number by one
            pair_number = pair_number + 1

        ! Record indices of current pair
            knockout_pairs(pair_number,1) = lower
            knockout_pairs(pair_number,2) = upper

        ! Copy gamma from master version
            sm_gamma_mod = sm_gamma

        ! Set elements of current transition to zero
            sm_gamma_mod(:,:, lower, upper) = 0.0_WP
            sm_gamma_mod(:,:, upper, lower) = 0.0_WP

        ! Set total gamma to zero
            total_gamma = 0.0_WP

        ! Sum gammas for each mode into the true total gamma
            total_gamma = SUM(sm_gamma_mod, 2)

        ! Set all diagonal elements of gamma to zero
            DO row = 1, max_state
                total_gamma(:,row, row) = 0.0_WP
            END DO

        ! Calculate diagonal elements of true gamma
            DO t = 1, num_temps
                DO i = 1, max_state
                ! Diagonal Entries summing down from diagonal
                    DO f = i+1, max_state
                        total_gamma(t,i,i) = total_gamma(t,i,i) &
                                                   - total_gamma(t,f,i)
                    END DO
                ! Diagonal Entries summing up from diagonal
                    DO f = i-1, 1, -1
                        total_gamma(t,i,i) = total_gamma(t,i,i) &
                                                   - total_gamma(t,f,i)
                    END DO
                END DO

                IF (select_MM_diag) THEN
                    ! Write gamma matrix to temporary file
                    CALL WRITE_GAMMA_MATRIX_FILE(total_gamma(t,:,:), temp(t), &
                                                 .TRUE.)
                END IF

            END DO

            IF (select_MM_diag) THEN
                ! Diagonalise gamma for each temperature with mathematica and read in rates
                CALL DIAGONALISE_GAMMA_MM(max_state, num_temps, t_min, t_step, t_max, .TRUE., &
                                          .TRUE., vals)
            ELSE
                ! Diagonalise gamma with C++
                CALL DIAGONALISE_GAMMA_CPP(max_state, total_gamma, num_temps, vals)
            END IF
            knockout_rates(:, pair_number) = vals(:, max_state-1)

        END DO
    END DO

END SUBROUTINE KNOCKOUT_TRANSITIONS

SUBROUTINE PRINT_TOP_TRANSITIONS(temp, num_temps, knockout_pairs, knockout_rates, num_pairs,&
                                 top_trans_unique)
    USE global, only : files
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(INOUT)     :: knockout_rates(:,:)
    REAL(KIND = WP), INTENT(IN)        :: temp(:)
    INTEGER, ALLOCATABLE               :: top_trans(:,:,:)
    INTEGER, INTENT(OUT), ALLOCATABLE  :: top_trans_unique(:,:)
    INTEGER, INTENT(INOUT)             :: knockout_pairs(:,:)
    INTEGER, INTENT(IN)                :: num_temps, num_pairs
    INTEGER                            :: t, p
    INTEGER, ALLOCATABLE               :: companion(:,:)
    CHARACTER(LEN = 200)               :: FMT

    AlLOCATE(top_trans(num_temps, 5, 2))
    top_trans = 0

    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

        WRITE(66,*)
        CALL SECTION_HEADER('Knockout',66)
        WRITE(66,*)
        WRITE(66,'(A)')' By sequential deletion of the corresponding elements of gamma and subsequent '
        WRITE(66,'(A)')' rediagonalisation,  the following five transitions were found to be the most '
        WRITE(66,'(A)')' important to the relaxation rate for each listed temperature.'
        WRITE(66,*)
        WRITE(66,'(A)')' For each transition the new (and hopefully slower) relaxation rate is printed'
        WRITE(66,*)

        ! Create companion array used for sorting rates
        ALLOCATE(companion(num_pairs, 2))

        ! Loop over each temperature, bubble sort the rates for each deleted pair and print the slowest 5
        DO t = 1, num_temps

            ! Sort rates from slow to fast
            companion = 0
            CALL BUBBLE(knockout_rates(t,:), companion, 'min')

            ! Write temperature header
            WRITE(66,'(A, I3, A)') 'T = ',NINT(temp(t)),' K'
            WRITE(66,'(A)') REPEAT('‾',9)

            ! Write top five most important steps
            FMT = '(A, I3, A, I3, A, E47.36E4, A)'
            DO p = 1, 5
                WRITE(66,FMT) 'State ', knockout_pairs(companion(p,2),1),' to state ', &
                              knockout_pairs(companion(p,2),2), '  tau^-1 = ', &
                              knockout_rates(t,p), ' s^-1'

                ! Record transition indices for top 5
                top_trans(t, p, 1) =  knockout_pairs(companion(p,2),1)
                top_trans(t, p, 2) =  knockout_pairs(companion(p,2),2)

            END DO

            ! Blank separating line
            WRITE(66,*)
        END DO

    CLOSE(66)

    IF (num_temps > 1) THEN
        CALL UNIQUE(top_trans, top_trans_unique)
    ELSE
        AlLOCATE(top_trans_unique(5,2))
        top_trans_unique = 0
        top_trans_unique = top_trans(1,:,:)
    END IF

END SUBROUTINE PRINT_TOP_TRANSITIONS

SUBROUTINE UNIQUE(matrix, unique_vals)
    IMPLICIT NONE
    INTEGER, INTENT(IN)               :: matrix(:,:,:)
    INTEGER, ALLOCATABLE, INTENT(OUT) :: unique_vals(:,:)
    INTEGER, ALLOCATABLE              :: temporary(:,:)
    INTEGER                           :: row, col, fd, num_unique, add_on

    ALLOCATE(unique_vals(SIZE(matrix,1)*SIZE(matrix,2),2))
    unique_vals = 0
    num_unique = SIZE(matrix,2)

    unique_vals(1:num_unique,:) = matrix(1,:,:)

    DO row = 2, SIZE(matrix, 1)
        DO col = 1, SIZE(matrix, 2)

            add_on = 1

            DO fd = 1, SIZE(unique_vals,1)

                IF (unique_vals(fd,1) == 0) CYCLE

                IF (matrix(row, col, 1) == unique_vals(fd,1) &
                    .AND. matrix(row, col, 2) == unique_vals(fd,2)) THEN
                    add_on = 0

                END IF

            END DO

            IF (add_on == 1) THEN

                num_unique = num_unique + 1
                unique_vals(num_unique,1) = matrix(row, col, 1)
                unique_vals(num_unique,2) = matrix(row, col, 2)

            END IF

        END DO
    END DO

    ALLOCATE(temporary(num_unique, 2))
    temporary = unique_vals(1:num_unique,:)
    DEALLOCATE(unique_vals)
    ALLOCATE(unique_vals(num_unique, 2))
    unique_vals = temporary
    DEALLOCATE(temporary)

END SUBROUTINE UNIQUE


PURE SUBROUTINE WEIGHT_ORBACH_COMPONENTS(max_state, num_temps, n_modes, sm_gamma, &
                                    sm_sp, sm_occ, sm_pdos, &
                                    total_gamma, mode_weighted_sp, mode_weighted_occ, &
                                    mode_weighted_pdos, mode_weighted_gamma, effective_n_modes)
    ! Weights SP, DOS, OCC, and Gamma arrays for each temperature for individual modes by their %
    !  contribution to the total gamma for each temperature
    USE constants, ONLY : pi, hbar
    IMPLICIT NONE
    INTEGER, INTENT(IN)                          :: num_temps, n_modes, max_state
    INTEGER                                      :: t, j
    REAL(KIND = WP), INTENT(INOUT)               :: sm_sp(:,:,:,:), &
                                                    sm_occ(:,:,:,:), &
                                                    sm_pdos(:,:,:,:), &
                                                    sm_gamma(:,:,:,:)
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT)    :: mode_weighted_sp(:,:,:), &
                                                    mode_weighted_occ(:,:,:), &
                                                    mode_weighted_pdos(:,:,:), &
                                                    mode_weighted_gamma(:,:,:), &
                                                    effective_n_modes(:,:,:)
    REAL(KIND = WP), ALLOCATABLE                 :: percentage(:,:,:,:)
    REAL(KIND = WP), INTENT(IN)                  :: total_gamma(:,:,:)


    ALLOCATE(           percentage(num_temps, n_modes, max_state, max_state), &
                  mode_weighted_sp(num_temps, max_state, max_state), &
                 mode_weighted_occ(num_temps, max_state, max_state), &
                mode_weighted_pdos(num_temps, max_state, max_state), &
               mode_weighted_gamma(num_temps, max_state, max_state), &
                   effective_n_modes(num_temps, max_state, max_state))

    percentage          = 0.0_WP
    mode_weighted_sp    = 0.0_WP
    mode_weighted_occ   = 0.0_WP
    mode_weighted_pdos  = 0.0_WP
    mode_weighted_gamma = 0.0_WP
    effective_n_modes     = 0.0_WP


    ! Calculate % contribution of each mode to total gamma at each temperature
    DO t = 1, num_temps
        DO j = 1, n_modes
            percentage(t,j,:,:) = sm_gamma(t,j,:,:) / total_gamma(t,:,:)
        END DO
    END DO


    ! Calculate mode-weighted SP, PDOS and OCC matrices as the sum of
    ! single mode SP, PDOS and OCC multiplied by that mode's % contribution to gamma
    DO t = 1, num_temps
        DO j = 1, n_modes
            mode_weighted_sp(t,:,:)    = mode_weighted_sp(t,:,:) &
                                       + sm_sp(t,j,:,:) &
                                       * percentage(t,j,:,:)
            mode_weighted_occ(t,:,:)   = mode_weighted_occ(t,:,:) &
                                       + sm_occ(t,j,:,:) &
                                       * percentage(t,j,:,:)
            mode_weighted_pdos(t,:,:)  = mode_weighted_pdos(t,:,:) &
                                       + sm_pdos(t,j,:,:) &
                                       * percentage(t,j,:,:)
        END DO
    END DO

    ! Calculate mode_weighted_gamma as 2pi/hbar * SP * OCC * PDOS
    DO t = 1, num_temps
        mode_weighted_gamma(t,:,:)    = (2.0_WP*pi/hbar) &
                                      * mode_weighted_sp(t,:,:) &
                                      * mode_weighted_occ(t,:,:) &
                                      * mode_weighted_pdos(t,:,:)
    END DO

    ! Calculate effective number of modes as ratio of mode-weighted gamma to total gamma
    DO t = 1, num_temps
        effective_n_modes(t,:,:) = total_gamma(t,:,:) &
                                   / mode_weighted_gamma(t,:,:)
    END DO

    DO j =1, max_state
        effective_n_modes(:,j,j) = 0.0_WP
    END DO


END SUBROUTINE WEIGHT_ORBACH_COMPONENTS

SUBROUTINE WRITE_WEIGHTED_ORBACH_COMPONENTS(temp, num_temps, &
                                            mode_weighted_sp, mode_weighted_occ, &
                                            mode_weighted_pdos, mode_weighted_gamma, &
                                            effective_n_modes)
    USE global, only : files
    IMPLICIT NONE
    ! Writes files containing the mode weighted CF, DOS, OCC, and Gamma matrices for each
    ! temperature
    INTEGER, INTENT(IN)             :: num_temps
    INTEGER                         :: t
    REAL(KIND = WP), INTENT(IN)     :: mode_weighted_sp(:,:,:), mode_weighted_occ(:,:,:), &
                                       mode_weighted_pdos(:,:,:), mode_weighted_gamma(:,:,:), &
                                       effective_n_modes(:,:,:), temp(:)
    CHARACTER(LEN = 500)            :: file_name

    DO t = 1, num_temps

        WRITE(file_name,'(A, I0, A)') TRIM(files%path)//'mode_weighted_sp_',NINT(temp(t)),'_K.dat'
        CALL WRITE_ARRAY(array = mode_weighted_sp(t,:,:), fmt='E47.36E4', file = file_name)

        WRITE(file_name,'(A, I0, A)') TRIM(files%path)//'mode_weighted_occ_',NINT(temp(t)),'_K.dat'
        CALL WRITE_ARRAY(array = mode_weighted_occ(t,:,:), fmt='E47.36E4', file = file_name)

        WRITE(file_name,'(A, I0, A)') TRIM(files%path)//'mode_weighted_pdos_',NINT(temp(t)),'_K.dat'
        CALL WRITE_ARRAY(array = mode_weighted_pdos(t,:,:), fmt='E47.36E4', file = file_name)

        WRITE(file_name,'(A, I0, A)') TRIM(files%path)//'mode_weighted_gamma_',NINT(temp(t)),'_K.dat'
        CALL WRITE_ARRAY(array = mode_weighted_gamma(t,:,:), fmt='E47.36E4', file = file_name)

        WRITE(file_name,'(A, I0, A)') TRIM(files%path)//'effective_n_modes_',NINT(temp(t)),'_K.dat'
        CALL WRITE_ARRAY(array = effective_n_modes(t,:,:), fmt='E47.36E4', file = file_name)

    END DO

    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
        WRITE(66,*)
        CALL WARNING_HEADER('mode weighted specified - components written to file', 66)
        WRITE(66,*)
    CLOSE(66)

END SUBROUTINE WRITE_WEIGHTED_ORBACH_COMPONENTS

SUBROUTINE WRITE_ORBACH_COMPONENTS(num_single_modes, single_modes, temp, &
                                   num_temps, sm_gamma, sm_sp, &
                                   sm_occ, sm_pdos)
    ! Writes files containing the CF, DOS, OCC, and Gamma matrices for each temperature for
    ! individual modes
    USE global, only : files
    IMPLICIT NONE
    INTEGER, INTENT(IN)         :: num_single_modes, single_modes(:), num_temps
    INTEGER                     :: t, j
    REAL(KIND = WP), INTENT(IN) :: sm_sp(:,:,:,:), sm_occ(:,:,:,:), &
                                   sm_pdos(:,:,:,:), sm_gamma(:,:,:,:), &
                                   temp(:)
    CHARACTER(LEN = 500)        :: file_name, FMT

    FMT = '(A, I0, A, I0, A)'


    ! Write arrays at each temperature for each mode
    DO t = 1, num_temps

        DO j = 1, num_single_modes

            WRITE(file_name,FMT) TRIM(files%path)//'sp_mode_',single_modes(j),'_',NINT(temp(t)),'_K.dat'
            CALL WRITE_ARRAY(array = sm_sp(t,single_modes(j),:,:), fmt='E47.36E4', file = file_name)

            WRITE(file_name,FMT) TRIM(files%path)//'occ_mode_',single_modes(j),'_',NINT(temp(t)),'_K.dat'
            CALL WRITE_ARRAY(array = sm_occ(t,single_modes(j),:,:), fmt='E47.36E4', file = file_name)

            WRITE(file_name,FMT) TRIM(files%path)//'dos_mode_', single_modes(j),'_',NINT(temp(t)),'_K.dat'
            CALL WRITE_ARRAY(array = sm_pdos(t,single_modes(j),:,:), fmt='E47.36E4', file = file_name)

            WRITE(file_name,FMT) TRIM(files%path)//'gamma_mode_',single_modes(j),'_',NINT(temp(t)),'_K.dat'
            CALL WRITE_ARRAY(array = sm_gamma(t,single_modes(j),:,:), fmt='E47.36E4', file = file_name)

        END DO

    END DO

    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
        WRITE(66,*)
        CALL WARNING_HEADER('single mode specified - single mode components are written to file', 66)
        WRITE(66,*)
    CLOSE(66)

END SUBROUTINE WRITE_ORBACH_COMPONENTS

SUBROUTINE CALC_DETAILED_BALANCE(max_state, total_gamma, EQ_evals, temp, detailed_value, num_temps)
    ! Calculates detailed balance value for a given temperature by summing
    ! the ratio of each pair of diagonally opposite gamma matrix elements minus
    ! the boltzmann population of an energy equal to the difference between their
    ! respective energies
    !$|\frac{\gamma_{fi}}{\gamma_{if}} - \exp{\frac{E_i - E_f}{k_B T}}|$
    USE constants, ONLY : kB
    USE global, only : files
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)               :: total_gamma(:,:,:), EQ_evals(:), temp(:)
    REAL(KIND = WP), INTENT(OUT), ALLOCATABLE :: detailed_value(:)
    REAL(KIND = WP), ALLOCATABLE              :: detailed_matrix(:,:,:)
    INTEGER, INTENT(IN)                       :: max_state, num_temps
    INTEGER                                   :: i, f, t

    ! Allocate detailed_balance value array
    ALLOCATE(detailed_value(num_temps))
    ALLOCATE(detailed_matrix(num_temps, max_state, max_state))
    detailed_value = 0.0_WP
    detailed_matrix = 0.0_WP

    DO t = 1, num_temps
        DO i = 1, max_state
            DO f = i, max_state
                IF(i == f) cycle
                IF(total_gamma(t,i,f) /= 0.0_WP) THEN
                    detailed_matrix(t, i, f) = ABS(total_gamma(t,f,i)/total_gamma(t,i,f) &
                                             - EXP((EQ_evals(i)-EQ_evals(f))/(kB*temp(t))))
                    detailed_matrix(t, f, i) = ABS(total_gamma(t,i,f)/total_gamma(t,f,i) &
                                             - EXP((EQ_evals(f)-EQ_evals(i))/(kB*temp(t))))
                    detailed_value(t) = detailed_value(t) &
                                      + ABS(total_gamma(t,f,i)/total_gamma(t,i,f) &
                                      - EXP((EQ_evals(i)-EQ_evals(f))/(kB*temp(t))))
                ELSE IF(total_gamma(t,f,i) == 0.0_WP .AND. &
                        total_gamma(t,i,f) == 0.0_WP) THEN
                    detailed_value(t) = detailed_value(t) &
                                      + ABS(1.0_WP &
                                      - EXP((EQ_evals(i)-EQ_evals(f))/(kB*temp(t))))
                    detailed_matrix(t, i, f) = ABS(1.0_WP - EXP((EQ_evals(i)-EQ_evals(f)) &
                                             / (kB*temp(t))))
                    detailed_matrix(t, f, i) = ABS(1.0_WP - EXP((EQ_evals(f)-EQ_evals(i)) &
                                             / (kB*temp(t))))
                END IF
            END DO
        END DO
    END DO

    IF (debug) THEN
        CALL WRITE_ARRAY(array = detailed_matrix(1,:,:), fmt='E47.36E4', file=TRIM(files%path)//'detailed_matrix.dat')
    END IF


END SUBROUTINE CALC_DETAILED_BALANCE

SUBROUTINE CHECK_DETAILED_BALANCE(detailed_value, temp, num_temps)
    USE global, only : files
! Checks whether detailed balance is obeyed at each temperature using an array of
! detailed balance values
! these values are provided from the PURE SUBROUTINE CALC_DETAILED_BALANCE
! Detailed balance is obeyed when detailed value > 1
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)  :: detailed_value(:), temp(:)
    INTEGER, INTENT(IN)          :: num_temps
    INTEGER                      :: i
    LOGICAL                      :: detailed_broken
    CHARACTER(LEN = 500)         :: FMT

    ! Set flag to mark that detailed balance is broken at some point
    detailed_broken = .FALSE.

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! If the first temperature value does not obey detailed balance then terminate
    ! program and inform user - not an error termination as this can occur when
    ! fwhm is too small - e.g. when autau feeds tau a small value
    ! Instead a stop code of 5 is returned as autau knows to pick up on this as
    ! a detailed balance violation
        IF (detailed_value(1) > 1.0_WP ) THEN
            IF (.NOT. debug) THEN
                CALL WRITE_ERROR('Detailed balance is not obeyed at any temp', RC=5)
            END IF
        END IF

    ! Report temp at which detailed balance is observed
        DO i = 2, num_temps
            IF ((detailed_value(i) > 1.0_WP) .AND. .NOT. detailed_broken) THEN
                detailed_broken = .TRUE.

            ! Write to output file
                FMT = '(A, I3, A, I3, A)'
                WRITE(66,FMT) '****             Detailed balance is obeyed from ', &
                              NINT(temp(1)),' K to ', NINT(temp(i - 1)), &
                              ' K             ****'
            END IF
        END DO

    ! Not broken
    IF (.NOT. detailed_broken) THEN

        ! Write to output file
        WRITE(66,'(A)') ''
        FMT = '(A, I3, A, I3, A)'
        WRITE(66,FMT) '****             Detailed balance is obeyed from ', NINT(temp(1)),&
                      ' K to ', NINT(temp(num_temps)), ' K             ****'

    END IF

! Close output file
    CLOSE(66)

END SUBROUTINE CHECK_DETAILED_BALANCE

SUBROUTINE WRITE_DETAILED_BALANCE(detailed_value, temp, num_temps)
    USE global, only : files
    ! Writes temperature and detailed balance values to output file
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN) :: detailed_value(:), temp(:)
    INTEGER, INTENT(IN)         :: num_temps

! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Write header for detailed balance section header
        WRITE(66,'(A)') ''
        CALL SECTION_HEADER('Detailed Balance', 66)
        WRITE(66,'(A)') ''
        WRITE(66,'(A)') '             If detailed balance is obeyed, the value will be zero'
        WRITE(66,'(A)') ''
        CALL SUB_SECTION_HEADER('Temperature (K)   Detailed Balance', 66)

    ! Write detailed balance values for each temperature
        DO i = 1, num_temps
            WRITE(66,'(F5.1, E47.36E4)') temp(i), detailed_value(i)
        END DO
        WRITE(66,*)

! Close output file
    CLOSE(66)

END SUBROUTINE WRITE_DETAILED_BALANCE

SUBROUTINE WRITE_GAMMA_MATRIX_FILE(total_gamma, t, ko)
    ! Writes Gamma for a given temperature to file
    USE global, only : files
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN) :: total_gamma(:,:), t
    CHARACTER(LEN = 3)          :: charT
    CHARACTER(LEN = 200)        :: file_name
    LOGICAL, INTENT(IN)         :: ko

! Write temperature to character
    WRITE(charT, '(I0)') NINT(t)

! Create file name
    IF (ko) THEN
        WRITE(file_name, '(3A)') TRIM(files%path)//'gamma_matrix_ko_'//TRIM(charT)//'_K.dat'
    ELSE
        WRITE(file_name, '(3A)') TRIM(files%path)//'gamma_matrix_'//TRIM(charT)//'_K.dat'
    END IF

! Open file for gamma matrix at specific temperature
    CALL WRITE_ARRAY(array = total_gamma, fmt = 'E47.36E4', file = file_name)

END SUBROUTINE WRITE_GAMMA_MATRIX_FILE

SUBROUTINE DIAGONALISE_GAMMA_MM(max_state, num_temps, t_min, t_step, t_max, keep, ko, rates)
    ! Writes out mathematica script used to diagonalise gamma matrix
    ! Only diagonalises gamma for which detailed balance is maintained
    ! Runs Mathematica script
    ! Deletes gamma matrices and mathematica script if debug is false
    ! Reads in eigenvalues of gamma
    IMPLICIT NONE
    INTEGER, INTENT(IN)          :: t_step, t_max, t_min, num_temps, max_state
    CHARACTER(LEN = 100)         :: MM_CALL_COMMAND
    REAL(KIND = WP), INTENT(OUT) :: rates(:,:)
    LOGICAL, INTENT(IN)          :: keep, ko

#ifdef macos
    MM_CALL_COMMAND = 'WolframScript'
#else
    MM_CALL_COMMAND = 'math'
#endif

! Open mathematica script file
    OPEN(77, FILE = 'qp_mathematica.m', STATUS = 'UNKNOWN')

    ! Write mathematica script file
        WRITE(77,'(A)') '(* ::Package:: *)'
        WRITE(77,'(A)')
        WRITE(77,'(A)') '(*Functions to read in the rates from tau*)'
        WRITE(77,'(A)')
        IF (ko) THEN
            WRITE(77,'(A)') 'readmat[t_]:=SetPrecision[Import[StringJoin[Directory[],"/gamma_matrix_ko_",ToString[t],"_K.dat"],"Table"],36];'
        ELSE
            WRITE(77,'(A)') 'readmat[t_]:=SetPrecision[Import[StringJoin[Directory[],"/gamma_matrix_",ToString[t],"_K.dat"],"Table"],36];'
        END IF
        WRITE(77,'(A)') 'Alllntau[t_,n_]:={1/t,Log[1/(Abs[Re[Eigenvalues[readmat[t]]]][[n]])]};'
        WRITE(77,'(A)') 'Allrate[t_,n_]:={t,Abs[Re[Eigenvalues[readmat[t]]]][[n]]};'
        WRITE(77,'(A)') 'AllVecs[t_]:={t,Transpose[Eigenvectors[readmat[t]]]};'
        WRITE(77,'(A)')
        WRITE(77,'(A)') '(*temperature steps*)'
        WRITE(77,'(A,I0,A)') 'lowT=',INT(t_min),';'
        WRITE(77,'(A,I0,A)') 'Tstep=',ABS(t_step),';'
        WRITE(77,'(A,I0,A)') 'HighT = ',t_max,';'
        WRITE(77,'(A)') 'Tlist=Table[t,{t,lowT,HighT,Tstep}];'
        WRITE(77,'(A)')
        WRITE(77,'(A,I0,A)') 'allrates=Table[Allrate[t,n], {n,1,', max_state,',1},{t,lowT,HighT,Tstep}];'
        WRITE(77,'(A,I0,A)') 'Export[StringJoin[Directory[],"/all_rates_Mathematica.dat"], Transpose[Flatten[Table[Transpose[allrates[[n]]],{n,1,', max_state,'}],1]],"Table",Alignment->Left];'
        WRITE(77,'(A)')
        WRITE(77,'(A)') '(* ::Input:: *)'
        WRITE(77,'(A)') '(**)'

! Close mathematica script file
    CLOSE(77)

! Call system to run this within the CALL to tau
    CALL EXECUTE_COMMAND_LINE(TRIM(MM_CALL_COMMAND)//' -script qp_mathematica.m')

! Delete gamma matrices and mathematica script
    IF (.not. keep)  CALL EXECUTE_COMMAND_LINE('rm -f gamma_matrix_*')
    IF (.not. debug) CALL EXECUTE_COMMAND_LINE('rm -f qp_mathematica.m')
    IF (ko)     CALL EXECUTE_COMMAND_LINE('rm -f gamma_matrix_ko_*')

! Read in relaxation rates
    CALL READ_RELAXATION_RATES_MM(max_state-1, rates, num_temps)

END SUBROUTINE DIAGONALISE_GAMMA_MM


SUBROUTINE DIAGONALISE_GAMMA_CPP(max_state, total_gamma, num_temps, rates)
    ! Diagonalises gamma matrices using external c++ function
    USE fort_diag
    USE OMP_LIB
    IMPLICIT NONE
    INTEGER, INTENT(IN)          :: num_temps
    INTEGER                      :: t, max_state
    INTEGER(KIND = C_INT)        :: arr_len
    INTEGER, ALLOCATABLE         :: companion(:,:)
    REAL(KIND = WP), INTENT(IN)  :: total_gamma(:,:,:)
    REAL(KIND = WP), ALLOCATABLE :: vecs(:,:,:), vals(:,:), curr_gamma(:,:), curr_vecs(:,:), &
                                    curr_vals(:), imag_vecs(:, :), imag_vals(:)
    REAL(KIND = WP), INTENT(OUT) :: rates(:,:)

    ALLOCATE(vecs(num_temps,max_state, max_state), vals(num_temps,max_state), companion(max_state,2))
    vecs = 0.0_WP
    vals = 0.0_WP
    rates = 0.0_WP

    arr_len = INT(max_state, C_INT)

    !!$OMP PARALLEL SHARED(total_gamma, vecs, vals, arr_len)
     !   !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, companion, curr_gamma, curr_vecs, curr_vals, imag_vals, imag_vecs)
            DO t = 1, num_temps

                ALLOCATE(curr_gamma(max_state, max_state))
                ALLOCATE(curr_vecs(max_state, max_state))
                ALLOCATE(imag_vecs(max_state, max_state))
                ALLOCATE(imag_vals(max_state))
                ALLOCATE(curr_vals(max_state))

                curr_gamma = total_gamma(t,:max_state,:max_state)
                curr_vecs  = 0.0_WP
                curr_vals  = 0.0_WP
                imag_vecs = 0.0_WP
                imag_vals = 0.0_WP

                ! Call c function to diagonalise Gamma
                CALL real_c_diag(arr_len, curr_gamma, curr_vecs, curr_vals, imag_vecs, imag_vals)

                IF (ANY(imag_vals /= 0.0_WP)) THEN
                    WRITE(6,*) 'Imaginary eigenvalues detected'
                END IF

                ! Eigenvalues are -tau so make them positive
                curr_vals = curr_vals * (-1.0_WP)

                ! Sort eigenvalues from big to small
                CALL BUBBLE(curr_vals, companion, 'max')

                vecs(t,:,:) = curr_vecs
                vals(t,:) = curr_vals

                DEALLOCATE(curr_gamma, curr_vecs, curr_vals, imag_vals, imag_vecs)

            END DO
        !!$OMP END DO NOWAIT
    !!$OMP END PARALLEL
    rates = vals

END SUBROUTINE DIAGONALISE_GAMMA_CPP

SUBROUTINE READ_RELAXATION_RATES_MM(max_state, rates, num_temps)
    ! Read in relaxation rates from mathematica output file
    ! File is arranged as rows of (temperature, rate) pairs for the max_state eigenvalues
    !  of the gamma matrix
    ! The eigenvalues are written in descending order - so the over barrier rate is given
    !  by the penultimate pair
    USE matrix_tools
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(OUT) :: rates(:,:)
    REAL(KIND = WP)              :: dum
    INTEGER, INTENT(IN)          :: num_temps, max_state
    INTEGER                      :: row, col, reason


! Open mathematica rates file
    OPEN(33, FILE = 'all_rates_Mathematica.dat', STATUS = 'OLD')
    ! Read mathematica rates file
        DO row = 1, num_temps
            ! Read the rates from each line - loop over the fast rates and store the
            ! penultimate rate
            READ(33,*, IOSTAT = reason) (dum, rates(row, col), col = 1, max_state)
            ! Check file isnt corrupted or ends too soon
            IF (reason > 0)  THEN
                CALL WRITE_ERROR('Error reading all_rates_Mathematica.dat')
            ELSE IF (reason < 0) THEN
                CALL WRITE_ERROR('Error: Reached end of all_rates_Mathematica.dat too soon')
            END IF
        END DO

! Close mathematica file
    CLOSE(33)

! Delete mathematica rates file
    IF (.NOT. debug) CALL EXECUTE_COMMAND_LINE('rm -f all_rates_Mathematica.dat'//&
                                         ' slow_rates_Mathematica.dat')

! Flip rates upside down
    DO col = 1, max_state
        rates(:, col) = FLIPUD(rates(:, col))
    END DO

END SUBROUTINE READ_RELAXATION_RATES_MM

SUBROUTINE WRITE_RATES_AND_DBALANCE(eq_rates, slow_rates, dbalance, num_temps, temp)
    USE global, only : files
    ! Write a set of rates to ouput file
    ! temperature (K) and Tau^-1 (s^-1)
    IMPLICIT NONE
    REAL(KIND = WP), INTENT(IN)    :: eq_rates(:), slow_rates(:), temp(:), dbalance(:)
    INTEGER, INTENT(IN)            :: num_temps
    INTEGER                        :: row
    CHARACTER(LEN = 500)           :: head, desc, FMT

    FMT = '(F5.1, A, E47.36E4, A, E47.36E4, A, E47.36E4)'
! Open output file
    OPEN(66, FILE = TRIM(files%output), STATUS = 'UNKNOWN', POSITION = 'APPEND')
! Write rates section header
    WRITE(66,'(A)') ''
    CALL SECTION_HEADER(TRIM('Rates and Detailed balance'), 66)
    WRITE(66,'(A)') ''
    WRITE(66,'(A)') 'Temperature (K)    Detailed Balance    Eq. Tau^-1 (s^-1)    Slow Tau^-1 (s^-1)'
    WRITE(66,'(A)') REPEAT('-',79)
    ! Write rate at each temperature
    DO row = 1, num_temps
        IF(dbalance(row) <= 1.0_WP) WRITE(66,FMT) temp(row), '   ', dbalance(row), '   ', eq_rates(row), '   ', slow_rates(row)
    END DO
! Close output file
    CLOSE(66)

    FMT = '(F5.1, A, F10.5, A, I1, A, F10.5, A, F10.5)'
! Open output file for CC-FIT2 input
    OPEN(66, FILE = TRIM(files%cc_output), STATUS = 'UNKNOWN')
! Write header
    WRITE(66,'(A)') 'T 	<ln(tau)>	sigma_ln(tau)	fit_upper_ln(tau)	fit_lower_ln(tau)'
! Write rates
    DO row = 1, num_temps
        IF(dbalance(row) <= 1.0_WP) WRITE(66,FMT) temp(row), ' ', log(1.0_WP/slow_rates(row)), ' ', 0, ' ', log(1.0_WP/slow_rates(row)), ' ',log(1.0_WP/slow_rates(row))
    END DO
! Close output file
    CLOSE(66)
END SUBROUTINE WRITE_RATES_AND_DBALANCE

FUNCTION CHECK_SPACES(str) RESULT(num_spaces)
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: str
    CHARACTER(LEN = LEN(str))      :: keep_str
    INTEGER                        :: num_spaces

    ! Move extra spaces and commas
    keep_str = replace_space_comma(str)

    ! Now count number of spaces
    num_spaces = count_char(keep_str, ' ')

END FUNCTION CHECK_SPACES

PURE SUBROUTINE LOWERCASE(str)
    ! Converts str to lowercase characters
    IMPLICIT NONE
    INTEGER                           :: i,gap
    CHARACTER(LEN = *), INTENT(INOUT) :: str

    gap = ICHAR('a')-ICHAR('A')
    IF(len(str) > 0) THEN
        DO i=1,len(str)
            IF(str(i:i) <= 'Z') THEN
                IF(str(i:i) >= 'A') str(i:i) = CHAR(ICHAR(str(i:i)) + gap)
            END IF
        END DO
    END IF

END SUBROUTINE LOWERCASE

PURE SUBROUTINE removenumbers(str)
    ! Removes numbers from str
    IMPLICIT NONE
    INTEGER                           :: i
    CHARACTER(LEN = *), INTENT(INOUT) :: str

    IF(len(str) > 0) THEN
        DO i=1,len(str)
            IF(str(i:i) <= '9') THEN
                IF(str(i:i) >= '0') str(i:i)=' '
            END IF
        END DO
    END IF

END SUBROUTINE removenumbers

FUNCTION char_to_char_array(str) RESULT(out_str_array)
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN)         :: str
    CHARACTER(LEN = LEN(str)), ALLOCATABLE :: out_str_array(:)
    INTEGER, ALLOCATABLE                   :: num_entries, i

    num_entries = count_char(ADJUSTL(TRIM(str)), ' ') + 1
    ALLOCATE(out_str_array(num_entries))
    READ(str, *) out_str_array
    DO i = 1, num_entries
        IF (count_char(TRIM(ADJUSTL(out_str_array(i))), '-') == 1) THEN
            out_str_array(i) = replace_text(TRIM(ADJUSTL(out_str_array(i))), '-', ' ',&
                                            count_char(TRIM(ADJUSTL(str)), '-'))
        END IF
    END DO

END FUNCTION char_to_char_array

FUNCTION replace_space_comma(str) RESULT(out_str)
    IMPLICIT NONE
    CHARACTER(LEN = *)        :: str
    CHARACTER(LEN = LEN(str)) :: out_str

    out_str = str

    ! Replace triple space with single space
    ! out_str = Replace_Text(trim(adjustl(out_str)),'   ',' ', count_char(TRIM(ADJUSTL(out_str)), '   '))

    ! Replace double space with single space
    ! out_str = Replace_Text(trim(adjustl(out_str)),'  ',' ', count_char(TRIM(ADJUSTL(out_str)), '  '))

    ! Replace ' ,'
    out_str = Replace_Text(trim(adjustl(out_str)),' ,',',', &
                           count_char(TRIM(ADJUSTL(out_str)), ' ,'))

    ! Replace ','
    out_str = Replace_Text(trim(adjustl(out_str)),',',' ', &
                           count_char(TRIM(ADJUSTL(out_str)), ','))

    ! Replace triple space with single space
    out_str = Replace_Text(trim(adjustl(out_str)),'   ',' ', &
                           count_char(TRIM(ADJUSTL(out_str)), '   '))

    ! Replace double space with single space
    out_str = Replace_Text(trim(adjustl(out_str)),'  ',' ', &
                           count_char(TRIM(ADJUSTL(out_str)), '  '))

END FUNCTION replace_space_comma

FUNCTION replace_text (str, old_expr, new_expr, num_occ) RESULT(str_mod)
    ! Replaces old_expr in str with new_expr
    IMPLICIT NONE
    INTEGER                                                                 :: i, num_occ
    CHARACTER(LEN = *), INTENT(IN)                                          :: str, old_expr, &
                                                                               new_expr
    CHARACTER(LEN = LEN(str))                                               :: str_keep
    CHARACTER(LEN = LEN(str)+(num_occ*ABS(LEN(old_expr) - LEN(new_expr))))  :: str_mod

    str_mod = str

        DO i=1,len(str_keep)-(len(old_expr)-1)
            str_keep = str_mod
            IF (str_keep(i:i+len(old_expr)-1) == old_expr) THEN
                str_mod = str_keep(:i-1)//new_expr//str_keep(i+len(old_expr):)
            END IF
        END DO

END FUNCTION replace_text

FUNCTION count_char (str, expr) RESULT(num)
    ! counts number of times expr occurs in str
    IMPLICIT NONE
    INTEGER                        :: i, num
    CHARACTER(LEN = *), INTENT(IN) :: str, expr

    num = 0
    DO i=1,len(str)-(len(expr)-1)
        IF (str(i:i+len(expr)-1) == expr) THEN
            num = num + 1
        END IF
    END DO

END FUNCTION count_char

FUNCTION GET_TRIMMED_VAL(str, loc_1, loc_2) RESULT(str_out)
    !  Returns the substring located at loc_1:loc_2 of str after
    !  str has been trimmed and adjustled
    IMPLICIT NONE
    INTEGER, INTENT(IN)                :: loc_1, loc_2
    CHARACTER(LEN = *), INTENT(IN)     :: str
    CHARACTER(LEN = loc_2 - loc_1 + 1) :: str_out
    CHARACTER(LEN = LEN(str))          :: str_tmp

    str_tmp = TRIM(ADJUSTL(str))

    IF (loc_2 - loc_1 + 1 > LEN_TRIM(ADJUSTL(str))) THEN
        WRITE(6,'(A)') 'Error in FUNCTION GET_TRIMMED_VAL'
        WRITE(6,'(A)') 'Line is:'
        WRITE(6,'(A)') str
        WRITE(6,'(A)') 'ABORTING'
        STOP
    END IF

    str_out = str_tmp(loc_1:loc_2)

END FUNCTION GET_TRIMMED_VAL

SUBROUTINE SECTION_HEADER(title, unit)
    ! Writes a section header to unit
    ! Three lines
    ! ---------
    ! - title -
    ! ---------
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit
    INTEGER                        :: n_l_spaces, n_r_spaces

    n_l_spaces = (70 - LEN_TRIM(title)) / 2
    n_r_spaces = (70 - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        n_l_spaces = n_l_spaces + 1
    END IF

    WRITE(unit,'(A)')   REPEAT('-',80)
    WRITE(unit, '(5A)') REPEAT('-',5), REPEAT(' ',n_l_spaces), TRIM(title), &
                        REPEAT(' ',n_r_spaces), REPEAT('-',5)
    WRITE(unit,'(A)')   REPEAT('-',80)

END SUBROUTINE SECTION_HEADER

SUBROUTINE SUB_SECTION_HEADER(title, unit)
    ! Writes a subsection section header to unit
    ! Two lines
    !   title
    !   ‾‾‾‾‾
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit
    INTEGER                        :: n_l_spaces, n_r_spaces

    n_l_spaces = (80 - LEN_TRIM(title)) / 2
    n_r_spaces = (80 - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        n_l_spaces = n_l_spaces + 1
    END IF

    WRITE(unit, '(3A)') REPEAT(' ',n_l_spaces), &
                        TRIM(title), &
                        REPEAT(' ',n_r_spaces)

    WRITE(unit,'(3A)')  REPEAT(' ',n_l_spaces), &
                        REPEAT('‾',LEN_TRIM(title)), &
                        REPEAT(' ',n_r_spaces)

END SUBROUTINE SUB_SECTION_HEADER

SUBROUTINE WARNING_HEADER(title, unit)
    ! Writes a warning header to unit
    ! Three lines
    ! *********
    ! * title *
    ! *********
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit
    INTEGER                        :: n_l_spaces, n_r_spaces, width

    IF (LEN_TRIM(title) > 60) THEN
        width = LEN_TRIM(title) + 20
    ELSE
        width = 70
    END IF

    n_l_spaces = (width - LEN_TRIM(title)) / 2
    n_r_spaces = (width - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        n_l_spaces = n_l_spaces + 1
        width = width
    END IF

    WRITE(unit,'(A)')   REPEAT('*',width+10)
    WRITE(unit, '(5A)') REPEAT('*',5), REPEAT(' ',n_l_spaces), TRIM(title), &
                        REPEAT(' ',n_r_spaces), REPEAT('*',5)
    WRITE(unit,'(A)')   REPEAT('*',width+10)

END SUBROUTINE WARNING_HEADER

SUBROUTINE ERROR_HEADER(title, unit)
    ! Writes an error header to unit
    ! Three lines
    ! !!!!!!!!!
    ! ! title !
    ! !!!!!!!!!
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit
    INTEGER                        :: n_l_spaces, n_r_spaces, width

    IF (LEN_TRIM(title) > 60) THEN
        width = LEN_TRIM(title) + 20
    ELSE
        width = 70
    END IF

    n_l_spaces = (width - LEN_TRIM(title)) / 2
    n_r_spaces = (width - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        n_l_spaces = n_l_spaces + 1
        width = width
    END IF

    WRITE(unit,'(A)')   REPEAT('!',width+10)
    WRITE(unit, '(5A)') REPEAT('!',5), REPEAT(' ',n_l_spaces), TRIM(title), &
                        REPEAT(' ',n_r_spaces), REPEAT('!',5)
    WRITE(unit,'(A)')   REPEAT('!',width+10)

END SUBROUTINE ERROR_HEADER
    
SUBROUTINE SETUP_HDF5
    IMPLICIT NONE
    INTEGER(8) :: real_size, real_complex_size
    
    real_size = (storage_size(1_8, 8) / 8)
    real_complex_size =  real_size * 2_8
    
    ! Initialize FORTRAN interface.
    CALL h5open_f(hdf5_error)

    ! Create the memory data type.
    CALL H5Tcreate_f(H5T_COMPOUND_F, real_complex_size, hdf5_complex_datatype,hdf5_error)
    CALL H5Tinsert_f( hdf5_complex_datatype, "r", &
       0_8, h5kind_to_type(8,H5_REAL_KIND), hdf5_error)
    CALL H5Tinsert_f( hdf5_complex_datatype, "i", &
       real_size, h5kind_to_type(8,H5_REAL_KIND), hdf5_error)
    
END SUBROUTINE SETUP_HDF5

SUBROUTINE CLOSE_HDF5
    IMPLICIT NONE
    CALL H5Tclose_f(hdf5_complex_datatype, hdf5_error)
END SUBROUTINE

SUBROUTINE GET_HDF5_DIMENSION(dimension)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: dimension
    INTEGER(HID_T)   :: hdf5_data_id, hdf5_file_id, hdf5_dspace_id
    INTEGER(HSIZE_T), dimension(1) :: hdf5_check_dims,hdf5_check_maxdims
    INTEGER :: hdf5_error
    ! Open the file
    CALL H5Fopen_f("tau.hdf5", H5F_ACC_RDONLY_F, hdf5_file_id, hdf5_error)
    ! Read EQ Eigenstate Energies
    CALL H5Dopen_f(hdf5_file_id, "energies", hdf5_data_id, hdf5_error)
    CALL H5Dget_space_f(hdf5_data_id,hdf5_dspace_id,hdf5_error)
    CALL H5Sget_simple_extent_dims_f(hdf5_dspace_id, hdf5_check_dims, hdf5_check_maxdims, hdf5_error)
    dimension = hdf5_check_dims(1)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Fclose_f(hdf5_file_id, hdf5_error)
END SUBROUTINE GET_HDF5_DIMENSION

SUBROUTINE READ_EQ_HAMILTONIAN_HDF5(dimension,vals)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: dimension
    REAL(KIND = WP), ALLOCATABLE, INTENT(OUT)    :: vals(:)
    INTEGER(HSIZE_T), DIMENSION(1:1) :: dim
    REAL(kind=8) :: energies(1:dimension)
    INTEGER(HID_T)   :: hdf5_data_id, hdf5_file_id, hdf5_dspace_id
    INTEGER(HSIZE_T), dimension(1) :: hdf5_check_dims,hdf5_check_maxdims
    INTEGER :: hdf5_error,i
    dim = dimension
    allocate(vals(dimension))
    ! Open the file
    CALL H5Fopen_f("tau.hdf5", H5F_ACC_RDONLY_F, hdf5_file_id, hdf5_error)
    ! Read EQ Eigenstate Energies
    CALL H5Dopen_f(hdf5_file_id, "energies", hdf5_data_id, hdf5_error)
    CALL H5Dread_f(hdf5_data_id, H5T_NATIVE_DOUBLE, energies, dim, hdf5_error)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    vals = energies
    CALL H5Fclose_f(hdf5_file_id, hdf5_error)
    ! Symmetrise eigenvalues at WP if a Kramers system
    IF(config_logi(35)) THEN
        DO i = 1,dimension-1,2
            vals(i) = (vals(i)+vals(i+1))*0.5_WP
            vals(i+1) = vals(i)
        END DO
    END IF
END SUBROUTINE READ_EQ_HAMILTONIAN_HDF5

SUBROUTINE READ_ANGMOM_HDF5(dimension,spin,angmom)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: dimension
    COMPLEX(KIND = WP), ALLOCATABLE, INTENT(OUT)    :: spin(:,:,:), angmom(:,:,:)
    INTEGER(HID_T)   :: hdf5_data_id, hdf5_file_id, hdf5_group_id
    INTEGER :: hdf5_error, j, k, l
    REAL (KIND = WP) :: avg_mag
    COMPLEX(KIND=8), DIMENSION(1:dimension,1:dimension), TARGET :: hdf5_data_buffer
    TYPE(C_PTR) :: hdf5_ptr

    allocate(spin(3,dimension,dimension),angmom(3,dimension,dimension))

    ! Open the file, group and the dataset
    CALL H5Fopen_f("tau.hdf5", H5F_ACC_RDONLY_F, hdf5_file_id, hdf5_error)
    CALL H5Gopen_f(hdf5_file_id,"spin",hdf5_group_id,hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "x", hdf5_data_id, hdf5_error)
    
    ! Read the data
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    spin(1,:,:) = transpose(hdf5_data_buffer)
    
    ! Close the dataset
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    
    ! Rinse and repeat
    CALL H5Dopen_f(hdf5_group_id, "y", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    spin(2,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "z", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    spin(3,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    
    ! Close the group
    CALL H5Gclose_f(hdf5_group_id, hdf5_error)

    ! Repeat for angmom
    CALL H5Gopen_f(hdf5_file_id,"angmom",hdf5_group_id,hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "x", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    angmom(1,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "y", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    angmom(2,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "z", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    angmom(3,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Gclose_f(hdf5_group_id, hdf5_error)
    
    ! Close the file
    CALL H5Fclose_f(hdf5_file_id, hdf5_error)

    ! Enforce Hermicity at WP
    spin(1,:,:) = (spin(1,:,:) + transpose(conjg(spin(1,:,:))))*0.5_WP
    spin(2,:,:) = (spin(2,:,:) + transpose(conjg(spin(2,:,:))))*0.5_WP
    spin(3,:,:) = (spin(3,:,:) + transpose(conjg(spin(3,:,:))))*0.5_WP
    angmom(1,:,:) = (angmom(1,:,:) + transpose(conjg(angmom(1,:,:))))*0.5_WP
    angmom(2,:,:) = (angmom(2,:,:) + transpose(conjg(angmom(2,:,:))))*0.5_WP
    angmom(3,:,:) = (angmom(3,:,:) + transpose(conjg(angmom(3,:,:))))*0.5_WP

    ! Symmetrise time-reversal symmetry at WP if a Kramers system
    IF(config_logi(35)) THEN
        DO l = 1,3
            CALL TR_SYMMETRISE(angmom(l,:,:), dimension, .FALSE.)
            CALL TR_SYMMETRISE(spin(l,:,:), dimension, .FALSE.)
        END DO
    END IF

END SUBROUTINE READ_ANGMOM_HDF5

SUBROUTINE TR_SYMMETRISE(op, dimension, even)
    USE constants, ONLY : pi
    IMPLICIT NONE
    COMPLEX(KIND = WP) :: op(:,:)
    INTEGER :: dimension, j, k
    LOGICAL :: even ! parity of op under time-reversal
    REAL (KIND = WP) :: avg_mag, del_phase

    DO j = 1,dimension-1,2
        DO k = 1,dimension-1,2

            !WRITE(*,*) j, k
            ! WRITE(*,*) EXP(COMPLEX(0.0_WP, PHASE(op(j,k)/op(k+1,j+1)) - PHASE(op(j,k+1)/op(k,j+1)) + pi))
            ! WRITE(*,*) ABS(op(j,k)), ABS(op(k+1,j+1))
            ! WRITE(*,*) ABS(op(j,k+1)), ABS(op(k,j+1))
            ! WRITE(*,*) op(j,k), op(k+1,j+1)
            ! WRITE(*,*) op(j,k+1), op(k,j+1)

            IF (j == k) THEN
                IF(even) THEN
			        !op(j,j) = COMPLEX(0.5_WP * ( REAL(op(j,j),WP) + REAL(op(j+1,j+1),WP) ), 0.0_WP)
                    op(j,j) = (0.5_WP,0.0_WP) * ( REAL(op(j,j),WP) + REAL(op(j+1,j+1),WP) )
                	op(j+1,j+1) = op(j,j)
                	op(j,j+1) = (0.0_WP,0.0_WP)
                	op(j+1,j) = (0.0_WP,0.0_WP)
		ELSE
			!op(j,j) = COMPLEX(0.5_WP * ( REAL(op(j,j),WP) - REAL(op(j+1,j+1),WP) ), 0.0_WP)
            op(j,j) = (0.5_WP,0.0_WP) * ( REAL(op(j,j),WP) - REAL(op(j+1,j+1),WP) )
			op(j+1,j+1) = -op(j,j)
			op(j,j+1) = 0.5_WP * (op(j,j+1) + CONJG(op(j+1,j)))
			op(j+1,j) = CONJG(op(j,j+1))
		END IF
            ELSE
                del_phase = MOD(PHASE(op(j,k)) - PHASE(op(k+1,j+1)) - (PHASE(op(j,k+1)) - PHASE(op(k,j+1)) - pi), 2.0_WP * pi)
                if (del_phase > pi) del_phase = del_phase - 2.0_WP * pi
                if (del_phase < -pi) del_phase = del_phase + 2.0_WP * pi
                !WRITE(*,*) del_phase
                avg_mag = 0.5_WP * (ABS(op(j,k)) + ABS(op(k+1,j+1)))
                !op(j,k) = avg_mag * EXP(COMPLEX(0.0_WP, PHASE(op(j,k)) - 0.25_WP * del_phase))
                op(j,k) = avg_mag * EXP((0.0_WP,1.0_WP)*(PHASE(op(j,k)) - 0.25_WP * del_phase))
                !op(k+1,j+1) = avg_mag * EXP(COMPLEX(0.0_WP, PHASE(op(k+1,j+1)) + 0.25_WP * del_phase))
                op(k+1,j+1) = avg_mag * EXP((0.0_WP,1.0_WP)*(PHASE(op(k+1,j+1)) + 0.25_WP * del_phase))
                avg_mag = 0.5_WP * (ABS(op(j,k+1)) + ABS(op(k,j+1)))
                !op(j,k+1) = avg_mag * EXP(COMPLEX(0.0_WP, PHASE(op(j,k+1)) + 0.25_WP * del_phase))
                op(j,k+1) = avg_mag * EXP((0.0_WP,1.0_WP)*(PHASE(op(j,k+1)) + 0.25_WP * del_phase))
                !op(k,j+1) = avg_mag * EXP(COMPLEX(0.0_WP, PHASE(op(k,j+1)) - 0.25_WP * del_phase))
                op(k,j+1) = avg_mag * EXP((0.0_WP,1.0_WP)*(PHASE(op(k,j+1)) - 0.25_WP * del_phase))
            END IF

            ! WRITE(*,*) op(j,k), op(k+1,j+1)
            ! WRITE(*,*) op(j,k+1), op(k,j+1)
            ! WRITE(*,*) EXP(COMPLEX(0.0_WP, PHASE(op(j,k)/op(k+1,j+1)) - PHASE(op(j,k+1)/op(k,j+1)) + pi))
            ! WRITE(*,*) ABS(op(j,k)), ABS(op(k+1,j+1))
            ! WRITE(*,*) ABS(op(j,k+1)), ABS(op(k,j+1))
        END DO
    END DO
END SUBROUTINE TR_SYMMETRISE

RECURSIVE FUNCTION PHASE(z) RESULT(angle)
    COMPLEX(KIND = WP),INTENT(IN) :: z
    REAL (KIND = WP) :: angle
    angle = ATAN2(AIMAG(z), REAL(z))
END FUNCTION PHASE

SUBROUTINE READ_PHONON_MATRIX_HDF5(dimension,nmodes,phonon_matrix)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: dimension, nmodes
    COMPLEX(KIND = WP), INTENT(OUT)    :: phonon_matrix(:,:,:)
    REAL(KIND = WP) :: avg_mag
    INTEGER(HID_T)   :: hdf5_data_id, hdf5_file_id, hdf5_group_id
    INTEGER :: hdf5_error, i, j
    COMPLEX(KIND=8), DIMENSION(1:dimension,1:dimension), TARGET :: hdf5_data_buffer
    TYPE(C_PTR) :: hdf5_ptr
    CHARACTER(len=10) :: mode_char

    ! Open the file and group
    CALL H5Fopen_f("tau.hdf5", H5F_ACC_RDONLY_F, hdf5_file_id, hdf5_error)
    CALL H5Gopen_f(hdf5_file_id,"couplings",hdf5_group_id,hdf5_error)

    ! Loop over nmodes and read data
    DO i = 1,nmodes
        WRITE(mode_char,'(I10)') i
        mode_char = ADJUSTL(mode_char)
        CALL H5Dopen_f(hdf5_group_id, TRIM(mode_char), hdf5_data_id, hdf5_error)
        hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
        CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
        phonon_matrix(i,:,:) = transpose(hdf5_data_buffer)
        CALL H5Dclose_f(hdf5_data_id, hdf5_error)
        ! Enforce Hermicity at WP
        phonon_matrix(i,:,:) = (phonon_matrix(i,:,:) + transpose(conjg(phonon_matrix(i,:,:))))*0.5_WP
        ! Enforce time-reversal symmetry if a Kramers system
        IF(config_logi(35)) THEN
            CALL TR_SYMMETRISE(phonon_matrix(i,:,:), dimension, .TRUE.)
        END IF
    END DO

    ! Close group
    CALL H5Gclose_f(hdf5_group_id, hdf5_error)
    
    ! Close the file
    CALL H5Fclose_f(hdf5_file_id, hdf5_error)
END SUBROUTINE READ_PHONON_MATRIX_HDF5

! SUBROUTINE TEMP_DIAG(dimension, CF_matrix, vecs, vals)
!     USE fort_diag
!     IMPLICIT NONE
!     COMPLEX(KIND = WP):: vecs(:,:)
!     REAL(KIND = WP)  :: vals(:)
!     COMPLEX(KIND = WP), INTENT(IN)               :: CF_matrix(:,:)
!     INTEGER, INTENT(IN)                          :: dimension
!     INTEGER(KIND = C_INT)                        :: arr_len
!     vecs = (0.0_WP, 0.0_WP)
!     vals = 0.0_WP
!     arr_len = dimension
!     WRITE(6,*) "next..."
!     WRITE(6,*) CF_matrix(:,:)
!     CALL complex_c_diag(arr_len, CF_matrix, vecs, vals)
! END SUBROUTINE TEMP_DIAG

END PROGRAM tau
