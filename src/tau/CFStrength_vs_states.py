#!/usr/bin/env python

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import os
import argparse, textwrap
import matplotlib.cm as cm
from sys import exit


def define_argparse():
    description = '''
    Program to plot the Crystal Field Strength vs the electronic states. 
    It does the same as strength_deltas('CFP_steps.dat') function in D_CFPs, but it is written as a post-processing tool.
    '''

    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.RawDescriptionHelpFormatter 
                                )
    parser.add_argument(
        'eq_CFPs', 
        type=str,
        help='Molcas output file of the equilibrium geometry'
                       )

    parser.add_argument(
        'mode_energies', 
        type=str,
        help='File containing the mode_energies with a single header line. Obtained with extract_freq-gauss.py' 
                       )

    parser.add_argument(
        'CFP_steps',
        type=str,
        help='File containing the change in CFPs. Obtained with D_CFPs.py.'
                       )

    parser.add_argument(
        '--metal', metavar='<Atomic_symbol>', 
        type=str, 
        default='Dy',
        help='Metal type - used to define Operator Equivalent Factors. Default:Dy'
                       )

    parser.add_argument(
        '--TAU', metavar='tau_output_file <temperature>',
        type=str, nargs='+', 
        help=textwrap.dedent('''Use the equilibrium energies from CF Hamiltonian (tau_output_file) instead of CASSCF (eq_CFPs) in the strength plots. 
If TAU output contains a transition knockout section, these transitions are plotted, instead of the first 5. 
<temperature> defines which transition knockout block is considered. Default: 100 K.''')
                       )

    args = parser.parse_args()

    # --TAU has been indicated
    if len(args.TAU) >= 1: 
        # Check the file exists
        if os.path.exists(args.TAU[0]) == False:
            exit('\nError: TAU output file cannot be found in the folder.\n')
        # Define the knockout temperature if not provided.
        if len(args.TAU) == 1:
            args.TAU.append(100)
        # Check that the temperature is a number
        elif len(args.TAU) == 2:
            try:
                float(args.TAU[1])
            except:
                exit('\nValueError: The indicated temperature is not correct.\n')
        else: 
            exit('\nError: --TAU has not been specified correctly.\n')

    return args


def define_CFH_Params(metal):
    '''Define the Crystal Field Hamiltonian parameters.

    Parameters: 
        Metal type (str) : User defined.

    Returns:
        rank  (array)    : Rank of the CFPs.
        order (array)    : Order of the CFPs.
        OEFs  (array)    : Operator Equivalent Factors.
    '''

    (k,q) = [ np.hstack((np.ones(5)*2,        np.ones(9)*4,        np.ones(13)*6)), 
              np.hstack((np.linspace(-2,2,5), np.linspace(-4,4,9), np.linspace(-6,6,13)))]
    
    if metal == 'Dy' or metal == 'dy':
        OEFs = np.array([ -2.0/315.0,    -2.0/315.0,    -2.0/315.0,    -2.0/315.0,    -2.0/315.0,    \
                          -8.0/135135.0, -8.0/135135.0, -8.0/135135.0, -8.0/135135.0,                \
                          -8.0/135135.0, -8.0/135135.0, -8.0/135135.0, -8.0/135135.0, -8.0/135135.0, \
                           4.0/3864861.0, 4.0/3864861.0, 4.0/3864861.0, 4.0/3864861.0, 4.0/3864861.0,\
                           4.0/3864861.0, 4.0/3864861.0, 4.0/3864861.0, 4.0/3864861.0, 4.0/3864861.0,\
                           4.0/3864861.0, 4.0/3864861.0, 4.0/3864861.0 ]) # Operator equivalent factors for Dy
    
     
    elif metal == 'Yb' or metal == 'yb':
        OEFs = np.array([2./63. ,    2./63.,    2./63.,    2./63.,    2./63.,    \
                          -2./1155., -2./1155., -2./1155., -2./1155.,                \
                          -2./1155., -2./1155., -2./1155., -2./1155., -2./1155., \
                           4./27027, 4./27027, 4./27027, 4./27027, 4./27027,\
                           4./27027, 4./27027, 4./27027, 4./27027, 4./27027,\
                           4./27027, 4./27027, 4./27027 ]) # Operator equivalent factors for Yb

    return k, q, OEFs


def read_CFPs(molcas_file):
    '''Read a Molcas file and store the CFPs from Single Aniso section.

    Parameters: 
        molcas_file (str) : Molcas file.

    Returns:
        CFPs (list)       : The CFPs from the Molcas file.
    '''

    #if path.exists(molcas_file) == False: exit('FileNotFound')

    CFPs = []
    with open(molcas_file,'r') as f:
        for line in f:
            # Find the line containing the appropriate pointer. There are more than one, so use a second unique identifier.
            if line.find('Hcf = SUM_{k,q} * [ B(k,q) * O(k,q) ];') != -1:
                # while executes when the result of the boolean comparison is True. It will skip all the lines in between the two strings 
                while 'J. Chem. Phys., 137, 064112 (2012).' not in line: 
                    line = next(f)
                else:
                    # Skip the headers.
                    for header in range(3):
                        line = next(f)
                    #Read in parameters
                    for i in range(29): # 27 CFPs plus the separating lines
                        line = next(f)
                        if line.split()[0] == '2' or line.split()[0] == '4' or line.split()[0] == '6':
                            CFPs.append(float(line.split()[6])) # add cfps to array
                    break

    return np.array(CFPs)


def read_energies(molcas_file):
    '''Read the spin-orbit coupled energies from a Molcas file.

    Parameters: 
        molcas_file (str) : Molcas file.

    Returns:
        energies (list)   : The energies of one of each Kramers doublets pairs. 
    '''
    if args.metal == 'Dy' or args.metal == 'dy': states = 16
    if args.metal == 'Yb' or args.metal == 'yb': states = 8
    energies = []
    with open(molcas_file, 'r') as f:
        for line in f:
            if line.find('Total energies including SO') != -1:
                for energy in range(states):
                    line = next(f)
                    energies.append(float(line.split()[6]))
    energies = [ (x-energies[0])*np.float128(2.19474625E+05) for x in energies ]
    energies = energies[::2]
    return energies  

def read_TAU(tau_file, temp):
    '''Read a TAU output file.

    Parameters: 
        tau_file (str)     : Tau file.

    Returns:
        CF_energies     (list) : The energies of Kramers CF states. All if knockout == True, One of each pairs else.
        knockout_states (list) : The most important states identified by the knockout method. Only if that section is present in TAU file. 
    Raises:
        Error: if the indicated knockout temperature is not present in the TAU output file.
    '''

    check_temp = []
    CF_energies = []
    knockout_states = []
    knockout = False

    # Collect the temperatures indicated and check the indicated knockout_temp is within them. 
    with open(tau_file, 'r') as f:
        for line in f:
            if line.find('Temperature (K)   Detailed Balance') != -1:
                next(f)
                for i in range(500):
                    try:
                        check_temp.append(float(next(f).split()[0]))
                    except:
                        break

    if float(temp) not in check_temp:
        exit('\nError: the provided TAU output file does not contain data at the requested temperature.\n')

    # Get the CFPs energies. Check if the knockout section is present and store the states involved
    with open(tau_file, 'r') as f:
        for line in f:
            if line.find('Equilibrium Crystal Field Eigenvalues (cm^-1)') != -1:
                next(f)
                for i in range(16):
                    CF_energies.append(float(next(f).split()[2]))
            if line.find('-----                               Knockout                               -----') != -1:
                knockout=True
                while 'T = '+str(temp)+' K' not in line: 
                    line = next(f)
                else:
                    next(f)
                    for i in range(5):
                        knockout_states.append( next(f).split()[1:5:3] ) # split the line and get the 2nd and 5th element
                    break

    #CF_energies = CF_energies[::2]

    return CF_energies[::2] if knockout == False else CF_energies, knockout_states


def read_mode_energies(frequencies_file):
    '''Read the mode energies, from a file generated by extract_freq_gauss.py.

    Parameters: 
        frequencies_file (str)   : File containing the mode energies.

    Returns:
        mode_energies (np.array) : The mode energies.
        num_modes     (int)      : Number of modes.
    '''
    mode_energies = np.loadtxt(frequencies_file, skiprows=1, ndmin=1)
    num_modes = np.size(mode_energies)

    return mode_energies, num_modes


def Stevens_to_Wybourne(CFPs, rank, order):
    '''Transform the CFPs from Stevens to Wybourne notation, to calculate the strenght parameters.

    Parameters: 
        CFPs (array) : CFPs in Stevens notation.

    Returns:
        Wbqk (array) : Square of the norm.
    '''
    lmbda = [
             np.sqrt(6)/3.,
             -np.sqrt(6)/6.,
             2.0,
             -np.sqrt(6)/6.,
             np.sqrt(6)/3.,
             4.*np.sqrt(70)/35.,
             -2.*np.sqrt(35)/35.,
             2.*np.sqrt(10)/5.,
             -2*np.sqrt(5)/5.,
             8.,
             -2.*np.sqrt(5)/5.,
             2.*np.sqrt(10)/5.,
             -2.*np.sqrt(35)/35.,
             4*np.sqrt(70)/35.,
             16*np.sqrt(231)/231.,
             -8*np.sqrt(77)/231.,
             8*np.sqrt(14)/21.,
             -8*np.sqrt(105)/105.,
             16.*np.sqrt(105)/105.,
             -4*np.sqrt(42)/21.,
             16.,
             -4*np.sqrt(42)/21.,
             16.*np.sqrt(105)/105.,
             -8*np.sqrt(105)/105.,
             8*np.sqrt(14)/21.,
             -8*np.sqrt(77)/231.,
             16*np.sqrt(231)/231.
            ]

    Wbqk = np.zeros(27, dtype = "complex_")

    for it,val in enumerate(order):
        if val == 0:
            Wbqk[it] = lmbda[it]*CFPs[it]
        elif val < 0: # it for -q and index for q
            if it == 0: index = 4
            elif it == 1: index = 3
            elif it == 5: index = 13
            elif it == 6: index = 12
            elif it == 7: index = 11
            elif it == 8: index = 10
            elif it == 14: index = 26
            elif it == 15: index = 25
            elif it == 16: index = 24
            elif it == 17: index = 23
            elif it == 18: index = 22
            elif it == 19: index = 21
            Wbqk[it] = lmbda[it]*(-1)**val*(CFPs[index] - 1j*CFPs[it])
        elif val > 0: # it for q and index for -q
            if it == 4: index = 0
            elif it == 3: index = 1
            elif it == 13: index = 5
            elif it == 12: index = 6
            elif it == 11: index = 7
            elif it == 10: index = 8
            elif it == 26: index = 14
            elif it == 25: index = 15
            elif it == 24: index = 16
            elif it == 23: index = 17
            elif it == 22: index = 18
            elif it == 21: index = 19
            Wbqk[it] = lmbda[it]*(CFPs[it] + 1j*CFPs[index])

    return abs(Wbqk)**2 # abs takes the norm of the imaginary number


def calculate_strength(wCFPs):
    '''Calculate the strenght parameters.

    Parameters: 
        wCFPs (array) : Square of the Wbqk norm.

    Returns:
        S     (float) : Crystal Field Strenght parameter.
    '''

    s_k2, s_k4, s_k6 = 0., 0., 0.
    
    for k2 in range(0,5):
        s_k2 += wCFPs[k2]
    for k4 in range(5,14):
        s_k4 += wCFPs[k4]
    for k6 in range(14,26):
        s_k6 += wCFPs[k6]

    s_k2, s_k4, s_k6 = 1./(2*2+1.)*s_k2, 1./(2*4+1.)*s_k4, 1./(2*6+1.)*s_k6

    S = np.sqrt(1./3.*(s_k2 + s_k4 + s_k6))

    return S


def strength_deltas(DCFPs):

    print('...plotting the strength of the delta CFPs...')

    with open(DCFPs, 'r') as f:
        strength_deltas = []
        for line in f:
            if line.find('Vibration:') != -1:
                pos = []
                neg = []
                for CFP in range(27):
                    line = next(f)
                    pos.append(float( line.split()[2] ))
                    neg.append(float( line.split()[3] ))

                wpos = Stevens_to_Wybourne(pos, rank, order)
                wneg = Stevens_to_Wybourne(neg, rank, order)

            strength_deltas.append(
                ( calculate_strength(wpos) + calculate_strength(wneg) ) / 2.
                                  )

    plot_strength('strength_deltas', strength=strength_deltas, Spos=None, Sneg=None)

    return strength_deltas


def plot_strength( type_plot, strength=None, Spos=None, Sneg=None, CF_energies=None, knockout_states=None ):
    '''Plot the Crystal Field Strenght vs mode energy.

    Parameters: 
        type_plot   (str)   : Specifies the type of plot. Options: strength_deltas or delta_strength.
        strength    (array) : Contains the calculated Crystal Field Strength. Required if type_plot=strength_deltas.
        Spos        (array) : Contains the calculated Crystal Field Strength of the positive displacement. Required if type_plot=strength_delta.
        Sneg        (array) : Contains the calculated Crystal Field Strength of the negative displacement. Required if type_plot=delta_strength.
        CF_energies (array) : Contains the crystal field energies. They can be read from Molcas output file (default) or from a TAU file.
        knockout_states     : The most important states determined with the knockout method. Written in the TAU file.

    Returns:
        plot : A plot of the strength of the Crystal Field change and a plot of the difference in Crystal Field Strength.
    '''

    knockout = False

    # Use the equilibrium energies from the crystal field hamiltonian.
    if len(args.TAU) >= 1:

        # Read the TAU file
        TAU_info = read_TAU(args.TAU[0], args.TAU[1])

        if len(TAU_info) > 1: # knockout section is present in TAU file
            CF_energies, knockout_states = TAU_info[0], TAU_info[1]
            knockout = True
        else:
             CF_energies = TAU_info


    # Use the equilibrium energies from the CASSCF output.
    else:
        CF_energies = read_energies(args.eq_CFPs)

    label_states = ['$|1,2\\rangle$', '$|3,4\\rangle$', '$|5,6\\rangle$', 
                    '$|7,8\\rangle$', '$|9,10\\rangle$', '$|11,12\\rangle$', 
                    '$|13,14\\rangle$', '$|15,16\\rangle$' ]

    # Prepare the transitions
    if knockout == True:
        transitions = [ CF_energies[int(final)-1] - CF_energies[int(initial)-1] for initial,final in knockout_states ] # initial, final start from 1
        label_transitions = [ '$\|'+initial+'\\rangle\leftrightarrow\|'+final+'\\rangle$' for initial,final in knockout_states ]
        # if knockout, read_TAU returns the 16 states to be able to calculate the transitions, so I need to get every 2nd element
        CF_energies = CF_energies[::2] 

    else:
        transitions = [ CF_energies[i+1] - CF_energies[i] for i in range(1, len(CF_energies)-1) ] # Dont calculate first transition, it is first state
        label_transitions = ['$\|2\\rangle\leftrightarrow\|3\\rangle$',
                             '$\|3\\rangle\leftrightarrow\|4\\rangle$',
                             '$\|4\\rangle\leftrightarrow\|5\\rangle$',
                             '$\|5\\rangle\leftrightarrow\|6\\rangle$',
                             '$\|6\\rangle\leftrightarrow\|7\\rangle$',
                             '$\|7\\rangle\leftrightarrow\|8\\rangle$']

    # Sort the transitions and their labels. It makes handling the text in the plot easier
    label_transitions = [ x for _,x in sorted(zip(transitions,label_transitions))] 
    transitions.sort() 

    # Full plots of S vs mode_energy - includes lines indicating the electronic state energies and electronic state energy differences

    #Create plot handle
    fig, (ax1,ax2) = plt.subplots(2, 1, sharex=False, sharey=False, figsize=(8,5.5) )
    fig.subplots_adjust(hspace=0.13, wspace=0.)

    #Plot options for plotting strength of Delta CFPs
    if type_plot == 'strength_deltas':

        #Create stem plot of strength vs mode energies
        for ax in [ax1,ax2]:
            markerline, stemlines, baseline = ax.stem(mode_energies, strength, basefmt=' ', markerfmt='ob', label=r'$S$' )
            #Set stem marker and line sizes
            plt.setp(markerline, 'markersize', 5, markerfacecolor='None')
            plt.setp(stemlines, 'linewidth', 0.8)

        #Find the largest strength value to define the upper bound of the CF_energies lines and to define the y axis upper limit
        stretch_ax1 = 50
        stretch_ax2 = 20
        y_max_vline = np.amax( 
            strength[
                     :np.where( mode_energies < CF_energies[-1]+stretch_ax1 )[0][-1]+1
                    ]
            ) # np.where( mode_energies < transitions[1] )[0] returns a tuple ([0]) with the indexes of the mode energies. 
              # I then take the last element and use strength[:that+1] to retrieve all the S within that range. np.amax() gives me the max
        y_max_vline2 = np.amax( strength[ :np.where( mode_energies < transitions[-1]+stretch_ax2 )[0][-1]+1 ] ) 
        
        #All strength values are positive so the electronic state energy lines start from the axis
        y_min_vline = 0.

        #y axis limits - y_max is stretched to give room for labels
        y_max1 = y_max_vline*1.2
        y_max2 = y_max_vline2*1.2
        y_min = 0.

        #y label
        y_label = r'$S(\Delta CFPs)\ (cm^{-1}$)'


    # Plot the CF energies with vertical lines and label them
    for it in range(len(CF_energies)):
        ax1.vlines( x=CF_energies[it], lw=1.5, ymin=y_min_vline, ymax=y_max_vline, 
                    colors='#ff7f0e', label=r'$\|i\rangle$' if it == 0 else '' )
        if it > 0:
            ax1.text( x=CF_energies[it], y=y_max_vline, s=r''+label_states[it], 
                     rotation=0, verticalalignment='bottom', horizontalalignment='center', fontsize='9' )

    # Plot the transitions with vertical lines and label them
    for it in range(len(transitions)):
        ax1.vlines( x=transitions[it], lw=1.5, ymin=y_min_vline, ymax=y_max_vline,
                    colors='#2ca02c', label=r'$\Delta(\|i\rangle\leftrightarrow\|j\rangle)$' if it == 1 else '' )

        ax2.vlines( x=transitions[it], lw=1.5, ymin=y_min_vline, ymax=y_max_vline2,
                    colors='#2ca02c', label=r'$\Delta(\|i\rangle\leftrightarrow\|j\rangle)$' if it == 1 else '' )
        ax2.text( x=transitions[it], y=y_max_vline2-it*1.5, s=r''+label_transitions[it], 
                 rotation=0, verticalalignment='top', horizontalalignment='center', fontsize='9')

    #Set x limits - add stretch_ax1,2 cm-1 to give room for state labels
    ax1.set_xlim([mode_energies[0]-10,CF_energies[-1]+stretch_ax1])
    ax2.set_xlim([mode_energies[0]-10,transitions[-1]+stretch_ax2])
    #Set y limits
    ax1.set_ylim([y_min,y_max1])
    ax2.set_ylim([y_min,y_max2])

    #set axis labels
    ax1.set_ylabel(y_label)
    ax2.set_ylabel(y_label)
    ax2.set_xlabel(r'Mode Energy (cm$^{-1}$)')

    # Remove parts of the frame
    for ax in [ax1,ax2]:
        ax.spines["top"].set_visible(False)
        #ax.spines["left"].set_visible(False)
        ax.spines["right"].set_visible(False)

    #Set legend options
    ax1.legend(loc=2, fontsize='9', numpoints=1, ncol=4, frameon=False, handletextpad=0.3, labelspacing=0.3, columnspacing=1, bbox_to_anchor=(0., 1.05))
    ax2.legend(loc=2, fontsize='9', numpoints=1, ncol=4, frameon=False, handletextpad=0.3, labelspacing=0.3, columnspacing=1, bbox_to_anchor=(0., 1.05) )

    plt.tight_layout()

    #Save plot and close
    if len(args.TAU) >= 1:
        plt.savefig(str(type_plot)+'_TAU.png', dpi=300)
    else:
        plt.savefig(str(type_plot)+'.png', dpi=300)
    plt.close('all')


    return


# -------------------------------------------------------------------------------------

if __name__ == '__main__':

    args = define_argparse()

    rank, order, OEFs = define_CFH_Params(args.metal)

    mode_energies, num_modes = read_mode_energies(args.mode_energies)
    
    strength_deltas = strength_deltas('CFP_steps.dat') # Plots the strength of the delta CFPs
    
