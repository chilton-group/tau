#!/usr/bin/env python3

#  /$$$$$$$$$/
# |___ $$___/
#     |$$       $$$$$$\  $$\   $$\
#     |$$       \____$$\ $$ |  $$ |
#     |$$       $$$$$$$ |$$ |  $$ |
#     |$$  $$\/$$  __$$ |$$ |  $$ |
#     | $$$$ / \$$$$$$$ |\$$$$$$  |
#     \_____/   \_______| \______/

####################################################################################################
#                                                                                                  #
#                                tau_rates_plot.py                                                 #
#                                                                                                  #
#                                Plots relaxation rates from tau output files                      #
#                                   Authors :                                                      #
#                                    Jon Kragskow                                                  #
#                                    Daniel Reta                                                   # 
#                                                                                                  #
####################################################################################################

import argparse
import textwrap
import numpy as np
import numpy.ma as ma
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter, NullFormatter
import os

mpl.rcParams["savefig.directory"] = ""
mpl.rcParams['savefig.dpi'] = 300


def read_in_command_line():
    # Read in command line arguments

    parser = argparse.ArgumentParser(description="Plots tau and experimental rate data.",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('tau_outputs', nargs='+', type=str, metavar='FILES',
                        help=textwrap.dedent("""
Output files from tau """))
    parser.add_argument("--exp_tau", type=str, metavar="FILE",
                        help=textwrap.dedent("""
File with experimental (T, tau) in rows with no header"""))
    parser.add_argument("--remove_transitions", action="store_true",
                        help=textwrap.dedent("""
Plot the results from a remove_transition output file.
This assumes that the working directory contains the "removal_results" folder
from an autau --remove_transition calculation."""))
    parser.add_argument("--show", action="store_true",
                        help=textwrap.dedent("""
Display plot in interactive window"""))
    parser.add_argument('--legend', type=str,
                        help=textwrap.dedent("""
File containing rows of legend entries in same order as tau.out files'
If --Exp is enabled then the final line is a label for the experimental data"""))
    parser.add_argument('--x_lims', nargs=2, type=float, metavar=('lower', 'upper'),
                        help=textwrap.dedent("""
Lower and upper x limits of final plot e.g. --x_lims 0 100"""))
    parser.add_argument('--font_size', type=float, metavar='Number',
                        help=textwrap.dedent("""
Size of font in final plot"""))
    parser.add_argument('--n_barrier_x_ticks', type=int, metavar='Number', default=9,
                        help=textwrap.dedent("""
Number of ticks in x axis of barrier plot - only valid for removal plots"""))

    user_args = parser.parse_args()

    # Make array of limits
    if user_args.x_lims and len(user_args.x_lims) != 2:
        parser.error('Incorrect x limits specified!')

    # Do not let more the user provide more than one tau_output file when asking 
    # for remove_transitions
    if user_args.remove_transitions and len(user_args.tau_outputs) > 1:
        exit("--remove_transitions requested, but more than one tau output provided \n" +
             "The tau output file must be from autau --remove_transition.")

    return user_args


def check_removal_folder():
    """
    Checks removal_results folder is in cwd
    """
    if not os.path.isdir(os.path.join(os.getcwd(), 'removal_results')):
        exit('\nremoval_results is not in the current directory\n')

    return


class CalcInfoStore:
    """
    Class for tau output file data
    """

    def __init__(self):

        # tau output files with errors
        self.file_stats = []

        # fwhm values
        self.fwhm_vals = []

        # Temperature step sizes 
        self.t_step_vals = []

        # Maximum temperatures
        self.t_max_vals = []

        # Minimum temperatures 
        self.t_min_vals = []

        # Number of temperatures per file
        self.num_temps = []

        # Max number of temperature points of all files
        self.most_points = 0

        # Files which do (not) have rate data
        self.rates_found = []

        # Array for final data
        self.data = None

        # Value for minimum of minimum relaxation rates
        self.min_min_rate = np.nan

        # Value for maximum of maximum relaxation rates
        self.max_max_rate = np.nan

        # Value for minimum of minimum temperatures
        self.min_min_T = np.nan

        # Value for maximum of maximum temperatures
        self.max_max_T = np.nan

        # Mask for excluding invalid rate data
        self.rates_mask = None

    def get_tau_lims(self, file_name):

        """
        Retrieve rate and temperature information from tau output file

        Parameters:
            self
            file_name (str): tau output file calculated with a transition removed.

        Returns:
            None
        """

        # Set initial values for error flags
        fwhm_found = False
        t_step_found = False
        db_print_found = False

        # Read fwhm value, temperature step, and detailed balance limits (max and min temps) 
        # from current file
        with open(file_name, 'r') as f:
            for line in f:
                if line.find('fwhm') != -1 and not fwhm_found:

                    # Catch for the wrong fwhm section
                    if line.split()[1] == '=':
                        continue

                    self.fwhm_vals.append(float(line.split()[1]))
                    fwhm_found = True

                # Read in temperature step size
                if line.find('temp') != -1 and not t_step_found:
                    self.t_step_vals.append(float(line.split()[3]))
                    t_step_found = True

                # Read in max and min temperatures for which detailed balance is obeyed
                if line.find('****             Detailed balance is obeyed from') != -1 \
                        and not db_print_found:
                    num_1 = float(line.split()[6])
                    num_2 = float(line.split()[9])
                    self.t_max_vals.append(max(num_1, num_2))
                    self.t_min_vals.append(min(num_1, num_2))
                    db_print_found = True

                if fwhm_found and t_step_found and db_print_found:
                    break

        # Close file
        f.close()

        # Report errors and cycle
        if not fwhm_found:
            print('Error : Could not find FWHM value in file "' + file_name + '"')
            self.file_stats.append(False)
            return

        elif not t_step_found:
            print('Error : Could not find temperature step in file "' + file_name + '"')
            self.file_stats.append(False)
            return

        elif not db_print_found:
            print('Error : Could not find detailed balance temperatures in file "'
                  + file_name + '"')
            self.file_stats.append(False)
            return

        self.file_stats.append(True)

        # Calculate maximum number of temperatures
        num_t = int(1. + (self.t_max_vals[-1] - self.t_min_vals[-1]) / self.t_step_vals[-1])
        self.num_temps.append(num_t)
        self.most_points = np.max([num_t, self.most_points])

        return

    def get_tau_rates(self, file_name, it):

        """
        Retrieve rate and temperature information from tau output file

        Parameters:
            self
            it        (int) : index of current file
            file_name (str) : tau output file calculated with a transition removed.

        Returns:
            None
        """

        # Reset temporary error flag
        rates_line_found = False

        # Temporary rate and temperature lists
        temps = []
        rates = []

        # If file is not ok then skip
        if not self.file_stats[it]:
            return

        # Read the rates
        with open(file_name, 'r') as f:
            for line in f:
                if line.find('-----                           Relaxation Rates                           ----') != -1:
                    rates_line_found = True
                    for num in range(4):
                        next(f)
                    for i in range(self.num_temps[it]):
                        line = next(f)
                        temps.append(float(line.split()[0]))
                        line.split()
                        rates.append(float(line.split()[1]))

        # Record minimum rate if slower than current minimum rate

        self.min_min_rate = np.nanmin([np.min(rates), self.min_min_rate])

        # Record maximum rate if faster than current maximum rate

        self.max_max_rate = np.nanmax([np.max(rates), self.max_max_rate])

        # Append rate and temperature data lists to final data array
        self.data[it, 0: self.num_temps[it], 0] = np.asarray(rates)
        self.data[it, 0: self.num_temps[it], 1] = np.asarray(temps)

        # Keep track of broken files
        #  If a file is broken then update the mask to be false all over
        if rates_line_found:
            self.rates_found.append(True)
        else:
            self.rates_found.append(False)
            self.mask[it, :, :] = 1
        return


def get_calculated_data(file_list):
    """
    Read in data from all tau output files

    Parameters:
        file_list : List of tau output file names

    Returns:
        info: Object containing rates, temperatures, max/min temperatures, 
              file status, ... for all tau.out files
    """

    # Create object to hold information on calculation files
    info = CalcInfoStore()
    # Loop over each file and extract min/max temperatures and detailed balance info
    for file_name in file_list:
        info.get_tau_lims(file_name)

    # Check at least one tau output file is ok
    if not all(stats for stats in info.file_stats):
        print('Error : No valid tau output files given')
        exit(-1)

    # Create mask for rates array
    #  This will be as long as the maximum number of temperature points
    #  as are can be different numbers of datapoints per file
    info.data = np.zeros([len(file_list), info.most_points, 2])

    # Create mask for rates array
    #  This will be as long as the maximum number of temperature points
    #  as there can be different numbers of datapoints per file
    #  Mask has same dimensions as final data array
    info.rates_mask = np.zeros([len(file_list), info.most_points, 2])

    # Loop over files and build mask
    #  0 == Valid data
    #  1 == empty element (Invalid data)
    for it in range(len(info.file_stats)):
        info.rates_mask[it, :, :] = 1
        if info.file_stats[it]:
            num_t = int(1. + (info.t_max_vals[it] - info.t_min_vals[it]) / info.t_step_vals[it])
            info.rates_mask[it, 0: num_t, :] = 0

    # Reopen file and read in rate data into correct part of data_array
    for it, file_name in enumerate(file_list):
        info.get_tau_rates(file_name, it)

    # Check at least one tau output file has rate data
    if not all(entries for entries in info.rates_found):
        print('Error : No rate data found in any of the given tau output files')
        exit(-1)

        # Apply mask to the array
    info.data = ma.masked_array(info.data, mask=info.rates_mask)

    # Record minimum and maximum temperatures
    info.min_min_T = np.min(info.t_min_vals)
    info.max_max_T = np.max(info.t_max_vals)

    return info


class ExpInfoStore:
    """
    Class for experimental data variables
    """

    def __init__(self):
        # Value for minimum of minimum relaxation rates
        self.min_min_rate = np.nan

        # Value for maximum of maximum relaxation rates
        self.max_max_rate = np.nan

        # Value for minimum of minimum temperatures
        self.min_min_T = np.nan

        # Value for maximum of maximum temperatures
        self.max_max_T = np.nan

        # Temperature and rate data
        self.data = None


def get_experimental_data(args):
    """
    Read in rate data experimental data file

    Parameters:
        args : Input arguments parser object from read_in_command_line

    Returns:
        info: Object containing rates, temperatures, max/min temperatures, 
              for experimental data
    """
    # Create object to hold information on experimental data
    info = ExpInfoStore()

    # Quit if experiment is not requested
    if not args.exp_tau:
        return info

    # Read in data - if normal reading fails then add a headerline and see if that works
    try:
        info.data = np.loadtxt(args.exp_tau)  # Data is T, tau
    except IOError:
        info.data = np.loadtxt(args.exp_tau,
                               skiplines=1)  # Data is T, tau with header

    # Find min and max temperatures
    info.min_min_T = np.nanmin(info.data[:, 0])
    info.max_max_T = np.nanmax(info.data[:, 0])

    # Convert tau data to rates
    info.data[:, 1] = np.reciprocal(info.data[:, 1])

    # Find min and max rates
    info.min_min_rate = np.nanmin(info.data[:, 1])
    info.max_max_rate = np.nanmax(info.data[:, 1])

    return info


def normal_plot(calc_info, exp_info, args):
    """
    Creates plot object, plots data, and adjusts axis for a normal plot of
        just rates vs temperature with no special remove transitions treatment

    Parameters:
        calc_info : tau data object
        exp_info : experimental data object
        args : Input arguments parser object from read_in_command_line

    Returns:

        None
    """

    # Change plot font size - needs to be done before plot is created
    if args.font_size:
        plt.rcParams.update({'font.size': args.font_size})
    else:
        plt.rcParams.update({'font.size': 10})

    # Create plot objects
    fig, (ax1) = plt.subplots(1, 1,
                              figsize=(5, 5),
                              num='tau rate data')

    # Load labels for legend
    leg_list = []
    if args.legend:
        with open(args.legend) as f:
            for line in f:
                try:
                    leg_list.append(line)
                except IOError:
                    pass

        f.close()

    # Plot tau data and experimental data
    plot_rates(ax1, calc_info, exp_info, args, leg_list)

    t_lims = [0, 1]
    tau_lims = [0, 1]

    # Get minimum and maximum temperatures for plot axes
    t_lims[0] = np.nanmin([calc_info.min_min_T, exp_info.min_min_T])
    t_lims[1] = np.nanmax([calc_info.max_max_T, exp_info.max_max_T])

    # Get minimum and maximum rates
    tau_lims[0] = np.nanmin([calc_info.min_min_rate, exp_info.min_min_rate])
    tau_lims[1] = np.nanmax([calc_info.max_max_rate, exp_info.max_max_rate])

    # Re-set x_lims if requested
    if args.x_lims:
        t_lims[0] = args.x_lims[0]
        t_lims[1] = args.x_lims[1]
        if args.exp_tau:
            tau_lims[0] = exp_info.data[(np.abs(exp_info.data[:, 0] - t_lims[0])).argmin(), 1]
            tau_lims[1] = exp_info.data[(np.abs(exp_info.data[:, 0] - t_lims[1])).argmin(), 1]

    # Adjust plot - x and y axes etc.
    adjust_rate_plot(ax1, t_lims, tau_lims)

    # Show/save figure
    fig.tight_layout()
    if args.show:
        plt.show()

    fig.savefig('tau_rates.svg',
                dpi=fig.dpi)


def plot_rates(axis, calc_info, exp_info, args, leg_list):
    """
    Plots tau rates vs experimental rates with no special remove transitions treatment

    Parameters:
        axis : plot axis object
        calc_info : tau data object
        exp_info : experimental data object
        args : Input arguments parser object from read_in_command_line
        leg_list : list of legend entries

    """

    # Check there are enough entries in the legend file
    if args.legend:
        # Excluding experimental data
        if not args.exp_tau:
            if np.size(leg_list) != len(calc_info.file_stats):
                print('Error : Incorrect number of entries in legend file')
                exit()

        # Including experimental data
        elif np.size(leg_list) != len(calc_info.file_stats) + 1:
            print('Error : Incorrect number of entries in legend file')
            exit()

    # Plot the data
    for it in range(np.shape(calc_info.data)[0]):

        if not calc_info.file_stats[it]:
            continue

        # No labels given then make labels with FWHM values
        if len(leg_list) > 0:
            label = leg_list[it]
        else:
            label = 'FWHM = ' + '{:.2f}'.format(calc_info.fwhm_vals[it]) + ' cm$^{-1}$'

        # Plot calculated data
        axis.loglog(calc_info.data[it, :, 1], calc_info.data[it, :, 0],
                    linewidth=2,
                    label=label)

    # Plot experimental data
    if args.exp_tau:
        if args.legend:
            axis.loglog(exp_info.data[:, 0], exp_info.data[:, 1],
                        color='black',
                        marker='o',
                        linewidth=0,
                        label=leg_list[-1])
        else:
            axis.loglog(exp_info.data[:, 0], exp_info.data[:, 1],
                        color='black',
                        marker='o',
                        linewidth=0,
                        label='Experiment')

    return


def adjust_rate_plot(axis, t_lims, tau_lims):
    """
    Adjusts rate plot axes and options

    Parameters:
        axis      (mpl.ax) : plot axis object
        t_lims    (list)   : [min, max] temperatures
        tau_lims  (list)   : [min, max] rates


    Returns:
        None

    """

    # Set legend
    axis.legend(loc=2, numpoints=1, ncol=1, frameon=False, handletextpad=0.3, borderaxespad=0.3, markerscale=1.0,
                handlelength=1.2)

    # Set y axis options
    axis.set_ylabel(r'$\tau^{-1} $ (s$^{-1}$)')

    # Set x and y limits
    axis.set_ylim([tau_lims[0] * 0.80, tau_lims[1] * 1.20])
    axis.set_xlim([np.max([myround(t_lims[0]) * 0.95, 1]), myround(t_lims[1]) * 1.05])

    # Set x axis options
    axis.set_xlabel('Temperature (K)')
    axis.xaxis.set_minor_formatter(NullFormatter())
    axis.xaxis.set_major_formatter(ScalarFormatter())
    axis.tick_params(axis='both',
                     which='both',
                     length=2.0)

    # Set x axis tick locatons
    axis.xaxis.set_tick_params(which='minor',bottom=False)
    min_steps = {50:10, 100:15, 150:25, 200:35, 250:50, 300:60}
    t_range = myround(myround(t_lims[1]) - myround(t_lims[0]),50)
    if t_range > 300 :
        tick_step = 100
    else:
        tick_step = min_steps[t_range]
    x_tick_vals = np.arange(myround(t_lims[0]), myround(t_lims[1])+tick_step, tick_step)
    axis.set_xticks(x_tick_vals)

    # Remove right and top parts of plot box
    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)

    return

def myround(x, base=5):
    return base * np.ceil(x/base)

class RemovalCommonStore:
    # Class to contain non-rate information used by remove transitions plot

    def __init__(self):

        # List of tau output files - 1 for each removed transition
        self.removal_files = None

        # List of removed transitions 
        self.transitions = None

        # J quantum number value
        self.J = None

        # Jz expectation value
        self.J_z = []

        # Crystal field energies
        self.CF_energies = []

    def get_transitions(self, results_folder):
        """
        Retrieves tau output files for removal calculations, and transition labels

        Parameters:
            results_folder (str) : Absolute path to the tau outputs generated from autau --remove_transition option.

        Returns:
            None
        """

        # List of files in removal_results folder.
        self.removal_files = os.listdir(results_folder)

        # Remove files which don't contain a dash - separating transitions
        self.removal_files = [i for i in self.removal_files if '-' in i]

        if len(self.removal_files) < 1:
            print('No valid files found in removal_results')
            exit('Files should be named removal_x-y by autau')

        # Get transitions from filenames
        self.transitions = [(i.split('-')[0].split('_')[-1], i.split('-')[1].split('.')[0]) for i in self.removal_files]

        # Add folder to start of file names
        self.removal_files = [os.path.join('removal_results', i) for i in self.removal_files]

        return

    def get_electronic_structure(self, input_file):

        """
        Retrieve the electronic structure from the crystal field description.

        Parameters:
            input_file (str): tau output file.

        Returns:
            J_z         (list): J_z values of each of the electronic states.
            CF_energies (list): Crystal field energies of the electronic states.
        """

        # Get the J_z and CF_energies values.
        with open(input_file, 'r') as f:
            for line in f:
                if line.find('Metal Ion Data') != -1:
                    for i in range(6):
                        next(f)
                    line = next(f).split()[-1]
                    if len(line.split('/')) == 2:
                        self.J = float(line.split('/')[0])/float(line.split('/')[1])
                    else:
                        self.J = float(line)
                if line.find('<J_z> Values') != -1:
                    next(f)
                    for i in range(int(2 * self.J + 1)):
                        self.J_z.append(float(next(f).split()[2]))

                if line.find('Equilibrium Crystal Field Eigenvalues (cm^-1)') != -1:
                    next(f)
                    for i in range(16):
                        self.CF_energies.append(float(next(f).split()[2]))
        return


def plot_levels_and_transitions(axis, removal_common, labels):
    """
    Plot energy levels with arrows for each removed transition

    Parameters
    ----------
    axis
    removal_common
    labels
    """

    # Energy levels
    axis.plot(removal_common.J_z, removal_common.CF_energies,
              marker='_',
              markersize=30,
              mew=2.5,
              linewidth=0.0,
              c='k', zorder=0)

    colors = ['#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b']

    # Transition arrows
    for count, (ini, fin) in enumerate(removal_common.transitions):
        """
        quiver([X, Y], U, V, [C], **kw) 
        where X, Y define the coordinates of the arrow locations, 
              U, V define the arrow directions (The x and y direction components of the arrow vectors). 
        """
        axis.quiver(removal_common.J_z[int(ini) - 1],
                    removal_common.CF_energies[int(ini) - 1],
                    removal_common.J_z[int(fin) - 1] - removal_common.J_z[int(ini) - 1],
                    removal_common.CF_energies[int(fin) - 1] - removal_common.CF_energies[int(ini) - 1],
                    scale_units='xy', angles='xy', color=colors[count], scale=1, lw=1.5, label=labels[count], zorder=10)

def adjust_barrier_plot(axis, removal_common, num_x_ticks):
    """
    Adjust plot axes and options of barrier figure

    Parameters
    ----------
    axis
    removal_common
    num_x_ticks - number of x_ticks requested by user
    """

    # Set x axis tick positions
    # num_x_ticks must be deincremented by 1 to give the correct number of ticks
    # because tick positions are at steps of 2J/(num_x_ticks-1)

    if (num_x_ticks == 1): num_x_ticks = 2
    tick_step = (2*removal_common.J)/(num_x_ticks-1)
    Jz_tick_vals = np.arange(-removal_common.J, removal_common.J + 1, tick_step)
    axis.set_xticks(Jz_tick_vals)

    # Set x axis tick labels
    labels = []

    # Write fractions if Kramers
    if removal_common.J % 2 != 0:
        for it, val in enumerate(Jz_tick_vals):
            if val == 0:
                labels.append('0')
            else:
                labels.append('{:d}/2'.format(int(val*2.)))
    # Write pure numbers if non-Kramers
    else:
        for it, val in enumerate(Jz_tick_vals):
            labels.append('{:d}'.format(int(val)))

    axis.set_xticklabels(labels, rotation=45)

    # Set x and y limits 
    axis.set_xlim([np.min(removal_common.J_z) * 1.2, np.max(removal_common.J_z) * 1.2])

    # Set legend 
    axis.legend(loc=0, numpoints=1, ncol=1, frameon=False)

    # Set axis labels
    axis.set_xlabel(r'$\langle \ \hat{J}_{z} \ \rangle$')
    axis.set_ylabel(r'Energy (cm$^{-1}$)')

    # Remove spines
    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)


def removal_plot(args, removal_common, removal_info, full_info, exp_info):
    """
    Plot energy levels with arrows for each removed transition alongside rates for each removed transition

    Parameters:
        args             (parser.parse_args) : Input arguments parser object from read_in_command_line
        removal_common   (RemovalCommonStore) : object containing common data for all removal calcs - Jz, CF energies,
                                    transition labels
        removal_info     (CalcInfoStore) : removal tau calculations data object
        full_info        (CalcInfoStore) : full tau calculation data object
        exp_info         (ExpInfoStore) : Experimental data object

    Returns:
        None
    """

    if args.font_size:
        plt.rcParams.update({'font.size': args.font_size})
    else:
        plt.rcParams.update({'font.size': 10})

    # Make figure, lhs = energy levels, rhs = rates plot
    fig, (ax1, ax2) = plt.subplots(1, 2,
                                   figsize=(8, 4.5),
                                   num='Removal plot')

    # Transition labels
    labels_left = [r'$\| {} \rangle \leftrightarrow \| {} \rangle$'.format(initial, final) for initial, final in
                   removal_common.transitions]
    labels_right = [r'$\| {} \rangle \nleftrightarrow \| {} \rangle$'.format(initial, final) for initial, final in
                    removal_common.transitions]

    # Plot arrows and levels
    plot_levels_and_transitions(ax1, removal_common, labels_left)

    # Plot experimental data
    if args.exp_tau:
        ax2.loglog(exp_info.data[:, 0], exp_info.data[:, 1],
                   color='black',
                   marker='o',
                   markersize=6,
                   linewidth=0,
                   label='Experiment')

    # Initialise the limits
    t_lims = [0, 1]
    tau_lims = [0, 1]

    # Get minimum and maximum temperatures for plot axes
    t_lims[0] = np.nanmin([full_info.min_min_T, removal_info.min_min_T, exp_info.min_min_T])
    t_lims[1] = np.nanmax([full_info.max_max_T, removal_info.max_max_T, exp_info.max_max_T])

    # Get minimum and maximum rates
    tau_lims[0] = np.nanmin([full_info.min_min_rate, removal_info.min_min_rate, exp_info.min_min_rate])
    tau_lims[1] = np.nanmax([full_info.max_max_rate, removal_info.max_max_rate, exp_info.max_max_rate])

    # Use user provided lims if requested
    if args.x_lims:
        t_lims[0] = args.x_lims[0]
        t_lims[1] = args.x_lims[1]
        # If Experiment is given then readjust y-lims to match max and min of experiment
        if args.exp_tau:
            tau_lims[0] = exp_info.data[(np.abs(exp_info.data[:, 0] - t_lims[0])).argmin(), 1]
            tau_lims[1] = exp_info.data[(np.abs(exp_info.data[:, 0] - t_lims[1])).argmin(), 1]

    # Plot full rate data
    args.exp_tau = None
    plot_rates(ax2, full_info, exp_info, args, ['Total,\nFWHM={:.1f}'.format(full_info.fwhm_vals[0]) + r' cm$^{-1}$'])

    # Plot removal rates
    plot_rates(ax2, removal_info, exp_info, args, labels_right)

    # Adjust rates plot axes and set legend
    adjust_rate_plot(ax2, t_lims, tau_lims)

    # Adjust rates plot axes
    adjust_barrier_plot(ax1, removal_common, args.n_barrier_x_ticks)

    # Show/save figure
    fig.tight_layout()
    if args.show:
        plt.show()

    fwhm_name = str(full_info.fwhm_vals[0]).split('.')
    save_name = 'tau_removal_rates_fwhm' + fwhm_name[0] + '-' + fwhm_name[1] + '.png'

    fig.savefig(save_name, dpi=300)

    return


if __name__ == '__main__':
    # Read in command line arguments
    args = read_in_command_line()

    if args.remove_transitions:

        # Create object to hold information on full file
        # i.e. no transitions removed
        full_info = get_calculated_data(args.tau_outputs)

        # Check if removal folder is in cwd
        check_removal_folder()

        # Object to hold transition labels, tau file locations, Jz, CF Energies
        removal_common = RemovalCommonStore()

        # Get transition labels and tau files
        folder = os.getcwd()
        folder = os.path.join(folder, 'removal_results')
        removal_common.get_transitions(folder)

        # Get Jz and CF energies from full file
        removal_common.get_electronic_structure(args.tau_outputs[0])

        # Create object to hold information (rates, temps...) on removal files
        # i.e. tau calculations with transitions removed
        removal_info = get_calculated_data(removal_common.removal_files)

        # Read in data from experiment if requested
        exp_info = get_experimental_data(args)

        # Create plot
        removal_plot(args, removal_common, removal_info, full_info, exp_info)

    else:

        # Read in data from tau
        calc_info = get_calculated_data(args.tau_outputs)

        # Read in data from experiment if requested
        exp_info = get_experimental_data(args)

        # Create plot
        normal_plot(calc_info, exp_info, args)

######################################
