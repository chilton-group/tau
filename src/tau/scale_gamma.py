#!/usr/bin/env python3

import numpy as np
import argparse
import subprocess


def get_user_args():

    description = """
    Script to swap customary transition elements between two gamma matrices.
    """

    parser = argparse.ArgumentParser( description=description,
                                      formatter_class=argparse.RawDescriptionHelpFormatter )

    parser.add_argument('gamma1',                 type=str, help='File containing the first gamma matrix.' )
    parser.add_argument('gamma2',                 type=str, help='File containing the second gamma matrix.' )
    parser.add_argument('transitions', nargs='*', type=str, help='String of initial states. Examples: 1-3 1-5 or 1-3 2-all or 1-[5,12].' )
    parser.add_argument('temperature',            type=str, help='Temperature used to calculate the input gamma matrices.' )

    args = parser.parse_args()
    
    return args

def init_final(transition):

    initial = []
    final   = []

    # Define the initial state
    initial.append( int( transition.split('-')[0] ) )

    # Define the final state(s) from this initial state
    if transition.split('-')[1] == 'all':
        first_final = int( transition.split('-')[0] ) + 1
        last_final  = num_states
        final.append( np.arange(first_final, last_final+1) )
    elif '[' and ']' in transition.split('-')[1]:
        first_final = int( transition.split('-')[1].replace('[','').replace(']','').split(',')[0] )
        last_final  = int( transition.split('-')[1].replace('[','').replace(']','').split(',')[1] )
        final.append( np.arange(first_final, last_final+1) )
    else:
        final.append( int( transition.split('-')[1] ) )

    return initial, final

# ---------------------

if __name__ == '__main__':

    # Store the names of the gamma files and the transitions requested
    args = get_user_args()
    if args.transitions == [] :
        exit(10)

    # Get the dimensions of the matrix
    g1, g2 = np.loadtxt(args.gamma1), np.loadtxt(args.gamma2)
    if np.shape(g1) != np.shape(g2) or g1.shape[0] != g1.shape[1] or g1.shape[0] != g2.shape[0]:
        print('The indicated gammas do not have the correct size. Exiting.')
        exit()
    num_states = g1.shape[0]

    # Define a matrix of ones which will be used to scale the original matrices g1 and g2.    
    mask = np.zeros([num_states,num_states])
    
    # Get the transitions and define initial to final states for each transition
    transitions = args.transitions
    for trans in transitions:
        initial, final = init_final(trans)

        # only the elements corresponding to the investigated transitions are updated
        for i in initial:
            for f in final:
                mask[i-1,f-1] = 1
                mask[f-1,i-1] = 1

    # !!!!!!!!!!!!!!
    #scale based only on the upper triangular matrix with an optional argument (--upper_trian)
    # !!!!!!!!!!!!!!

    np.savetxt('mask.tmp', mask, fmt = '%d')

    # Call fortran program - AKA the real stuff 

    cmd = "scale_gamma_fort " + str(num_states) + " " + args.gamma1 + " " + args.gamma2 
    subprocess.call(cmd, shell=True)

    # Change the scaled matrices' file names
    mod = '_'.join(transitions)

    cmd = "mv gamma_1_mod.dat gamma_1_"+mod+"_"+args.temperature+"_K.dat"
    subprocess.call(cmd, shell=True)
    cmd = "mv gamma_2_mod.dat gamma_2_"+mod+"_"+args.temperature+"_K.dat"
    subprocess.call(cmd, shell=True)

