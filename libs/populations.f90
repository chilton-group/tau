MODULE POPULATIONS
    USE, INTRINSIC :: iso_fortran_env
    USE prec, ONLY : WP
    IMPLICIT NONE

    CONTAINS

    SUBROUTINE POPSIM(total_gamma, energies, n_states, temps, moments, time_step, n_time_steps)
        USE OMP_LIB
        USE matrix_tools
        REAL(KIND = WP), INTENT(IN) :: total_gamma(:,:,:), energies(:), moments(:), temps(:), &
                                       time_step
        INTEGER, INTENT(IN) :: n_states, n_time_steps
        REAL(KIND = WP), ALLOCATABLE :: pops(:,:), time(:), augmented_gamma(:,:), aug_vecs(:,:), &
                                        aug_vals(:), sim_moment(:)
        INTEGER :: t, row, col, unit
        CHARACTER(LEN=1000) :: file_name, FMT, FMTT

        !$OMP PARALLEL SHARED(temps, energies, moments, n_states, total_gamma, n_time_steps)
        !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(t, pops, time, augmented_gamma, aug_vecs, aug_vals), &
        !$OMP& PRIVATE(file_name, FMTT, FMT, sim_moment, unit)
        DO t = 1, SIZE(temps)

            ! Calculate and diagonalise augmented gamma matrix
            ! See JChemEd on Master Equation

            ALLOCATE(augmented_gamma(n_states, n_states))

            CALL AUGMENT_GAMMA(total_gamma(t, :, :), energies, n_states, temps(t), augmented_gamma)

            CALL DIAGONALISE_AUGMENTED_GAMMA(n_states, augmented_gamma, aug_vecs, aug_vals)

            ! Calculate population at 100 K of each state over time

            CALL SIMULATE_POPULATIONS(temps(t), n_states, energies, aug_vecs, aug_vals, time_step, &
                                      n_time_steps, pops, time)

            ! Write out population file
            WRITE(file_name, '(A, I0, A)') "populations_",INT(temps(t)), "_K.dat"
            unit = 85+t
            OPEN(unit, FILE = file_name, STATUS = 'UNKNOWN')

                WRITE(FMTT,'(I0)') n_time_steps
                FMT = '('// trim(adjustl(FMTT)) //'E47.36E4)'

                WRITE(unit, FMT) time
                DO row = 1, n_states
                    write(unit,FMT) (pops(row, col), col = 1, n_time_steps)
                END DO

            CLOSE(unit)

            ! Calculate magnetic moment over time
            ALLOCATE(sim_moment(SIZE(time)))
            sim_moment = 0.0_WP
            sim_moment = ALT_MATMUL(TRANSPOSE(pops), moments)

            ! Write out population file
            WRITE(file_name, '(A, I0, A)') "moments_",INT(temps(t)), "_K.dat"
            OPEN(unit, FILE = TRIM(file_name), STATUS = 'UNKNOWN')
                DO row = 1, SIZE(time)
                    WRITE(unit,"(2E47.36E4)") time(row), sim_moment(row)
                END DO
            CLOSE(unit)

            DEALLOCATE(augmented_gamma, aug_vecs, aug_vals, sim_moment, time, pops)

        END DO
        !$OMP END DO NOWAIT
        !$OMP END PARALLEL

    END SUBROUTINE POPSIM


    SUBROUTINE AUGMENT_GAMMA(gamma_mat, EQ_evals, max_state, T, augmented_gamma)
        USE constants, ONLY : kB
        USE matrix_tools
        ! Symmetrises Gamma by multiplying by diagonal matrix of crystal field state
        ! boltzmann populations on either side
        IMPLICIT NONE
        REAL(KIND = WP), INTENT(IN)  :: gamma_mat(:,:),EQ_evals(:), T
        REAL(KIND = WP), INTENT(OUT) :: augmented_gamma(:,:)
        REAL(KIND = WP), ALLOCATABLE :: Pi_sqrt_inv(:,:), Pi_sqrt(:,:)
        REAL(KIND = WP)              :: p_func
        INTEGER, INTENT(IN)          :: max_state
        INTEGER                      :: K, row
    
        ALLOCATE(Pi_sqrt(max_state, max_state), Pi_sqrt_inv(max_state, max_state))
        Pi_sqrt = 0.0_WP
        Pi_sqrt_inv = 0.0_WP
    
        ! Calculate partition function
        p_func = 0.0_WP
        DO K = 1, max_state
            p_func = p_func + EXP(-EQ_evals(K)/(kB*T))
        END DO
    
        ! Calculate diagonal matrix of Boltzmann populations - (Pi^1/2)^-1 in /10.1021/ed2001329
        DO row = 1, max_state
            Pi_sqrt(row,row) = SQRT(EXP(-EQ_evals(row)/(kB*T)) / p_func)
            Pi_sqrt_inv(row,row) = 1.0_WP/Pi_sqrt(row,row)
        END DO
    
        ! Calculate augmented_gamma_matrix weighted by populations
        augmented_gamma = ALT_MATMUL(ALT_MATMUL(Pi_sqrt_inv, gamma_mat),Pi_sqrt)
    
        DEALLOCATE(Pi_sqrt, Pi_sqrt_inv)

    END SUBROUTINE AUGMENT_GAMMA
    
    SUBROUTINE DIAGONALISE_AUGMENTED_GAMMA(n_states, aug_gamma, vecs, vals)
        ! Diagonalises gamma matrices using external c++ function
        USE fort_diag
        USE OMP_LIB
        USE matrix_tools
        IMPLICIT NONE
        INTEGER                      :: it
        INTEGER, INTENT(IN)          :: n_states
        INTEGER(KIND = C_INT)        :: arr_len
        INTEGER, ALLOCATABLE         :: companion(:,:)
        REAL(KIND = WP), INTENT(IN)  :: aug_gamma(:,:)
        REAL(KIND = WP), ALLOCATABLE :: tmp_vec(:,:), imag_vecs(:,:), imag_vals(:)
        REAL(KIND = WP), INTENT(OUT), ALLOCATABLE :: vecs(:,:), vals(:)
    
        ALLOCATE(tmp_vec(n_states, n_states), vecs(n_states, n_states), vals(n_states), companion(n_states,2))
        ALLOCATE(imag_vecs(n_states, n_states))
        ALLOCATE(imag_vals(n_states))
        vecs = 0.0_WP
        tmp_vec = 0.0_WP
        vals = 0.0_WP
        companion = 0
        imag_vals = 0.0_WP
        imag_vecs = 0.0_WP
    
        arr_len = INT(n_states, C_INT)
    
        ! Call c function to diagonalise Gamma
        CALL real_c_diag(arr_len, aug_gamma, tmp_vec, vals, imag_vecs, imag_vals)

        IF (ANY(imag_vals /= 0.0_WP)) THEN
            WRITE(6,*) 'Imaginary eigenvalues detected'
        END IF
    
        ! Sort eigenvalues from big to small
        CALL BUBBLE(vals, companion, 'max')
    
        DO it = 1, n_states
            vecs(companion(it,1),:) = tmp_vec(companion(it,2),:)
        END DO

        DEALLOCATE(tmp_vec, companion, imag_vecs, imag_vals)
    
    END SUBROUTINE DIAGONALISE_AUGMENTED_GAMMA
    
    
    SUBROUTINE SIMULATE_POPULATIONS(T, n_states, EQ_evals, aug_vecs, aug_vals, time_step, n_time_steps, pop, time)
        USE constants, ONLY : kB
        USE matrix_tools
        IMPLICIT NONE
        REAL(KIND = WP), INTENT(IN)       :: EQ_evals(:), T, aug_vecs(:,:), aug_vals(:), time_step
        INTEGER, INTENT(IN)               :: n_states, n_time_steps
    
        INTEGER                           :: row, K, t_ind
        REAL(KIND = WP), ALLOCATABLE      :: CPi(:,:), Pi_sqrt_inv(:,:), Pi_sqrt(:,:), pop0(:), &
                                             pop0_NM(:)
        REAL(KIND = WP)                   :: p_func
        REAL(KIND = WP), INTENT(OUT), ALLOCATABLE :: pop(:,:), time(:)
    
        ALLOCATE(CPi(n_states, n_states), Pi_sqrt(n_states, n_states), &
                 Pi_sqrt_inv(n_states, n_states))
    
        CPi = 0.0_WP
        Pi_sqrt = 0.0_WP
        Pi_sqrt_inv = 0.0_WP
    
        ! Calculate partition function
        p_func = 0.0_WP
    
        DO K = 1, n_states
            p_func = p_func + EXP(-EQ_evals(K)/(kB*T))
        END DO
    
        ! Calculate diagonal matrix of Boltzmann populations - (CPi^1/2)^-1 in /10.1021/ed2001329
        DO row = 1, n_states
            CPi(row,row) = EXP(-EQ_evals(row)/(kB*T)) / p_func
            Pi_sqrt(row,row) = SQRT(CPi(row,row))
            Pi_sqrt_inv(row,row) = 1.0_WP/Pi_sqrt(row,row)
        END DO
    
        ! Write initial population and then express in normal mode basis
        ALLOCATE(pop0(n_states), pop0_NM(n_states))
        pop0 = 0.0_WP
        pop0_NM = 0.0_WP
        pop0(1) = 1.0_WP ! Unit population in lowest energy state
        ! population in normal mode basis
        pop0_NM = ALT_MATMUL(ALT_MATMUL(TRANSPOSE(aug_vecs),Pi_sqrt_inv),pop0)
    
        ! Calculate timestep vector
        ALLOCATE(time(n_time_steps))
        time = 0.0_WP
    
        DO t_ind = 1, n_time_steps
            time(t_ind) = t_ind * time_step
        END DO
    
        ALLOCATE(pop(n_states, n_time_steps))
        pop = 0.0_WP
    
        DO t_ind = 1, n_time_steps
            pop(:, t_ind) = ALT_MATMUL(Pi_sqrt, ALT_MATMUL(aug_vecs, pop0_NM * EXP(aug_vals*time(t_ind))))
            pop(:, t_ind) = ABS(pop(:, t_ind)) / SUM(ABS(pop(:, t_ind)))
        END DO
    
        DEALLOCATE(Pi_sqrt, Pi_sqrt_inv, CPi, pop0, pop0_NM)

    END SUBROUTINE SIMULATE_POPULATIONS

END MODULE POPULATIONS

