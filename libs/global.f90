MODULE global
    USE types, ONLY : file_list
    IMPLICIT NONE
    TYPE(file_list) :: files

CONTAINS

    SUBROUTINE SET_IO_FILE_NAMES(files, input, autau_tf)
        IMPLICIT NONE
        TYPE(file_list), INTENT(OUT)   :: files
        LOGICAL, INTENT(IN)            :: autau_tf
        CHARACTER(LEN = *), INTENT(IN) :: input
        CHARACTER(LEN = 1000)          :: temp

        ! If using autau then get the path from the input file name
        IF (autau_tf) THEN
            files%path   = TRIM(GET_PATH(input))
        ! Else make a system call for the path
        ELSE
            CALL GETCWD(temp)
            files%path = TRIM(ADJUSTL(temp))//'/'
        END IF

        files%input  = TRIM(input)
        files%output = TRIM(files%path)//'tau.out'
        files%cc_output = TRIM(files%path)//'tau_input_to_CC-FIT2.txt'

    END SUBROUTINE SET_IO_FILE_NAMES

    SUBROUTINE SET_FILE_NAMES(files, static_cfps)
        IMPLICIT NONE
        TYPE(file_list), INTENT(INOUT)   :: files
        LOGICAL, INTENT(IN)              :: static_cfps

        files%EQ_CFPs = TRIM(files%path)//'EQ_CFPs.dat'
        files%mode_energies = TRIM(files%path)//'mode_energies.dat'
        files%mode_weights = TRIM(files%path)//'mode_weights.dat'
        files%fwhm = TRIM(files%path)//'fwhm.dat'

    ! Set correct file name for parameter files
        IF (static_cfps) THEN
            files%SP_CFPs = TRIM(files%path)//'CFP_steps.dat'
        ELSE
            files%SP_CFPs = TRIM(files%path)//'CFP_polynomials.dat'
        END IF

    END SUBROUTINE SET_FILE_NAMES

    FUNCTION GET_PATH(name) RESULT(path)
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN) :: name
        CHARACTER(LEN = LEN(name))     :: path

        path = Cut_at_Char(name, '/')

    END FUNCTION GET_PATH

    FUNCTION Cut_at_Char (s,cutchar)  RESULT(outs)
        ! Truncates a string at a given character
        ! e.g Cut_To_Char(Hello_World,_) = Hello

        CHARACTER(*)            :: s,cutchar
        CHARACTER(LEN(s)+100)   :: outs
        INTEGER                 :: i

        i = INDEX(s,cutchar, back=.TRUE.)
        outs = s(1:i)
    END FUNCTION Cut_at_Char

END MODULE global
