MODULE constants

    USE, INTRINSIC::iso_fortran_env
    USE prec
    IMPLICIT NONE

    REAL(KIND = WP), parameter    :: pi = 3.141592653589793238462643383279502884197_WP
    REAL(KIND = WP), parameter    :: kB = 0.6950387_WP !cm-1 / K
    REAL(KIND = WP), parameter    :: hbar = 5.30886139629910E-12_WP !cm-1 . s
    REAL(KIND = WP), parameter    :: light = 299792458.0_WP !m / s
    REAL(KIND = WP), parameter    :: muB = 0.466866577042538_WP !cm-1 / T
    INTEGER, parameter            :: harmonic_limit = 1000
    INTEGER, parameter            :: num_time_steps = 10000
    REAL(KIND = WP), parameter    :: vib_temp = 100.0_WP
    COMPLEX(KIND = WP), parameter :: ii=(0.0_WP, 1.0_WP)
    REAL(KIND = WP), parameter    :: ge = 2.00231930436256_WP


END MODULE constants
