module prec
    use, intrinsic::iso_fortran_env
    implicit none
    integer, parameter::WP = selected_real_kind(33, 4931)
end module prec
