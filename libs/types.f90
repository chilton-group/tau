MODULE types
    IMPLICIT NONE
    TYPE file_list
       CHARACTER(LEN = 1000) :: path, input, mode_energies, mode_weights, SP_CFPs, EQ_CFPs, calibration, output, fwhm, cc_output
    END TYPE file_list

END MODULE types
