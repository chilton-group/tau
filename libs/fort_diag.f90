MODULE fort_diag
    USE iso_c_binding
    USE prec
    IMPLICIT NONE

! Module for interfaces to C++/eigen quad precision diagonalisation routines
    INTERFACE
        SUBROUTINE real_c_diag(arr_len, array, vecs, vals, imag_vecs, imag_vals) bind(C,name='real_c_diag_')
        USE iso_c_binding
        USE prec
        INTEGER(C_INT)      :: arr_len
        REAL(KIND = WP)     :: array(arr_len, arr_len), vecs(arr_len, arr_len), vals(arr_len), &
                               imag_vecs(arr_len, arr_len), imag_vals(arr_len)
        END SUBROUTINE real_c_diag
    END INTERFACE

    INTERFACE
        SUBROUTINE complex_c_diag(arr_len, array, vecs, vals) bind(C,name='complex_c_diag_')
        USE iso_c_binding
        USE prec
        INTEGER(C_INT)      :: arr_len
        COMPLEX(KIND = WP)  :: array(arr_len, arr_len), vecs(arr_len, arr_len)
        REAL(KIND = WP)     :: vals(arr_len)
        END SUBROUTINE complex_c_diag
    END INTERFACE

END MODULE fort_diag