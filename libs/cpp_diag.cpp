#include <boost/multiprecision/float128.hpp>
#include <boost/multiprecision/complex128.hpp>
#include <iostream>
#include <boost/multiprecision/eigen.hpp>
#include <quadmath.h>
#include <Eigen/Dense>
#include <complex>

using namespace boost::multiprecision;
using namespace Eigen;
using namespace std;

//template<class Complex>

extern "C" {
void real_c_diag_(int *size, float128 *array, float128 *vecs, float128 *vals, float128 *imag_vecs, float128 *imag_vals)
{

	// Make a dynamically sized matrix A and initialise to zero
    Matrix<float128, Dynamic, Dynamic> A(*size, *size);
    A.setZero();

    // Loop over values of input array and copy into A
	int k = 0;
    for(int i = 0; i < *size; i++){
    	for(int j = 0; j< *size; j++) {
	    	A(j,i) = array[k];
			k++;
	    }
    }

    // Find eigenvalues and eigenvectors
	EigenSolver<Matrix<float128, Dynamic, Dynamic>> es(A);

	// Print array, vectors, and values
    //cout << endl << "You gave me the matrix:" << endl << A << endl << endl;
    //cout.setf(ios_base::showpoint);
    //cout << setprecision(std::numeric_limits<float128>::max_digits10);
    //cout << scientific << fixed;
    //cout << "The eigenvalues of this matrix are:" << endl << es.eigenvalues() << endl << endl;
    //cout << "The matrix of eigenvectors is:" << endl << es.eigenvectors() << endl << endl;
    
    // Write eigenvectors to vecs - this is then passed back to fortran
    k = 0;
    for(int i = 0; i < *size; i++){
    	for(int j = 0; j<*size;j++) {
	    	vecs[k] = es.eigenvectors()(j,i).real();
	    	k++;
	    }
    }

    // Write eigenvalues to array - this is then passed back to fortran
    for(int i = 0; i < *size; i++){
	    vals[i] = es.eigenvalues()(i).real();
    }
}
}

extern "C" {
void complex_c_diag_(int *size, complex128 *array, complex128 *vecs, float128 *vals)
{

	// Make a dynamically sized matrix A and initialise to zero
    Matrix<complex128, Dynamic, Dynamic> A(*size, *size);
    A.setZero();

    // Loop over values of input array and copy into A
	int k = 0;
    for(int i = 0; i < *size; i++){
    	for(int j = 0; j< *size; j++) {
	    	A(j,i) = array[k];
			k++;
	    }
    }

    // Find eigenvalues and eigenvectors
	SelfAdjointEigenSolver<Matrix<complex128, Dynamic, Dynamic>> es(A);

    //cout << typeid(es.eigenvectors()(0,0)).name() << endl;

    //
    // Write eigenvectors to vecs - this is then passed back to fortran
    k = 0;
    for(int i = 0; i < *size; i++){
    	for(int j = 0; j<*size;j++) {
	    	vecs[k] = es.eigenvectors()(j,i) ;
	    	k++;
	    }
    }
    // Write eigenvalues to array - this is then passed back to fortran
    for(int i = 0; i < *size; i++){
	    vals[i] = es.eigenvalues()(i).real() ;
    }

    //cout.setf(ios_base::showpoint);
    //cout << setprecision(numeric_limits<complex128>::digits10);
    //cout << setprecision(numeric_limits<float128>::max_digits10);
    //cout << scientific << fixed;
    //cout << "The eigenvalues of this matrix are:" << endl ;
    // Print array, vectors, and values
    //cout << endl << "You gave me the matrix:" << endl << A << endl << endl;
    //cout << "The matrix of eigenvectors is:" << endl << es.eigenvectors() << endl << endl;


    //for(int i = 0; i < *size; i++){
    //    cout << *vals << endl;
    //    vals++;
    //}
}
}
