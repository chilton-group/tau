Theory
======

.. toctree::
   :maxdepth: 3
   :hidden:

    Spin Dynamics <spin_dynamics>
    Normal Mode Displacements in Gaussian <normal_mode_displacements>
    Mode-weighted Matrix Elements <mode_weighted_matrix_elements>

``tau`` is a program for investigating the ab initio magnetic relaxation dynamics of single molecule magnets (SMMs). Tau calculates magnetic relaxation rates typically obtained by DC decay and AC susceptibility measurements. The theory used in Tau can be found principally in Kragskow `et al` but also in other published works, though a recap is given here for the purposes of highlighting the features of the program. 


References

D. Reta, J. G. C. Kragskow and N. F. Chilton, `J. Am. Chem. Soc.`, 2021, 143, 15, 5943-5950

J. G. C. Kragskow, A. Mattioni, J. K. Staab, D. Reta, J. M. Skelton and N. F. Chilton, `Coord. Chem. Rev.`, 2023, in press.
