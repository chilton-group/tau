Normal Mode Displacements from Gaussian
====================

The zero point (\ :math:`n_j=0`) displacement :math:`Q_{j,0}` of mode :math:`j` with force constant :math:`k_j` and zero point energy :math:`\hbar\omega_j=hc{\bar{\nu}}_j=h\nu_j` is

.. math ::
    :label: eq_1

	Q_{j,0}=\sqrt{\frac{\hbar\omega_j}{k_j}}=\sqrt{\frac{hc{\bar{\nu}}_j}{k_j}}=\sqrt{\frac{h\nu_j}{k_j}}

Gaussian gives :math:`k` in [mDyne Å\ :sup:`-1`] =  100 [N m\ :sup:`-1`] = [N cm\ :sup:`-1`] in SI units, and :math:`\bar{\nu}` in wavenumbers [cm\ :sup:`-1`]. Therefore we must use the speed of light :math:`c` in units of [m s\ :sup:`-1`] to obtain :math:`Q_{j,0}` in [m].

.. math ::
    :label: eq_2

	Q_{j,0}\left[\mathrm{m}\right]=\sqrt{\frac{h\left[\mathrm{J}\ \mathrm{s}\right]\ c\ \left[\mathrm{m}\ \mathrm{s}^{-1}\right]\ \bar{\nu}\left[\mathrm{cm}^{-1}\right]\ }{k\ \left[\mathrm{N \ cm}^{-1}\right]}}=\sqrt{\frac{\left[\mathrm{J \ m} \right]}{\left[\mathrm{N}\right]}}=\sqrt{\frac{\left[\mathrm{N}\ \mathrm{m}^2\right]}{\left[\mathrm{N}\right]}}=\sqrt{\left[\mathrm{m}\right]^2}=[\mathrm{m}] 

Converting to Angstroms:

.. math ::
    :label: eq_3

	Q_{j,0}[\mathrm{Å}]=10^{10}{\mathrm{Åm}}^{-1} Q_{j,0}[\mathrm{m}]

Gaussian prints force constants with terrible precision (F6.4). For some modes, the force constant will be very small i.e. <0.0001, meaning our displacements will suffer from this lack of precision.

To get around this we can use the reduced mass  :math:`\mu` [AMU] printed by Gaussian to obtain the force constant of a given mode.

We know 

.. math ::
    :label: eq_4

	\nu=\bar{\nu}c=\frac{1}{2\pi}\sqrt{\frac{k}{\mu}}

So rearranging this, and adding in the units explicitly 

.. math ::
    :label: eq_5

	k\left[\mathrm{N \ cm}^{-1}\right]=\ k\ \left[100\mathrm{\ N\ m}^{-1}\right]=100 \times 4\pi^2\ {\bar{\nu}}^2\ \left[\mathrm{cm}^{-1}\right]^2\ c^2\ \left[\mathrm{cm \ s}^{-1}\right]^2\ \mu\ [{\mathrm{AMU}}]
`
Where we must convert from [AMU] to [kg] 

.. math ::
    :label: eq_6

	1\ \left[{\mathrm{AMU}}\right]=\ 1.66054\times{10}^{-27}\ [\mathrm{kg}]


Then, finally

.. math ::
    :label: eq_7

	k\ \left[100\ \mathrm{N \ m}^{-1}\right]=100\times1.66054\times\ {10}^{-27}[\mathrm{kg \ AMU}^{-1}] \times 4\pi^2 \bar{\nu}^2 [\mathrm{cm}^{-1}] c^2 [\mathrm{cm \ s}^{-1}]^2 \mu [{\mathrm{AMU}}]

Putting the values for \mu and \bar{\nu} from a Gaussian .log file into this equation will give a value for k which matches the one in the log file, albeit at much higher precision.
