Mode-weighted Matrix Elements
==============================

The total gamma matrix :math:`\bar{\bar{\Gamma}}` can be expressed as a linear combination of single mode gamma matrices: :sup:`[1]`

.. math ::
    \bar{\bar{\Gamma}}= \sum_{j}{\bar{\bar{\Gamma}}}_j

The contribution of a given single mode gamma to the total gamma is then:

.. math ::
    \frac{{\bar{\bar{\Gamma}}}_j}{\bar{\bar{\Gamma}}}= \bar{\bar{c_j}}	

A new set of mode-weighted spin-phonon (SP), occupancy (Occ), and phonon DOS (pDOS) matrices can be formed
as linear combinations of the mode-specific SP, Occ, and pDOS matrices, where the expansion coefficients
are the elements of :math:`\bar{\bar{c_j}}`. For example:

.. math ::
    \langle{\bar{\bar{H}}}_\mathrm{SP}\rangle = \sum_{j}\bar{\bar{c_j}}\circ{\bar{\bar{H}}}_{\mathrm{SP},j}

were :math:`\circ` is the Hadamard product (element-wise matrix multiplication). Then, the gamma matrix can be exactly decomposed as:

.. math ::
    \bar{\bar{\Gamma}} = \frac{2\pi}{\hbar} \langle{\bar{\bar{H}}}_\mathrm{SP}\rangle \circ \langle\bar{\bar{Q}}\rangle \circ \langle\bar{\bar{\rho}}\rangle \circ \langle\bar{\bar{n}}\rangle

where the matrix of effective number of modes :math:`\widetilde{\bar{\bar{n}}}` is defined as:

.. math ::
    \langle\bar{\bar{n}}\rangle = \frac{\bar{\bar{\Gamma}}}{\frac{2\pi}{\hbar} \langle{\bar{\bar{H}}}_\mathrm{SP}\rangle \circ \langle\bar{\bar{Q}}\rangle \circ \langle\bar{\bar{\rho}}\rangle}

This allows any of the mode-averaged components to be swapped for one from another molecule, and the resulting ficticious :math:`\bar{\bar{\Gamma}}` diagonalised,
allowing one to examine the effect of different parts of the rate equation on the reaxation rates.

References

1. D. Reta, J. G. C. Kragskow and N. F. Chilton, `J. Am. Chem. Soc.`, 2021, 143, 15, 5943-5950
