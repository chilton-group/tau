Spin Dynamics
====================

``tau`` is currently able to model Orbach and Raman-I relaxation process. :sup:`[1]`
In order to calculate the rate of magnetic relaxation, the master equation describing the time evolution of the 
populations :math:`p_i` of the set of states :math:`{i}` must be solved. :sup:`[2,3]`

.. math ::
    :label: eq_1

    \frac{d}{dt}\ p_i(t)=\sum_{f\neq i}[\gamma_{if}p_f(t)-\gamma_{fi}p_i(t)] 

Written in spectroscopic notation, the :math:`\gamma_{fi}` in Equation :eq:`eq_1` are the rate constants (or transition probabilities) for the 
movement of population from state :math:`i` to state :math:`f`. In the case of a lanthanide single-molecule
magnet, the states are linear combinattions of the :math:`m_J` states of the ground spin-orbit multiplet.

The :math:`\gamma_{fi}` should obey the principle of detailed balance (Equation :eq:`eq_2`, where :math:`\Delta E_{if}=|E_i-E_f|`) such that at 
equilibrium the forwards (:math:`f\gets i`) and backwards (:math:`i\gets f`) rates :math:`\gamma_{fi}` and 
:math:`\gamma_{if}` are equal due to microscopic reversibility. If detailed balance is not obeyed,
the total population is not conserved and the dynamics are unphysical. ``tau`` reports the quantity
:math:`\frac{\gamma_{fi}}{\gamma_{if}}-\exp{{\left(-\frac{\mathrm{\Delta}E_{if}}{k_\mathrm{B} T}\right)}}`, which is zero when detailed balance is obeyed.

.. math ::
    :label: eq_2

    \frac{\gamma_{fi}}{\gamma_{if}} = \exp{\left ({-\frac{\Delta E_{if}}{k_\mathrm{B} T}}\right )}

The form of :math:`\gamma_{fi}` depends upon which relaxation process is considered. :sup:`[1]` The Orbach process consists of muliple
single-phonon trasitions between pairs of states, and :math:`\gamma_{fi}` is given by Equation :eq:`eq_3`. :sup:`[1]`

.. math ::
    :label: eq_3

    \gamma_{fi}^- &= \frac{2\pi}{\hbar} \sum_{\boldsymbol{q}j} \int \left | \langle \psi_f | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_i \rangle \right |^2
    {\bar{n}(\hbar\omega)} \delta(E_f-E_i-\hbar\omega) \rho_{\boldsymbol{q}j} (\hbar\omega) \mathrm{d}\hbar\omega

    \gamma_{fi}^+ &= \frac{2\pi}{\hbar} \sum_{\boldsymbol{q}j} \int \left | \langle \psi_f | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_i \rangle \right |^2
        ({\bar{n}(\hbar\omega)+1}) \delta(E_f-E_i+\hbar\omega) \rho_{\boldsymbol{q}j} (\hbar\omega) \mathrm{d}\hbar\omega

Here, the total rate is the sum over contributions for each mode :math:`\boldsymbol{q}j` at an arbitrary phonon wavevector :math:`\boldsymbol{q}`,
where :math:`\langle \psi_f | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_i \rangle` is a matrix element of the spin-phonon coupling Hamiltonian,
:math:`\bar{n}(\hbar\omega) = [e^{(\hbar\omega/k_\mathrm{B} T)} - 1]^{-1}` is the thermally-averaged phonon occupation number (Bose-Einstein occupation number),
and :math:`\rho_{\boldsymbol{q}j} (\hbar\omega)` is the phonon lineshape which is integrated over. Note that the terms for phonon
absorption (:math:`\gamma^-_{fi}`) and emission (:math:`\gamma^+_{fi}`) have been separated.

The Raman-I process is a two-phonon transition, that can either involve absorbtion of two phonons (:math:`\gamma^{I,--}_{fi}`),
emission of two phonons (:math:`\gamma^{I,++}_{fi}`), or a scattering of one phonon into another (:math:`\gamma^{I,-+}_{fi}` or :math:`\gamma^{I,+-}_{fi}`);
symbols have the same meaning as above. :sup:`[1]`

.. math ::
    :label: eq_4

    \gamma^{I,--}_{fi} = \frac{2\pi}{\hbar} \sum_{\boldsymbol{q}j \geq \boldsymbol{q}'j'} \left(1-\frac{1}{2}\delta_{\boldsymbol{q}j,\boldsymbol{q}'j'}\right) \iint
    \left| \sum_c
        \frac{\langle \psi_f | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_c \rangle \langle \psi_c | \hat{V}_{\boldsymbol{q}'j'}^{(1e)} | \psi_i \rangle}{E_c - E_i - \hbar \omega'} +
        \frac{\langle \psi_f | \hat{V}_{\boldsymbol{q}'j'}^{(1e)} | \psi_c \rangle \langle \psi_c | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_i \rangle}{E_c - E_i - \hbar \omega} \right|^2
        \\ \times \bar{n}(\hbar\omega){\bar{n}(\hbar\omega')}\ \delta(E_f - E_i - \hbar \omega - \hbar \omega') \ \rho_{\boldsymbol{q}j}(\hbar\omega) \rho_{\boldsymbol{q}'j'}(\hbar\omega')
        \mathrm{d}\hbar\omega\  \mathrm{d}\hbar\omega'

    \gamma^{I,++}_{fi} = \frac{2\pi}{\hbar} \sum_{\boldsymbol{q}j \geq \boldsymbol{q}'j'} \left(1-\frac{1}{2}\delta_{\boldsymbol{q}j,\boldsymbol{q}'j'}\right) \iint
    \left| \sum_c
        \frac{\langle \psi_f | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_c \rangle \langle \psi_c | \hat{V}_{\boldsymbol{q}'j'}^{(1e)} | \psi_i \rangle}{E_c - E_i + \hbar \omega'} +
        \frac{\langle \psi_f | \hat{V}_{\boldsymbol{q}'j'}^{(1e)} | \psi_c\rangle \langle \psi_c | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_i\rangle}{E_c - E_i + \hbar \omega} \right|^2
        \\ \times(\bar{n}(\hbar\omega)+1)(\bar{n}(\hbar\omega')+1) \ \delta(E_f - E_i + \hbar \omega + \hbar \omega') \ \rho_{\boldsymbol{q}j}(\hbar\omega) \rho_{\boldsymbol{q}'j'}(\hbar\omega')
        \mathrm{d}\hbar\omega\  \mathrm{d}\hbar\omega'

    \gamma^{I,-+}_{fi} = \frac{2\pi}{\hbar} \sum_{\boldsymbol{q}j \geq \boldsymbol{q}'j'} \left(1-\frac{1}{2}\delta_{\boldsymbol{q}j,\boldsymbol{q}'j'}\right) \iint
    \left| \sum_c
        \frac{\langle \psi_f | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_c\rangle \langle \psi_c | \hat{V}_{\boldsymbol{q}'j'}^{(1e)} | \psi_i\rangle}{E_c - E_i + \hbar \omega'} +
        \frac{\langle \psi_f | \hat{V}_{\boldsymbol{q}'j'}^{(1e)} | \psi_c\rangle \langle \psi_c | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_i\rangle}{E_c - E_i - \hbar \omega} \right|^2
        \\ \times \bar{n}(\hbar\omega) (\bar{n}(\hbar\omega')+1)\ \delta(E_f - E_i - \hbar \omega + \hbar \omega') \ \rho_{\boldsymbol{q}j}(\hbar\omega) \rho_{\boldsymbol{q}'j'}(\hbar\omega')
        \mathrm{d}\hbar\omega\  \mathrm{d}\hbar\omega'

    \gamma^{I,+-}_{fi} = \frac{2\pi}{\hbar} \sum_{\boldsymbol{q}j \geq \boldsymbol{q}'j'} \left(1-\frac{1}{2}\delta_{\boldsymbol{q}j,\boldsymbol{q}'j'}\right) \iint
    \left| \sum_c
        \frac{\langle \psi_f | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_c\rangle \langle \psi_c | \hat{V}_{\boldsymbol{q}'j'}^{(1e)} | \psi_i\rangle}{E_c - E_i - \hbar \omega'} +
        \frac{\langle \psi_f | \hat{V}_{\boldsymbol{q}'j'}^{(1e)} | \psi_c\rangle \langle \psi_c | \hat{V}_{\boldsymbol{q}j}^{(1e)} | \psi_i\rangle}{E_c - E_i + \hbar \omega} \right|^2
        \\ \times(\bar{n}(\hbar\omega)+1) \bar{n}(\hbar\omega') \ \delta(E_f - E_i + \hbar \omega - \hbar \omega') \ \rho_{\boldsymbol{q}j}(\hbar\omega) \rho_{\boldsymbol{q}'j'}(\hbar\omega')
        \mathrm{d}\hbar\omega\  \mathrm{d}\hbar\omega'


In our implementation of the Raman-I equations, we transform the two-dimensional integral into a one-dimensional integral
via the conservation of energy :math:`\delta` function, and perform the integral numerically using the trapezoidal method.

In ``tau``, the spin-phonon coupling Hamiltonian can either be expressed as derivatives of 
the crystal field Hamiltonian written in Stevens form (Equation :eq:`eq_5`)
or directly as derivatives of the ab initio Hamiltonian (Equation :eq:`eq_6`). :sup:`[4]`

.. math ::
    :label: eq_5

    \hat{V}_{\boldsymbol{q}j}^{(1e)} = \sum_{k,q} \theta_k \left(\frac{\partial B_k^q}{\partial X_{\boldsymbol{q}j}}\right)_\mathrm{eq} \hat{O}_k^q

.. math ::
    :label: eq_6

    \hat{V}_{\boldsymbol{q}j}^{(1e)} = \left(\frac{\partial \hat{H}_{ab-initio}}{\partial X_{\boldsymbol{q}j}}\right)_\mathrm{eq}


The lineshape :math:`\rho_{\boldsymbol{q}j} (\hbar\omega)` can either be described as a Gaussian (Equation :eq:`eq_7`,
where :math:`\sigma_{\boldsymbol{q}j} = \frac{\Gamma_{\boldsymbol{q}j}}{\sqrt{8\mathrm{ln}[2]}}`),
Lorentzian (Equation :eq:`eq_8`) or AntiLorentzian (Equation :eq:`eq_9`) function,
centred at :math:`\hbar\omega_{\boldsymbol{q}j}` with full-width-at-half-maximum linewidth :math:`\Gamma_{\boldsymbol{q}j}`.

.. math ::
    :label: eq_7

    \rho_{\boldsymbol{q}j}(\hbar\omega) = \frac{1}{\sigma_{\boldsymbol{q}j}\sqrt{2\pi}}\exp{{\left(-\frac{1}{2}\left(\frac{\hbar \omega_{\boldsymbol{q}j}-\hbar\omega_j}{\sigma_{\boldsymbol{q}j}}\right)^2\right)}}

    

.. math ::
    :label: eq_8

    \rho_{\boldsymbol{q}j}(\hbar\omega) = L^{-}_{\boldsymbol{q}j}(\hbar\omega)

.. math ::
    :label: eq_9

    \rho_{\boldsymbol{q}j}(\hbar\omega) = \pi \frac{ L^-_{\boldsymbol{q}j}(\hbar\omega) -L^+_{\boldsymbol{q}j}(\hbar\omega)} { 2 \tan ^{-1} \left( 2 \hbar\omega_{\boldsymbol{q}j} /\Gamma_{\boldsymbol{q}j }\right) }

with :math:`L^\pm_{\boldsymbol{q}j}(\hbar\omega) = \frac{ \Gamma_{\boldsymbol{q}j} }{ 2 \pi \left[ \left( \Gamma_{\boldsymbol{q}j} / 2 \right)^2 + (\hbar\omega \pm \hbar \omega_{\boldsymbol{q}j} )^2 \right] }`.

``tau`` then constructs the matrix :math:`\Gamma` whose elements are the :math:`\gamma_{fi}`, where the diagonal elements are 
defined as the sum of all rate constants corresponding to leaving a given state: :sup:`[3]`

.. math ::
    :label: eq_10

    \gamma_{ii}=\ \sum_{f\neq i\ }{-\gamma_{fi}}

Although the matrix :math:`\Gamma` is not symmetric, it can be shown to have real eigenvalues. :sup:`[2,3]` These eigenvalues 
are the negative relaxation rates :math:`-\tau^{-1}_k` for the system and, for any given :math:`\Gamma`, one is 
zero as it is the result of solving the master equation in equilibrium. Of the remaining rates, one is orders of magnitude slower than the others,
corresponding to magnetic relaxation: the remaining rates are fluctuations of spins either side of the barrier. :sup:`2` ``tau`` calcuates and diagonalizes this matrix as a function of 
temperature and reports the slowest rate at each temperature. The temperature range is given by the user, though ``tau`` 
only returns relaxation rates for temperatures at which detailed balance is obeyed.


References

1. J. G. C. Kragskow, A. Mattioni, J. K. Staab, D. Reta, J. M. Skelton and N. F. Chilton, `Chem. Soc. Rev.`, 2023, in press.
2. D. Gatteschi, R. Sessoli and J. Villain, `Molecular Nanomagnets`, Oxford University Press, Oxford, 2007.
3. M. H. Alexander, G. E. Hall and P. J. Dagdigian, `J. Chem. Educ.`, 2011, 88, 1538–1543.
4. J. K. Staab and N. F. Chilton, `J. Chem. Theor. Comput.`, 2022, 11, 6588.
