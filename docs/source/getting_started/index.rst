Getting Started
===============

.. toctree::
   :maxdepth: 2
   :hidden:
 
        
    Obtaining spin-phonon coupling terms <obtain_spc>
    Input Files <input_files>
    Input Keywords <input_kwrds>
    Running a simulation <running>
    Visualising Output Data <output_plot>

`tau` accepts input in the form of a plain text input file which configures the current simulation,
alongside a series of plain text data files which contain parameters used in the simulation.