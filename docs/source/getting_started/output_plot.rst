Visualizing Computed Rates
===========================
``tau_rates_plot`` is a python script which plots the relaxation data calculated by ``tau``.
``tau_rates_plot`` uses ``tau.out`` file(s) in order to plot :math:`\tau^{-1}` vs. :math:`T` on a log-log scale.
The optional arguments for ``tau_rates_plot`` are given by the command ``tau_rates_plot -h``.
