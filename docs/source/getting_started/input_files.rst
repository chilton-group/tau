Input Files
==================

The following files are required by `tau` and must be in a single directory:

* ``input.dat`` - Configuration options for simulation

``spin_phonon_suite`` prepares all numeric input required:

* ``mode_energies.dat`` - Vibrational mode energies in cm\ :sup:`-1` and

for a crystal field model Hamiltonian based calculation:

* ``CFP_polynomials.dat`` - Spin-phonon coupling parameters for each mode
* ``EQ_CFPs.dat`` - Equilibrium CFPs in cm\ :sup:`-1`

Alternatively, to run `tau` without a model Hamiltonian, you must have:

* ``tau.hdf5`` - Spin-phonon coupling parameters, equilibrium electronic states and total angular momentum operators


input.dat
-------------

Below is an annotated example of an input file (using the crystal field model) for a Dy\ :sup:`3+` complex with 246 normal modes. ::

   ****core parameters #This line is a comment
   ion Dy 3 #Dy in the 3+ oxidation state
   modes 246 #Number of modes
   temperatures 25 100 5  #From 25 K to 100 K in steps of 5 K 
   dynamics ORBACH   #Calculate dynamics for the Orbach process
   fwhm 10 #Use an FWHM of 10cm-1 to broaden the mode energies

Alternatively, without a model Hamiltonain: ::

    ****core parameters #This line is a comment
   hdf5 16 #use HDF5 input and specify number of low-lying states in the Hamiltonian
   modes 246 #Number of modes
   temperatures 25 100 5  #From 25 K to 100 K in steps of 5 K 
   dynamics ORBACH   #Calculate dynamics for the Orbach process
   fwhm 10 #Use an FWHM of 10cm-1 to broaden the mode energies

A full list of options is given on the next page.

CFP_polynomials.dat
-------------

This file contains the :math:`c = \left(\frac{\partial B_k^q}{\partial X_{\boldsymbol{q}j}}\right)_\mathrm{eq}` coefficients (See Theory).
These must be pure crystal field parameters, i.e. they do *not* include the operator equivalent factors (OEFs).

The required format of this file is as follows, and an example is given in the ``example`` directory. ::

    Data for mode 1 (comment line)
    2 -2 0 0 c
    2 -1 0 0 c
    2  0 0 0 c
    2  1 0 0 c
    2  2 0 0 c
    4 -4 0 0 c
    …
    6 -6 0 0 c
    Data for mode 2 (comment line)
    …
    Data for mode ... = (comment line)
    ...

EQ_CFPs.dat
----------------

This file contains the equilibrium crystal field parameters up to rank (:math:`k`) 6.
The required format of this file is shown below, and an example is given in the ``example`` directory. ::

    Parameter for k = 2 q = - 2
    Parameter for k = 2 q = - 1
    Parameter for k = 2 q =   0
    Parameter for k = 2 q = + 1
    Parameter for k = 2 q = + 2
    Parameter for k = 4 q = - 4
    Parameter for k = 4 q = - 3
    …
    Parameter for k = 6 q = + 6

mode_energies.dat
-------------

This file contains the vibrational mode energies in cm\ :sup:`-1` written in a single column with no headers. An example can be seen in the example directory.


fwhm.dat
-------------

This file contains the full-width at half maximum (FWHM) values for used when
the ``fwhm read`` keyword is specified.
FWHM values should be in cm\ :sup:`-1` written in a single column with no headers, with one value per mode in the same order as ``mode_energies.dat``.


jobscript.sh
-------------

If you are on the CSF you must create a jobscript to run Tau on the batch system. The command ``tau -subscript`` creates the jobscript ``tau.sh`` which can be submitted to the SGE queue system with the command ``qsub tau.sh``. The basic layout of ``tau.sh`` is given below, and can be edited to change the number of cores, input file name, and so on. ::

    #!/bin/bash -login #Allows this job to load modules
    #$ -S /bin/bash
    #$ -N tau_job #Name for your job - change this freely
    #$ -cwd #Runs job in current directory
    #$ -pe smp.pe 4 #Requests 4 cores - up to 12 possible for short queue
    #$ -l short #Use the short queue - less queuing but only 1 hour jobs - Tau is usually <5 minutes!
    export OMP_NUM_THREADS=$NSLOTS #Uses all cores on assigned nodes
    module unload compilers/intel/18.0.3 #Unload intel compilers
    module load compilers/gcc/8.2.0 #Load GCC compilers
    tau input.dat #Run tau on your input file - change this if you use a different input file name

