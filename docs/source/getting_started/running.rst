Running `tau`
===================

Following the successful completion of a set of spin-phonon calculations, `tau` can be used to simulate relaxation dynamics. It is advisable to run all `tau` calculations in a dedicated folder to avoid confusion and/or loss of data. Currently there is no way to point `tau` to different directories other than the one which it is executed in.

To run ``tau`` using a given input file, simply execute ::

    tau <input_file>

in a terminal. The files given in :ref:`Input files<Input Files>` must be present in the current directory.

autau 
--------

``autau`` is a python program which makes it possible to repeat a ``tau`` calculation on a range of FWHM values. Currently ``autau`` is only available to use on the CSF. ``autau`` must be run in a folder containing the necessary ``tau`` input files, see :ref:`Input files<Input Files>`. The input to ``autau`` is a list of FWHM values, e.g. ``autau 2 4 6 8 10 20`` will run 6 separate ``tau`` calculations for FWHM values of 2 cm\ :sup:`-1`, 4 cm\ :sup:`-1`, 6 cm\ :sup:`-1`, 8 cm\ :sup:`-1`, 10 cm\ :sup:`-1`, and 20 cm\ :sup:`-1`. The resulting individual ``tau.out`` files are then copied to the directory ``Results`` and are named according to their FWHM value.
