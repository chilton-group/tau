Obtaining spin-phonon coupling terms
==================
In order to use `tau` you must first calculate the electronic structure of the
molecule and obtain the spin-phonon coupling terms.  This is readily done with
our `spin_phonon_suite` python package, currently supporting the evaluation of
spin-phonon couplings and electronic states from a single MCSCF calculation and
a set of molecular gradient + nonadiabatic couplings interfacing with
OpenMolcas. The overall workflow is outlined below (including the level of
theory typically employed) and is further detailed at
`<https://chilton-group.gitlab.io/spin_phonon_suite/>`_ with comprehensive
tutorials for gas-phase as well as solid state systems.

1. Geometry optimisation + vibrational harmonic analysis (DFT)
2. Computation of equilibrium electronic states and couplings (CASSCF)
3. Evaluation of magnetic relaxation rates with `tau`

