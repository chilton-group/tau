
Home
====

.. toctree::
   Installation <installation>
   Theory <theory/index>
   Getting Started <getting_started/index>
   :maxdepth: 3
   :caption: Contents:
   :hidden:

Welcome to the documentation for ``tau``.

These pages contain instructions for working with ``tau``, along with explanations of the theory used within the code.

If you use ``tau`` please cite:

1. D. Reta, J. G. C. Kragskow and N. F. Chilton, `J. Am. Chem. Soc.`, 2021, 143, 15, 5943-5950
2. J. G. C. Kragskow, A. Mattioni, J. K. Staab, D. Reta, J. M. Skelton and N. F. Chilton, `Coord. Chem. Rev.`, 2023, in press.
