Installation
============

Requirements
------------
•       HDF5-compatible Fortran compiler (i.e. h5fc, which wraps gfortran)
•       Linear algebra Fortran libraries (lapack+blas or MKL)
•       HDF5 Fortran libraries (libhdf5-dev=1.10.6)
•       Python3 with packages listed in `src/requirements.txt`
•       Make
•       git

Compilation
------------
To install ``tau``, first clone a copy of the repository using git, or download manually through the gitlab page::

    git clone https://gitlab.com/chilton-group/tau

then navigate to ``src``::

    cd src

and use the makefile::

    make all install

This installs ``tau`` and its ancillary programs to::

    /usr/local/bin

To test that the installation was successful, run::

    tau -h

in the terminal. This should print the help seciton.

Compilation and installation takes around a minute or so on modern personal computers.

Updating
--------

To update the code simply navigate to the repository on your machine and run::

    git pull

then move to ``src`` and run ``make``::

    cd src;make all install

If there is an error about local changes, first type::

    git stash

and repeat the above ``make`` command.
